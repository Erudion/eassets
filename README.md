# EAssets

This project contains assets I created for Unity.

It contains systems to:

-Store data scene-persistent

-Trigger events and register methods

-Extensions for the object type

-Simple IO commands and a savefile
* You can store Gameobjects but only components with
    defined serialization (currently 100+! [List](https://docs.google.com/spreadsheets/d/1bAYIfQfmobcLJsG4QxIgjffv87Hs5vnqrWk9Hmc3oR0/edit?usp=sharing))
* You can now also store Monobehaviours and there values (currently only simple types)


-Pooling with Gameobjects and generics

-An interface for saving named booleans

-named integers that trigger events when reaching a certain value 
(called Triggervalues) and groups of these values (called TriggerGroups)

-Wrapper classes to allow Monobehaviours to directly acces these things
(called EMonoBehaviour)

-NEW!: MultiTag support using a prefab and a namespace

You can find a forum for discussion [here](https://forum.unity.com/threads/my-asset-collection.719099/)