﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a vector3
/// </summary>
[System.Serializable]
public class EMaterial : SerializableUnityData<Material>
{
    /// <summary>
    /// The shader of this material
    /// </summary>
    public EShader shader;
    /// <summary>
    /// The color of this material
    /// </summary>
    public EColor color;
    /// <summary>
    /// The maintexture of this material
    /// </summary>
    public ETexture mainTexture;
    /// <summary>
    /// The maintexture offset of this material
    /// </summary>
    public EVector2 mainTextureOffset;
    /// <summary>
    /// The maintexture scale of this material
    /// </summary>
    public EVector2 mainTextureScale;
    /// <summary>
    /// The renderqueue of this material
    /// </summary>
    public int renderQueue;
    /// <summary>
    /// The shaderkeywords of this material
    /// </summary>
    public string[] shaderKeywords;
    /// <summary>
    /// The globalIlluminationFlags of this material
    /// </summary>
    public int globalIlluminationFlags;
    /// <summary>
    /// The enableInstancing of this material
    /// </summary>
    public bool enableInstancing;
    /// <summary>
    /// The doubleSidedGI of this material
    /// </summary>
    public bool doubleSidedGI;
    /// <summary>
    /// The name of this material
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this material
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EMaterial
    /// </summary>
    public EMaterial() : base(true)
    {

    }

    /// <summary>
    /// Create a new EMaterial
    /// </summary>
    public EMaterial(Material mat) : base(true)
    {
        if(mat.shader != null)
            this.shader = new EShader(mat.shader);

        this.color = new EColor(mat.color);

        if(mat.mainTexture != null)
            this.mainTexture = new ETexture(mat.mainTexture);

        this.mainTextureOffset = mat.mainTextureOffset;
        this.mainTextureScale = mat.mainTextureScale;
        this.renderQueue = mat.renderQueue;
        this.shaderKeywords = mat.shaderKeywords;
        this.globalIlluminationFlags = (int)mat.globalIlluminationFlags;
        this.enableInstancing = mat.enableInstancing;
        this.doubleSidedGI = mat.doubleSidedGI;
        this.name = mat.name;
        this.hideFlags = (int)mat.hideFlags;
    }

    public override void SetData(ref Material reference)
    {
        if (this.shader != null && reference.shader != null)
        {
            Shader tmp = reference.shader;
            this.shader.SetData(ref tmp);
        }

        reference.color = this.color;

        if (mainTexture != null && reference.mainTexture != null)
        {
            Texture tex = reference.mainTexture;
            this.mainTexture.SetData(ref tex);
        }

        reference.mainTextureOffset = this.mainTextureOffset;
        reference.mainTextureScale = this.mainTextureScale;
        reference.renderQueue = this.renderQueue;
        reference.shaderKeywords = this.shaderKeywords;
        reference.globalIlluminationFlags = (MaterialGlobalIlluminationFlags)this.globalIlluminationFlags;
        reference.enableInstancing = this.enableInstancing;
        reference.doubleSidedGI = this.doubleSidedGI;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override Material ToUnityType()
    {
        Material mat = new Material(Shader.Find("Standard"));
        SetData(ref mat);
        return mat;
    }
}
