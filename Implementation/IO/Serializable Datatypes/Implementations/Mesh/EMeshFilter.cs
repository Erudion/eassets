﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a MeshFilter
/// </summary>
[System.Serializable]
public class EMeshFilter : SerializableUnityData<MeshFilter>
{
    /// <summary>
    /// The mesh of this meshfilter
    /// </summary>
    public EMesh mesh;
    /// <summary>
    /// The sharedMesh of this meshfilter
    /// </summary>
    public EMesh sharedMesh;
    /// <summary>
    /// The tag of this MeshFilter
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this MeshFilter
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this MeshFilter
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EMeshFilter
    /// </summary>
    public EMeshFilter() : base(true)
    {

    }

    /// <summary>
    /// Create a new EMeshFilter
    /// </summary>
    /// <param name="meshfilter">The MeshFilter you want to use</param>
    public EMeshFilter(MeshFilter meshfilter) : base(true)
    {
        //if (meshfilter.mesh != null)
            //this.mesh = new EMesh(meshfilter.mesh);

        //if (meshfilter.sharedMesh != null)
            //this.sharedMesh = new EMesh(meshfilter.sharedMesh);

        this.tag = meshfilter.tag;
        this.name = meshfilter.name;
        this.hideFlags = (int)meshfilter.hideFlags;
    }

    public override void SetData(ref MeshFilter reference)
    {
        if (this.mesh != null)
        {
            Mesh mesh = new Mesh();
            this.mesh.SetData(ref mesh);
            reference.mesh = mesh;
        }

        if (this.sharedMesh != null)
        {
            Mesh mesh = new Mesh();
            this.sharedMesh.SetData(ref mesh);
            reference.sharedMesh = mesh;
        }

        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override MeshFilter ToUnityType()
    {
        MeshFilter filter = new MeshFilter();
        SetData(ref filter);
        return filter;
    }
}
