﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a shader
/// </summary>
[System.Serializable]
public class EShader : SerializableUnityData<Shader>
{
    /// <summary>
    /// The name of this shader
    /// </summary>
    public string name;
    /// <summary>
    /// The maximum level of detail
    /// </summary>
    public int maximumLOD;
    /// <summary>
    /// The hideflags
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EShader
    /// </summary>
    public EShader() : base(false)
    {

    }

    /// <summary>
    /// Create a new EShader
    /// </summary>
    /// <param name="sha">The shader you want to copy</param>
    public EShader(Shader sha) : base(false)
    {
        this.name = sha.name;
        this.maximumLOD = sha.maximumLOD;
        this.hideFlags = (int)sha.hideFlags;
    }

    public override void SetData(ref Shader reference)
    {
        Shader shader = Shader.Find(this.name);
        shader.maximumLOD = this.maximumLOD;
        shader.hideFlags = (HideFlags)this.hideFlags;
        reference = shader;
    }

    public override Shader ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}
