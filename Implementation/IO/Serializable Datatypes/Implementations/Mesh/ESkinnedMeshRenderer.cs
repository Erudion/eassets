﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// This class can be used to serialize a SkinnedMeshRenderer
/// </summary>
[System.Serializable]
public class ESkinnedMeshRenderer : SerializableUnityData<SkinnedMeshRenderer>
{
    /// <summary>
    /// The blendShapeWeights of this ESkinnedMeshRenderer
    /// </summary>
    public float[] blendShapeWeights;
    /// <summary>
    /// The bones of this ESkinnedMeshRenderer
    /// </summary>
    public ETransform[] bones;
    /// <summary>
    /// The rootBone of this ESkinnedMeshRenderer
    /// </summary>
    public ETransform rootBone;
    /// <summary>
    /// The quality of this ESkinnedMeshRenderer
    /// </summary>
    public int quality;
    /// <summary>
    /// The sharedMesh of this ESkinnedMeshRenderer
    /// </summary>
    public EMesh sharedMesh;
    /// <summary>
    /// The updateWhenOffscreen of this ESkinnedMeshRenderer
    /// </summary>
    public bool updateWhenOffscreen;
    /// <summary>
    /// The skinnedMotionVectors state of this ESkinnedMeshRenderer
    /// </summary>
    public bool skinnedMotionVectors;
    /// <summary>
    /// The localBounds of this ESkinnedMeshRenderer
    /// </summary>
    public EBounds localBounds;
    /// <summary>
    /// The enabled state of this ESkinnedMeshRenderer
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The shadowCastingMode of this ESkinnedMeshRenderer
    /// </summary>
    public int shadowCastingMode;
    /// <summary>
    /// The receiveShadows state of this ESkinnedMeshRenderer
    /// </summary>
    public bool receiveShadows;
    /// <summary>
    /// The material of this ESkinnedMeshRenderer
    /// </summary>
    public EMaterial material;
    /// <summary>
    /// The sharedMaterial of this ESkinnedMeshRenderer
    /// </summary>
    public EMaterial sharedMaterial;
    /// <summary>
    /// The materials of this ESkinnedMeshRenderer
    /// </summary>
    public EMaterial[] materials;
    /// <summary>
    /// The sharedMaterials of this ESkinnedMeshRenderer
    /// </summary>
    public EMaterial[] sharedMaterials;
    /// <summary>
    /// The lightmapIndex of this ESkinnedMeshRenderer
    /// </summary>
    public int lightmapIndex;
    /// <summary>
    /// The realtimeLightmapIndex of this ESkinnedMeshRenderer
    /// </summary>
    public int realtimeLightmapIndex;
    /// <summary>
    /// The lightmapScaleOffset of this ESkinnedMeshRenderer
    /// </summary>
    public EVector4 lightmapScaleOffset;
    /// <summary>
    /// The motionVectorGenerationMode of this ESkinnedMeshRenderer
    /// </summary>
    public int motionVectorGenerationMode;
    /// <summary>
    /// The realtimeLightmapScaleOffset of this ESkinnedMeshRenderer
    /// </summary>
    public EVector4 realtimeLightmapScaleOffset;
    /// <summary>
    /// The lightProbeUsage of this ESkinnedMeshRenderer
    /// </summary>
    public int lightProbeUsage;
    /// <summary>
    /// The lightProbeProxyVolumeOverride of this ESkinnedMeshRenderer
    /// </summary>
    public EGameObject lightProbeProxyVolumeOverride;
    /// <summary>
    /// The probeAnchor of this ESkinnedMeshRenderer
    /// </summary>
    public ETransform probeAnchor;
    /// <summary>
    /// The reflectionProbeUsage of this ESkinnedMeshRenderer
    /// </summary>
    public int reflectionProbeUsage;
    /// <summary>
    /// The sortingLayerName of this ESkinnedMeshRenderer
    /// </summary>
    public string sortingLayerName;
    /// <summary>
    /// The sortingLayerID of this ESkinnedMeshRenderer
    /// </summary>
    public int sortingLayerID;
    /// <summary>
    /// The sortingOrder of this ESkinnedMeshRenderer
    /// </summary>
    public int sortingOrder;
    /// <summary>
    /// The tag of this SkinnedMeshRenderer
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this SkinnedMeshRenderer
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this SkinnedMeshRenderer
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ESkinnedMeshRenderer
    /// </summary>
    public ESkinnedMeshRenderer() : base(false)
    {

    }

    /// <summary>
    /// Create a new ESkinnedMeshRenderer
    /// </summary>
    /// <param name="reference">The SkinnedMeshRenderer you want to use</param>
    public ESkinnedMeshRenderer(SkinnedMeshRenderer reference) : base(false)
    {
        this.blendShapeWeights = new float[reference.sharedMesh.blendShapeCount];
        for(int i = 0;i< reference.sharedMesh.blendShapeCount; i++)
        {
            this.blendShapeWeights[i] = reference.GetBlendShapeWeight(i);
        }

        this.bones = new ETransform[reference.bones.Length];
        for(int i = 0; i < reference.bones.Length; i++)
        {
            this.bones[i] = new ETransform(reference.bones[i]);
        }

        this.rootBone = new ETransform(reference.rootBone);

        this.quality = (int)reference.quality;
        this.sharedMesh = new EMesh(reference.sharedMesh);
        this.updateWhenOffscreen = reference.updateWhenOffscreen;

        this.skinnedMotionVectors = reference.skinnedMotionVectors;

        this.localBounds = new EBounds(reference.localBounds);
        this.enabled = reference.enabled;
        this.shadowCastingMode = (int)reference.shadowCastingMode;
        this.receiveShadows = reference.receiveShadows;

        this.material = new EMaterial(reference.material);
        this.sharedMaterial = new EMaterial(reference.sharedMaterial);

        this.materials = new EMaterial[reference.materials.Length];
        for(int i = 0; i < reference.materials.Length; i++)
        {
            this.materials[i] = new EMaterial(reference.materials[i]);
        }

        this.sharedMaterials = new EMaterial[reference.sharedMaterials.Length];
        for (int i = 0; i < reference.sharedMaterials.Length; i++)
        {
            this.sharedMaterials[i] = new EMaterial(reference.sharedMaterials[i]);
        }

        this.lightmapIndex = reference.lightmapIndex;
        this.realtimeLightmapIndex = reference.realtimeLightmapIndex;
        this.lightmapScaleOffset = new EVector4(reference.lightmapScaleOffset);

        this.motionVectorGenerationMode = (int)reference.motionVectorGenerationMode;
        this.realtimeLightmapScaleOffset = new EVector4(reference.realtimeLightmapScaleOffset);
        this.lightProbeUsage = (int)reference.lightProbeUsage;
        this.lightProbeProxyVolumeOverride = new EGameObject(reference.lightProbeProxyVolumeOverride);
        this.probeAnchor = new ETransform(reference.probeAnchor);
        this.reflectionProbeUsage = (int)reference.reflectionProbeUsage;
        this.sortingLayerName = reference.sortingLayerName;
        this.sortingLayerID = reference.sortingLayerID;
        this.sortingOrder = reference.sortingOrder;

        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref SkinnedMeshRenderer reference)
    {
        if (reference.sharedMesh.blendShapeCount >= this.blendShapeWeights.Length)
        {
            for (int i = 0; i < this.blendShapeWeights.Length; i++)
            {
                reference.SetBlendShapeWeight(i, this.blendShapeWeights[i]);
            }
        }

        if (this.bones != null)
        {
            reference.bones = new Transform[this.bones.Length];
            for (int i = 0; i < this.bones.Length; i++)
            {
                if(reference.bones[i]!=null && this.bones[i]!=null)
                    this.bones[i].SetData(ref reference.bones[i]);
            }
        }

        if (this.rootBone != null && reference.rootBone != null)
        {
            Transform trans = reference.rootBone;
            this.rootBone.SetData(ref trans);
        }

        reference.quality = (SkinQuality)this.quality;

        if(this.sharedMesh != null && reference.sharedMesh != null)
        {
            Mesh mesh = reference.sharedMesh;
            this.sharedMesh.SetData(ref mesh);
        }

        reference.updateWhenOffscreen = this.updateWhenOffscreen;

        reference.skinnedMotionVectors = this.skinnedMotionVectors;

        reference.localBounds = this.localBounds.ToUnityType();

        reference.enabled = this.enabled;
        reference.shadowCastingMode = (ShadowCastingMode)this.shadowCastingMode;
        reference.receiveShadows = this.receiveShadows;


        if (this.material != null && reference.material != null)
        {
            Material mat = reference.material;
            this.material.SetData(ref mat);
        }
        if (this.sharedMaterial != null && reference.sharedMaterial != null)
        {
            Material mat = reference.sharedMaterial;
            this.sharedMaterial.SetData(ref mat);
        }

        if (this.materials != null && reference.materials != null)
        {
            reference.materials = new Material[this.materials.Length];
            for (int i = 0; i < this.materials.Length; i++)
            {
                reference.materials[i] = new Material(Shader.Find(null)); //TODO: Have a look at this
                this.materials[i].SetData(ref reference.materials[i]);
            }
        }

        if (this.sharedMaterials != null && reference.sharedMaterials != null)
        {
            reference.sharedMaterials = new Material[this.sharedMaterials.Length];
            for (int i = 0; i < this.sharedMaterials.Length; i++)
            {
                reference.sharedMaterials[i] = new Material(Shader.Find(null)); //TODO: Have a look at this
                this.sharedMaterials[i].SetData(ref reference.sharedMaterials[i]);
            }
        }

        reference.lightmapIndex = this.lightmapIndex;
        reference.realtimeLightmapIndex = this.realtimeLightmapIndex;
        reference.lightmapScaleOffset = this.lightmapScaleOffset.ToUnityType();

        reference.motionVectorGenerationMode = (MotionVectorGenerationMode)this.motionVectorGenerationMode;

        reference.realtimeLightmapScaleOffset = this.realtimeLightmapScaleOffset.ToUnityType();

        reference.lightProbeUsage = (LightProbeUsage)this.lightProbeUsage;

        if (this.lightProbeProxyVolumeOverride != null && reference.lightProbeProxyVolumeOverride)
        {
            GameObject go = reference.lightProbeProxyVolumeOverride;
            this.lightProbeProxyVolumeOverride.SetData(ref go);
        }

        if (this.probeAnchor != null && reference.probeAnchor != null)
        {
            Transform trans = reference.probeAnchor;
            this.probeAnchor.SetData(ref trans);
        }

        reference.reflectionProbeUsage = (ReflectionProbeUsage)this.reflectionProbeUsage;
        reference.sortingLayerName = this.sortingLayerName;
        reference.sortingLayerID = this.sortingLayerID;
        reference.sortingOrder = this.sortingOrder;

        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override SkinnedMeshRenderer ToUnityType()
    {
        SkinnedMeshRenderer renderer = new SkinnedMeshRenderer();
        SetData(ref renderer);
        return renderer;
    }
}
