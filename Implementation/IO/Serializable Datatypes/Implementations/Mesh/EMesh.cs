﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// This class can be used to serialize a Mesh
/// </summary>
[System.Serializable]
public class EMesh : SerializableUnityData<Mesh>
{
    /// <summary>
    /// The bounds of this mesh
    /// </summary>
    public EBounds bounds;
    /// <summary>
    /// The subMeshCount of this mesh
    /// </summary>
    public int subMeshCount;
    /// <summary>
    /// The boneWeights of this mesh
    /// </summary>
    public EBoneWeight[] boneWeights;
    /// <summary>
    /// The bindposes of this mesh
    /// </summary>
    public EMatrix4x4[] bindposes;
    /// <summary>
    /// The vertices of this mesh
    /// </summary>
    public EVector3[] vertices;
    /// <summary>
    /// The normals of this mesh
    /// </summary>
    public EVector3[] normals;
    /// <summary>
    /// The tangents of this mesh
    /// </summary>
    public EVector4[] tangents;
    /// <summary>
    /// The uvs of this mesh
    /// </summary>
    public List<EVector2[]> uvs;
    /// <summary>
    /// The colors of this mesh
    /// </summary>
    public EColor[] colors;
    /// <summary>
    /// The colors32 of this mesh
    /// </summary>
    public EColor32[] colors32;
    /// <summary>
    /// The triangles of this mesh
    /// </summary>
    public int[] triangles;
    /// <summary>
    /// The name of this Mesh
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this Mesh
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EMesh
    /// </summary>
    public EMesh() : base(true)
    {

    }

    /// <summary>
    /// Create a new EMesh
    /// </summary>
    /// <param name="mesh">The Mesh you want to use</param>
    public EMesh(Mesh mesh) : base(true)
    {
        if (mesh.bounds != null)
        {
            this.bounds = new EBounds(mesh.bounds);
        }

        this.subMeshCount = mesh.subMeshCount;

        this.boneWeights = new EBoneWeight[mesh.boneWeights.Length];
        for (int i = 0; i < mesh.boneWeights.Length; i++)
        {
            this.boneWeights[i] = new EBoneWeight(mesh.boneWeights[i]);
        }

        this.bindposes = new EMatrix4x4[mesh.bindposes.Length];
        for (int i = 0; i < mesh.bindposes.Length; i++)
        {
            this.bindposes[i] = new EMatrix4x4(mesh.bindposes[i]);
        }

        this.vertices = new EVector3[mesh.vertices.Length];
        for(int i = 0; i < mesh.vertices.Length;i++)
        {
            this.vertices[i] = new EVector3(mesh.vertices[i]);
        }

        this.normals = new EVector3[mesh.normals.Length];
        for (int i = 0; i < mesh.normals.Length; i++)
        {
            this.normals[i] = new EVector3(mesh.normals[i]);
        }

        this.tangents = new EVector4[mesh.tangents.Length];
        for (int i = 0; i < mesh.tangents.Length; i++)
        {
            this.tangents[i] = new EVector4(mesh.tangents[i]);
        }

        //Store UVs
        List<Vector2> tmp = new List<Vector2>();
        EVector2[] tmp2;
        for(int i = 0; i < 8; i++)
        {
            mesh.GetUVs(i,tmp); //Get UV

            tmp2 = new EVector2[tmp.Count]; //Create new array
            for (int j = 0; j < tmp.Count; j++) //Store uvs in array
                tmp2[j] = tmp[j];

            this.uvs.Add(tmp2); //Store array in List
        }

        this.colors = new EColor[mesh.colors.Length];
        for (int i = 0; i < mesh.colors.Length; i++)
        {
            this.colors[i] = new EColor(mesh.colors[i]);
        }

        this.colors32 = new EColor32[mesh.colors32.Length];
        for (int i = 0; i < mesh.colors32.Length; i++)
        {
            this.colors32[i] = new EColor32(mesh.colors32[i]);
        }

        this.triangles = new int[mesh.triangles.Length];
        for (int i = 0; i < mesh.triangles.Length; i++)
        {
            this.triangles[i] = mesh.triangles[i];
        }

        this.name = mesh.name;
        this.hideFlags = (int)mesh.hideFlags;
    }

    public override void SetData(ref Mesh reference)
    {
        if (this.bounds != null)
        {
            Bounds bounds = reference.bounds;
            this.bounds.SetData(ref bounds);
            reference.bounds = bounds;
        }

        reference.subMeshCount = this.subMeshCount;

        reference.boneWeights = new BoneWeight[this.boneWeights.Length];
        for (int i = 0; i < this.boneWeights.Length; i++)
        {
            reference.boneWeights[i] = this.boneWeights[i].ToUnityType();
        }

        reference.bindposes = new Matrix4x4[this.bindposes.Length];
        for (int i = 0; i < this.bindposes.Length; i++)
        {
            reference.bindposes[i] = this.bindposes[i].ToUnityType();
        }

        reference.vertices = new Vector3[this.vertices.Length];
        for (int i = 0; i < this.vertices.Length; i++)
        {
            reference.vertices[i] = this.vertices[i].ToUnityType();
        }

        reference.normals = new Vector3[this.normals.Length];
        for (int i = 0; i < this.normals.Length; i++)
        {
            reference.normals[i] = this.normals[i].ToUnityType();
        }

        reference.tangents = new Vector4[this.tangents.Length];
        for (int i = 0; i < this.tangents.Length; i++)
        {
            reference.tangents[i] = this.tangents[i].ToUnityType();
        }

        //Load UVs
        List<Vector2> tmp = new List<Vector2>();
        for(int i = 0; i < this.uvs.Count; i++)
        {
            for(int j = 0;j< this.uvs[i].Length;j++)
                tmp.Add(this.uvs[i][j]);

            reference.SetUVs(i, tmp);
            tmp.Clear();
        }

        reference.colors = new Color[this.colors.Length];
        for (int i = 0; i < this.colors.Length; i++)
        {
            reference.colors[i] = this.colors[i].ToUnityType();
        }

        reference.colors32 = new Color32[this.colors32.Length];
        for (int i = 0; i < this.colors32.Length; i++)
        {
            reference.colors32[i] = this.colors32[i].ToUnityType();
        }

        reference.triangles = new int[this.triangles.Length];
        for (int i = 0; i < this.triangles.Length; i++)
        {
            reference.triangles[i] = this.triangles[i];
        }

        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override Mesh ToUnityType()
    {
        Mesh mesh = new Mesh();
        SetData(ref mesh);
        return mesh;
    }
}
