﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a BoneWeight
/// </summary>
[System.Serializable]
public class EBoneWeight : SerializableUnityData<BoneWeight>
{
    public float weight0;
    public float weight1;
    public float weight2;
    public float weight3;
    public int boneIndex0;
    public int boneIndex1;
    public int boneIndex2;
    public int boneIndex3;

    /// <summary>
    /// Create a new EBoneWeight
    /// </summary>
    public EBoneWeight() : base(true)
    {

    }

    /// <summary>
    /// Create a new EBoneWeight
    /// </summary>
    /// <param name="bw">The BoneWeight you want to use</param>
    public EBoneWeight(BoneWeight bw) : base(true)
    {
        this.weight0 = bw.weight0;
        this.weight1 = bw.weight1;
        this.weight2 = bw.weight2;
        this.weight3 = bw.weight3;

        this.boneIndex0 = bw.boneIndex0;
        this.boneIndex1 = bw.boneIndex1;
        this.boneIndex2 = bw.boneIndex2;
        this.boneIndex3 = bw.boneIndex3;
    }

    public override void SetData(ref BoneWeight reference)
    {
        reference.weight0 = this.weight0;
        reference.weight1 = this.weight1;
        reference.weight2 = this.weight2;
        reference.weight3 = this.weight3;

        reference.boneIndex0 = this.boneIndex0;
        reference.boneIndex1 = this.boneIndex1;
        reference.boneIndex2 = this.boneIndex2;
        reference.boneIndex3 = this.boneIndex3;
    }

    public override BoneWeight ToUnityType()
    {
        BoneWeight bw = new BoneWeight
        {
            weight0 = this.weight0,
            weight1 = this.weight1,
            weight2 = this.weight2,
            weight3 = this.weight3,
            boneIndex0 = this.boneIndex0,
            boneIndex1 = this.boneIndex1,
            boneIndex2 = this.boneIndex2,
            boneIndex3 = this.boneIndex3
        };
        return bw;
    }
}
