﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a PhysicsMaterial2D
/// </summary>
[System.Serializable]
public class EPhysicMaterial2D : SerializableUnityData<PhysicsMaterial2D>
{
    /// <summary>
    /// The bounciness of this PhysicsMaterial2D
    /// </summary>
    public float bounciness;
    /// <summary>
    /// The friction of this PhysicsMaterial2D
    /// </summary>
    public float friction;
    /// <summary>
    /// The name of this PhysicsMaterial2D
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this PhysicsMaterial2D
    /// </summary>
    public int hideFlags;

    public EPhysicMaterial2D() : base(true)
    {

    }

    public EPhysicMaterial2D(PhysicsMaterial2D mat) : base(true)
    {
        this.bounciness = mat.bounciness;
        this.friction = mat.friction;
        this.name = mat.name;
        this.hideFlags = (int)mat.hideFlags;
    }

    public override void SetData(ref PhysicsMaterial2D reference)
    {
        reference.bounciness = this.bounciness;
        reference.friction = this.friction;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override PhysicsMaterial2D ToUnityType()
    {
        PhysicsMaterial2D mat = new PhysicsMaterial2D();
        SetData(ref mat);
        return mat;
    }
}
