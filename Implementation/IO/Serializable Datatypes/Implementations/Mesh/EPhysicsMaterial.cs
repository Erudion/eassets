﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a PhysicsMaterial
/// </summary>
[System.Serializable]
public class EPhysicsMaterial : SerializableUnityData<PhysicMaterial>
{
    /// <summary>
    /// The dynamicFriction of this PhysicsMaterial
    /// </summary>
    public float dynamicFriction;
    /// <summary>
    /// The staticFriction of this PhysicsMaterial
    /// </summary>
    public float staticFriction;
    /// <summary>
    /// The bounciness of this PhysicsMaterial
    /// </summary>
    public float bounciness;
    /// <summary>
    /// The frictionCombine of this PhysicsMaterial
    /// </summary>
    public int frictionCombine;
    /// <summary>
    /// The bounceCombine of this PhysicsMaterial
    /// </summary>
    public int bounceCombine;
    /// <summary>
    /// The name of this PhysicsMaterial
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this PhysicsMaterial
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new PhysicsMaterial
    /// </summary>
    public EPhysicsMaterial() : base(true)
    {

    }

    /// <summary>
    /// Create a new PhysicsMaterial
    /// </summary>
    /// <param name="mat">The physicsmaterial you want to use</param>
    public EPhysicsMaterial(PhysicMaterial mat) : base(true)
    {
        this.dynamicFriction = mat.dynamicFriction;
        this.staticFriction = mat.staticFriction;
        this.bounciness = mat.bounciness;
        this.frictionCombine = (int)mat.frictionCombine;
        this.bounceCombine = (int)mat.bounceCombine;
        this.name = mat.name;
        this.hideFlags = (int)mat.hideFlags;
    }

    public override void SetData(ref PhysicMaterial reference)
    {
        reference.dynamicFriction = this.dynamicFriction;
        reference.staticFriction = this.staticFriction;
        reference.bounciness = this.bounciness;
        reference.frictionCombine = (PhysicMaterialCombine)this.frictionCombine;
        reference.bounceCombine = (PhysicMaterialCombine)this.bounceCombine;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override PhysicMaterial ToUnityType()
    {
        PhysicMaterial mat = new PhysicMaterial();
        SetData(ref mat);
        return mat;
    }
}
