﻿using EAssets.IO.SerializableDatatypes;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// This class can be used to serialize a MeshRenderer
/// </summary>
[System.Serializable]
public class EMeshRenderer : SerializableUnityData<MeshRenderer>
{
    /// <summary>
    /// The additionalVertexStreams of this MeshRenderer
    /// </summary>
    public EMesh additionalVertexStreams;
    /// <summary>
    /// The enabled state of this MeshRenderer
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The shadowCastingMode of this MeshRenderer
    /// </summary>
    public int shadowCastingMode;
    /// <summary>
    /// The receiveShadows state of this MeshRenderer
    /// </summary>
    public bool receiveShadows;
    /// <summary>
    /// The material of this MeshRenderer
    /// </summary>
    public EMaterial material;
    /// <summary>
    /// The sharedMaterial of this MeshRenderer
    /// </summary>
    public EMaterial sharedMaterial;
    /// <summary>
    /// The materials of this MeshRenderer
    /// </summary>
    public EMaterial[] materials;
    /// <summary>
    /// The sharedMaterials of this MeshRenderer
    /// </summary>
    public EMaterial[] sharedMaterials;
    /// <summary>
    /// The lightmapIndex of this MeshRenderer
    /// </summary>
    public int lightmapIndex;
    /// <summary>
    /// The realtimeLightmapIndex of this MeshRenderer
    /// </summary>
    public int realtimeLightmapIndex;
    /// <summary>
    /// The lightmapScaleOffset of this MeshRenderer
    /// </summary>
    public EVector4 lightmapScaleOffset;
    /// <summary>
    /// The motionVectorGenerationMode of this MeshRenderer
    /// </summary>
    public int motionVectorGenerationMode;
    /// <summary>
    /// The realtimeLightmapScaleOffset of this MeshRenderer
    /// </summary>
    public EVector4 realtimeLightmapScaleOffset;
    /// <summary>
    /// The lightProbeUsage of this MeshRenderer
    /// </summary>
    public int lightProbeUsage;
    /// <summary>
    /// The lightProbeProxyVolumeOverride of this MeshRenderer
    /// </summary>
    public EGameObject lightProbeProxyVolumeOverride;
    /// <summary>
    /// The probeAnchor of this MeshRenderer
    /// </summary>
    public ETransform probeAnchor;
    /// <summary>
    /// The reflectionProbeUsage of this MeshRenderer
    /// </summary>
    public int reflectionProbeUsage;
    /// <summary>
    /// The sortingLayerName of this MeshRenderer
    /// </summary>
    public string sortingLayerName;
    /// <summary>
    /// The sortingLayerID of this MeshRenderer
    /// </summary>
    public int sortingLayerID;
    /// <summary>
    /// The sortingOrder of this MeshRenderer
    /// </summary>
    public int sortingOrder;
    /// <summary>
    /// The tag of this MeshRenderer
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this MeshRenderer
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this MeshRenderer
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EMeshRenderer
    /// </summary>
    public EMeshRenderer() : base(true)
    {

    }

    /// <summary>
    /// Create a new EMeshRenderer
    /// </summary>
    /// <param name="reference">The MeshRenderer you want to use</param>
    public EMeshRenderer(MeshRenderer reference) : base(true)
    {
        if(reference.additionalVertexStreams != null)
            this.additionalVertexStreams = new EMesh(reference.additionalVertexStreams);

        this.enabled = reference.enabled;
        this.shadowCastingMode = (int)reference.shadowCastingMode;
        this.receiveShadows = reference.receiveShadows;

        if(reference.material != null)
            this.material = new EMaterial(reference.material);

        if(reference.sharedMaterial != null)
            this.sharedMaterial = new EMaterial(reference.sharedMaterial);

        if (reference.materials != null && reference.materials.Length > 0) {
            this.materials = new EMaterial[reference.materials.Length];
            for (int i = 0; i < reference.materials.Length; i++)
            {
                this.materials[i] = new EMaterial(reference.materials[i]);
            }
        }

        if (reference.sharedMaterials != null && reference.sharedMaterials.Length > 0)
        {
            this.sharedMaterials = new EMaterial[reference.sharedMaterials.Length];
            for (int i = 0; i < reference.sharedMaterials.Length; i++)
            {
                this.sharedMaterials[i] = new EMaterial(reference.sharedMaterials[i]);
            }
        }

        this.lightmapIndex = reference.lightmapIndex;
        this.realtimeLightmapIndex = reference.realtimeLightmapIndex;
        this.lightmapScaleOffset = new EVector4(reference.lightmapScaleOffset);
        this.motionVectorGenerationMode = (int)reference.motionVectorGenerationMode;
        this.realtimeLightmapScaleOffset = new EVector4(reference.realtimeLightmapScaleOffset);
        this.lightProbeUsage = (int)reference.lightProbeUsage;

        if(reference.lightProbeProxyVolumeOverride!=null)
            this.lightProbeProxyVolumeOverride = new EGameObject(reference.lightProbeProxyVolumeOverride);

        if (reference.probeAnchor != null)
        {
            this.probeAnchor = new ETransform(reference.probeAnchor);
        }

        this.reflectionProbeUsage = (int)reference.reflectionProbeUsage;
        this.sortingLayerName = reference.sortingLayerName;
        this.sortingLayerID = reference.sortingLayerID;
        this.sortingOrder = reference.sortingOrder;
        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref MeshRenderer reference)
    {
        if (this.additionalVertexStreams != null)
        {
            reference.additionalVertexStreams = this.additionalVertexStreams.ToUnityType();
        }

        reference.enabled = this.enabled;
        reference.shadowCastingMode = (ShadowCastingMode)this.shadowCastingMode;
        reference.receiveShadows = this.receiveShadows;

        if (this.material != null)
        {
            reference.material = this.material.ToUnityType();
        }

        if (this.sharedMaterial != null)
        {
            reference.sharedMaterial = this.sharedMaterial.ToUnityType();
        }

        if (this.materials != null && this.materials.Length > 0)
        {
            Material tmp;
            Material[] materials = new Material[this.materials.Length];
            for (int i = 0; i < this.materials.Length; i++)
            {
                tmp = materials[i];

                tmp = this.sharedMaterials[i].ToUnityType();

                materials[i] = tmp;
            }
            reference.materials = materials;
        }

        if (this.sharedMaterials != null && this.sharedMaterials.Length > 0)
        {
            Material tmp;
            Material[] materials = new Material[this.sharedMaterials.Length];
            for (int i = 0; i < this.sharedMaterials.Length; i++)
            {
                tmp = materials[i];//reference.sharedMaterials[i];

                tmp = this.sharedMaterials[i].ToUnityType();

                materials[i] = tmp;
            }
            reference.sharedMaterials = materials;
        }

        reference.lightmapIndex = this.lightmapIndex;
        reference.realtimeLightmapIndex = this.realtimeLightmapIndex;
        reference.lightmapScaleOffset = this.lightmapScaleOffset.ToUnityType();
        reference.motionVectorGenerationMode = (MotionVectorGenerationMode)this.motionVectorGenerationMode;
        reference.realtimeLightmapScaleOffset = this.realtimeLightmapScaleOffset.ToUnityType();
        reference.lightProbeUsage = (LightProbeUsage)this.lightProbeUsage;

        if (this.lightProbeProxyVolumeOverride != null)
        {
            GameObject go = reference.lightProbeProxyVolumeOverride;

            if(go != null)
                this.lightProbeProxyVolumeOverride.SetData(ref go);
        }

        if (this.probeAnchor != null && reference.probeAnchor != null)
        {
            Transform trans = reference.probeAnchor;

            if(trans != null)
                this.probeAnchor.SetData(ref trans);
        }

        reference.reflectionProbeUsage = (ReflectionProbeUsage)this.reflectionProbeUsage;
        reference.sortingLayerName = this.sortingLayerName;
        reference.sortingLayerID = this.sortingLayerID;
        reference.sortingOrder = this.sortingOrder;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override MeshRenderer ToUnityType()
    {
        MeshRenderer renderer = new MeshRenderer();
        SetData(ref renderer);
        return renderer;
    }
}
