﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a JointSpring
/// </summary>
[System.Serializable]
public class EJointSpring : SerializableUnityData<JointSpring>
{
    /// <summary>
    /// The spring of this EJointSpring
    /// </summary>
    public float spring;
    /// <summary>
    /// The damper of this EJointSpring
    /// </summary>
    public float damper;
    /// <summary>
    /// The targetPosition of this EJointSpring
    /// </summary>
    public float targetPosition;

    /// <summary>
    /// Create a new EJointSpring
    /// </summary>
    public EJointSpring() : base(false)
    {

    }

    /// <summary>
    /// Create a new EJointSpring
    /// </summary>
    /// <param name="reference">The JointSpring you want to use</param>
    public EJointSpring(JointSpring reference) : base(false)
    {
        this.spring = reference.spring;
        this.damper = reference.damper;
        this.targetPosition = reference.targetPosition;
    }

    public override void SetData(ref JointSpring reference)
    {
        reference.spring = this.spring;
        reference.damper = this.damper;
        reference.targetPosition = this.targetPosition;
    }

    public override JointSpring ToUnityType()
    {
        JointSpring spring = new JointSpring();
        SetData(ref spring);
        return spring;
    }
}
