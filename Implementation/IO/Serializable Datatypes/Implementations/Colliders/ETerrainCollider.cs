﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a TerrainCollider
/// </summary>
[System.Serializable]
public class ETerrainCollider : SerializableUnityData<TerrainCollider>
{
    /// <summary>
    /// The terrainData of this TerrainCollider
    /// </summary>
    public ETerrainData terrainData;
    /// <summary>
    /// The isTrigger state of this TerrainCollider
    /// </summary>
    public bool isTrigger;
    /// <summary>
    /// The contactOffset of this TerrainCollider
    /// </summary>
    public float contactOffset;
    /// <summary>
    /// The material of this TerrainCollider
    /// </summary>
    public EPhysicsMaterial material;
    /// <summary>
    /// The sharedMaterial of this TerrainCollider
    /// </summary>
    public EPhysicsMaterial sharedMaterial;
    /// <summary>
    /// The enabled state of this TerrainCollider
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The tag of this TerrainCollider
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this TerrainCollider
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this TerrainCollider
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ETerrainCollider
    /// </summary>
    public ETerrainCollider() : base(true)
    {

    }

    /// <summary>
    /// Create a new ETerrainCollider
    /// </summary>
    /// <param name="reference">The TerrainCollider you want to use</param>
    public ETerrainCollider(TerrainCollider reference) : base(true)
    {
        if(reference.terrainData != null)
        {
            this.terrainData = new ETerrainData(reference.terrainData);
        }

        this.isTrigger = reference.isTrigger;
        this.contactOffset = reference.contactOffset;

        if (reference.material != null)
        {
            this.material = new EPhysicsMaterial(reference.material);
        }

        if (reference.sharedMaterial != null)
        {
            this.sharedMaterial = new EPhysicsMaterial(reference.sharedMaterial);
        }

        this.enabled = reference.enabled;
        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref TerrainCollider reference)
    {
        if (this.terrainData != null)
        {
            if (reference.terrainData != null)
            {
                TerrainData data = reference.terrainData;
                this.terrainData.SetData(ref data);
            }
            else
            {
                reference.terrainData = this.terrainData.ToUnityType();
            }
        }

        reference.isTrigger = this.isTrigger;
        reference.contactOffset = this.contactOffset;

        if (this.material != null)
        {
            if (reference.material != null)
            {
                PhysicMaterial mat = reference.material;
                this.material.SetData(ref mat);
            }
            else
            {
                reference.material = this.material.ToUnityType();
            }
        }

        if (this.sharedMaterial != null)
        {
            if (reference.sharedMaterial != null)
            {
                PhysicMaterial mat = reference.sharedMaterial;
                this.sharedMaterial.SetData(ref mat);
            }
            else
            {
                reference.material = this.sharedMaterial.ToUnityType();
            }
        }

        reference.enabled = this.enabled;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override TerrainCollider ToUnityType()
    {
        TerrainCollider col = new TerrainCollider();
        SetData(ref col);
        return col;
    }
}
