﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a CompositeCollider2D
/// </summary>
[System.Serializable]
public class ECompositeCollider2D : SerializableUnityData<CompositeCollider2D>
{
    /// <summary>
    /// The geometryType of this CompositeCollider2D
    /// </summary>
    public int geometryType;
    /// <summary>
    /// The generationType of this CompositeCollider2D
    /// </summary>
    public int generationType;
    /// <summary>
    /// The vertexDistance of this CompositeCollider2D
    /// </summary>
    public float vertexDistance;
    /// <summary>
    /// The edgeRadius of this CompositeCollider2D
    /// </summary>
    public float edgeRadius;
    /// <summary>
    /// The density of this CompositeCollider2D
    /// </summary>
    public float density;
    /// <summary>
    /// The isTrigger state of this CompositeCollider2D
    /// </summary>
    public bool isTrigger;
    /// <summary>
    /// The usedByEffector state of this CompositeCollider2D
    /// </summary>
    public bool usedByEffector;
    /// <summary>
    /// The usedByComposite state of this CompositeCollider2D
    /// </summary>
    public bool usedByComposite;
    /// <summary>
    /// The offset of this CompositeCollider2D
    /// </summary>
    public EVector2 offset;
    /// <summary>
    /// The sharedMaterial of this CompositeCollider2D
    /// </summary>
    public EPhysicMaterial2D sharedMaterial;
    /// <summary>
    /// The enabled state of this CompositeCollider2D
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The tag of this CompositeCollider2D
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this CompositeCollider2D
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this CompositeCollider2D
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ECompositeCollider2D
    /// </summary>
    public ECompositeCollider2D() : base(true)
    {

    }

    /// <summary>
    /// Create a new ECompositeCollider2D
    /// </summary>
    /// <param name="reference">The CompositeCollider2D you want to use</param>
    public ECompositeCollider2D(CompositeCollider2D reference) : base(true)
    {
        this.geometryType = (int)reference.geometryType;
        this.generationType = (int)reference.generationType;
        this.edgeRadius = reference.edgeRadius;
        this.density = reference.density;
        this.isTrigger = reference.isTrigger;
        this.usedByEffector = reference.usedByEffector;
        this.usedByComposite = reference.usedByComposite;
        this.offset = new EVector2(reference.offset);

        if (reference.sharedMaterial != null)
        {
            this.sharedMaterial = new EPhysicMaterial2D(reference.sharedMaterial);
        }

        this.enabled = reference.enabled;
        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref CompositeCollider2D reference)
    {
        reference.geometryType = (CompositeCollider2D.GeometryType)this.geometryType;
        reference.generationType = (CompositeCollider2D.GenerationType)this.generationType;
        reference.edgeRadius = this.edgeRadius;
        reference.density = this.density;
        reference.isTrigger = this.isTrigger;
        reference.usedByEffector = this.usedByEffector;
        reference.usedByComposite = this.usedByComposite;
        reference.offset = this.offset.ToUnityType();

        if (reference.sharedMaterial != null)
        {
            PhysicsMaterial2D mat = reference.sharedMaterial;
            this.sharedMaterial.SetData(ref mat);
        }

        reference.enabled = this.enabled;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override CompositeCollider2D ToUnityType()
    {
        CompositeCollider2D col = new CompositeCollider2D();
        SetData(ref col);
        return col;
    }
}
