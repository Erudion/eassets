﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a capsuleCollider
/// </summary>
[System.Serializable]
public class ECapsuleCollider : SerializableUnityData<CapsuleCollider>
{
    /// <summary>
    /// The center of this capsuleCollider
    /// </summary>
    public EVector3 center;
    /// <summary>
    /// The radius of this capsuleCollider
    /// </summary>
    public float radius;
    /// <summary>
    /// The height of this capsuleCollider
    /// </summary>
    public float height;
    /// <summary>
    /// The direction of this capsuleCollider
    /// </summary>
    public int direction;
    /// <summary>
    /// The enabled state of this capsuleCollider
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The isTrigger state of this capsuleCollider
    /// </summary>
    public bool isTrigger;
    /// <summary>
    /// The contactOffset of this capsuleCollider
    /// </summary>
    public float contactOffset;
    /// <summary>
    /// The material of this capsuleCollider
    /// </summary>
    public EPhysicsMaterial material;
    /// <summary>
    /// The sharedMaterial of this capsuleCollider
    /// </summary>
    public EPhysicsMaterial sharedMaterial;
    /// <summary>
    /// The tag of this capsuleCollider
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this capsuleCollider
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this capsuleCollider
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ECapsuleCollider
    /// </summary>
    public ECapsuleCollider() : base(false)
    {

    }

    /// <summary>
    /// Create a new ECapsuleCollider
    /// </summary>
    /// <param name="col">The capsulecollider you want to use</param>
    public ECapsuleCollider(CapsuleCollider col) : base(false)
    {
        this.center = new EVector3(col.center);
        this.radius = col.radius;
        this.height = col.height;
        this.direction = col.direction;
        this.enabled = col.enabled;
        this.isTrigger = col.isTrigger;
        this.contactOffset = col.contactOffset;
        this.material = new EPhysicsMaterial(col.material);
        this.sharedMaterial = new EPhysicsMaterial(col.sharedMaterial);
        this.tag = col.tag;
        this.name = col.name;
        this.hideFlags = (int)col.hideFlags;
    }

    public override void SetData(ref CapsuleCollider reference)
    {
        reference.center = this.center;
        reference.radius = this.radius;
        reference.height = this.height;
        reference.direction = this.direction;
        reference.enabled = this.enabled;
        reference.isTrigger = this.isTrigger;
        reference.contactOffset = this.contactOffset;

        if (this.material != null)
        {
            PhysicMaterial mat = reference.material;
            this.material.SetData(ref mat);
        }
        if (this.sharedMaterial != null)
        {
            PhysicMaterial mat = reference.sharedMaterial;
            this.material.SetData(ref mat);
        }

        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override CapsuleCollider ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}
