﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a MeshCollider
/// </summary>
[System.Serializable]
public class EMeshCollider : SerializableUnityData<MeshCollider>
{
    /// <summary>
    /// The sharedMesh of this EMeshCollider
    /// </summary>
    public EMesh sharedMesh;
    /// <summary>
    /// The sharedMesh of this EMeshCollider
    /// </summary>
    public bool convex;
#if !UNITY_2017_1_OR_NEWER
    /// <summary>
    /// The inflateMesh state of this EMeshCollider
    /// </summary>
    public bool inflateMesh;
    /// <summary>
    /// The skinWidth of this EMeshCollider
    /// </summary>
    public float skinWidth;
#endif
    /// <summary>
    /// The enabled state of this EMeshCollider
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The isTrigger state of this EMeshCollider
    /// </summary>
    public bool isTrigger;
    /// <summary>
    /// The contactOffset of this EMeshCollider
    /// </summary>
    public float contactOffset;
    /// <summary>
    /// The material of this EMeshCollider
    /// </summary>
    public EPhysicsMaterial material;
    /// <summary>
    /// The sharedMaterial of this EMeshCollider
    /// </summary>
    public EPhysicsMaterial sharedMaterial;
    /// <summary>
    /// The tag of this MeshCollider
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this MeshCollider
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this MeshCollider
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EMeshCollider
    /// </summary>
    public EMeshCollider() : base(true)
    {

    }

    /// <summary>
    /// Create a new EMeshCollider
    /// </summary>
    /// <param name="reference">The MeshCollider you want to use</param>
    public EMeshCollider(MeshCollider reference) : base(true)
    {
        if(reference.sharedMesh != null)
            this.sharedMesh = new EMesh(reference.sharedMesh);

        this.convex = reference.convex;
#if !UNITY_2017_1_OR_NEWER
        this.inflateMesh = reference.inflateMesh;
        this.skinWidth = reference.skinWidth;
#endif
        this.enabled = reference.enabled;
        this.isTrigger = reference.isTrigger;
        this.contactOffset = reference.contactOffset;

        if(reference.material != null)
            this.material = new EPhysicsMaterial(reference.material);

        if (reference.sharedMaterial != null)
            this.sharedMaterial = new EPhysicsMaterial(reference.sharedMaterial);

        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref MeshCollider reference)
    {
        if(this.sharedMesh != null && reference.sharedMesh != null)
        {
            Mesh mesh = reference.sharedMesh;
            this.sharedMesh.SetData(ref mesh);
        }

        reference.convex = this.convex;
#if !UNITY_2017_1_OR_NEWER
        reference.inflateMesh = this.inflateMesh;
        reference.skinWidth = this.skinWidth;
#endif
        reference.enabled = this.enabled;
        reference.isTrigger = this.isTrigger;
        reference.contactOffset = this.contactOffset;

        if (this.material != null && reference.material != null)
        {
            PhysicMaterial mat = reference.material;
            this.material.SetData(ref mat);
        }

        if (this.sharedMaterial != null && reference.sharedMaterial != null)
        {
            PhysicMaterial mat = reference.sharedMaterial;
            this.sharedMaterial.SetData(ref mat);
        }

        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override MeshCollider ToUnityType()
    {
        MeshCollider collider = new MeshCollider();
        SetData(ref collider);
        return collider;
    }
}
