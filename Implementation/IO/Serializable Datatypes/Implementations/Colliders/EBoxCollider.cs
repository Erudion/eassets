﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a vector3
/// </summary>
[System.Serializable]
public class EBoxCollider : SerializableUnityData<BoxCollider>
{
    /// <summary>
    /// The center of this BoxCollider
    /// </summary>
    public EVector3 center;
    /// <summary>
    /// The size of this BoxCollider
    /// </summary>
    public EVector3 size;
    /// <summary>
    /// The enabled state of this BoxCollider
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The isTrigger state of this BoxCollider
    /// </summary>
    public bool isTrigger;
    /// <summary>
    /// The contactOffset of this BoxCollider
    /// </summary>
    public float contactOffset;
    /// <summary>
    /// The PhysicsMaterial of this BoxCollider
    /// </summary>
    public EPhysicsMaterial material;
    /// <summary>
    /// The tag of this BoxCollider
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this BoxCollider
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this BoxCollider
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EBoxCollider
    /// </summary>
    public EBoxCollider() : base(true)
    {

    }

    /// <summary>
    /// Create a new EBoxCollider
    /// </summary>
    /// <param name="box">The BoxCollider you want to copy</param>
    public EBoxCollider(BoxCollider box) : base(true)
    {
        this.center = box.center;
        this.size = box.size;
        this.enabled = box.enabled;
        this.isTrigger = box.isTrigger;
        this.contactOffset = box.contactOffset;

        if(box.material != null) 
            this.material = new EPhysicsMaterial(box.material);

        this.tag = box.tag;
        this.name = box.name;
        this.hideFlags = (int)box.hideFlags;
    }

    /// <summary>
    /// Converts this vector to a unity BoxCollider
    /// </summary>
    /// <returns></returns>
    public override BoxCollider ToUnityType()
    {
        BoxCollider col = new BoxCollider();
        SetData(ref col);
        return col;
    }

    public override void SetData(ref BoxCollider reference)
    {
        reference.center = this.center;
        reference.size = this.size;
        reference.enabled = this.enabled;
        reference.isTrigger = this.isTrigger;
        reference.contactOffset = this.contactOffset;

        if (this.material != null)
        {
            PhysicMaterial physicsMaterial = reference.material;

            if (physicsMaterial == null)
                physicsMaterial = new PhysicMaterial();

            this.material.SetData(ref physicsMaterial);
        }

        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }
}

