﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a CircleCollider2D
/// </summary>
[System.Serializable]
public class ECircleCollider2D : SerializableUnityData<CircleCollider2D>
{
    /// <summary>
    /// The radius of this CircleCollider2D
    /// </summary>
    public float radius;
    /// <summary>
    /// The density of this CircleCollider2D
    /// </summary>
    public float density;
    /// <summary>
    /// The isTrigger state of this CircleCollider2D
    /// </summary>
    public bool isTrigger;
    /// <summary>
    /// The usedByEffector state of this CircleCollider2D
    /// </summary>
    public bool usedByEffector;
    /// <summary>
    /// The usedByComposite state of this CircleCollider2D
    /// </summary>
    public bool usedByComposite;
    /// <summary>
    /// The offset of this CircleCollider2D
    /// </summary>
    public EVector2 offset;
    /// <summary>
    /// The sharedMaterial of this CircleCollider2D
    /// </summary>
    public EPhysicMaterial2D sharedMaterial;
    /// <summary>
    /// The enabled state of this CircleCollider2D
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The tag of this CircleCollider2D
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this CircleCollider2D
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this CircleCollider2D
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ECircleCollider2D
    /// </summary>
    public ECircleCollider2D() : base(true)
    {

    }

    /// <summary>
    /// Create a new ECircleCollider2D
    /// </summary>
    /// <param name="reference">The CircleCollider2D you want to use</param>
    public ECircleCollider2D(CircleCollider2D reference) : base(true)
    {
        this.radius = reference.radius;
        this.density = reference.density;
        this.isTrigger = reference.isTrigger;
        this.usedByEffector = reference.usedByEffector;
        this.usedByComposite = reference.usedByComposite;
        this.offset = new EVector2(reference.offset);

        if (sharedMaterial != null)
        {
            this.sharedMaterial = new EPhysicMaterial2D(reference.sharedMaterial);
        }
        this.enabled = reference.enabled;

        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref CircleCollider2D reference)
    {
        reference.radius = this.radius;
        reference.density = this.density;
        reference.isTrigger = this.isTrigger;
        reference.usedByEffector = this.usedByEffector;
        reference.usedByComposite = this.usedByComposite;
        reference.offset = this.offset.ToUnityType();

        if (sharedMaterial != null)
        {
            PhysicsMaterial2D mat = reference.sharedMaterial;
            this.sharedMaterial.SetData(ref mat);
        }

        reference.enabled = this.enabled;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override CircleCollider2D ToUnityType()
    {
        CircleCollider2D col = new CircleCollider2D();
        SetData(ref col);
        return col;
    }
}
