﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a SphereCollider
/// </summary>
[System.Serializable]
public class ESphereCollider : SerializableUnityData<SphereCollider>
{
    /// <summary>
    /// The center of this ESphereCollider
    /// </summary>
    public EVector3 center;
    /// <summary>
    /// The radius of this ESphereCollider
    /// </summary>
    public float radius;
    /// <summary>
    /// The enabled state of this ESphereCollider
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The isTrigger state of this ESphereCollider
    /// </summary>
    public bool isTrigger;
    /// <summary>
    /// The contactOffset of this ESphereCollider
    /// </summary>
    public float contactOffset;
    /// <summary>
    /// The material of this ESphereCollider
    /// </summary>
    public EPhysicsMaterial material;
    /// <summary>
    /// The sharedMaterial of this ESphereCollider
    /// </summary>
    public EPhysicsMaterial sharedMaterial;
    /// <summary>
    /// The tag of this SphereCollider
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this SphereCollider
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this SphereCollider
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ESphereCollider
    /// </summary>
    public ESphereCollider() : base(true)
    {

    }

    /// <summary>
    /// Create a new ESphereCollider
    /// </summary>
    /// <param name="reference">The SphereCollider you want to use</param>
    public ESphereCollider(SphereCollider reference) : base(true)
    {
        this.center = new EVector3(reference.center);
        this.radius = reference.radius;
        this.enabled = reference.enabled;
        this.isTrigger = reference.isTrigger;
        this.contactOffset = reference.contactOffset;

        if (reference.material != null)
        {
            this.material = new EPhysicsMaterial(reference.material);
        }
        if (reference.sharedMaterial != null)
        {
            this.sharedMaterial = new EPhysicsMaterial(reference.sharedMaterial);
        }

        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref SphereCollider reference)
    {
        reference.center = this.center.ToUnityType();
        reference.radius = this.radius;
        reference.enabled = this.enabled;
        reference.isTrigger = this.isTrigger;
        reference.contactOffset = this.contactOffset;

        if (this.material != null && reference.material != null)
        {
            PhysicMaterial material = reference.material;
            this.material.SetData(ref material);
        }
        if (this.sharedMaterial != null && reference.sharedMaterial != null)
        {
            PhysicMaterial material = reference.sharedMaterial;
            this.sharedMaterial.SetData(ref material);
        }

        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;

    }

    public override SphereCollider ToUnityType()
    {
        SphereCollider col = new SphereCollider();
        SetData(ref col);
        return col;
    }
}
