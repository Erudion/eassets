﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a PolygonCollider2D
/// </summary>
[System.Serializable]
public class EPolygonCollider2D : SerializableUnityData<PolygonCollider2D>
{
    /// <summary>
    /// The points of this PolygonCollider2D
    /// </summary>
    public EVector2[] points;
    /// <summary>
    /// The pathCount of this PolygonCollider2D
    /// </summary>
    public int pathCount;
    /// <summary>
    /// The autoTiling state of this PolygonCollider2D
    /// </summary>
    public bool autoTiling;
    /// <summary>
    /// The density of this PolygonCollider2D
    /// </summary>
    public float density;
    /// <summary>
    /// The isTrigger state of this PolygonCollider2D
    /// </summary>
    public bool isTrigger;
    /// <summary>
    /// The usedByEffector state of this PolygonCollider2D
    /// </summary>
    public bool usedByEffector;
    /// <summary>
    /// The usedByComposite state of this PolygonCollider2D
    /// </summary>
    public bool usedByComposite;
    /// <summary>
    /// The offset of this PolygonCollider2D
    /// </summary>
    public EVector2 offset;
    /// <summary>
    /// The sharedMaterial of this PolygonCollider2D
    /// </summary>
    public EPhysicMaterial2D sharedMaterial;
    /// <summary>
    /// The enabled state of this PolygonCollider2D
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The tag of this PolygonCollider2D
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this PolygonCollider2D
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this PolygonCollider2D
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EPolygonCollider2D
    /// </summary>
    public EPolygonCollider2D() : base(true)
    {

    }

    /// <summary>
    /// Create a new EPolygonCollider2D
    /// </summary>
    /// <param name="reference">The PolygonCollider2D you want to use</param>
    public EPolygonCollider2D(PolygonCollider2D reference) : base(true)
    {
        this.points = new EVector2[reference.points.Length];
        for(int i = 0;i< reference.points.Length; i++)
        {
            this.points[i] = new EVector2(reference.points[i]);
        }

        this.pathCount = reference.pathCount;
        this.autoTiling = reference.autoTiling;
        this.density = reference.density;
        this.isTrigger = reference.isTrigger;
        this.usedByEffector = reference.usedByEffector;
        this.usedByComposite = reference.usedByComposite;
        this.offset = new EVector2(reference.offset);

        if (sharedMaterial != null)
        {
            this.sharedMaterial = new EPhysicMaterial2D(reference.sharedMaterial);
        }

        this.enabled = reference.enabled;
        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref PolygonCollider2D reference)
    {
        reference.points = new Vector2[this.points.Length];
        for (int i = 0; i < this.points.Length; i++)
        {
            reference.points[i] = this.points[i].ToUnityType();
        }

        reference.pathCount = this.pathCount;
        reference.autoTiling = this.autoTiling;
        reference.density = this.density;
        reference.isTrigger = this.isTrigger;
        reference.usedByEffector = this.usedByEffector;
        reference.usedByComposite = this.usedByComposite;
        reference.offset = this.offset.ToUnityType();

        if (this.sharedMaterial != null)
        {
            if (reference.sharedMaterial != null)
            {
                PhysicsMaterial2D mat = reference.sharedMaterial;
                this.sharedMaterial.SetData(ref mat);
            }
            else
            {
                reference.sharedMaterial = this.sharedMaterial.ToUnityType();
            }
        }

        reference.enabled = this.enabled;

        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override PolygonCollider2D ToUnityType()
    {
        PolygonCollider2D col = new PolygonCollider2D();
        SetData(ref col);
        return col;
    }
}
