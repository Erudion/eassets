﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a EdgeCollider2D
/// </summary>
[System.Serializable]
public class EEdgeCollider2D : SerializableUnityData<EdgeCollider2D>
{
    /// <summary>
    /// The edgeRadius of this EdgeCollider2D
    /// </summary>
    public float edgeRadius;
    /// <summary>
    /// The points of this EdgeCollider2D
    /// </summary>
    public EVector2[] points;
    /// <summary>
    /// The density of this EdgeCollider2D
    /// </summary>
    public float density;
    /// <summary>
    /// The isTrigger state of this EdgeCollider2D
    /// </summary>
    public bool isTrigger;
    /// <summary>
    /// The usedByEffector state of this EdgeCollider2D
    /// </summary>
    public bool usedByEffector;
    /// <summary>
    /// The usedByComposite state of this EdgeCollider2D
    /// </summary>
    public bool usedByComposite;
    /// <summary>
    /// The offset of this EdgeCollider2D
    /// </summary>
    public EVector2 offset;
    /// <summary>
    /// The sharedMaterial of this EdgeCollider2D
    /// </summary>
    public EPhysicMaterial2D sharedMaterial;
    /// <summary>
    /// The enabled state of this EdgeCollider2D
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The tag of this EdgeCollider2D
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this EdgeCollider2D
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this EdgeCollider2D
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EEdgeCollider2D
    /// </summary>
    public EEdgeCollider2D() : base(true)
    {

    }

    /// <summary>
    /// Create a new EEdgeCollider2D
    /// </summary>
    /// <param name="reference">The EdgeCollider2D you want to use</param>
    public EEdgeCollider2D(EdgeCollider2D reference) : base(true)
    {
        this.edgeRadius = reference.edgeRadius;

        this.points = new EVector2[reference.points.Length];
        for(int i = 0; i < reference.points.Length; i++)
        {
            this.points[i] = new EVector2(reference.points[i]);
        }

        this.density = reference.density;
        this.isTrigger = reference.isTrigger;
        this.usedByEffector = reference.usedByEffector;
        this.usedByComposite = reference.usedByComposite;
        this.offset = new EVector2(reference.offset);

        if(reference.sharedMaterial != null)
        {
            this.sharedMaterial = new EPhysicMaterial2D(reference.sharedMaterial);
        }

        this.enabled = reference.enabled;
        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref EdgeCollider2D reference)
    {
        reference.edgeRadius = this.edgeRadius;

        reference.points = new Vector2[this.points.Length];
        for (int i = 0; i < this.points.Length; i++)
        {
            reference.points[i] = this.points[i].ToUnityType();
        }

        reference.density = this.density;
        reference.isTrigger = this.isTrigger;
        reference.usedByEffector = this.usedByEffector;
        reference.usedByComposite = this.usedByComposite;
        reference.offset = this.offset.ToUnityType();

        if (this.sharedMaterial != null && reference.sharedMaterial != null)
        {
            PhysicsMaterial2D mat = reference.sharedMaterial;
            this.sharedMaterial.SetData(ref mat);
        }

        reference.enabled = this.enabled;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override EdgeCollider2D ToUnityType()
    {
        EdgeCollider2D col = new EdgeCollider2D();
        SetData(ref col);
        return col;
    }
}
