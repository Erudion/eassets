﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a WheelCollider
/// </summary>
[System.Serializable]
public class EWheelCollider : SerializableUnityData<WheelCollider>
{
    /// <summary>
    /// The center of this EWheelCollider
    /// </summary>
    public EVector3 center;
    /// <summary>
    /// The radius of this EWheelCollider
    /// </summary>
    public float radius;
    /// <summary>
    /// The suspensionDistance of this EWheelCollider
    /// </summary>
    public float suspensionDistance;
    /// <summary>
    /// The suspensionSpring of this EWheelCollider
    /// </summary>
    public EJointSpring suspensionSpring;
    /// <summary>
    /// The forceAppPointDistance of this EWheelCollider
    /// </summary>
    public float forceAppPointDistance;
    /// <summary>
    /// The mass of this EWheelCollider
    /// </summary>
    public float mass;
    /// <summary>
    /// The wheelDampingRate of this EWheelCollider
    /// </summary>
    public float wheelDampingRate;
    /// <summary>
    /// The forwardFriction of this EWheelCollider
    /// </summary>
    public EWheelFrictionCurve forwardFriction;
    /// <summary>
    /// The sidewaysFriction of this EWheelCollider
    /// </summary>
    public EWheelFrictionCurve sidewaysFriction;
    /// <summary>
    /// The motorTorque of this EWheelCollider
    /// </summary>
    public float motorTorque;
    /// <summary>
    /// The brakeTorque of this EWheelCollider
    /// </summary>
    public float brakeTorque;
    /// <summary>
    /// The steerAngle of this EWheelCollider
    /// </summary>
    public float steerAngle;
    /// <summary>
    /// The enabled state of this EWheelCollider
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The isTrigger state of this EWheelCollider
    /// </summary>
    public bool isTrigger;
    /// <summary>
    /// The contactOffset of this EWheelCollider
    /// </summary>
    public float contactOffset;
    /// <summary>
    /// The material of this EWheelCollider
    /// </summary>
    public EPhysicsMaterial material;
    /// <summary>
    /// The sharedMaterial of this EWheelCollider
    /// </summary>
    public EPhysicsMaterial sharedMaterial;
    /// <summary>
    /// The tag of this WheelCollider
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this WheelCollider
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this WheelCollider
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EWheelCollider
    /// </summary>
    public EWheelCollider() : base(true)
    {

    }

    /// <summary>
    /// Create a new EWheelCollider
    /// </summary>
    /// <param name="reference">The WheelCollider you want to use</param>
    public EWheelCollider(WheelCollider reference) : base(true)
    {
        this.center = new EVector3(reference.center);
        this.radius = reference.radius;
        this.suspensionDistance = reference.suspensionDistance;
        this.suspensionSpring = new EJointSpring(reference.suspensionSpring);
        this.forceAppPointDistance = reference.forceAppPointDistance;
        this.mass = reference.mass;
        this.wheelDampingRate = reference.wheelDampingRate;
        this.forwardFriction = new EWheelFrictionCurve(reference.forwardFriction);
        this.sidewaysFriction = new EWheelFrictionCurve(reference.sidewaysFriction);
        this.motorTorque = reference.motorTorque;
        this.brakeTorque = reference.brakeTorque;
        this.steerAngle = reference.steerAngle;
        this.enabled = reference.enabled;
        this.isTrigger = reference.isTrigger;
        this.contactOffset = reference.contactOffset;
        
        if(reference.material != null)
        {
            this.material = new EPhysicsMaterial(reference.material);
        }
        if (reference.sharedMaterial != null)
        {
            this.sharedMaterial = new EPhysicsMaterial(reference.sharedMaterial);
        }

        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref WheelCollider reference)
    {
        reference.center = this.center;
        reference.radius = this.radius;
        reference.suspensionDistance = this.suspensionDistance;
        reference.suspensionSpring = this.suspensionSpring.ToUnityType();
        reference.forceAppPointDistance = this.forceAppPointDistance;
        reference.mass = this.mass;
        reference.wheelDampingRate = this.wheelDampingRate;
        reference.forwardFriction = this.forwardFriction.ToUnityType();
        reference.sidewaysFriction = this.sidewaysFriction.ToUnityType();
        reference.motorTorque = this.motorTorque;
        reference.brakeTorque = this.brakeTorque;
        reference.steerAngle = this.steerAngle;
        reference.enabled = this.enabled;
        reference.isTrigger = this.isTrigger;
        reference.contactOffset = this.contactOffset;

        if (this.material != null && reference.material != null)
        {
            PhysicMaterial mat = reference.material;
            this.material.SetData(ref mat);
        }
        if (this.sharedMaterial != null && reference.sharedMaterial != null)
        {
            PhysicMaterial mat = reference.sharedMaterial;
            this.sharedMaterial.SetData(ref mat);
        }

        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;

    }

    public override WheelCollider ToUnityType()
    {
        WheelCollider col = new WheelCollider();
        SetData(ref col);
        return col;
    }
}
