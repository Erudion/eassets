﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a CapsuleCollider2D
/// </summary>
[System.Serializable]
public class ECapsuleCollider2D : SerializableUnityData<CapsuleCollider2D>
{
    /// <summary>
    /// The size of this CapsuleCollider2D
    /// </summary>
    public EVector2 size;
    /// <summary>
    /// The direction of this CapsuleCollider2D
    /// </summary>
    public int direction;
    /// <summary>
    /// The density of this CapsuleCollider2D
    /// </summary>
    public float density;
    /// <summary>
    /// The isTrigger state of this CapsuleCollider2D
    /// </summary>
    public bool isTrigger;
    /// <summary>
    /// The usedByEffector state of this CapsuleCollider2D
    /// </summary>
    public bool usedByEffector;
    /// <summary>
    /// The usedByComposite state of this CapsuleCollider2D
    /// </summary>
    public bool usedByComposite;
    /// <summary>
    /// The offset of this CapsuleCollider2D
    /// </summary>
    public EVector2 offset;
    /// <summary>
    /// The sharedMaterial of this CapsuleCollider2D
    /// </summary>
    public EPhysicMaterial2D sharedMaterial;
    /// <summary>
    /// The enabled state of this CapsuleCollider2D
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The tag of this CapsuleCollider2D
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this CapsuleCollider2D
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this CapsuleCollider2D
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ECapsuleCollider2D
    /// </summary>
    public ECapsuleCollider2D() : base(false)
    {

    }

    /// <summary>
    /// Create a new ECapsuleCollider2D
    /// </summary>
    /// <param name="col">The capsulecollider you want to use</param>
    public ECapsuleCollider2D(CapsuleCollider2D col) : base(false)
    {
        this.size = new EVector2(col.size);
        this.direction = (int)col.direction;
        this.density = col.density;
        this.isTrigger = col.isTrigger;
        this.usedByEffector = col.usedByEffector;
        this.usedByComposite = col.usedByComposite;
        this.offset = new EVector2(col.offset);
        this.sharedMaterial = new EPhysicMaterial2D(col.sharedMaterial);
        this.enabled = col.enabled;
        this.tag = col.tag;
        this.name = col.name;
        this.hideFlags = (int)col.hideFlags;
    }

    public override void SetData(ref CapsuleCollider2D reference)
    {
        reference.size = this.size;
        reference.direction = (CapsuleDirection2D)this.direction;
        reference.density = this.density;
        reference.isTrigger = this.isTrigger;
        reference.usedByEffector = this.usedByEffector;
        reference.usedByComposite = this.usedByComposite;
        reference.offset = this.offset;

        if (this.sharedMaterial != null)
        {
            PhysicsMaterial2D mat = reference.sharedMaterial;
            this.sharedMaterial.SetData(ref mat);
        }

        reference.enabled = this.enabled;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override CapsuleCollider2D ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}