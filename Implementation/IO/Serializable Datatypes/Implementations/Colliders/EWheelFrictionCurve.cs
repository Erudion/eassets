﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a WheelFrictionCurve
/// </summary>
[System.Serializable]
public class EWheelFrictionCurve : SerializableUnityData<WheelFrictionCurve>
{
    /// <summary>
    /// The extremumSlip of this EWheelFrictionCurve
    /// </summary>
    public float extremumSlip;
    /// <summary>
    /// The extremumValue of this EWheelFrictionCurve
    /// </summary>
    public float extremumValue;
    /// <summary>
    /// The asymptoteSlip of this EWheelFrictionCurve
    /// </summary>
    public float asymptoteSlip;
    /// <summary>
    /// The asymptoteValue of this EWheelFrictionCurve
    /// </summary>
    public float asymptoteValue;
    /// <summary>
    /// The stiffness of this EWheelFrictionCurve
    /// </summary>
    public float stiffness;

    /// <summary>
    /// Create a new EWheelFrictionCurve
    /// </summary>
    public EWheelFrictionCurve() : base(true)
    {

    }

    /// <summary>
    /// Create a new EWheelFrictionCurve
    /// </summary>
    /// <param name="reference">The WheelFrictionCurve you want to use</param>
    public EWheelFrictionCurve(WheelFrictionCurve reference) : base(true)
    {
        this.extremumSlip = reference.extremumSlip;
        this.extremumValue = reference.extremumValue;
        this.asymptoteSlip = reference.asymptoteSlip;
        this.asymptoteValue = reference.asymptoteValue;
        this.stiffness = reference.stiffness;
    }

    public override void SetData(ref WheelFrictionCurve reference)
    {
        reference.extremumSlip = this.extremumSlip;
        reference.extremumValue = this.extremumValue;
        reference.asymptoteSlip = this.asymptoteSlip;
        reference.asymptoteValue = this.asymptoteValue;
        reference.stiffness = this.stiffness;
    }

    public override WheelFrictionCurve ToUnityType()
    {
        WheelFrictionCurve curve = new WheelFrictionCurve();
        SetData(ref curve);
        return curve;
    }
}
