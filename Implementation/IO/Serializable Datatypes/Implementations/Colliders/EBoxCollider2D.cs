﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a BoxCollider2D
/// </summary>
[System.Serializable]
public class EBoxCollider2D : SerializableUnityData<BoxCollider2D>
{
    /// <summary>
    /// The size of this Boxcollider2D
    /// </summary>
    public EVector2 size;
    /// <summary>
    /// The edgeRadius of this Boxcollider2D
    /// </summary>
    public float edgeRadius;
    /// <summary>
    /// The autoTiling state of this Boxcollider2D
    /// </summary>
    public bool autoTiling;
    /// <summary>
    /// The density of this Boxcollider2D
    /// </summary>
    public float density;
    /// <summary>
    /// The isTrigger state of this Boxcollider2D
    /// </summary>
    public bool isTrigger;
    /// <summary>
    /// The usedByEffector state of this Boxcollider2D
    /// </summary>
    public bool usedByEffector;
    /// <summary>
    /// The usedByComposite state of this Boxcollider2D
    /// </summary>
    public bool usedByComposite;
    /// <summary>
    /// The offset of this Boxcollider2D
    /// </summary>
    public EVector2 offset;
    /// <summary>
    /// The sharedMaterial of this Boxcollider2D
    /// </summary>
    public EPhysicMaterial2D sharedMaterial;
    /// <summary>
    /// The enabled state of this Boxcollider2D
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The tag of this Boxcollider2D
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this Boxcollider2D
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this Boxcollider2D
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EBoxCollider2D
    /// </summary>
    public EBoxCollider2D() : base(false)
    {

    }

    /// <summary>
    /// Create a new EBoxCollider2D
    /// </summary>
    /// <param name="box">The boxcollider you want to use</param>
    public EBoxCollider2D(BoxCollider2D box) : base(false)
    {
        this.size = box.size;
        this.edgeRadius = box.edgeRadius;
        this.autoTiling = box.autoTiling;
        this.density = box.density;
        this.isTrigger = box.isTrigger;
        this.usedByEffector = box.usedByEffector;
        this.usedByComposite = box.usedByComposite;
        this.offset = box.offset;
        this.sharedMaterial = new EPhysicMaterial2D(box.sharedMaterial);
        this.enabled = box.enabled;
    }

    public override void SetData(ref BoxCollider2D reference)
    {
        reference.size = this.size;
        reference.edgeRadius = this.edgeRadius;
        reference.autoTiling = this.autoTiling;
        reference.density = this.density;
        reference.isTrigger = this.isTrigger;
        reference.usedByEffector = this.usedByEffector;
        reference.usedByComposite = this.usedByComposite;
        reference.offset = this.offset;

        if (this.sharedMaterial != null)
        {
            PhysicsMaterial2D mat = reference.sharedMaterial;
            this.sharedMaterial.SetData(ref mat);
        }

        reference.enabled = this.enabled;
    }

    public override BoxCollider2D ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}
