﻿using EAssets.IO.SerializableDatatypes;
using System;
using UnityEngine;

/// <summary>
/// This class can be used to serialize an AnimationEvent
/// </summary>
[System.Serializable]
public class EAnimationEvent : SerializableUnityData<AnimationEvent>
{
    /// <summary>
    /// The stringParameter of this EAnimationEvent
    /// </summary>
    public string stringParameter;
    /// <summary>
    /// The floatParameter of this EAnimationEvent
    /// </summary>
    public float floatParameter;
    /// <summary>
    /// The intParameter of this EAnimationEvent
    /// </summary>
    public int intParameter;
    /// <summary>
    /// The objectReferenceParameterType of this EAnimationEvent
    /// </summary>
    public string objectReferenceParameterType;
    /// <summary>
    /// The objectReferenceParameter of this EAnimationEvent
    /// </summary>
    public object objectReferenceParameter; //TODO: Use a different type: this should not really work
    /// <summary>
    /// The functionName of this EAnimationEvent
    /// </summary>
    public string functionName;
    /// <summary>
    /// The time of this EAnimationEvent
    /// </summary>
    public float time;
    /// <summary>
    /// The messageOptions of this EAnimationEvent
    /// </summary>
    public int messageOptions;

    /// <summary>
    /// Create a new EAnimationEvent
    /// </summary>
    public EAnimationEvent() : base(true)
    {

    }

    /// <summary>
    /// Create a new EAnimationEvent
    /// </summary>
    /// <param name="reference">The AnimationEvent you want to use</param>
    public EAnimationEvent(AnimationEvent reference) : base(true)
    {
        this.stringParameter = reference.stringParameter;
        this.floatParameter = reference.floatParameter;
        this.intParameter = reference.intParameter;

        if (reference.objectReferenceParameter != null)
        {
            this.objectReferenceParameterType = reference.objectReferenceParameter.GetType().AssemblyQualifiedName;
            this.objectReferenceParameter = reference.objectReferenceParameter;
        }

        this.functionName = reference.functionName;
        this.time = reference.time;
        this.messageOptions = (int)reference.messageOptions;
    }

    public override void SetData(ref AnimationEvent reference)
    {
        reference.stringParameter = this.stringParameter;
        reference.floatParameter = this.floatParameter;
        reference.intParameter = this.intParameter;

        if (this.objectReferenceParameter != null)
        {
            //This is a solution for all instanciable types. This will not always work
            Type t = Type.GetType(this.objectReferenceParameterType);
            reference.objectReferenceParameter = (UnityEngine.Object)Activator.CreateInstance(t);
        }

        reference.functionName = this.functionName;
        reference.time = this.time;
        reference.messageOptions = (SendMessageOptions)this.messageOptions;
    }

    public override AnimationEvent ToUnityType()
    {
        AnimationEvent animationEvent = new AnimationEvent();
        SetData(ref animationEvent);
        return animationEvent;
    }
}
