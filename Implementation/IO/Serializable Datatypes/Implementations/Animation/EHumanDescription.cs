﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a HumanDescription
/// </summary>
[System.Serializable]
public class EHumanDescription : SerializableUnityData<HumanDescription>
{
    public float armStretch;
    public float feetSpacing;
    public bool hasTranslationDoF;
    public EHumanBone[] human;
    public ESkeletonBone[] skeleton;
    public float legStretch;
    public float lowerArmTwist;
    public float lowerLegTwist;
    public float upperArmTwist;
    public float upperLegTwist;

    /// <summary>
    /// Create a new EHumanDescription
    /// </summary>
    public EHumanDescription() : base(false)
    {

    }

    /// <summary>
    /// Create a new EHumanDescription
    /// </summary>
    /// <param name="reference">The HumanDescription you want to use</param>
    public EHumanDescription(HumanDescription reference) : base(false)
    {
        this.armStretch = reference.armStretch;
        this.feetSpacing = reference.feetSpacing;
        this.hasTranslationDoF = reference.hasTranslationDoF;
        this.legStretch = reference.legStretch;
        this.lowerArmTwist = reference.lowerArmTwist;
        this.lowerLegTwist = reference.lowerLegTwist;
        this.upperArmTwist = reference.upperArmTwist;
        this.upperLegTwist = reference.upperLegTwist;
    }

    public override void SetData(ref HumanDescription reference)
    {

    }

    public override HumanDescription ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}
