﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a Keyframe
/// </summary>
[System.Serializable]
public class EKeyframe : SerializableUnityData<Keyframe>
{
    /// <summary>
    /// The time of this Keyframe
    /// </summary>
    public float time;
    /// <summary>
    /// The value of this Keyframe
    /// </summary>
    public float value;
    /// <summary>
    /// The inTangent of this Keyframe
    /// </summary>
    public float inTangent;
    /// <summary>
    /// The outTangent of this Keyframe
    /// </summary>
    public float outTangent;

    /// <summary>
    /// Create a new EKeyframe
    /// </summary>
    public EKeyframe() : base(true)
    {

    }

    /// <summary>
    /// Create a new EKeyframe
    /// </summary>
    /// <param name="reference">The Keyframe you want to use</param>
    public EKeyframe(Keyframe reference) : base(true)
    {
        this.time = reference.time;
        this.value = reference.value;
        this.inTangent = reference.inTangent;
        this.outTangent = reference.outTangent;
    }

    public override void SetData(ref Keyframe reference)
    {
        reference.time = this.time;
        reference.value = this.value;
        reference.inTangent = this.inTangent;
        reference.outTangent = this.outTangent;
    }

    public override Keyframe ToUnityType()
    {
        Keyframe frame = new Keyframe();
        SetData(ref frame);
        return frame;
    }
}
