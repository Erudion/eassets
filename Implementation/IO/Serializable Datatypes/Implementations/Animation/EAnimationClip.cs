﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize an AnimationClip
/// </summary>
[System.Serializable]
public class EAnimationClip : SerializableUnityData<AnimationClip>
{
    /// <summary>
    /// The frameRate of this EAnimationClip
    /// </summary>
    public float frameRate;
    /// <summary>
    /// The wrapMode of this EAnimationClip
    /// </summary>
    public int wrapMode;
    /// <summary>
    /// The localBounds of this EAnimationClip
    /// </summary>
    public EBounds localBounds;
    /// <summary>
    /// The legacy state of this EAnimationClip
    /// </summary>
    public bool legacy;
    /// <summary>
    /// The events of this EAnimationClip
    /// </summary>
    public EAnimationEvent[] events;
    /// <summary>
    /// The name of this AnimationClip
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this AnimationClip
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EAnimationClip
    /// </summary>
    public EAnimationClip() : base(true)
    {

    }

    /// <summary>
    /// Create a new EAnimationClip
    /// </summary>
    /// <param name="reference">The AnimationClip you want to use</param>
    public EAnimationClip(AnimationClip reference) : base(true)
    {
        this.frameRate = reference.frameRate;
        this.wrapMode = (int)reference.wrapMode;
        this.localBounds = new EBounds(reference.localBounds);
        this.legacy = reference.legacy;

        if (reference.events.Length > 0)
        {
            this.events = new EAnimationEvent[reference.events.Length];
            for (int i = 0; i < reference.events.Length; i++)
            {
                this.events[i] = new EAnimationEvent(reference.events[i]);
            }
        }

        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref AnimationClip reference)
    {
        reference.frameRate = this.frameRate;
        reference.wrapMode = (WrapMode)this.wrapMode;
        reference.localBounds = this.localBounds.ToUnityType();
        reference.legacy = this.legacy;

        if (this.events.Length > 0)
        {
            reference.events = new AnimationEvent[this.events.Length];
            for (int i = 0; i < this.events.Length; i++)
            {
                reference.events[i] = this.events[i].ToUnityType();
            }
        }

        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override AnimationClip ToUnityType()
    {
        AnimationClip clip = new AnimationClip();
        SetData(ref clip);
        return clip;
    }
}
