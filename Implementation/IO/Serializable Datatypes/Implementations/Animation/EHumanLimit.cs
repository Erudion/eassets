﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a HumanLimit
/// </summary>
[System.Serializable]
public class EHumanLimit : SerializableUnityData<HumanLimit>
{
    /// <summary>
    /// The axisLength of this EHumanLimit
    /// </summary>
    public float axisLength;
    /// <summary>
    /// The center of this EHumanLimit
    /// </summary>
    public EVector3 center;
    /// <summary>
    /// The max of this EHumanLimit
    /// </summary>
    public EVector3 max;
    /// <summary>
    /// The min of this EHumanLimit
    /// </summary>
    public EVector3 min;
    /// <summary>
    /// The useDefaultValues state of this EHumanLimit
    /// </summary>
    public bool useDefaultValues;

    /// <summary>
    /// Create a new EHumanLimit
    /// </summary>
    public EHumanLimit() : base(true)
    {

    }

    /// <summary>
    /// Create a new EHumanLimit
    /// </summary>
    /// <param name="reference">The HumanLimit you want to use</param>
    public EHumanLimit(HumanLimit reference) : base(true)
    {
        this.axisLength = reference.axisLength;
        this.center = new EVector3(reference.center);
        this.max = new EVector3(reference.max);
        this.min = new EVector3(reference.min);
        this.useDefaultValues = reference.useDefaultValues;
    }

    public override void SetData(ref HumanLimit reference)
    {
        reference.axisLength = this.axisLength;
        reference.center = this.center.ToUnityType();
        reference.max = this.max.ToUnityType();
        reference.min = this.min.ToUnityType();
        reference.useDefaultValues = this.useDefaultValues;
    }

    public override HumanLimit ToUnityType()
    {
        HumanLimit limit = new HumanLimit();
        SetData(ref limit);
        return limit;
    }
}
