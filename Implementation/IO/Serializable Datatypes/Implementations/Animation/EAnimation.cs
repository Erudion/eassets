﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize an Animation
/// </summary>
[System.Serializable]
public class EAnimation : SerializableUnityData<Animation>
{
    /// <summary>
    /// The clip of this EAnimation
    /// </summary>
    public EAnimationClip clip;
    /// <summary>
    /// The playAutomatically state of this EAnimation
    /// </summary>
    public bool playAutomatically;
    /// <summary>
    /// The wrapMode of this EAnimation
    /// </summary>
    public int wrapMode;
    /// <summary>
    /// The animatePhysics state of this EAnimation
    /// </summary>
    public bool animatePhysics;
    /// <summary>
    /// The cullingType of this EAnimation
    /// </summary>
    public int cullingType;
    /// <summary>
    /// The localBounds of this EAnimation
    /// </summary>
    public EBounds localBounds;
    /// <summary>
    /// The enabled state of this EAnimation
    /// </summary>
    public bool enabled;

    /// <summary>
    /// The tag of this Animation
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this Animation
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this Animation
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EAnimation
    /// </summary>
    public EAnimation() : base(false)
    {

    }

    /// <summary>
    /// Create a new EAnimation
    /// </summary>
    /// <param name="reference">The Animation you want to use</param>
    public EAnimation(Animation reference) : base(false)
    {
        this.clip = new EAnimationClip(reference.clip);
        this.playAutomatically = reference.playAutomatically;
        this.wrapMode = (int)reference.wrapMode;
        this.animatePhysics = reference.animatePhysics;
        this.cullingType = (int)reference.cullingType;
        this.localBounds = new EBounds(reference.localBounds);
        this.enabled = reference.enabled;
        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref Animation reference)
    {
        reference.clip = this.clip.ToUnityType();
        reference.playAutomatically = this.playAutomatically;
        reference.wrapMode = (WrapMode)this.wrapMode;
        reference.animatePhysics = this.animatePhysics;
        reference.cullingType = (AnimationCullingType)this.cullingType;
        reference.localBounds = this.localBounds.ToUnityType();
        reference.enabled = this.enabled;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override Animation ToUnityType()
    {
        Animation animation = new Animation();
        SetData(ref animation);
        return animation;
    }
}
