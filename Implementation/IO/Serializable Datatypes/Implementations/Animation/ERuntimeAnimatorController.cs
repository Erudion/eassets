﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a RuntimeAnimatorController
/// </summary>
[System.Serializable]
public class ERuntimeAnimatorController : SerializableUnityData<RuntimeAnimatorController>
{
    /// <summary>
    /// The name of this RuntimeAnimatorController
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this RuntimeAnimatorController
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ERuntimeAnimatorController
    /// </summary>
    public ERuntimeAnimatorController() : base(false)
    {

    }

    /// <summary>
    /// Create a new ERuntimeAnimatorController
    /// </summary>
    /// <param name="reference">The RuntimeAnimatorController you want to use</param>
    public ERuntimeAnimatorController(RuntimeAnimatorController reference) : base(false)
    {
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref RuntimeAnimatorController reference)
    {
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override RuntimeAnimatorController ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}
