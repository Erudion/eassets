﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a AvatarMask
/// </summary>
[System.Serializable]
public class EAvatarMask : SerializableUnityData<AvatarMask>
{
    /// <summary>
    /// The transformCount of this AvatarMask
    /// </summary>
    public int transformCount;
    /// <summary>
    /// The name of this AvatarMask
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this AvatarMask
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EAvatarMask
    /// </summary>
    public EAvatarMask() : base(true)
    {

    }

    /// <summary>
    /// Create a new EAvatarMask
    /// </summary>
    /// <param name="reference">The AvatarMask you want to use</param>
    public EAvatarMask(AvatarMask reference) : base(true)
    {
        this.transformCount = reference.transformCount;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref AvatarMask reference)
    {
        reference.transformCount = this.transformCount;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override AvatarMask ToUnityType()
    {
        AvatarMask mask = new AvatarMask();
        SetData(ref mask);
        return mask;
    }
}
