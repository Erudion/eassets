﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize an AnimationCurve
/// </summary>
[System.Serializable]
public class EAnimationCurve : SerializableUnityData<AnimationCurve>
{
    /// <summary>
    /// The keys of this EAnimationCurve
    /// </summary>
    public EKeyframe[] keys;
    /// <summary>
    /// The preWrapMode of this EAnimationCurve
    /// </summary>
    public int preWrapMode;
    /// <summary>
    /// The postWrapMode of this EAnimationCurve
    /// </summary>
    public int postWrapMode;

    /// <summary>
    /// Create a new EAnimationCurve
    /// </summary>
    public EAnimationCurve() : base(true)
    {

    }

    /// <summary>
    /// Create a new EAnimationCurve
    /// </summary>
    /// <param name="reference">The AnimationCurve you want to use</param>
    public EAnimationCurve(AnimationCurve reference) : base(true)
    {
        this.keys = new EKeyframe[reference.keys.Length];
        for(int i = 0; i < reference.keys.Length; i++)
        {
            keys[i] = new EKeyframe(reference.keys[i]);
        }

        this.preWrapMode = (int)reference.preWrapMode;
        this.postWrapMode = (int)reference.postWrapMode;
    }

    public override void SetData(ref AnimationCurve reference)
    {
        reference.keys = new Keyframe[this.keys.Length];
        Keyframe tmp = new Keyframe();
        for (int i = 0; i < this.keys.Length; i++)
        {
            this.keys[i].SetData(ref tmp);
            reference.keys[i] = tmp;
        }

        reference.preWrapMode = (WrapMode)this.preWrapMode;
        reference.postWrapMode = (WrapMode)this.postWrapMode;
    }

    public override AnimationCurve ToUnityType()
    {
        AnimationCurve curve = new AnimationCurve();
        SetData(ref curve);
        return curve;
    }
}
