﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a Animator
/// </summary>
[System.Serializable]
public class EAnimator : SerializableUnityData<Animator>
{
    /// <summary>
    /// The rootPosition of this Animator
    /// </summary>
    public EVector3 rootPosition;
    /// <summary>
    /// The rootRotation of this Animator
    /// </summary>
    public EQuaternion rootRotation;
    /// <summary>
    /// The applyRootMotion state of this Animator
    /// </summary>
    public bool applyRootMotion;
    /// <summary>
    /// The updateMode of this Animator
    /// </summary>
    public int updateMode;
    /// <summary>
    /// The bodyPosition of this Animator
    /// </summary>
    public EVector3 bodyPosition;
    /// <summary>
    /// The bodyRotation of this Animator
    /// </summary>
    public EQuaternion bodyRotation;
    /// <summary>
    /// The stabilizeFeet state of this Animator
    /// </summary>
    public bool stabilizeFeet;
    /// <summary>
    /// The feetPivotActive of this Animator
    /// </summary>
    public float feetPivotActive;
    /// <summary>
    /// The speed of this Animator
    /// </summary>
    public float speed;
    /// <summary>
    /// The cullingMode of this Animator
    /// </summary>
    public int cullingMode;
    /// <summary>
    /// The playbackTime of this Animator
    /// </summary>
    public float playbackTime;
    /// <summary>
    /// The recorderStartTime of this Animator
    /// </summary>
    public float recorderStartTime;
    /// <summary>
    /// The recorderStopTime of this Animator
    /// </summary>
    public float recorderStopTime;
    /// <summary>
    /// The runtimeAnimatorController of this Animator
    /// </summary>
    public ERuntimeAnimatorController runtimeAnimatorController;
    /// <summary>
    /// The avatar of this Animator
    /// </summary>
    public EAvatar avatar;
    /// <summary>
    /// The layersAffectMassCenter state of this Animator
    /// </summary>
    public bool layersAffectMassCenter;
    /// <summary>
    /// The logWarnings state of this Animator
    /// </summary>
    public bool logWarnings;
    /// <summary>
    /// The fireEvents state of this Animator
    /// </summary>
    public bool fireEvents;
    /// <summary>
    /// The enabled state of this Animator
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The tag of this Animator
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this Animator
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this Animator
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EAnimator
    /// </summary>
    public EAnimator() : base(true)
    {

    }

    /// <summary>
    /// Create a new EAnimator
    /// </summary>
    /// <param name="reference">The Animator you want to use</param>
    public EAnimator(Animator reference) : base(true)
    {
        this.rootPosition = new EVector3(reference.rootPosition);
        this.rootRotation = new EQuaternion(reference.rootRotation);
        this.applyRootMotion = reference.applyRootMotion;
        this.updateMode = (int)reference.updateMode;
        this.bodyPosition = new EVector3(reference.bodyPosition);
        this.bodyRotation = new EQuaternion(reference.bodyRotation);
        this.stabilizeFeet = reference.stabilizeFeet;
        this.feetPivotActive = reference.feetPivotActive;
        this.speed = reference.speed;
        this.cullingMode = (int)reference.cullingMode;
        this.playbackTime = reference.playbackTime;
        this.recorderStartTime = reference.recorderStartTime;
        this.recorderStopTime = reference.recorderStopTime;

        if (reference.runtimeAnimatorController != null)
        {
            this.runtimeAnimatorController = 
                new ERuntimeAnimatorController(reference.runtimeAnimatorController); 
        }

        if (reference.avatar != null)
        {
            this.avatar = new EAvatar(reference.avatar);
        }

        this.layersAffectMassCenter = reference.layersAffectMassCenter;
        this.logWarnings = reference.logWarnings;
        this.fireEvents = reference.fireEvents;
        this.enabled = reference.enabled;
        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref Animator reference)
    {
        reference.rootPosition = this.rootPosition.ToUnityType();
        reference.rootRotation = this.rootRotation.ToUnityType();
        reference.applyRootMotion = this.applyRootMotion;
        reference.updateMode = (AnimatorUpdateMode)this.updateMode;
        reference.bodyPosition = this.bodyPosition.ToUnityType();
        reference.bodyRotation = this.bodyRotation.ToUnityType();
        reference.stabilizeFeet = this.stabilizeFeet;
        reference.feetPivotActive = this.feetPivotActive;
        reference.speed = this.speed;
        reference.cullingMode = (AnimatorCullingMode)this.cullingMode;
        reference.playbackTime = this.playbackTime;
        reference.recorderStartTime = this.recorderStartTime;
        reference.recorderStopTime = this.recorderStopTime;

        if (this.runtimeAnimatorController != null && reference.runtimeAnimatorController != null)
        {
            RuntimeAnimatorController controller = reference.runtimeAnimatorController;
            this.runtimeAnimatorController.SetData(ref controller);
        }

        if (this.avatar != null && reference.avatar != null)
        {
            Avatar avatar = reference.avatar;
            this.avatar.SetData(ref avatar);
        }

        reference.layersAffectMassCenter = this.layersAffectMassCenter;
        reference.logWarnings = this.logWarnings;
        reference.fireEvents = this.fireEvents;
        reference.enabled = this.enabled;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override Animator ToUnityType()
    {
        Animator anim = new Animator();
        SetData(ref anim);
        return anim;
    }
}
