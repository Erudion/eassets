﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a SkeletonBone
/// </summary>
[System.Serializable]
public class ESkeletonBone : SerializableUnityData<SkeletonBone>
{
    /// <summary>
    /// The position of this ESkeletonBone
    /// </summary>
    public EVector3 position;
    /// <summary>
    /// The rotation of this ESkeletonBone
    /// </summary>
    public EQuaternion rotation;
    /// <summary>
    /// The scale of this ESkeletonBone
    /// </summary>
    public EVector3 scale;
    /// <summary>
    /// The name of this SkeletonBone
    /// </summary>
    public string name;

    /// <summary>
    /// Create a new ESkeletonBone
    /// </summary>
    public ESkeletonBone() : base(true)
    {

    }

    /// <summary>
    /// Create a new ESkeletonBone
    /// </summary>
    /// <param name="reference">The SkeletonBone you want to use</param>
    public ESkeletonBone(SkeletonBone reference) : base(true)
    {
        this.position = new EVector3(reference.position);
        this.rotation = new EQuaternion(reference.rotation);
        this.scale = new EVector3(reference.scale);
        this.name = reference.name;
    }

    public override void SetData(ref SkeletonBone reference)
    {
        reference.position = this.position.ToUnityType();
        reference.rotation = this.rotation.ToUnityType();
        reference.scale = this.scale.ToUnityType();
        reference.name = this.name;
    }

    public override SkeletonBone ToUnityType()
    {
        SkeletonBone bone = new SkeletonBone();
        SetData(ref bone);
        return bone;
    }
}
