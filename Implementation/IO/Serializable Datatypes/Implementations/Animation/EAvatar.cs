﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a Avatar
/// </summary>
[System.Serializable]
public class EAvatar : SerializableUnityData<Avatar>
{
    /// <summary>
    /// The name of this Avatar
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this Avatar
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EAvatar
    /// </summary>
    public EAvatar() : base(false)
    {

    }

    /// <summary>
    /// Create a new EAvatar
    /// </summary>
    /// <param name="reference">The Avatar you want to use</param>
    public EAvatar(Avatar reference) : base(false)
    {
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref Avatar reference)
    {
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override Avatar ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}
