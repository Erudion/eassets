﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a HumanBone
/// </summary>
[System.Serializable]
public class EHumanBone : SerializableUnityData<HumanBone>
{

    /// <summary>
    /// The boneName of this EHumanBone
    /// </summary>
    public string boneName;
    /// <summary>
    /// The humanName of this EHumanBone
    /// </summary>
    public string humanName;
    /// <summary>
    /// The limit of this EHumanBone
    /// </summary>
    public EHumanLimit limit;

    /// <summary>
    /// Create a new EHumanBone
    /// </summary>
    public EHumanBone() : base(false)
    {

    }

    /// <summary>
    /// Create a new EHumanBone
    /// </summary>
    /// <param name="reference">The HumanBone you want to use</param>
    public EHumanBone(HumanBone reference) : base(false)
    {
        this.boneName = reference.boneName;
        this.humanName = reference.humanName;
        this.limit = new EHumanLimit(reference.limit);
    }

    public override void SetData(ref HumanBone reference)
    {
        reference.boneName = this.boneName;
        reference.humanName = this.humanName;
        reference.limit = this.limit.ToUnityType();
    }

    public override HumanBone ToUnityType()
    {
        HumanBone bone = new HumanBone();
        SetData(ref bone);
        return bone;
    }
}
