﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;
using UnityEngine.Audio;

/// <summary>
/// This class can be used to serialize a AudioSource
/// </summary>
[System.Serializable]
public class EAudioSource : SerializableUnityData<AudioSource>
{
    /// <summary>
    /// The tag of this AudioSource
    /// </summary>
    public float volume;
    /// <summary>
    /// The pitch of this AudioSource
    /// </summary>
    public float pitch;
    /// <summary>
    /// The time of this AudioSource
    /// </summary>
    public float time;
    /// <summary>
    /// The timeSamples of this AudioSource
    /// </summary>
    public int timeSamples;
    /// <summary>
    /// The clip of this AudioSource
    /// </summary>
    public EAudioClip clip;
    /// <summary>
    /// The outputAudioMixerGroup of this AudioSource
    /// </summary>
    public EAudioMixerGroup outputAudioMixerGroup;
    /// <summary>
    /// The loop state of this AudioSource
    /// </summary>
    public bool loop;
    /// <summary>
    /// The ignoreListenerVolume state of this AudioSource
    /// </summary>
    public bool ignoreListenerVolume;
    /// <summary>
    /// The playOnAwake state of this AudioSource
    /// </summary>
    public bool playOnAwake;
    /// <summary>
    /// The ignoreListenerPause state of this AudioSource
    /// </summary>
    public bool ignoreListenerPause;
    /// <summary>
    /// The velocityUpdateMode of this AudioSource
    /// </summary>
    public int velocityUpdateMode;
    /// <summary>
    /// The panStereo of this AudioSource
    /// </summary>
    public float panStereo;
    /// <summary>
    /// The spatialBlend of this AudioSource
    /// </summary>
    public float spatialBlend;
    /// <summary>
    /// The spatialize state of this AudioSource
    /// </summary>
    public bool spatialize;
    /// <summary>
    /// The spatializePostEffects state of this AudioSource
    /// </summary>
    public bool spatializePostEffects;
    /// <summary>
    /// The reverbZoneMix of this AudioSource
    /// </summary>
    public float reverbZoneMix;
    /// <summary>
    /// The bypassEffects state of this AudioSource
    /// </summary>
    public bool bypassEffects;
    /// <summary>
    /// The bypassListenerEffects state of this AudioSource
    /// </summary>
    public bool bypassListenerEffects;
    /// <summary>
    /// The bypassReverbZones state of this AudioSource
    /// </summary>
    public bool bypassReverbZones;
    /// <summary>
    /// The dopplerLevel of this AudioSource
    /// </summary>
    public float dopplerLevel;
    /// <summary>
    /// The spread of this AudioSource
    /// </summary>
    public float spread;
    /// <summary>
    /// The priority of this AudioSource
    /// </summary>
    public int priority;
    /// <summary>
    /// The mute state of this AudioSource
    /// </summary>
    public bool mute;
    /// <summary>
    /// The minDistance of this AudioSource
    /// </summary>
    public float minDistance;
    /// <summary>
    /// The maxDistance of this AudioSource
    /// </summary>
    public float maxDistance;
    /// <summary>
    /// The rolloffMode of this AudioSource
    /// </summary>
    public int rolloffMode;
    /// <summary>
    /// The enabled state of this AudioSource
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The tag of this AudioSource
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this AudioSource
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this AudioSource
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EAudioSource
    /// </summary>
    public EAudioSource() : base(true)
    {

    }

    /// <summary>
    /// Create a new EAudioSource
    /// </summary>
    /// <param name="reference">The AudioSource you want to use</param>
    public EAudioSource(AudioSource reference) : base(true)
    {
        this.volume = reference.volume;
        this.pitch = reference.pitch;
        this.time = reference.time;
        this.timeSamples = reference.timeSamples;

        if (reference.clip != null)
        {
            this.clip = new EAudioClip(reference.clip);
        }

        if(reference.outputAudioMixerGroup != null)
        {
            this.outputAudioMixerGroup = new EAudioMixerGroup(reference.outputAudioMixerGroup);
        }

        this.loop = reference.loop;
        this.ignoreListenerVolume = reference.ignoreListenerVolume;
        this.playOnAwake = reference.playOnAwake;
        this.ignoreListenerPause = reference.ignoreListenerPause;
        this.velocityUpdateMode = (int)reference.velocityUpdateMode;
        this.panStereo = reference.panStereo;
        this.spatialBlend = reference.spatialBlend;
        this.spatialize = reference.spatialize;
        this.spatializePostEffects = reference.spatializePostEffects;
        this.reverbZoneMix = reference.reverbZoneMix;
        this.bypassEffects = reference.bypassEffects;
        this.bypassListenerEffects = reference.bypassListenerEffects;
        this.bypassReverbZones = reference.bypassReverbZones;
        this.dopplerLevel = reference.dopplerLevel;
        this.spread = reference.spread;
        this.priority = reference.priority;
        this.mute = reference.mute;
        this.minDistance = reference.minDistance;
        this.maxDistance = reference.maxDistance;
        this.rolloffMode = (int)reference.rolloffMode;
        this.enabled = reference.enabled;

        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref AudioSource reference)
    {
        reference.volume = this.volume;
        reference.pitch = this.pitch;
        reference.time = this.time;
        reference.timeSamples = this.timeSamples;

        if (reference.clip != null)
        {
            if (this.clip != null)
            {
                AudioClip clip = reference.clip;
                this.clip.SetData(ref clip);
            }
            else //if there is no clip lets create one
            {
                reference.clip = this.clip.ToUnityType();
            }
        }

        if (reference.outputAudioMixerGroup != null && this.outputAudioMixerGroup != null)
        {
            AudioMixerGroup group = reference.outputAudioMixerGroup;
            this.outputAudioMixerGroup.SetData(ref group);
        }

        reference.loop = this.loop;
        reference.ignoreListenerVolume = this.ignoreListenerVolume;
        reference.playOnAwake = this.playOnAwake;
        reference.ignoreListenerPause = this.ignoreListenerPause;
        reference.velocityUpdateMode = (AudioVelocityUpdateMode)this.velocityUpdateMode;
        reference.panStereo = this.panStereo;
        reference.spatialBlend = this.spatialBlend;
        reference.spatialize = this.spatialize;
        reference.spatializePostEffects = this.spatializePostEffects;
        reference.reverbZoneMix = this.reverbZoneMix;
        reference.bypassEffects = this.bypassEffects;
        reference.bypassListenerEffects = this.bypassListenerEffects;
        reference.bypassReverbZones = this.bypassReverbZones;
        reference.dopplerLevel = this.dopplerLevel;
        reference.spread = this.spread;
        reference.priority = this.priority;
        reference.mute = this.mute;
        reference.minDistance = this.minDistance;
        reference.maxDistance = this.maxDistance;
        reference.rolloffMode = (AudioRolloffMode)this.rolloffMode;
        reference.enabled = this.enabled;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override AudioSource ToUnityType()
    {
        AudioSource source = new AudioSource();
        SetData(ref source);
        return source;
    }
}
