﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;
using UnityEngine.Audio;

/// <summary>
/// This class can be used to serialize a AudioMixer
/// </summary>
[System.Serializable]
public class EAudioMixer : SerializableUnityData<AudioMixer>
{
    /// <summary>
    /// The outputAudioMixerGroup of this AudioMixer
    /// </summary>
    public EAudioMixerGroup outputAudioMixerGroup;
    /// <summary>
    /// The updateMode of this AudioMixer
    /// </summary>
    public int updateMode;
    /// <summary>
    /// The name of this AudioMixer
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this AudioMixer
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EAudioMixer
    /// </summary>
    public EAudioMixer() : base(false)
    {

    }

    /// <summary>
    /// Create a new EAudioMixer
    /// </summary>
    /// <param name="reference">The AudioMixer you want to use</param>
    public EAudioMixer(AudioMixer reference) : base(false)
    {
        if (reference.outputAudioMixerGroup != null)
        {
            this.outputAudioMixerGroup = new EAudioMixerGroup(reference.outputAudioMixerGroup);
        }

        this.updateMode = (int)reference.updateMode;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref AudioMixer reference)
    {
        if (this.outputAudioMixerGroup != null)
        {
            AudioMixerGroup group = reference.outputAudioMixerGroup;
            this.outputAudioMixerGroup.SetData(ref group);
        }

        reference.updateMode = (AudioMixerUpdateMode)this.updateMode;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override AudioMixer ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}
