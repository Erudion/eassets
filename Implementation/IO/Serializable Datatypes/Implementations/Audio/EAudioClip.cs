﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;
using EAssets.Extensions.Objects;

/// <summary>
/// This class can be used to serialize a AudioClip
/// </summary>
[System.Serializable]
public class EAudioClip : SerializableUnityData<AudioClip>
{
    /// <summary>
    /// The data of this AudioClip
    /// </summary>
    public float[] data;
    /// <summary>
    /// The channels of this AudioClip
    /// </summary>
    public int channels;
    /// <summary>
    /// The frequence of this AudioClip
    /// </summary>
    public int frequency;
    /// <summary>
    /// The name of this AudioClip
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this AudioClip
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EAudioClip
    /// </summary>
    public EAudioClip() : base(true)
    {

    }

    /// <summary>
    /// Create a new EAudioClip
    /// </summary>
    /// <param name="reference">The AudioClip you want to use</param>
    public EAudioClip(AudioClip reference) : base(true)
    {
        //Get the audio data
        data = new float[reference.samples];
        reference.GetData(data, 0);

        this.channels = reference.channels;
        this.frequency = reference.frequency;

        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref AudioClip reference)
    {
        //Set the audio data
        reference.SetData(this.data, 0);

        //Use reflection to set read only data
        reference.SetPropertyValue("channels", channels);
        reference.SetPropertyValue("frequency", frequency);

        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override AudioClip ToUnityType()
    {
        AudioClip clip = AudioClip.Create(this.name, this.data.Length, this.channels, this.frequency, false);
        return clip;
    }
}
