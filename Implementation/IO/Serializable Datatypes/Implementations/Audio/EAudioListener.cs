﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize an AudioListener
/// </summary>
[System.Serializable]
public class EAudioListener : SerializableUnityData<AudioListener>
{
    /// <summary>
    /// The velocityUpdateMode of this AudioListener
    /// </summary>
    public int velocityUpdateMode;
    /// <summary>
    /// The enabled state of this AudioListener
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The tag of this AudioListener
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this AudioListener
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this AudioListener
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EAudioListener
    /// </summary>
    public EAudioListener() : base(true)
    {

    }

    /// <summary>
    /// Create a new EAudioListener
    /// </summary>
    /// <param name="reference">The AudioListener you want to use</param>
    public EAudioListener(AudioListener reference) : base(true)
    {
        this.velocityUpdateMode = (int)reference.velocityUpdateMode;
        this.enabled = reference.enabled;
        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref AudioListener reference)
    {
        reference.velocityUpdateMode = (AudioVelocityUpdateMode)this.velocityUpdateMode;
        reference.enabled = this.enabled;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override AudioListener ToUnityType()
    {
        AudioListener listener = new AudioListener();
        SetData(ref listener);
        return listener;
    }
}
