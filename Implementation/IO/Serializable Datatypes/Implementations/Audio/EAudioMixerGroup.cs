﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;
using UnityEngine.Audio;

/// <summary>
/// This class can be used to serialize an AudioMixerGroup
/// </summary>
[System.Serializable]
public class EAudioMixerGroup : SerializableUnityData<AudioMixerGroup>
{
    //Dont store the audiomixer to not even get close to recursion

    /// <summary>
    /// The name of this AudioMixerGroup
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this AudioMixerGroup
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EAudioMixerGroup
    /// </summary>
    public EAudioMixerGroup() : base(false)
    {

    }

    /// <summary>
    /// Create a new EAudioMixerGroup
    /// </summary>
    /// <param name="reference">The AudioMixerGroup you want to use</param>
    public EAudioMixerGroup(AudioMixerGroup reference) : base(false)
    {
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref AudioMixerGroup reference)
    {
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override AudioMixerGroup ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}
