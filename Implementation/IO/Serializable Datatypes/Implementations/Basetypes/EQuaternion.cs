﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a quaternion
/// </summary>
[System.Serializable]
public class EQuaternion : SerializableUnityData<Quaternion>
{
    /// <summary>
    /// The x-value of this quaternion
    /// </summary>
    public float x;
    /// <summary>
    /// The y-value of this quaternion
    /// </summary>
    public float y;
    /// <summary>
    /// The z-value of this quaternion
    /// </summary>
    public float z;
    /// <summary>
    /// The w-value of this quaternion
    /// </summary>
    public float w;

    /// <summary>
    /// Create a new EQuaternion
    /// </summary>
    public EQuaternion() : base()
    {
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.w = 0;
    }

    /// <summary>
    /// Create a new EQuaternion
    /// </summary>
    /// <param name="x">The x value</param>
    /// <param name="y">The y value</param>
    /// <param name="z">The z value</param>
    /// <param name="w">The w value</param>
    public EQuaternion(float x = 0, float y = 0, float z = 0, float w = 0) : base()
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    /// <summary>
    /// Create a new EQuaternion
    /// </summary>
    /// <param name="quat">The Quaternion you want to copy</param>
    public EQuaternion(Quaternion quat) : base()
    {
        this.x = quat.x;
        this.y = quat.y;
        this.z = quat.z;
        this.w = quat.w;
    }

    /// <summary>
    /// Get an EQuaternion made by euler-angles
    /// </summary>
    /// <param name="vec3">The euler coordinates</param>
    /// <returns></returns>
    public static EQuaternion Euler(Vector3 vec3)
    {
        return new EQuaternion(Quaternion.Euler(vec3));
    }
    /// <summary>
    /// Get an EQuaternion made by euler-angles
    /// </summary>
    /// <param name="x">The x angle</param>
    /// <param name="y">The y angle</param>
    /// <param name="z">The z angle</param>
    /// <returns></returns>
    public static EQuaternion Euler(float x, float y, float z)
    {
        return new EQuaternion(Quaternion.Euler(x, y, z));
    }

    /// <summary>
    /// Returns a Unity-Quaternion
    /// </summary>
    /// <returns></returns>
    public override Quaternion ToUnityType()
    {
        return new Quaternion(x, y, z, w);
    }

    public static implicit operator Quaternion(EQuaternion q)
    {
        return new Quaternion(q.x, q.y, q.z, q.w);
    }
    public static implicit operator EQuaternion(Quaternion q)
    {
        return new EQuaternion(q.x, q.y, q.z, q.w);
    }


    public override string ToString()
    {
        return "(" + x + " " + y + " " + z + " " + w + ")";
    }

    public override void SetData(ref Quaternion reference)
    {
        reference.x = x;
        reference.y = y;
        reference.z = z;
        reference.w = w;
    }
}
