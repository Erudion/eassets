﻿using UnityEngine;
using EAssets.IO.SerializableDatatypes;

/// <summary>
/// This can be used to serialize a Matrix4x4
/// </summary>
[System.Serializable]
public class EMatrix4x4 : SerializableUnityData<Matrix4x4>
{
    /// <summary>
    /// The column 0 of this matrix
    /// </summary>
    public EVector4 column0;
    /// <summary>
    /// The column 1 of this matrix
    /// </summary>
    public EVector4 column1;
    /// <summary>
    /// The column 2 of this matrix
    /// </summary>
    public EVector4 column2;
    /// <summary>
    /// The column 3 of this matrix
    /// </summary>
    public EVector4 column3;

    /// <summary>
    /// Create a new Matrix4x4
    /// </summary>
    public EMatrix4x4() : base(true)
    {

    }

    /// <summary>
    /// Create a new Matrix4x4
    /// </summary>
    /// <param name="matrix">The matrix you want to use</param>
    public EMatrix4x4(Matrix4x4 matrix) : base(true)
    {
        column0 = new EVector4(matrix.GetColumn(0));
        column1 = new EVector4(matrix.GetColumn(1));
        column2 = new EVector4(matrix.GetColumn(2));
        column3 = new EVector4(matrix.GetColumn(3));
    }

    // Start is called before the first frame update
    public override void SetData(ref Matrix4x4 reference)
    {
        reference.SetColumn(0, column0.ToUnityType());
        reference.SetColumn(1, column1.ToUnityType());
        reference.SetColumn(2, column2.ToUnityType());
        reference.SetColumn(3, column3.ToUnityType());
    }

    public override Matrix4x4 ToUnityType()
    {
        if (column0 == null && column1 == null && column2 == null && column3 == null)
        {
            Debug.LogWarning("All values were null");
            return default;
        }

        return new Matrix4x4(column0.ToUnityType(),column1.ToUnityType(), column2.ToUnityType(), column3.ToUnityType());
    }
}
