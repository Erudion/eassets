﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a vector2
/// </summary>
[System.Serializable]
public class EVector2 : SerializableUnityData<Vector2>
{
    /// <summary>
    /// The x-value of this vector
    /// </summary>
    public float x;
    /// <summary>
    /// The y-value of this vector
    /// </summary>
    public float y;

    /// <summary>
    /// Create a new EVector2
    /// </summary>
    public EVector2() : base()
    {
        this.x = 0;
        this.y = 0;

    }

    /// <summary>
    /// Create a new EVector2
    /// </summary>
    /// <param name="x">The x value</param>
    /// <param name="y">The y value</param>
    public EVector2(float x = 0, float y = 0) : base()
    {
        this.x = x;
        this.y = y;
    }

    /// <summary>
    /// Create a new EVector2
    /// </summary>
    /// <param name="vec2">The vector you want to copy</param>
    public EVector2(Vector2 vec2) : base()
    {
        this.x = vec2.x;
        this.y = vec2.y;
    }

    /// <summary>
    /// Converts this vector to a unity Vector2
    /// </summary>
    /// <returns></returns>
    public override Vector2 ToUnityType()
    {
        return new Vector2(this.x, this.y);
    }

    public static implicit operator Vector2(EVector2 v2)
    {
        return new Vector2(v2.x, v2.y);
    }

    public static implicit operator EVector2(Vector2 v2)
    {
        return new EVector2(v2.x, v2.y);
    }


    public override string ToString()
    {
        return "(" + x + " " + y + ")";
    }

    public override void SetData(ref Vector2 reference)
    {
        reference.x = x;
        reference.y = y;
    }
}
