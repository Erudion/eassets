﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a vector3
/// </summary>
[System.Serializable]
public class EVector3 : SerializableUnityData<Vector3>
{
    /// <summary>
    /// The x-value of this vector
    /// </summary>
    public float x;
    /// <summary>
    /// The y-value of this vector
    /// </summary>
    public float y;
    /// <summary>
    /// The z-value of this vector
    /// </summary>
    public float z;

    /// <summary>
    /// Create a new EVector3
    /// </summary>
    public EVector3() : base()
    {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    /// <summary>
    /// Create a new EVector3
    /// </summary>
    /// <param name="x">The x value</param>
    /// <param name="y">The y value</param>
    /// <param name="z">The z value</param>
    public EVector3(float x = 0, float y = 0, float z = 0) : base()
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /// <summary>
    /// Create a new EVector3
    /// </summary>
    /// <param name="vec2">The vector you want to copy</param>
    public EVector3(Vector3 vec3) : base()
    {
        this.x = vec3.x;
        this.y = vec3.y;
        this.z = vec3.z;
    }

    /// <summary>
    /// Converts this vector to a unity Vector3
    /// </summary>
    /// <returns></returns>
    public override Vector3 ToUnityType()
    {
        return new Vector3(this.x, this.y, this.z);
    }

    /*public static implicit operator Vector3(EVector3 v3)
    {
        return new Vector3(v3.x, v3.y, v3.z);
    }*/
    public static implicit operator EVector3(Vector3 v3)
    {
        return new EVector3(v3.x, v3.y, v3.z);
    }
    public static implicit operator Vector3(EVector3 e3) => new Vector3(e3.x, e3.y, e3.z);

    public override string ToString()
    {
        return "(" + x + " " + y + " " + z + ")";
    }

    public override void SetData(ref Vector3 reference)
    {
        reference.x = x;
        reference.y = y;
        reference.z = z;
    }
}
