﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a Vector4
/// </summary>
[System.Serializable]
public class EVector4 : SerializableUnityData<Vector4>
{
    /// <summary>
    /// The x-value of this vector
    /// </summary>
    public float x;
    /// <summary>
    /// The y-value of this vector
    /// </summary>
    public float y;
    /// <summary>
    /// The z-value of this vector
    /// </summary>
    public float z;
    /// <summary>
    /// The w-value of this vector
    /// </summary>
    public float w;

    /// <summary>
    /// Create a new EVector4
    /// </summary>
    public EVector4() : base()
    {
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.w = 0;
    }

    /// <summary>
    /// Create a new EVector4
    /// </summary>
    /// <param name="x">The x value</param>
    /// <param name="y">The y value</param>
    /// <param name="z">The z value</param>
    /// <param name="w">The w value</param>
    public EVector4(float x = 0, float y = 0, float z = 0, float w = 0) : base()
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    /// <summary>
    /// Create a new EVector3
    /// </summary>
    /// <param name="vec2">The vector you want to copy</param>
    public EVector4(Vector4 vec4) : base()
    {
        this.x = vec4.x;
        this.y = vec4.y;
        this.z = vec4.z;
        this.w = vec4.w;
    }

    /// <summary>
    /// Converts this vector to a unity Vector3
    /// </summary>
    /// <returns></returns>
    public override Vector4 ToUnityType()
    {
        return new Vector4(this.x, this.y, this.z,this.w);
    }

    public static implicit operator EVector4(Vector4 vec4)
    {
        return new EVector4(vec4.x, vec4.y, vec4.z,vec4.w);
    }
    public static implicit operator Vector4(EVector4 evec4)
    {
        return new Vector4(evec4.x, evec4.y, evec4.z, evec4.w);
    }

    public override string ToString()
    {
        return "(" + x + " " + y + " " + z + " " + w + ")";
    }

    public override void SetData(ref Vector4 reference)
    {
        reference.x = x;
        reference.y = y;
        reference.z = z;
        reference.w = w;
    }
}
