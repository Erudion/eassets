﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a color
/// </summary>
[System.Serializable]
public class EColor : SerializableUnityData<Color>
{
    /// <summary>
    /// The red value
    /// </summary>
    public float r;
    /// <summary>
    /// The green value
    /// </summary>
    public float g;
    /// <summary>
    /// The blue value
    /// </summary>
    public float b;
    /// <summary>
    /// The alpha value
    /// </summary>
    public float a;

    /// <summary>
    /// Create a new EColor
    /// </summary>
    public EColor() : base()
    {
        this.r = 0;
        this.g = 0;
        this.b = 0;
        this.a = 255;
    }

    /// <summary>
    /// Create a new EColor
    /// </summary>
    /// <param name="r">The red value</param>
    /// <param name="g">The green value</param>
    /// <param name="b">The blue value</param>
    /// <param name="a">The alpha value</param>
    public EColor(float r = 0, float g = 0, float b = 0, float a = 255) : base()
    {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    /// <summary>
    /// Create a new EColor
    /// </summary>
    /// <param name="col">The color you want to use</param>
    public EColor(Color col) : base()
    {
        this.r = col.r;
        this.g = col.g;
        this.b = col.b;
        this.a = col.a;
    }

    /// <summary>
    /// Returns a Unity-Color
    /// </summary>
    /// <returns></returns>
    public override Color ToUnityType()
    {
        return new Color(r, g, b, a);
    }

    public override void SetData(ref Color reference)
    {
        reference.r = r;
        reference.g = g;
        reference.b = b;
        reference.a = a;
    }

    public static implicit operator Color(EColor ec)
    {
        return new Color(ec.r, ec.g, ec.b, ec.a);
    }
    public static implicit operator EColor(Color c)
    {
        return new EColor(c.r, c.g, c.b, c.a);
    }

    public override string ToString()
    {
        return "(" + r + ", " + g + ", " + b + ", " + a + ")";
    }
}
