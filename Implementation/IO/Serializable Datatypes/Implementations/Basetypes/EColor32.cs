﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a color
/// </summary>
[System.Serializable]
public class EColor32 : SerializableUnityData<Color32>
{
    /// <summary>
    /// The red value
    /// </summary>
    public byte r;
    /// <summary>
    /// The green value
    /// </summary>
    public byte g;
    /// <summary>
    /// The blue value
    /// </summary>
    public byte b;
    /// <summary>
    /// The alpha value
    /// </summary>
    public byte a;

    /// <summary>
    /// Create a new EColor
    /// </summary>
    public EColor32() : base(true)
    {
        this.r = 0;
        this.g = 0;
        this.b = 0;
        this.a = 255;
    }

    /// <summary>
    /// Create a new EColor
    /// </summary>
    /// <param name="r">The red value</param>
    /// <param name="g">The green value</param>
    /// <param name="b">The blue value</param>
    /// <param name="a">The alpha value</param>
    public EColor32(byte r = 0, byte g = 0, byte b = 0, byte a = 255) : base(true)
    {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    /// <summary>
    /// Create a new EColor
    /// </summary>
    /// <param name="col">The color32 you want to use</param>
    public EColor32(Color32 col) : base(true)
    {
        this.r = col.r;
        this.g = col.g;
        this.b = col.b;
        this.a = col.a;
    }

    /// <summary>
    /// Returns a Unity-Color
    /// </summary>
    /// <returns></returns>
    public override Color32 ToUnityType()
    {
        return new Color32(r, g, b, a);
    }

    public override void SetData(ref Color32 reference)
    {
        reference.r = r;
        reference.g = g;
        reference.b = b;
        reference.a = a;
    }

    public static implicit operator Color32(EColor32 ec)
    {
        return new Color32(ec.r, ec.g, ec.b, ec.a);
    }
    public static implicit operator EColor32(Color32 c)
    {
        return new EColor32(c.r, c.g, c.b, c.a);
    }

    public override string ToString()
    {
        return "(" + r + ", " + g + ", " + b + ", " + a + ")";
    }
}
