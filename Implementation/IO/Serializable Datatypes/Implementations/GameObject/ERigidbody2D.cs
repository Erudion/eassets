﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a Rigidbody2D
/// </summary>
[System.Serializable]
public class ERigidbody2D : SerializableUnityData<Rigidbody2D>
{
    /// <summary>
    /// The tag of this Rigidbody2D
    /// </summary>
    public EVector2 position;
    /// <summary>
    /// The rotation of this Rigidbody2D
    /// </summary>
    public float rotation;
    /// <summary>
    /// The velocity of this Rigidbody2D
    /// </summary>
    public EVector2 velocity;
    /// <summary>
    /// The angularVelocity of this Rigidbody2D
    /// </summary>
    public float angularVelocity;
    /// <summary>
    /// The useAutoMass state of this Rigidbody2D
    /// </summary>
    public bool useAutoMass;
    /// <summary>
    /// The mass of this Rigidbody2D
    /// </summary>
    public float mass;
    /// <summary>
    /// The sharedMaterial of this Rigidbody2D
    /// </summary>
    public EPhysicMaterial2D sharedMaterial;
    /// <summary>
    /// The centerOfMass of this Rigidbody2D
    /// </summary>
    public EVector2 centerOfMass;
    /// <summary>
    /// The inertia of this Rigidbody2D
    /// </summary>
    public float inertia;
    /// <summary>
    /// The drag of this Rigidbody2D
    /// </summary>
    public float drag;
    /// <summary>
    /// The angularDrag of this Rigidbody2D
    /// </summary>
    public float angularDrag;
    /// <summary>
    /// The gravityScale of this Rigidbody2D
    /// </summary>
    public float gravityScale;
    /// <summary>
    /// The bodyType of this Rigidbody2D
    /// </summary>
    public int bodyType;
    /// <summary>
    /// The useFullKinematicContacts state of this Rigidbody2D
    /// </summary>
    public bool useFullKinematicContacts;
    /// <summary>
    /// The isKinematic state of this Rigidbody2D
    /// </summary>
    public bool isKinematic;
    /// <summary>
    /// The freezeRotation state of this Rigidbody2D
    /// </summary>
    public bool freezeRotation;
    /// <summary>
    /// The constraints of this Rigidbody2D
    /// </summary>
    public int constraints;
    /// <summary>
    /// The simulated state of this Rigidbody2D
    /// </summary>
    public bool simulated;
    /// <summary>
    /// The interpolation of this Rigidbody2D
    /// </summary>
    public int interpolation;
    /// <summary>
    /// The sleepMode of this Rigidbody2D
    /// </summary>
    public int sleepMode;
    /// <summary>
    /// The collisionDetectionMode of this Rigidbody2D
    /// </summary>
    public int collisionDetectionMode;
    /// <summary>
    /// The tag of this Rigidbody2D
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this Rigidbody2D
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this Rigidbody2D
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ERigidbody2D
    /// </summary>
    public ERigidbody2D() : base(false)
    {

    }

    /// <summary>
    /// Create a new ERigidbody2D
    /// </summary>
    /// <param name="col">The Rigidbody2D you want to use</param>
    public ERigidbody2D(Rigidbody2D col) : base(false)
    {
        this.position = new EVector2(col.position);
        this.rotation = col.rotation;
        this.velocity = new EVector2(col.velocity);
        this.angularVelocity = col.angularVelocity;
        this.useAutoMass = col.useAutoMass;
        this.mass = col.mass;

        if(col.sharedMaterial != null)
            this.sharedMaterial = new EPhysicMaterial2D(col.sharedMaterial);

        this.centerOfMass = new EVector2(col.centerOfMass);
        this.inertia = col.inertia;
        this.drag = col.drag;
        this.angularDrag = col.angularDrag;
        this.gravityScale = col.gravityScale;
        this.bodyType = (int)col.bodyType;
        this.useFullKinematicContacts = col.useFullKinematicContacts;
        this.isKinematic = col.isKinematic;
        this.freezeRotation = col.freezeRotation;
        this.constraints = (int)col.constraints;
        this.simulated = col.simulated;
        this.interpolation = (int)col.interpolation;
        this.sleepMode = (int)col.sleepMode;
        this.collisionDetectionMode = (int)col.collisionDetectionMode;
        this.tag = col.tag;
        this.name = col.name;
        this.hideFlags = (int)col.hideFlags;
    }

    public override void SetData(ref Rigidbody2D reference)
    {
        reference.position = this.position;
        reference.rotation = this.rotation;
        reference.velocity = this.velocity;
        reference.angularVelocity = this.angularVelocity;
        reference.useAutoMass = this.useAutoMass;
        reference.mass = this.mass;

        if (this.sharedMaterial != null)
        {
            PhysicsMaterial2D mat = reference.sharedMaterial;
            this.sharedMaterial.SetData(ref mat);
        }

        reference.centerOfMass = this.centerOfMass;
        reference.inertia = this.inertia;
        reference.drag = this.drag;
        reference.angularDrag = this.angularDrag;
        reference.gravityScale = this.gravityScale;
        reference.bodyType = (RigidbodyType2D)this.bodyType;
        reference.useFullKinematicContacts = this.useFullKinematicContacts;
        reference.isKinematic = this.isKinematic;
        reference.freezeRotation = this.freezeRotation;
        reference.constraints = (RigidbodyConstraints2D)this.constraints;
        reference.simulated = this.simulated;
        reference.interpolation = (RigidbodyInterpolation2D)this.interpolation;
        reference.sleepMode = (RigidbodySleepMode2D)this.sleepMode;
        reference.collisionDetectionMode = (CollisionDetectionMode2D)this.collisionDetectionMode;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override Rigidbody2D ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}
