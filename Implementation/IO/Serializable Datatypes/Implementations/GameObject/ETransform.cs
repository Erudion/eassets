﻿using System;
using UnityEngine;
using EAssets.IO.SerializableDatatypes;

/// <summary>
/// This class can be used to serialize a Transform
/// </summary>
[Serializable]
public class ETransform : SerializableUnityData<Transform>
{
    /// <summary>
    /// The position of this transform
    /// </summary>
    public EVector3 position;
    /// <summary>
    /// The localPosition of this transform
    /// </summary>
    public EVector3 localPosition;
    /// <summary>
    /// The rotation of this transform
    /// </summary>
    public EQuaternion rotation;
    /// <summary>
    /// The localRotation of this transform
    /// </summary>
    public EQuaternion localRotation;
    /// <summary>
    /// The scale of this transform
    /// </summary>
    public EVector3 localScale;
    /// <summary>
    /// The parent of this transform
    /// </summary>
    public ETransform parent;
    /// <summary>
    /// The tag of this transform
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this transform
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this transform
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ETransform
    /// </summary>
    public ETransform() : base(false)
    {

    }

    /// <summary>
    /// Create a new ETransform
    /// </summary>
    /// <param name="trans">The Transform you want to use</param>
    public ETransform(Transform trans) : base(false)
    {
        this.position = new EVector3(trans.position);
        this.localPosition = new EVector3(trans.localPosition);
        this.rotation = new EQuaternion(trans.rotation);
        this.localRotation = new EQuaternion(trans.localRotation);
        this.localScale = trans.localScale;

        if (trans.parent != null)
        {
            this.parent = new ETransform(trans.parent);
        }

        this.tag = trans.tag;
        this.name = trans.name;
        this.hideFlags = (int)trans.hideFlags;
    }

    public override Transform ToUnityType()
    {
        throw new NotImplementedException("Can not create a new Transform");
    }

    public override void SetData(ref Transform reference)
    {
        reference.position = this.position;
        reference.localPosition = this.localPosition;
        reference.rotation = this.rotation;
        reference.localRotation = this.localRotation;
        reference.localScale = this.localScale;

        if (this.parent != null && reference.parent != null)
        {
            Transform trans = reference.parent;
            this.parent.SetData(ref trans);
        }

        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override string ToString()
    {
        return "(" + position + "," + rotation + "," + localScale + ")";
    }
}
