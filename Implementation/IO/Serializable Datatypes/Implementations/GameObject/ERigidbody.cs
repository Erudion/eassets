﻿using System;
using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a Rigidbody
/// </summary>
[System.Serializable]
public class ERigidbody : SerializableUnityData<Rigidbody>
{
    /// <summary>
    /// The mass of this rigidbody
    /// </summary>
    public float mass;
    /// <summary>
    /// The drag of this rigidbody
    /// </summary>
    public float drag;
    /// <summary>
    /// The angular Drag of this rigidbody
    /// </summary>
    public float angularDrag;
    /// <summary>
    /// If true gravity is used
    /// </summary>
    public bool useGravity;
    /// <summary>
    /// If true physics wont affect this rigidbody
    /// </summary>
    public bool isKinematic;
    /// <summary>
    /// The mode of interpolation
    /// </summary>
    public int interpolate;
    /// <summary>
    /// The collision detection mode
    /// </summary>
    public int collisionDetectionMode;
    /// <summary>
    /// The rotation and translation constraints
    /// </summary>
    public int constraints;


    /// <summary>
    /// Create a new ERigidbody
    /// </summary>
    public ERigidbody()
    {

    }

    /// <summary>
    /// Create a new EGameObject
    /// </summary>
    /// <param name="rigid">The Rigidbody you want to use</param>
    public ERigidbody(Rigidbody rigid) : base(false)
    {
        this.mass = rigid.mass;
        this.drag = rigid.drag;
        this.angularDrag = rigid.angularDrag;
        this.useGravity = rigid.useGravity;
        this.isKinematic = rigid.isKinematic;
        this.interpolate = (int)rigid.interpolation;
        this.collisionDetectionMode = (int)rigid.collisionDetectionMode;
        this.constraints = (int)rigid.constraints;
    }


    public override Rigidbody ToUnityType()
    {
        throw new NotImplementedException("Should not create a new GameObject");
    }

    public override void SetData(ref Rigidbody reference)
    {
        reference.mass = this.mass;
        reference.drag = this.drag;
        reference.angularDrag = this.angularDrag;
        reference.useGravity = this.useGravity;
        reference.isKinematic = this.isKinematic;
        reference.interpolation = (RigidbodyInterpolation)this.interpolate;
        reference.collisionDetectionMode = (CollisionDetectionMode)this.collisionDetectionMode;
        reference.constraints = (RigidbodyConstraints)this.constraints;
    }
}