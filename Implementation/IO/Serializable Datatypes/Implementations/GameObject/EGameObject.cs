﻿using System;
using EAssets.IO.SerializableDatatypes;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using EAssets.Extensions.Objects;
using EAssets.Extensions.Types;

/// <summary>
/// This class can be used to serialize a GameObject
/// </summary>
[Serializable]
public class EGameObject : SerializableUnityData<GameObject>
{
    #region Component Addition
    private static List<Type> myUnityTypes = new List<Type>(), unityTypes = new List<Type>();
    private static Dictionary<Type, Type> UnityToCustomTypes,
        CustomToUnityTypes;
    #endregion

    //The transform is not part of the saved data since it is stored as a component anyways
    #region Attributes
    /// <summary>
    /// True if the EGameObject is active, else false
    /// </summary>
    public bool activeSelf;
    /// <summary>
    /// True if this EGameObject is static, else false
    /// </summary>
    public bool isStatic;
    /// <summary>
    /// The layer of this EGameObject
    /// </summary>
    public int layer;
    /// <summary>
    /// The tag of this EGameObject
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this EGameObject
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this EGameObject
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Contains the types of all components of the gameobject
    /// </summary>
    public List<Type> compList;
    /// <summary>
    /// Contains the data of all components of the gameobject
    /// </summary>
    public Dictionary<Type, object> compData;
    #endregion

    #region MonoBehaviour Saving
    /// <summary>
    /// These fields/properties are part of the UnityEngine.Object or related classes
    /// These classes are unity intern types and should not get stored or loaded
    /// </summary>
    string[] nonAllowedAttributes = new string[] { "transform", "gameObject", "rigidbody", "rigidbody2D",
                            "camera","light","animation","constantForce","renderer","audio","guiText","networkView",
                            "guiElement","guiTexture","collider","collider2D","hingeJoint","particleSystem",
                            "name","hideFlags","useGUILayout","runInEditMode","tag","isActiveAndEnabled"};
    /// <summary>
    /// Contains all data stored from a monobehaviour
    /// </summary>
    public Dictionary<Type, object> monoData = new Dictionary<Type, object>();
    #endregion

    #region Constructor
    /// <summary>
    /// Create a new EGameObject
    /// </summary>
    public EGameObject() : base(false)
    {

    }

    /// <summary>
    /// Create a new EGameObject
    /// </summary>
    /// <param name="go">The Gameobject you want to use</param>
    public EGameObject(GameObject go) : base(false)
    {
        #region Set Base Data
        //Set all basic data of a gameobject
        this.activeSelf = go.activeSelf;
        this.isStatic = go.isStatic;
        this.layer = go.layer;
        this.tag = go.tag;
        this.name = go.name;
        this.hideFlags = (int)go.hideFlags;
        #endregion

        #region Get Types
        //Get all supported types (types that can be stored)
        if (myUnityTypes == null || myUnityTypes.Count == 0)  //Setup the type information (only once!)
        {
            //Get all classes that inherit from ParentSerializableClass
            myUnityTypes = ReflectiveEnumerator.GetSubClassTypes<ParentSerializableClass>();

            if (unityTypes == null || unityTypes.Count == 0) //If not setup, setup the unityTypes
            {
                unityTypes = new List<Type>();
                foreach (Type t in myUnityTypes) //Get all implemented wrapper types
                {
                    unityTypes.Add(t.BaseType.GetGenericArguments()[0]);
                }
            }
        }
        //Set the type maps
        if (UnityToCustomTypes == null || CustomToUnityTypes == null)
        {
            UnityToCustomTypes = new Dictionary<Type, Type>();
            CustomToUnityTypes = new Dictionary<Type, Type>();
            for (int i = 0; i < myUnityTypes.Count; i++)
            {
                UnityToCustomTypes.Add(unityTypes[i], myUnityTypes[i]);
                CustomToUnityTypes.Add(myUnityTypes[i], unityTypes[i]);
            }
        }
        #endregion

        #region GetComponentData
        //Get component list
        compList = new List<Type>(); //This list contains the types of the components
        compData = new Dictionary<Type, object>(); //Contains the data of the components
        //Get all components and store everyone that can be stored
        //The types will always be storeable but the data maybe not
        foreach (Component comp in go.GetComponents<Component>())
        {
            #region Component Data
            compList.Add(comp.GetType()); //Add the type of the component
            if (unityTypes.Contains(comp.GetType())) //If this is a supported type
            {
                //Create a new instance of the wrapper type (give it the input data)
                object o = Activator.CreateInstance(UnityToCustomTypes[comp.GetType()], new object[] { comp });
                //Store the wrapper type
                compData.Add(comp.GetType(), o);
            }
            #endregion
            #region MonoData
            //Called when the component inherited from MonoBehaviour
            else if (comp.GetType().GetBaseClasses().Contains(typeof(MonoBehaviour)))
            {
                //A list for data of the monobehaviour
                Dictionary<string, object> data = new Dictionary<string, object>();
                foreach (string str in comp.GetAllWritableAttributes()) //Get all writable attributes
                {
                    //Dont save all Object related data
                    if (nonAllowedAttributes.Contains(str))
                        continue;

                    //Check if thie component is a property
                    if (comp.GetPropertyValue(str) != null)
                    {
                        if (unityTypes.Contains(comp.GetPropertyValue(str).GetType()))
                        {
                            //Check if the property is a unity type and serializable
                            //If so, create a new instance and store it
                            object o = Activator.CreateInstance(UnityToCustomTypes[comp.GetPropertyValue(str).GetType()], comp.GetPropertyValue(str));
                            data.Add(str, o);
                        }
                        else
                        {
                            if (!comp.GetPropertyValue(str).IsSimple()) //Ignore other non-simple types
                                continue;

                            data.Add(str, comp.GetPropertyValue(str));
                        }
                    }

                    //Check if this component is a field
                    if (comp.GetFieldValue(str) != null)
                    {
                        if (unityTypes.Contains(comp.GetFieldValue(str).GetType()))
                        {
                            //Check if the field is a unity type and serializable
                            //If so, create a new instance and store it
                            object o = Activator.CreateInstance(UnityToCustomTypes[comp.GetFieldValue(str).GetType()], comp.GetFieldValue(str));
                            data.Add(str, o);
                        }
                        else
                        {
                            if (!comp.GetFieldValue(str).IsSimple()) //Ignore other non-simple types
                                continue;

                            data.Add(str, comp.GetFieldValue(str));
                        }
                    }
                }
                //Store the new monobehaviour data
                monoData.Add(comp.GetType(), data);
            }
            #endregion
        }
        #endregion
    }
    #endregion

    /// <summary>
    /// This method can be used to clone a stored gameobject
    /// It can/should not be used (until now) in the savefile
    /// </summary>
    /// <returns></returns>
    public override GameObject ToUnityType()
    {
        GameObject go = new GameObject();
        SetData(ref go);
        return go;
    }

    public override void SetData(ref GameObject reference)
    {
        #region Set Basic Field
        reference.SetActive(this.activeSelf);
        reference.isStatic = this.isStatic;
        reference.layer = this.layer;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
        #endregion

        #region Set Component Data
        foreach (Type t in compList)
        {
            if (reference.GetComponent(t) == null)
            {
                reference.AddComponent(t); //Add non existent components
            }

            #region Set Serializable Data
            if (!t.GetBaseClasses().Contains(typeof(MonoBehaviour))) //Get all components
            {
                //Look if there is data for the component
                if (compData.TryGetValue(t, out object o))
                {
                    //Get the component from the Gameobject
                    Component comp = reference.GetComponent(t);
                    //Set the data
                    o.Invoke("SetData", new object[] { comp });

                    //var dataType = new Type[] { typeof(string) };
                    //Type typedGeneric = typeof(SerializableUnityData<>).MakeGenericType(t);
                    //dynamic dynamic = Activator.CreateInstance(typedGeneric, new object[] { o });
                    //dynamic.SetData("SetData",comp);
                }
            }
            #endregion
            #region Set MonoBehaviour Data
            else if (t.GetBaseClasses().Contains(typeof(MonoBehaviour)))
            {
                //This gets called if the type corresponds to a MonoBehaviour

                if (monoData.TryGetValue(t, out object o))
                {
                    //Get the stored dictionary
                    Dictionary<string, object> dic = o as Dictionary<string, object>;
                    //Get the component from the Gameobject
                    Component monoReference = reference.GetComponent(t);

                    //Set the monobehaviour attributes
                    foreach (KeyValuePair<string, object> pair in dic)
                    {
                        //Check if the key correlates to a property
                        if (monoReference.GetPropertyValue(pair.Key) != null)
                        {
                            //Found a property with the name pair.Key
                            if (unityTypes.Contains(monoReference.GetPropertyValue(pair.Key).GetType()))
                            {
                                //If the found property is a unity type, cast the data
                                if (pair.Value is ParentSerializableClass psc)
                                {
                                    if (psc.IsDirectlyLoadable) //Check if the data is directly loadable
                                        monoReference.SetPropertyValue(pair.Key, pair.Value.Invoke("ToUnityType", null));
                                }
                            }
                            else
                            {
                                //If the found property is no unity type we can simply set it
                                monoReference.SetPropertyValue(pair.Key, pair.Value);
                            }
                        }
                        //Check if the key correlates to a field
                        else if (monoReference.GetFieldValue(pair.Key) != null)
                        {
                            if (unityTypes.Contains(monoReference.GetFieldValue(pair.Key).GetType()))
                            {
                                //If the found field is a unity type, cast the data
                                if (pair.Value is ParentSerializableClass psc)
                                {
                                    if(psc.IsDirectlyLoadable) //Check if the data is directly loadable
                                        monoReference.SetFieldValue(pair.Key, pair.Value.Invoke("ToUnityType", null));
                                }
                            }
                            else
                            {
                                //If the found field is no unity type we can simply set it
                                monoReference.SetFieldValue(pair.Key, pair.Value);
                            }
                        }
                    }
                }
            }
            #endregion
        }
        #endregion
    }
}
