﻿using UnityEngine;
using UnityEngine.Rendering;
using EAssets.IO.SerializableDatatypes;

/// <summary>
/// This can be used to serialize a RenderTexture
/// </summary>
[System.Serializable]
public class ERenderTexture : SerializableUnityData<RenderTexture>
{
    /// <summary>
    /// The width of this rendertexture
    /// </summary>
    public int width;
    /// <summary>
    /// The height of this rendertexture
    /// </summary>
    public int height;
    /// <summary>
    /// The depth of this rendertexture
    /// </summary>
    public int depth;
    /// <summary>
    /// The isPowerOfTwo state of this rendertexture
    /// </summary>
    public bool isPowerOfTwo;
    /// <summary>
    /// The format of this rendertexture
    /// </summary>
    public int format;
    /// <summary>
    /// The useMipMap state of this rendertexture
    /// </summary>
    public bool useMipMap;
    /// <summary>
    /// The autoGenerateMips state of this rendertexture
    /// </summary>
    public bool autoGenerateMips;
    /// <summary>
    /// The dimension of this rendertexture
    /// </summary>
    public int dimension;
    /// <summary>
    /// The volumeDepth of this rendertexture
    /// </summary>
    public int volumeDepth;
    /// <summary>
    /// The memorylessMode of this rendertexture
    /// </summary>
    public int memorylessMode;
    /// <summary>
    /// The antiAliasing of this rendertexture
    /// </summary>
    public int antiAliasing;
    /// <summary>
    /// The enableRandomWrite of this rendertexture
    /// </summary>
    public bool enableRandomWrite;
    /// <summary>
    /// The descriptor of this rendertexture
    /// </summary>
    public ERenderTextureDescriptor descriptor;
    /// <summary>
    /// The filterMode of this rendertexture
    /// </summary>
    public int filterMode;
    /// <summary>
    /// The anisoLevel of this rendertexture
    /// </summary>
    public int anisoLevel;
    /// <summary>
    /// The wrapMode of this rendertexture
    /// </summary>
    public int wrapMode;
    /// <summary>
    /// The wrapModeU of this rendertexture
    /// </summary>
    public int wrapModeU;
    /// <summary>
    /// The wrapModeV of this rendertexture
    /// </summary>
    public int wrapModeV;
    /// <summary>
    /// The wrapModeW of this rendertexture
    /// </summary>
    public int wrapModeW;
    /// <summary>
    /// The mipMapBias of this rendertexture
    /// </summary>
    public float mipMapBias;
    /// <summary>
    /// The name of this rendertexture
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this rendertexture
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ERenderTexture
    /// </summary>
    public ERenderTexture() : base(false)
    {

    }

    /// <summary>
    /// Create a new ERenderTexture
    /// </summary>
    /// <param name="tex">The rendertexture you want to use</param>
    public ERenderTexture(RenderTexture tex) : base(false)
    {
        this.width = tex.width;
        this.height = tex.height;
        this.depth = tex.depth;
        this.isPowerOfTwo = tex.isPowerOfTwo;
        this.format = (int)tex.format;
        this.useMipMap = tex.useMipMap;
        this.autoGenerateMips = tex.autoGenerateMips;
        this.dimension = (int)tex.dimension;
        this.volumeDepth = tex.volumeDepth;
        this.memorylessMode = (int)tex.memorylessMode;
        this.antiAliasing = tex.antiAliasing;
        this.enableRandomWrite = tex.enableRandomWrite;
        this.descriptor = new ERenderTextureDescriptor(tex.descriptor);
        this.filterMode = (int)tex.filterMode;
        this.anisoLevel = tex.anisoLevel;
        this.wrapMode = (int)tex.wrapMode;
        this.wrapModeU = (int)tex.wrapModeU;
        this.wrapModeV = (int)tex.wrapModeV;
        this.wrapModeW = (int)tex.wrapModeW;
        this.mipMapBias = tex.mipMapBias;
        this.name = tex.name;
        this.hideFlags = (int)tex.hideFlags;
    }

    public override void SetData(ref RenderTexture reference)
    {
        reference.width = this.width;
        reference.height = this.height;
        reference.depth = this.depth;
        reference.isPowerOfTwo = this.isPowerOfTwo;
        reference.format = (RenderTextureFormat)this.format;
        reference.useMipMap = this.useMipMap;
        reference.autoGenerateMips = this.autoGenerateMips;
        reference.dimension = (TextureDimension)this.dimension;
        reference.volumeDepth = this.volumeDepth;
        reference.memorylessMode = (RenderTextureMemoryless)this.memorylessMode;
        reference.antiAliasing = this.antiAliasing;
        reference.enableRandomWrite = this.enableRandomWrite;

        if (this.descriptor != null)
        {
            RenderTextureDescriptor desc = reference.descriptor;
            this.descriptor.SetData(ref desc);
            reference.descriptor = desc;
        }

        reference.filterMode = (FilterMode)this.filterMode;
        reference.anisoLevel = this.anisoLevel;
        reference.wrapMode = (TextureWrapMode)this.wrapMode;
        reference.wrapModeU = (TextureWrapMode)this.wrapModeU;
        reference.wrapModeV = (TextureWrapMode)this.wrapModeV;
        reference.wrapModeW = (TextureWrapMode)this.wrapModeW;
        reference.mipMapBias = this.mipMapBias;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override RenderTexture ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}
