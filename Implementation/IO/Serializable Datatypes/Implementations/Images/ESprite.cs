﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a Sprite
/// </summary>
[System.Serializable]
public class ESprite : SerializableUnityData<Sprite>
{
    /// <summary>
    /// The texture2D of this sprite
    /// </summary>
    public ETexture2D texture;
    /// <summary>
    /// The rect of this sprite
    /// </summary>
    public ERect rect;
    /// <summary>
    /// The pivot of this sprite
    /// </summary>
    public EVector2 pivot;
    /// <summary>
    /// The pixelsPerUnit of this sprite
    /// </summary>
    public float pixelsPerUnit;
    /// <summary>
    /// The name of this Sprite
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this Sprite
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ESprite
    /// </summary>
    public ESprite() : base(false)
    {

    }

    /// <summary>
    /// Create a new ESprite
    /// </summary>
    /// <param name="sprite">The Sprite you want to use</param>
    public ESprite(Sprite sprite) : base(false)
    {
        if(sprite.texture != null)
            this.texture = new ETexture2D(sprite.texture);

        this.rect = new ERect(sprite.rect);
        this.pivot = new EVector2(sprite.pivot);
        this.pixelsPerUnit = sprite.pixelsPerUnit;
        this.name = sprite.name;
        this.hideFlags = (int)sprite.hideFlags;
    }

    public override void SetData(ref Sprite reference)
    {
        reference = ToUnityType();
    }

    public override Sprite ToUnityType()
    {
        Sprite sprite = Sprite.Create(this.texture.ToUnityType(),this.rect.ToUnityType(),
                                      this.pivot.ToUnityType(),this.pixelsPerUnit);
        sprite.name = this.name;
        sprite.hideFlags = (HideFlags)this.hideFlags;
        return sprite;
    }
}
