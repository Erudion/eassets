﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class can be used to serialize a Mask
/// </summary>
[System.Serializable]
public class EMask : SerializableUnityData<Mask>
{
    /// <summary>
    /// The showMaskGraphic state of this Mask
    /// </summary>
    public bool showMaskGraphic;
    /// <summary>
    /// The useGUILayout state of this Mask
    /// </summary>
    public bool useGUILayout;
    /// <summary>
    /// The enabled state of this Mask
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The tag of this Mask
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this Mask
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this Mask
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EMask
    /// </summary>
    public EMask() : base(false)
    {

    }

    /// <summary>
    /// Create a new EMask
    /// </summary>
    /// <param name="reference">The Mask you want to use</param>
    public EMask(Mask reference) : base(false)
    {
        this.showMaskGraphic = reference.showMaskGraphic;
        this.useGUILayout = reference.useGUILayout;
        this.enabled = reference.enabled;
        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref Mask reference)
    {
        reference.showMaskGraphic = this.showMaskGraphic;
        reference.useGUILayout = this.useGUILayout;
        reference.enabled = this.enabled;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override Mask ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}
