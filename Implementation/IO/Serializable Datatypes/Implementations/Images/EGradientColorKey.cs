﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a GradientColorKey
/// </summary>
[System.Serializable]
public class EGradientColorKey : SerializableUnityData<GradientColorKey>
{
    /// <summary>
    /// The color of this EGradientColorKey
    /// </summary>
    public EColor color;
    /// <summary>
    /// The time of this EGradientColorKey
    /// </summary>
    public float time;

    /// <summary>
    /// Create a new EGradientColorKey
    /// </summary>
    public EGradientColorKey() : base(true)
    {

    }

    /// <summary>
    /// Create a new EGradientColorKey
    /// </summary>
    /// <param name="reference">The GradientColorKey you want to use</param>
    public EGradientColorKey(GradientColorKey reference) : base(true)
    {
        this.color = new EColor(reference.color);
        this.time = reference.time;
    }

    public override void SetData(ref GradientColorKey reference)
    {
        reference.color = this.color.ToUnityType();
        reference.time = this.time;
    }

    public override GradientColorKey ToUnityType()
    {
        GradientColorKey key = new GradientColorKey();
        SetData(ref key);
        return key;
    }
}
