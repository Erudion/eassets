﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a Gradient
/// </summary>
[System.Serializable]
public class EGradient : SerializableUnityData<Gradient>
{
    /// <summary>
    /// The colorKeys for this EGradient
    /// </summary>
    public EGradientColorKey[] colorKeys;
    /// <summary>
    /// The alphaKeys for this EGradient
    /// </summary>
    public EGradientAlphaKey[] alphaKeys;
    /// <summary>
    /// The mode for this EGradient
    /// </summary>
    public int mode;

    /// <summary>
    /// Create a new E---name---
    /// </summary>
    public EGradient() : base(true)
    {

    }

    /// <summary>
    /// Create a new EGradient
    /// </summary>
    /// <param name="reference">The Gradient you want to use</param>
    public EGradient(Gradient reference) : base(true)
    {
        this.colorKeys = new EGradientColorKey[reference.colorKeys.Length];
        for(int i = 0; i < reference.colorKeys.Length; i++)
        {
            this.colorKeys[i] = new EGradientColorKey(reference.colorKeys[i]);
        }

        this.alphaKeys = new EGradientAlphaKey[reference.alphaKeys.Length];
        for (int i = 0; i < reference.alphaKeys.Length; i++)
        {
            this.alphaKeys[i] = new EGradientAlphaKey(reference.alphaKeys[i]);
        }

        this.mode = (int)reference.mode;
    }

    public override void SetData(ref Gradient reference)
    {
        GradientColorKey colorKey;
        reference.colorKeys = new GradientColorKey[reference.colorKeys.Length];
        for (int i = 0; i < this.colorKeys.Length; i++)
        {
            colorKey = new GradientColorKey(); //Create new key
            this.colorKeys[i].SetData(ref colorKey); //Set new key
            reference.colorKeys[i] = colorKey; //Store new key
        }

        GradientAlphaKey alphakey;
        reference.alphaKeys = new GradientAlphaKey[reference.alphaKeys.Length];
        for (int i = 0; i < this.alphaKeys.Length; i++)
        {
            alphakey = new GradientAlphaKey(); //Create new key
            this.alphaKeys[i].SetData(ref alphakey); //Set new key
            reference.alphaKeys[i] = alphakey; //Store new key
        }

        reference.mode = (GradientMode)this.mode;
    }

    public override Gradient ToUnityType()
    {
        Gradient gradient = new Gradient();
        SetData(ref gradient);
        return gradient;
    }
}
