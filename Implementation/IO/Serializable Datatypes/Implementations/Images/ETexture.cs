﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;
using UnityEngine.Rendering;


/// <summary>
/// This class can be used to serialize a texture
/// </summary>
[System.Serializable]
public class ETexture : SerializableUnityData<Texture>
{
    /// <summary>
    /// The width of this texture
    /// </summary>
    public int width;
    /// <summary>
    /// The height of this texture
    /// </summary>
    public int height;
    /// <summary>
    /// The dimension of this texture
    /// </summary>
    public int dimension;
    /// <summary>
    /// The filtermode of this texture
    /// </summary>
    public int filterMode;
    /// <summary>
    /// The anisoLevel of this texture
    /// </summary>
    public int anisoLevel;
    /// <summary>
    /// The Wrapmode of this texture
    /// </summary>
    public int wrapMode;

    /// <summary>
    /// The WrapmodeU of this texture
    /// </summary>
    public int wrapModeU;
    /// <summary>
    /// The WrapmodeV of this texture
    /// </summary>
    public int wrapModeV;
    /// <summary>
    /// The WrapmodeW of this texture
    /// </summary>
    public int wrapModeW;

    /// <summary>
    /// The mipmapbias of this texture
    /// </summary>
    public float mipMapBias;
    /// <summary>
    /// The name of this texture
    /// </summary>
    public string name;
    /// <summary>
    /// The hideflags of this texture
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ETexture
    /// </summary>
    public ETexture() : base(false)
    {

    }

    /// <summary>
    /// Create a new ETexture
    /// </summary>
    /// <param name="tex">The texture you want to use</param>
    public ETexture(Texture tex) : base(false)
    {
        this.width = tex.width;
        this.height = tex.height;
        this.dimension = (int)tex.dimension;
        this.filterMode = (int)tex.filterMode;
        this.anisoLevel = tex.anisoLevel;
        this.wrapMode = (int)tex.wrapMode;
        this.wrapModeU = (int)tex.wrapModeU;
        this.wrapModeV = (int)tex.wrapModeV;
        this.wrapModeW = (int)tex.wrapModeW;
        this.mipMapBias = tex.mipMapBias;
        this.name = tex.name;
        this.hideFlags = (int)tex.hideFlags;
    }

    public override void SetData(ref Texture reference)
    {
        reference.width = this.width;
        reference.height = this.height;
        reference.dimension = (TextureDimension)this.dimension;
        reference.filterMode = (FilterMode)this.filterMode;
        reference.anisoLevel = this.anisoLevel;
        reference.wrapMode = (TextureWrapMode)this.wrapMode;
        reference.wrapModeU = (TextureWrapMode)this.wrapModeU;
        reference.wrapModeV = (TextureWrapMode)this.wrapModeV;
        reference.wrapModeW = (TextureWrapMode)this.wrapModeW;
        reference.mipMapBias = this.mipMapBias;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override Texture ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}
