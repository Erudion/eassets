﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a GradientAlphaKey
/// </summary>
[System.Serializable]
public class EGradientAlphaKey : SerializableUnityData<GradientAlphaKey>
{
    /// <summary>
    /// The alpha of this GradientAlphaKey
    /// </summary>
    public float alpha;
    /// <summary>
    /// The time of this GradientAlphaKey
    /// </summary>
    public float time;

    /// <summary>
    /// Create a new EGradientAlphaKey
    /// </summary>
    public EGradientAlphaKey() : base(false)
    {

    }

    /// <summary>
    /// Create a new EGradientAlphaKey
    /// </summary>
    /// <param name="reference">The GradientAlphaKey you want to use</param>
    public EGradientAlphaKey(GradientAlphaKey reference) : base(false)
    {
        this.alpha = reference.alpha;
        this.time = reference.time;
    }

    public override void SetData(ref GradientAlphaKey reference)
    {
        reference.alpha = this.alpha;
        reference.time = this.time;
    }

    public override GradientAlphaKey ToUnityType()
    {
        GradientAlphaKey key = new GradientAlphaKey();
        SetData(ref key);
        return key;
    }
}
