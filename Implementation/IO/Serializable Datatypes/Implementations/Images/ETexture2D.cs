﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// This class can be used to serialize a Texture2D
/// </summary>
[System.Serializable]
public class ETexture2D : SerializableUnityData<Texture2D>
{
    /// <summary>
    /// The width of this Texture2D
    /// </summary>
    public int width;
    /// <summary>
    /// The height of this Texture2D
    /// </summary>
    public int height;
    /// <summary>
    /// The rawTextureData of this Texture2D
    /// </summary>
    public byte[] rawTextureData;
    /// <summary>
    /// The dimension of this Texture2D
    /// </summary>
    public int dimension;
    /// <summary>
    /// The filterMode of this Texture2D
    /// </summary>
    public int filterMode;
    /// <summary>
    /// The anisoLevel of this Texture2D
    /// </summary>
    public int anisoLevel;
    /// <summary>
    /// The wrapMode of this Texture2D
    /// </summary>
    public int wrapMode;
#if UNITY_2017_1_OR_NEWER
    /// <summary>
    /// The wrapModeU of this Texture2D
    /// </summary>
    public int wrapModeU;
    /// <summary>
    /// The wrapModeV of this Texture2D
    /// </summary>
    public int wrapModeV;
    /// <summary>
    /// The wrapModeW of this Texture2D
    /// </summary>
    public int wrapModeW;
#endif
    /// <summary>
    /// The mipMapBias of this Texture2D
    /// </summary>
    public float mipMapBias;
    /// <summary>
    /// The name of this Texture2D
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this Texture2D
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ETexture2D
    /// </summary>
    public ETexture2D() : base(true)
    {

    }

    /// <summary>
    /// Create a new ETexture2D
    /// </summary>
    /// <param name="tex">The Texture2D you want to use</param>
    public ETexture2D(Texture2D tex) : base(true)
    {
        this.width = tex.width;
        this.height = tex.height;
        this.rawTextureData = tex.EncodeToPNG();
        this.dimension = (int)tex.dimension;
        this.filterMode = (int)tex.filterMode;
        this.anisoLevel = tex.anisoLevel;
        this.wrapMode = (int)tex.wrapMode;
#if UNITY_2017_1_OR_NEWER
        this.wrapModeU = (int)tex.wrapModeU;
        this.wrapModeV = (int)tex.wrapModeV;
        this.wrapModeW = (int)tex.wrapModeW;
#endif
        this.mipMapBias = tex.mipMapBias;
        this.name = tex.name;
        this.hideFlags = (int)tex.hideFlags;
    }

    public override void SetData(ref Texture2D reference)
    {
        reference.width = this.width;
        reference.height = this.height;
        reference.LoadRawTextureData(this.rawTextureData);
        reference.dimension = (TextureDimension)this.dimension;
        reference.filterMode = (FilterMode)this.filterMode;
        reference.anisoLevel = this.anisoLevel;
        reference.wrapMode = (TextureWrapMode)this.wrapMode;
#if UNITY_2017_1_OR_NEWER
        reference.wrapModeU = (TextureWrapMode)this.wrapModeU;
        reference.wrapModeV = (TextureWrapMode)this.wrapModeV;
        reference.wrapModeW = (TextureWrapMode)this.wrapModeW;
#endif
        reference.mipMapBias = this.mipMapBias;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override Texture2D ToUnityType()
    {
        Texture2D tex = new Texture2D(this.width,this.height);
        SetData(ref tex);
        return tex;
    }
}
