﻿using UnityEngine;
using UnityEngine.Rendering;
using EAssets.IO.SerializableDatatypes;

/// <summary>
/// This can be used to serialize a rect
/// </summary>
[System.Serializable]
public class ERenderTextureDescriptor : SerializableUnityData<RenderTextureDescriptor>
{
    /// <summary>
    /// The width of this rendertexturedescriptor
    /// </summary>
    public int width;
    /// <summary>
    /// The height of this rendertexturedescriptor
    /// </summary>
    public int height;
    /// <summary>
    /// The msaaSamples of this rendertexturedescriptor
    /// </summary>
    public int msaaSamples;
    /// <summary>
    /// The volumeDepth of this rendertexturedescriptor
    /// </summary>
    public int volumeDepth;
    /// <summary>
    /// The colorFormat of this rendertexturedescriptor
    /// </summary>
    public int colorFormat;
    /// <summary>
    /// The depthBufferBits of this rendertexturedescriptor
    /// </summary>
    public int depthBufferBits;
    /// <summary>
    /// The dimension of this rendertexturedescriptor
    /// </summary>
    public int dimension;
    /// <summary>
    /// The shadowSamplingMode of this rendertexturedescriptor
    /// </summary>
    public int shadowSamplingMode;
    /// <summary>
    /// The vrUsage of this rendertexturedescriptor
    /// </summary>
    public int vrUsage;
    /// <summary>
    /// The memoryless of this rendertexturedescriptor
    /// </summary>
    public int memoryless;
    /// <summary>
    /// The sRGB state of this rendertexturedescriptor
    /// </summary>
    public bool sRGB;
    /// <summary>
    /// The useMipMap state of this rendertexturedescriptor
    /// </summary>
    public bool useMipMap;
    /// <summary>
    /// The autoGenerateMips state of this rendertexturedescriptor
    /// </summary>
    public bool autoGenerateMips;
    /// <summary>
    /// The enableRandomWrite state of this rendertexturedescriptor
    /// </summary>
    public bool enableRandomWrite;

    /// <summary>
    /// Create a new ERenderTextureDescriptor
    /// </summary>
    public ERenderTextureDescriptor() : base(true)
    {

    }

    /// <summary>
    /// Create a new ERenderTextureDescriptor
    /// </summary>
    /// <param name="desc">The rendertexturedescriptor you want to use</param>
    public ERenderTextureDescriptor(RenderTextureDescriptor desc) : base(true)
    {
        this.width = desc.width;
        this.height = desc.height;
        this.msaaSamples = desc.msaaSamples;
        this.volumeDepth = desc.volumeDepth;
        this.colorFormat = (int)desc.colorFormat;
        this.depthBufferBits = desc.depthBufferBits;
        this.dimension = (int)desc.dimension;
        this.shadowSamplingMode = (int)desc.shadowSamplingMode;
        this.vrUsage = (int)desc.vrUsage;
        this.memoryless = (int)desc.memoryless;
        this.sRGB = desc.sRGB;
        this.useMipMap = desc.useMipMap;
        this.autoGenerateMips = desc.autoGenerateMips;
        this.enableRandomWrite = desc.enableRandomWrite;
    }

    public override void SetData(ref RenderTextureDescriptor reference)
    {
        reference.width = this.width;
        reference.height = this.height;
        reference.msaaSamples = this.msaaSamples;
        reference.volumeDepth = this.volumeDepth;
        reference.colorFormat = (RenderTextureFormat)this.colorFormat;
        reference.depthBufferBits = this.depthBufferBits;
        reference.dimension = (TextureDimension)this.dimension;
        reference.shadowSamplingMode = (ShadowSamplingMode)this.shadowSamplingMode;
        reference.vrUsage = (VRTextureUsage)this.vrUsage;
        reference.memoryless = (RenderTextureMemoryless)this.memoryless;
        reference.sRGB = this.sRGB;
        reference.useMipMap = this.useMipMap;
        reference.autoGenerateMips = this.autoGenerateMips;
        reference.enableRandomWrite = this.enableRandomWrite;
    }

    public override RenderTextureDescriptor ToUnityType()
    {
        return new RenderTextureDescriptor(this.width,this.height,(RenderTextureFormat)this.colorFormat,this.depthBufferBits);
    }
}
