﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// This class can be used to serialize a Light
/// </summary>
[System.Serializable]
public class ELight : SerializableUnityData<Light>
{
    /// <summary>
    /// The type of this Light
    /// </summary>
    public int type;
    /// <summary>
    /// The color of this Light
    /// </summary>
    public EColor color;
    /// <summary>
    /// The colorTemperature of this Light
    /// </summary>
    public float colorTemperature;
    /// <summary>
    /// The intensity of this Light
    /// </summary>
    public float intensity;
    /// <summary>
    /// The bounceIntensity of this Light
    /// </summary>
    public float bounceIntensity;
    /// <summary>
    /// The shadows of this Light
    /// </summary>
    public int shadows;
    /// <summary>
    /// The shadowStrength of this Light
    /// </summary>
    public float shadowStrength;
    /// <summary>
    /// The shadowResolution of this Light
    /// </summary>
    public int shadowResolution;
    /// <summary>
    /// The shadowCustomResolution of this Light
    /// </summary>
    public int shadowCustomResolution;
    /// <summary>
    /// The shadowBias of this Light
    /// </summary>
    public float shadowBias;
    /// <summary>
    /// The shadowNormalBias of this Light
    /// </summary>
    public float shadowNormalBias;
    /// <summary>
    /// The shadowNearPlane of this Light
    /// </summary>
    public float shadowNearPlane;
    /// <summary>
    /// The range of this Light
    /// </summary>
    public float range;
    /// <summary>
    /// The spotAngle of this Light
    /// </summary>
    public float spotAngle;
    /// <summary>
    /// The cookieSize of this Light
    /// </summary>
    public float cookieSize;
    /// <summary>
    /// The cookie of this Light
    /// </summary>
    public ETexture cookie;
    /// <summary>
    /// The flare of this Light
    /// </summary>
    public EFlare flare;
    /// <summary>
    /// The renderMode of this Light
    /// </summary>
    public int renderMode;

#if UNITY_2017_3_OR_NEWER
    /// <summary>
    /// The isBaked state of this Light
    /// </summary>
    public bool isBaked;
#else
    /// <summary>
    /// The alreadyLightmapped state of this Light
    /// </summary>
    public bool alreadyLightmapped;
#endif
    /// <summary>
    /// The cullingMask of this Light
    /// </summary>
    public int cullingMask;
    /// <summary>
    /// The enabled state of this Light
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The tag of this Light
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this Light
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this Light
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ELight
    /// </summary>
    public ELight() : base(false)
    {

    }

    /// <summary>
    /// Create a new ELight
    /// </summary>
    /// <param name="light">The Light you want to use</param>
    public ELight(Light light) : base(false)
    {
        this.type = (int)light.type;
        this.color = new EColor(light.color);
        this.colorTemperature = light.colorTemperature;
        this.intensity = light.intensity;
        this.bounceIntensity = light.bounceIntensity;
        this.shadows = (int)light.shadows;
        this.shadowStrength = light.shadowStrength;
        this.shadowResolution = (int)light.shadowResolution;
        this.shadowCustomResolution = light.shadowCustomResolution;
        this.shadowBias = light.shadowBias;
        this.shadowNormalBias = light.shadowNormalBias;
        this.shadowNearPlane = light.shadowNearPlane;
        this.range = light.range;
        this.spotAngle = light.spotAngle;
        this.cookieSize = light.cookieSize;

        if(light.cookie != null)
            this.cookie = new ETexture(light.cookie);

        if(light.flare != null)
            this.flare = new EFlare(light.flare);

        this.renderMode = (int)light.renderMode;

#if UNITY_2017_3_OR_NEWER
        this.isBaked = light.bakingOutput.isBaked;
#else
       this.alreadyLightmapped = light.alreadyLightmapped;
#endif

        this.cullingMask = light.cullingMask;
        this.enabled = light.enabled;
        this.tag = light.tag;
        this.name = light.name;
        this.hideFlags = (int)light.hideFlags;
    }

    public override void SetData(ref Light reference)
    {
        reference.type = (LightType)this.type;
        reference.color = this.color;
        reference.colorTemperature = this.colorTemperature;
        reference.intensity = this.intensity;
        reference.bounceIntensity = this.bounceIntensity;
        reference.shadows = (LightShadows)this.shadows;
        reference.shadowStrength = this.shadowStrength;
        reference.shadowResolution = (LightShadowResolution)this.shadowResolution;
        reference.shadowCustomResolution = this.shadowCustomResolution;
        reference.shadowBias = this.shadowBias;
        reference.shadowNormalBias = this.shadowNormalBias;
        reference.shadowNearPlane = this.shadowNearPlane;
        reference.range = this.range;
        reference.spotAngle = this.spotAngle;
        reference.cookieSize = this.cookieSize;

        if (this.cookie != null)
        {
            Texture tex = reference.cookie;
            this.cookie.SetData(ref tex);
        }

        if (this.flare != null)
        {
            Flare flare = reference.flare;
            this.flare.SetData(ref flare);
        }

        reference.renderMode = (LightRenderMode)this.renderMode;

#if UNITY_2017_3_OR_NEWER
        LightBakingOutput output = reference.bakingOutput;
        output.isBaked = this.isBaked;
        reference.bakingOutput = output;
#else
       light.alreadyLightmapped = this.alreadyLightmapped;
#endif

        reference.cullingMask = this.cullingMask;
        reference.enabled = this.enabled;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override Light ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}
