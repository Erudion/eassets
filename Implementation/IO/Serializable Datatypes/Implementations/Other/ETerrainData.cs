﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize TerrainData
/// </summary>
[System.Serializable]
public class ETerrainData : SerializableUnityData<TerrainData>
{
    /// <summary>
    /// The alphamaps of this TerrainData
    /// </summary>
    public float[,,] alphamaps;
    /// <summary>
    /// The heights of this TerrainData
    /// </summary>
    public float[,] heights;
    /// <summary>
    /// The heightmapResolution of this TerrainData
    /// </summary>
    public int heightmapResolution;
    /// <summary>
    /// The size of this TerrainData
    /// </summary>
    public EVector3 size;
    /// <summary>
    /// The thickness of this TerrainData
    /// </summary>
    public float thickness;
    /// <summary>
    /// The wavingGrassStrength of this TerrainData
    /// </summary>
    public float wavingGrassStrength;
    /// <summary>
    /// The wavingGrassAmount of this TerrainData
    /// </summary>
    public float wavingGrassAmount;
    /// <summary>
    /// The wavingGrassSpeed of this TerrainData
    /// </summary>
    public float wavingGrassSpeed;
    /// <summary>
    /// The wavingGrassTint of this TerrainData
    /// </summary>
    public EColor wavingGrassTint;
    /// <summary>
    /// The detailPrototypes of this TerrainData
    /// </summary>
    public EDetailPrototype[] detailPrototypes;
    /// <summary>
    /// The treeInstances of this TerrainData
    /// </summary>
    public ETreeInstance[] treeInstances;
    /// <summary>
    /// The treePrototypes of this TerrainData
    /// </summary>
    public ETreePrototype[] treePrototypes;
    /// <summary>
    /// The alphamapResolution of this TerrainData
    /// </summary>
    public int alphamapResolution;
    /// <summary>
    /// The baseMapResolution of this TerrainData
    /// </summary>
    public int baseMapResolution;
    /// <summary>
    /// The splatPrototypes of this TerrainData
    /// </summary>
    public int splatPrototypes;
    /// <summary>
    /// The name of this TerrainData
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this TerrainData
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ETerrainData
    /// </summary>
    public ETerrainData() : base(true)
    {

    }

    /// <summary>
    /// Create a new ETerrainData
    /// </summary>
    /// <param name="reference">The TerrainData you want to use</param>
    public ETerrainData(TerrainData reference) : base(true)
    {
        this.alphamaps = reference.GetAlphamaps(0, 0, reference.alphamapWidth, reference.alphamapHeight);
        this.heights = reference.GetHeights(0, 0, reference.heightmapWidth, reference.heightmapHeight);
        this.heightmapResolution = reference.heightmapResolution;
        this.size = new EVector3(reference.size);
        this.thickness = reference.thickness;
        this.wavingGrassStrength = reference.wavingGrassStrength;
        this.wavingGrassAmount = reference.wavingGrassAmount;
        this.wavingGrassSpeed = reference.wavingGrassSpeed;
        this.wavingGrassTint = new EColor(reference.wavingGrassTint);

        if (reference.detailPrototypes != null && reference.detailPrototypes.Length > 0)
        {
            this.detailPrototypes = new EDetailPrototype[reference.detailPrototypes.Length];
            for(int i = 0;i< reference.detailPrototypes.Length; i++)
            {
                this.detailPrototypes[i] = new EDetailPrototype(reference.detailPrototypes[i]);
            }
        }

        if (reference.treeInstances != null && reference.treeInstances.Length > 0)
        {
            this.treeInstances = new ETreeInstance[reference.treeInstances.Length];
            for (int i = 0; i < reference.treeInstances.Length; i++)
            {
                this.treeInstances[i] = new ETreeInstance(reference.GetTreeInstance(i));
            }
        }


        if (reference.treePrototypes != null && reference.treePrototypes.Length > 0)
        {
            this.treePrototypes = new ETreePrototype[reference.treePrototypes.Length];
            for (int i = 0; i < reference.treePrototypes.Length; i++)
            {
                this.treePrototypes[i] = new ETreePrototype(reference.treePrototypes[i]);
            }
        }

        this.alphamapResolution = reference.alphamapResolution;
        this.baseMapResolution = reference.baseMapResolution;

        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref TerrainData reference)
    {
        reference.SetAlphamaps(0,0,this.alphamaps);
        reference.SetHeights(0, 0, this.heights);
        reference.heightmapResolution = this.heightmapResolution;
        reference.size = this.size.ToUnityType();
        reference.thickness = this.thickness;
        reference.wavingGrassStrength = this.wavingGrassStrength;
        reference.wavingGrassAmount = this.wavingGrassAmount;
        reference.wavingGrassSpeed = this.wavingGrassSpeed;
        reference.wavingGrassTint = this.wavingGrassTint.ToUnityType();

        if (this.detailPrototypes != null && this.detailPrototypes.Length > 0)
        {
            reference.detailPrototypes = new DetailPrototype[this.detailPrototypes.Length];
            for (int i = 0; i < this.detailPrototypes.Length; i++)
            {
                reference.detailPrototypes[i] = this.detailPrototypes[i].ToUnityType();
            }
        }

        if (this.treeInstances != null && this.treeInstances.Length > 0)
        {
            reference.treeInstances = new TreeInstance[this.treeInstances.Length];
            for (int i = 0; i < this.treeInstances.Length; i++)
            {
                reference.treeInstances[i] = this.treeInstances[i].ToUnityType();
            }
        }


        if (this.treePrototypes != null && this.treePrototypes.Length > 0)
        {
            reference.treePrototypes = new TreePrototype[this.treePrototypes.Length];
            for (int i = 0; i < this.treePrototypes.Length; i++)
            {
                reference.treePrototypes[i] = this.treePrototypes[i].ToUnityType();
            }
        }

        reference.alphamapResolution = this.alphamapResolution;
        reference.baseMapResolution = this.baseMapResolution;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override TerrainData ToUnityType()
    {
        TerrainData data = new TerrainData();
        SetData(ref data);
        return data;
    }
}
