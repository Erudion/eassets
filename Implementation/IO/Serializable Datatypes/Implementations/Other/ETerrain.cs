﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// This class can be used to serialize a Terrain
/// </summary>
[System.Serializable]
public class ETerrain : SerializableUnityData<Terrain>
{
    /// <summary>
    /// The terrainData of this Terrain
    /// </summary>
    public ETerrainData terrainData;
    /// <summary>
    /// The treeDistance of this Terrain
    /// </summary>
    public float treeDistance;
    /// <summary>
    /// The treeBillboardDistance of this Terrain
    /// </summary>
    public float treeBillboardDistance;
    /// <summary>
    /// The treeCrossFadeLength of this Terrain
    /// </summary>
    public float treeCrossFadeLength;
    /// <summary>
    /// The treeMaximumFullLODCount of this Terrain
    /// </summary>
    public int treeMaximumFullLODCount;
    /// <summary>
    /// The detailObjectDistance of this Terrain
    /// </summary>
    public float detailObjectDistance;
    /// <summary>
    /// The detailObjectDensity of this Terrain
    /// </summary>
    public float detailObjectDensity;
    /// <summary>
    /// The heightmapPixelError of this Terrain
    /// </summary>
    public float heightmapPixelError;
    /// <summary>
    /// The heightmapMaximumLOD of this Terrain
    /// </summary>
    public int heightmapMaximumLOD;
    /// <summary>
    /// The basemapDistance of this Terrain
    /// </summary>
    public float basemapDistance;
    /// <summary>
    /// The lightmapIndex of this Terrain
    /// </summary>
    public int lightmapIndex;
    /// <summary>
    /// The realtimeLightmapIndex of this Terrain
    /// </summary>
    public int realtimeLightmapIndex;
    /// <summary>
    /// The lightmapScaleOffset of this Terrain
    /// </summary>
    public EVector4 lightmapScaleOffset;
    /// <summary>
    /// The realtimeLightmapScaleOffset of this Terrain
    /// </summary>
    public EVector4 realtimeLightmapScaleOffset;
    /// <summary>
    /// The shadowCastingMode of this Terrain
    /// </summary>
    public int shadowCastingMode;
    /// <summary>
    /// The reflectionProbeUsage of this Terrain
    /// </summary>
    public int reflectionProbeUsage;
    /// <summary>
    /// The materialType of this Terrain
    /// </summary>
    public int materialType;
    /// <summary>
    /// The materialTemplate of this Terrain
    /// </summary>
    public EMaterial materialTemplate;
    /// <summary>
    /// The legacySpecular of this Terrain
    /// </summary>
    public EColor legacySpecular;
    /// <summary>
    /// The legacyShininess of this Terrain
    /// </summary>
    public float legacyShininess;
    /// <summary>
    /// The drawHeightmap state of this Terrain
    /// </summary>
    public bool drawHeightmap;
    /// <summary>
    /// The drawTreesAndFoliage state of this Terrain
    /// </summary>
    public bool drawTreesAndFoliage;
#if UNITY_2017_1_OR_NEWER
    /// <summary>
    /// The patchBoundsMultiplier of this Terrain
    /// </summary>
    public EVector3 patchBoundsMultiplier;
#endif
    /// <summary>
    /// The treeLODBiasMultiplier of this Terrain
    /// </summary>
    public float treeLODBiasMultiplier;
    /// <summary>
    /// The collectDetailPatches state of this Terrain
    /// </summary>
    public bool collectDetailPatches;
    /// <summary>
    /// The editorRenderFlags of this Terrain
    /// </summary>
    public int editorRenderFlags;
    /// <summary>
    /// The enabled state of this Terrain
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The tag of this Terrain
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this Terrain
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this Terrain
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ETerrain
    /// </summary>
    public ETerrain() : base(true)
    {

    }

    /// <summary>
    /// Create a new ETerrain
    /// </summary>
    /// <param name="reference">The Terrain you want to use</param>
    public ETerrain(Terrain reference) : base(true)
    {
        if(reference.terrainData != null)
        {
            this.terrainData = new ETerrainData(reference.terrainData);
        }

        this.treeDistance = reference.treeDistance;
        this.treeBillboardDistance = reference.treeBillboardDistance;
        this.treeCrossFadeLength = reference.treeCrossFadeLength;
        this.treeMaximumFullLODCount = reference.treeMaximumFullLODCount;
        this.detailObjectDistance = reference.detailObjectDistance;
        this.detailObjectDensity = reference.detailObjectDensity;
        this.heightmapPixelError = reference.heightmapPixelError;
        this.heightmapMaximumLOD = reference.heightmapMaximumLOD;
        this.basemapDistance = reference.basemapDistance;
        this.lightmapIndex = reference.lightmapIndex;
        this.realtimeLightmapIndex = reference.realtimeLightmapIndex;
        this.lightmapScaleOffset = new EVector4(reference.lightmapScaleOffset);
        this.realtimeLightmapScaleOffset = new EVector4(reference.realtimeLightmapScaleOffset);
        this.shadowCastingMode = (int)reference.shadowCastingMode;
        this.reflectionProbeUsage = (int)reference.reflectionProbeUsage;
        this.materialType = (int)reference.materialType;

        if (reference.materialTemplate != null)
        {
            this.materialTemplate = new EMaterial(reference.materialTemplate);
        }

        this.legacySpecular = new EColor(reference.legacySpecular);
        this.legacyShininess = reference.legacyShininess;
        this.drawHeightmap = reference.drawHeightmap;
        this.drawTreesAndFoliage = reference.drawTreesAndFoliage;
#if UNITY_2017_1_OR_NEWER
        this.patchBoundsMultiplier = new EVector3(reference.patchBoundsMultiplier);
#endif
        this.treeLODBiasMultiplier = reference.treeLODBiasMultiplier;
        this.collectDetailPatches = reference.collectDetailPatches;
        this.editorRenderFlags = (int)reference.editorRenderFlags;
        this.enabled = reference.enabled;

        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref Terrain reference)
    {
        if (this.terrainData != null)
        {
            if (reference.terrainData != null)
            {
                TerrainData data = reference.terrainData;
                this.terrainData.SetData(ref data);
            }
            else
            {
                reference.terrainData = this.terrainData.ToUnityType();
            }
        }

        reference.treeDistance = this.treeDistance;
        reference.treeBillboardDistance = this.treeBillboardDistance;
        reference.treeCrossFadeLength = this.treeCrossFadeLength;
        reference.treeMaximumFullLODCount = this.treeMaximumFullLODCount;
        reference.detailObjectDistance = this.detailObjectDistance;
        reference.detailObjectDensity = this.detailObjectDensity;
        reference.heightmapPixelError = this.heightmapPixelError;
        reference.heightmapMaximumLOD = this.heightmapMaximumLOD;
        reference.basemapDistance = this.basemapDistance;
        reference.lightmapIndex = this.lightmapIndex;
        reference.realtimeLightmapIndex = this.realtimeLightmapIndex;
        reference.lightmapScaleOffset = this.lightmapScaleOffset.ToUnityType();
        reference.realtimeLightmapScaleOffset = this.realtimeLightmapScaleOffset.ToUnityType();
        reference.shadowCastingMode = (ShadowCastingMode)this.shadowCastingMode;
        reference.reflectionProbeUsage = (ReflectionProbeUsage)this.reflectionProbeUsage;
        reference.materialType = (Terrain.MaterialType)this.materialType;

        if (this.materialTemplate != null)
        {
            if (reference.materialTemplate != null)
            {
                Material mat = reference.materialTemplate;
                this.materialTemplate.SetData(ref mat);
            }
            else
            {
                reference.materialTemplate = this.materialTemplate.ToUnityType();
            }
        }

        reference.legacySpecular = this.legacySpecular;
        reference.legacyShininess = this.legacyShininess;
        reference.drawHeightmap = this.drawHeightmap;
        reference.drawTreesAndFoliage = this.drawTreesAndFoliage;
#if UNITY_2017_1_OR_NEWER
        reference.patchBoundsMultiplier = this.patchBoundsMultiplier.ToUnityType();
#endif
        reference.treeLODBiasMultiplier = this.treeLODBiasMultiplier;
        reference.collectDetailPatches = this.collectDetailPatches;
        reference.editorRenderFlags = (TerrainRenderFlags)this.editorRenderFlags;
        reference.enabled = this.enabled;

        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override Terrain ToUnityType()
    {
        Terrain terrain = new Terrain();
        SetData(ref terrain);
        return terrain;
    }
}
