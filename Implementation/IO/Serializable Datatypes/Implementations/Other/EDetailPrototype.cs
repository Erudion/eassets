﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a DetailPrototype
/// </summary>
[System.Serializable]
public class EDetailPrototype : SerializableUnityData<DetailPrototype>
{
    /// <summary>
    /// The prototype of this DetailPrototype
    /// </summary>
    public EGameObject prototype;
    /// <summary>
    /// The prototypeTexture of this DetailPrototype
    /// </summary>
    public ETexture2D prototypeTexture;
    /// <summary>
    /// The minWidth of this DetailPrototype
    /// </summary>
    public float minWidth;
    /// <summary>
    /// The maxWidth of this DetailPrototype
    /// </summary>
    public float maxWidth;
    /// <summary>
    /// The minHeight of this DetailPrototype
    /// </summary>
    public float minHeight;
    /// <summary>
    /// The maxHeight of this DetailPrototype
    /// </summary>
    public float maxHeight;
    /// <summary>
    /// The noiseSpread of this DetailPrototype
    /// </summary>
    public float noiseSpread;
    /// <summary>
    /// The bendFactor of this DetailPrototype
    /// </summary>
    public float bendFactor;
    /// <summary>
    /// The healthyColor of this DetailPrototype
    /// </summary>
    public EColor healthyColor;
    /// <summary>
    /// The dryColor of this DetailPrototype
    /// </summary>
    public EColor dryColor;
    /// <summary>
    /// The renderMode of this DetailPrototype
    /// </summary>
    public int renderMode;
    /// <summary>
    /// The usePrototypeMesh state of this DetailPrototype
    /// </summary>
    public bool usePrototypeMesh;

    /// <summary>
    /// Create a new EDetailPrototype
    /// </summary>
    public EDetailPrototype() : base(true)
    {

    }

    /// <summary>
    /// Create a new EDetailPrototype
    /// </summary>
    /// <param name="reference">The DetailPrototype you want to use</param>
    public EDetailPrototype(DetailPrototype reference) : base(true)
    {
        if (reference.prototype != null)
        {
            this.prototype = new EGameObject(reference.prototype);
        }

        if(reference.prototypeTexture != null)
        {
            this.prototypeTexture = new ETexture2D(reference.prototypeTexture);
        }

        this.minWidth = reference.minWidth;
        this.maxWidth = reference.maxWidth;
        this.minHeight = reference.minHeight;
        this.maxHeight = reference.maxHeight;
        this.noiseSpread = reference.noiseSpread;
        this.bendFactor = reference.bendFactor;
        this.healthyColor = new EColor(reference.healthyColor);
        this.dryColor = new EColor(reference.dryColor);
        this.renderMode = (int)reference.renderMode;
        this.usePrototypeMesh = reference.usePrototypeMesh;
    }

    public override void SetData(ref DetailPrototype reference)
    {
        if (this.prototype != null)
        {
            if (reference.prototype != null)
            {
                GameObject go = reference.prototype;
                this.prototype.SetData(ref go);
            }
            else
            {
                reference.prototype = this.prototype.ToUnityType();
            }
        }

        if (this.prototypeTexture != null)
        {
            if (reference.prototypeTexture != null)
            {
                Texture2D tex = reference.prototypeTexture;
                this.prototypeTexture.SetData(ref tex);
            }
            else
            {
                reference.prototypeTexture = this.prototypeTexture.ToUnityType();
            }
        }

        reference.minWidth = this.minWidth;
        reference.maxWidth = this.maxWidth;
        reference.minHeight = this.minHeight;
        reference.maxHeight = this.maxHeight;
        reference.noiseSpread = this.noiseSpread;
        reference.bendFactor = this.bendFactor;
        reference.healthyColor = this.healthyColor.ToUnityType();
        reference.dryColor = this.dryColor.ToUnityType();
        reference.renderMode = (DetailRenderMode)this.renderMode;
        reference.usePrototypeMesh = this.usePrototypeMesh;
    }

    public override DetailPrototype ToUnityType()
    {
        DetailPrototype prototype = new DetailPrototype();
        SetData(ref prototype);
        return prototype;
    }
}
