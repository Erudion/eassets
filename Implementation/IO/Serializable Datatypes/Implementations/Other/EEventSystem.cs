﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// This class can be used to serialize a EventSystem
/// </summary>
[System.Serializable]
public class EEventSystem : SerializableUnityData<EventSystem>
{
    /// <summary>
    /// The sendNavigationEvents state of this EventSystem
    /// </summary>
    public bool sendNavigationEvents;
    /// <summary>
    /// The pixelDragThreshold of this EventSystem
    /// </summary>
    public int pixelDragThreshold;
    /// <summary>
    /// The firstSelectedGameObject of this EventSystem
    /// </summary>
    public EGameObject firstSelectedGameObject;
    /// <summary>
    /// The useGUILayout state of this EventSystem
    /// </summary>
    public bool useGUILayout;
    /// <summary>
    /// The enabled state of this EventSystem
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The tag of this EventSystem
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this EventSystem
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this EventSystem
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EEventSystem
    /// </summary>
    public EEventSystem() : base(false)
    {

    }

    /// <summary>
    /// Create a new EEventSystem
    /// </summary>
    /// <param name="reference">The EventSystem you want to use</param>
    public EEventSystem(EventSystem reference) : base(false)
    {
        this.sendNavigationEvents = reference.sendNavigationEvents;
        this.pixelDragThreshold = reference.pixelDragThreshold;

        if (reference.firstSelectedGameObject != null)
        {
            this.firstSelectedGameObject = new EGameObject(reference.firstSelectedGameObject);
        }

        this.useGUILayout = reference.useGUILayout;
        this.enabled = reference.enabled;
        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref EventSystem reference)
    {
        reference.sendNavigationEvents = this.sendNavigationEvents;
        reference.pixelDragThreshold = this.pixelDragThreshold;

        if (this.firstSelectedGameObject != null)
        {
            if (reference.firstSelectedGameObject != null)
            {
                GameObject go = reference.firstSelectedGameObject;
                this.firstSelectedGameObject.SetData(ref go);
            }
            else //Create a new gameobject if needed
            {
                reference.firstSelectedGameObject = this.firstSelectedGameObject.ToUnityType();
            }
        }

        reference.useGUILayout = this.useGUILayout;
        reference.enabled = this.enabled;

        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override EventSystem ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}
