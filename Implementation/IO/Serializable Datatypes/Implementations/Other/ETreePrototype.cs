﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a TreePrototype
/// </summary>
[System.Serializable]
public class ETreePrototype : SerializableUnityData<TreePrototype>
{
    /// <summary>
    /// The prefab of this TreePrototype
    /// </summary>
    public EGameObject prefab;
    /// <summary>
    /// The bendFactor of this TreePrototype
    /// </summary>
    public float bendFactor;

    /// <summary>
    /// Create a new ETreePrototype
    /// </summary>
    public ETreePrototype() : base(true)
    {

    }

    /// <summary>
    /// Create a new ETreePrototype
    /// </summary>
    /// <param name="reference">The TreePrototype you want to use</param>
    public ETreePrototype(TreePrototype reference) : base(true)
    {
        if (reference.prefab != null)
        {
            this.prefab = new EGameObject(reference.prefab);
        }

        this.bendFactor = reference.bendFactor;
    }

    public override void SetData(ref TreePrototype reference)
    {
        if(this.prefab != null)
        {
            if(reference.prefab != null)
            {
                GameObject go = reference.prefab;
                this.prefab.SetData(ref go);
            }
            else
            {
                reference.prefab = this.prefab.ToUnityType();
            }
        }

        reference.bendFactor = this.bendFactor;
    }

    public override TreePrototype ToUnityType()
    {
        TreePrototype tree = new TreePrototype();
        SetData(ref tree);
        return tree;
    }
}
