﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a TreeInstance
/// </summary>
[System.Serializable]
public class ETreeInstance : SerializableUnityData<TreeInstance>
{
    /// <summary>
    /// The position of this TreeInstance
    /// </summary>
    public EVector3 position;
    /// <summary>
    /// The widthScale of this TreeInstance
    /// </summary>
    public float widthScale;
    /// <summary>
    /// The heightScale of this TreeInstance
    /// </summary>
    public float heightScale;
    /// <summary>
    /// The rotation of this TreeInstance
    /// </summary>
    public float rotation;
    /// <summary>
    /// The color of this TreeInstance
    /// </summary>
    public EColor32 color;
    /// <summary>
    /// The lightmapColor of this TreeInstance
    /// </summary>
    public EColor32 lightmapColor;
    /// <summary>
    /// The prototypeIndex of this TreeInstance
    /// </summary>
    public int prototypeIndex;

    /// <summary>
    /// Create a new ETreeInstance
    /// </summary>
    public ETreeInstance() : base(true)
    {

    }

    /// <summary>
    /// Create a new ETreeInstance
    /// </summary>
    /// <param name="reference">The TreeInstance you want to use</param>
    public ETreeInstance(TreeInstance reference) : base(true)
    {
        this.position = new EVector3(reference.position);
        this.widthScale = reference.widthScale;
        this.heightScale = reference.heightScale;
        this.rotation = reference.rotation;
        this.color = new EColor32(reference.color);
        this.lightmapColor = new EColor32(reference.lightmapColor);
        this.prototypeIndex = reference.prototypeIndex;
    }

    public override void SetData(ref TreeInstance reference)
    {
        reference.position = this.position.ToUnityType();
        reference.widthScale = this.widthScale;
        reference.heightScale = this.heightScale;
        reference.rotation = this.rotation;
        reference.color = this.color.ToUnityType();
        reference.lightmapColor = this.lightmapColor.ToUnityType();
        reference.prototypeIndex = this.prototypeIndex;
    }

    public override TreeInstance ToUnityType()
    {
        TreeInstance instance = new TreeInstance();
        SetData(ref instance);
        return instance;
    }
}
