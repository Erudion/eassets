﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a WindZone
/// </summary>
[System.Serializable]
public class EWindZone : SerializableUnityData<WindZone>
{
    /// <summary>
    /// The mode of this EWindZone
    /// </summary>
    public int mode;
    /// <summary>
    /// The radius of this EWindZone
    /// </summary>
    public float radius;
    /// <summary>
    /// The windMain of this EWindZone
    /// </summary>
    public float windMain;
    /// <summary>
    /// The windTurbulence of this EWindZone
    /// </summary>
    public float windTurbulence;
    /// <summary>
    /// The windPulseMagnitude of this EWindZone
    /// </summary>
    public float windPulseMagnitude;
    /// <summary>
    /// The windPulseFrequency of this EWindZone
    /// </summary>
    public float windPulseFrequency;
    /// <summary>
    /// The tag of this WindZone
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this WindZone
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this WindZone
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EWindZone
    /// </summary>
    public EWindZone() : base(true)
    {

    }

    /// <summary>
    /// Create a new EWindZone
    /// </summary>
    /// <param name="reference">The WindZone you want to use</param>
    public EWindZone(WindZone reference) : base(true)
    {
        this.mode = (int)reference.mode;
        this.radius = reference.radius;
        this.windMain = reference.windMain;
        this.windTurbulence = reference.windTurbulence;
        this.windPulseMagnitude = reference.windPulseMagnitude;
        this.windPulseFrequency = reference.windPulseFrequency;
        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref WindZone reference)
    {
        reference.mode = (WindZoneMode)this.mode;
        reference.radius = this.radius;
        reference.windMain = this.windMain;
        reference.windTurbulence = this.windTurbulence;
        reference.windPulseMagnitude = this.windPulseMagnitude;
        reference.windPulseFrequency = this.windPulseFrequency;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override WindZone ToUnityType()
    {
        WindZone zone = new WindZone();
        SetData(ref zone);
        return zone;
    }
}
