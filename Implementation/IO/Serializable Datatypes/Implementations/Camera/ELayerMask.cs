﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a LayerMask
/// </summary>
[System.Serializable]
public class ELayerMask : SerializableUnityData<LayerMask>
{
    /// <summary>
    /// The value of this ELayerMask
    /// </summary>
    public int value;

    /// <summary>
    /// Create a new ELayerMask
    /// </summary>
    public ELayerMask() : base(true)
    {

    }

    /// <summary>
    /// Create a new ELayerMask
    /// </summary>
    /// <param name="reference">The LayerMask you want to use</param>
    public ELayerMask(LayerMask reference) : base(true)
    {
        this.value = reference.value;
    }

    public override void SetData(ref LayerMask reference)
    {
        reference.value = this.value;
    }

    public override LayerMask ToUnityType()
    {
        return new LayerMask { value = this.value };
    }
}
