﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a Flare
/// </summary>
[System.Serializable]
public class EFlare : SerializableUnityData<Flare>
{
    /// <summary>
    /// The name of this Flare
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this Flare
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EFlare
    /// </summary>
    public EFlare() : base(true)
    {

    }

    /// <summary>
    /// Create a new EFlare
    /// </summary>
    /// <param name="flare">The Flare you want to use</param>
    public EFlare(Flare flare) : base(true)
    {
        this.name = flare.name;
        this.hideFlags = (int)flare.hideFlags;
    }

    public override void SetData(ref Flare reference)
    {
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override Flare ToUnityType()
    {
        return new Flare { name = this.name, hideFlags = (HideFlags)this.hideFlags };
    }
}
