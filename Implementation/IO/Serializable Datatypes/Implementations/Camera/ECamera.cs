﻿using UnityEngine;
using UnityEngine.Rendering;
using EAssets.IO.SerializableDatatypes;

/// <summary>
/// This can be used to serialize a camera
/// </summary>
[System.Serializable]
public class ECamera : SerializableUnityData<Camera>
{
    /// <summary>
    /// The fieldOfView of this camera
    /// </summary>
    public float fieldOfView;
    /// <summary>
    /// The nearClipPlane of this camera
    /// </summary>
    public float nearClipPlane;
    /// <summary>
    /// The farClipPlane of this camera
    /// </summary>
    public float farClipPlane;
    /// <summary>
    /// The renderingPath of this camera
    /// </summary>
    public int renderingPath;
    /// <summary>
    /// The allowHDR state of this camera
    /// </summary>
    public bool allowHDR;
    /// <summary>
    /// The forceIntoRenderTexture state of this camera
    /// </summary>
    public bool forceIntoRenderTexture;
    /// <summary>
    /// The allowMSAA state of this camera
    /// </summary>
    public bool allowMSAA;
    /// <summary>
    /// The orthographicSize of this camera
    /// </summary>
    public float orthographicSize;
    /// <summary>
    /// The orthographic state of this camera
    /// </summary>
    public bool orthographic;
    /// <summary>
    /// The opaqueSortMode of this camera
    /// </summary>
    public int opaqueSortMode;
    /// <summary>
    /// The transparencySortMode of this camera
    /// </summary>
    public int transparencySortMode;
    /// <summary>
    /// The transparencySortAxis of this camera
    /// </summary>
    public EVector3 transparencySortAxis;
    /// <summary>
    /// The depth of this camera
    /// </summary>
    public float depth;
    /// <summary>
    /// The aspect of this camera
    /// </summary>
    public float aspect;
    /// <summary>
    /// The cullingMask of this camera
    /// </summary>
    public int cullingMask;
    /// <summary>
    /// The scene of this camera
    /// </summary>
    //public EScene scene;
    /// <summary>
    /// The eventMask of this camera
    /// </summary>
    public int eventMask;
    /// <summary>
    /// The backgroundColor of this camera
    /// </summary>
    public EColor backgroundColor;
    /// <summary>
    /// The rect of this camera
    /// </summary>
    public ERect rect;
    /// <summary>
    /// The pixelRect of this camera
    /// </summary>
    public ERect pixelRect;
    /// <summary>
    /// The targetTexture of this camera
    /// </summary>
    public ERenderTexture targetTexture;
    /// <summary>
    /// The worldToCameraMatrix of this camera
    /// </summary>
    public EMatrix4x4 worldToCameraMatrix;
    /// <summary>
    /// The projectionMatrix of this camera
    /// </summary>
    public EMatrix4x4 projectionMatrix;
    /// <summary>
    /// The nonJitteredProjectionMatrix of this camera
    /// </summary>
    public EMatrix4x4 nonJitteredProjectionMatrix;
    /// <summary>
    /// The enabled of this camera
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The tag of this camera
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this camera
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this camera
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ECamera
    /// </summary>
    public ECamera() : base(false)
    {

    }

    /// <summary>
    /// Create a new ECamera
    /// </summary>
    /// <param name="cam">The camera you want to use</param>
    public ECamera(Camera cam) : base(false)
    {
        this.fieldOfView = cam.fieldOfView;
        this.nearClipPlane = cam.nearClipPlane;
        this.farClipPlane = cam.farClipPlane;
        this.renderingPath = (int)cam.renderingPath;
        this.allowHDR = cam.allowHDR;
        this.forceIntoRenderTexture = cam.forceIntoRenderTexture;
        this.allowMSAA = cam.allowMSAA;
        this.orthographicSize = cam.orthographicSize;
        this.orthographic = cam.orthographic;
        this.opaqueSortMode = (int)cam.opaqueSortMode;
        this.transparencySortMode = (int)cam.transparencySortMode;
        this.transparencySortAxis = new EVector3(cam.transparencySortAxis);
        this.depth = cam.depth;
        this.aspect = cam.aspect;
        this.cullingMask = cam.cullingMask;
        //this.scene = cam.scene;       //Scene is not yet stored
        this.eventMask = cam.eventMask;
        this.backgroundColor = new EColor(cam.backgroundColor);
        this.rect = new ERect(cam.rect);
        this.pixelRect = new ERect(cam.pixelRect);

        if(cam.targetTexture != null)
            this.targetTexture = new ERenderTexture(cam.targetTexture);

        this.worldToCameraMatrix = new EMatrix4x4(cam.worldToCameraMatrix);
        this.projectionMatrix = new EMatrix4x4(cam.projectionMatrix);
        this.nonJitteredProjectionMatrix = new EMatrix4x4(cam.nonJitteredProjectionMatrix);

        this.enabled = cam.enabled;
        this.tag = cam.tag;
        this.name = cam.name;
        this.hideFlags = (int)cam.hideFlags;
    }

    public override void SetData(ref Camera reference)
    {
        reference.fieldOfView = this.fieldOfView;
        reference.nearClipPlane = this.nearClipPlane;
        reference.farClipPlane = this.farClipPlane;
        reference.renderingPath = (RenderingPath)this.renderingPath;
        reference.allowHDR = this.allowHDR;
        reference.forceIntoRenderTexture = this.forceIntoRenderTexture;
        reference.allowMSAA = this.allowMSAA;
        reference.orthographicSize = this.orthographicSize;
        reference.orthographic = this.orthographic;
        reference.opaqueSortMode = (OpaqueSortMode)this.opaqueSortMode;
        reference.transparencySortMode = (TransparencySortMode)this.transparencySortMode;
        reference.transparencySortAxis = this.transparencySortAxis;
        reference.depth = this.depth;
        reference.aspect = this.aspect;
        reference.cullingMask = this.cullingMask;
        /*if(this.scene != null){Scene scene = reference.scene; this.scene.SetData(scene);}*/
        reference.eventMask = this.eventMask;
        reference.backgroundColor = this.backgroundColor;

        if (this.rect != null)
        {
            Rect rect = reference.rect;
            this.rect.SetData(ref rect);
            reference.rect = rect; //This step is required since rect is a value type
        }
        if (this.pixelRect != null)
        {
            Rect rect = reference.pixelRect;
            this.pixelRect.SetData(ref rect);
            reference.pixelRect = rect; //This step is required since rect is a value type
        }

        if (this.targetTexture != null)
        {
            RenderTexture ren = reference.targetTexture;
            this.targetTexture.SetData(ref ren);
        }
        if (this.worldToCameraMatrix != null)
        {
            reference.worldToCameraMatrix = this.worldToCameraMatrix.ToUnityType();
        }
        if (this.projectionMatrix != null)
        {
            reference.projectionMatrix = this.projectionMatrix.ToUnityType();
        }
        if (this.nonJitteredProjectionMatrix != null)
        {
            reference.nonJitteredProjectionMatrix = this.nonJitteredProjectionMatrix.ToUnityType();
        }

        reference.enabled = this.enabled;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override Camera ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}
