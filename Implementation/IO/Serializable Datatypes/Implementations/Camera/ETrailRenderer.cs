﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// This class can be used to serialize a TrailRenderer
/// </summary>
[System.Serializable]
public class ETrailRenderer : SerializableUnityData<TrailRenderer>
{
    /// <summary>
    /// The time of this TrailRenderer
    /// </summary>
    public float time;
    /// <summary>
    /// The startWidth of this TrailRenderer
    /// </summary>
    public float startWidth;
    /// <summary>
    /// The endWidth of this TrailRenderer
    /// </summary>
    public float endWidth;
    /// <summary>
    /// The widthCurve of this TrailRenderer
    /// </summary>
    public EAnimationCurve widthCurve;
    /// <summary>
    /// The widthMultiplier of this TrailRenderer
    /// </summary>
    public float widthMultiplier;
    /// <summary>
    /// The startColor of this TrailRenderer
    /// </summary>
    public EColor startColor;
    /// <summary>
    /// The endColor of this TrailRenderer
    /// </summary>
    public EColor endColor;
    /// <summary>
    /// The colorGradient of this TrailRenderer
    /// </summary>
    public EGradient colorGradient;
    /// <summary>
    /// The autodestruct state of this TrailRenderer
    /// </summary>
    public bool autodestruct;
    /// <summary>
    /// The numCornerVertices of this TrailRenderer
    /// </summary>
    public int numCornerVertices;
    /// <summary>
    /// The numCapVertices of this TrailRenderer
    /// </summary>
    public int numCapVertices;
    /// <summary>
    /// The minVertexDistance of this TrailRenderer
    /// </summary>
    public float minVertexDistance;
    /// <summary>
    /// The textureMode of this TrailRenderer
    /// </summary>
    public int textureMode;
    /// <summary>
    /// The alignment of this TrailRenderer
    /// </summary>
    public int alignment;
#if UNITY_2017_1_OR_NEWER
    /// <summary>
    /// The generateLightingData state of this TrailRenderer
    /// </summary>
    public bool generateLightingData;
#endif
    /// <summary>
    /// The enabled state of this TrailRenderer
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The shadowCastingMode of this TrailRenderer
    /// </summary>
    public int shadowCastingMode;
    /// <summary>
    /// The receiveShadows state of this TrailRenderer
    /// </summary>
    public bool receiveShadows;
    /// <summary>
    /// The material of this TrailRenderer
    /// </summary>
    public EMaterial material;
    /// <summary>
    /// The sharedMaterial of this TrailRenderer
    /// </summary>
    public EMaterial sharedMaterial;
    /// <summary>
    /// The materials of this TrailRenderer
    /// </summary>
    public EMaterial[] materials;
    /// <summary>
    /// The sharedMaterials of this TrailRenderer
    /// </summary>
    public EMaterial[] sharedMaterials;
    /// <summary>
    /// The lightmapIndex of this TrailRenderer
    /// </summary>
    public int lightmapIndex;
    /// <summary>
    /// The realtimeLightmapIndex of this TrailRenderer
    /// </summary>
    public int realtimeLightmapIndex;
    /// <summary>
    /// The lightmapScaleOffset of this TrailRenderer
    /// </summary>
    public EVector4 lightmapScaleOffset;
    /// <summary>
    /// The motionVectorGenerationMode of this TrailRenderer
    /// </summary>
    public int motionVectorGenerationMode;
    /// <summary>
    /// The realtimeLightmapScaleOffset of this TrailRenderer
    /// </summary>
    public EVector4 realtimeLightmapScaleOffset;
    /// <summary>
    /// The lightProbeUsage of this TrailRenderer
    /// </summary>
    public int lightProbeUsage;
    /// <summary>
    /// The lightProbeProxyVolumeOverride of this TrailRenderer
    /// </summary>
    public EGameObject lightProbeProxyVolumeOverride;
    /// <summary>
    /// The probeAnchor of this TrailRenderer
    /// </summary>
    public ETransform probeAnchor;
    /// <summary>
    /// The reflectionProbeUsage of this TrailRenderer
    /// </summary>
    public int reflectionProbeUsage;
    /// <summary>
    /// The sortingLayerName of this TrailRenderer
    /// </summary>
    public string sortingLayerName;
    /// <summary>
    /// The sortingLayerID of this TrailRenderer
    /// </summary>
    public int sortingLayerID;
    /// <summary>
    /// The sortingOrder of this TrailRenderer
    /// </summary>
    public int sortingOrder;
    /// <summary>
    /// The tag of this TrailRenderer
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this TrailRenderer
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this TrailRenderer
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ETrailRenderer
    /// </summary>
    public ETrailRenderer() : base(true)
    {

    }

    /// <summary>
    /// Create a new ETrailRenderer
    /// </summary>
    /// <param name="reference">The TrailRenderer you want to use</param>
    public ETrailRenderer(TrailRenderer reference) : base(true)
    {
        this.time = reference.time;
        this.startWidth = reference.startWidth;
        this.endWidth = reference.endWidth;

        if (reference.widthCurve != null)
        {
            this.widthCurve = new EAnimationCurve(reference.widthCurve);
        }

        this.widthMultiplier = reference.widthMultiplier;
        this.startColor = new EColor(reference.startColor);
        this.endColor = new EColor(reference.endColor);
        this.colorGradient = new EGradient(reference.colorGradient);
        this.autodestruct = reference.autodestruct;
        this.numCornerVertices = reference.numCornerVertices;
        this.numCapVertices = reference.numCapVertices;
        this.minVertexDistance = reference.minVertexDistance;
        this.textureMode = (int)reference.textureMode;
        this.alignment = (int)reference.alignment;
#if UNITY_2017_1_OR_NEWER
        this.generateLightingData = reference.generateLightingData;
#endif
        this.enabled = reference.enabled;
        this.shadowCastingMode = (int)reference.shadowCastingMode;
        this.receiveShadows = reference.receiveShadows;

        if(reference.material != null)
        {
            this.material = new EMaterial(reference.material);
        }

        if (reference.sharedMaterial != null)
        {
            this.sharedMaterial = new EMaterial(reference.sharedMaterial);
        }

        if(reference.materials != null && reference.materials.Length > 0)
        {
            this.materials = new EMaterial[reference.materials.Length];
            for(int i = 0;i< reference.materials.Length; i++)
            {
                this.materials[i] = new EMaterial(reference.materials[i]);
            }
        }

        if (reference.sharedMaterials != null && reference.sharedMaterials.Length > 0)
        {
            this.sharedMaterials = new EMaterial[reference.sharedMaterials.Length];
            for (int i = 0; i < reference.sharedMaterials.Length; i++)
            {
                this.sharedMaterials[i] = new EMaterial(reference.sharedMaterials[i]);
            }
        }

        this.lightmapIndex = reference.lightmapIndex;
        this.realtimeLightmapIndex = reference.realtimeLightmapIndex;
        this.lightmapScaleOffset = new EVector4(reference.lightmapScaleOffset);
        this.motionVectorGenerationMode = (int)reference.motionVectorGenerationMode;
        this.realtimeLightmapScaleOffset = new EVector4(reference.realtimeLightmapScaleOffset);
        this.lightProbeUsage = (int)reference.lightProbeUsage;

        if(reference.lightProbeProxyVolumeOverride != null)
        {
            this.lightProbeProxyVolumeOverride = new EGameObject(reference.lightProbeProxyVolumeOverride);
        }

        if(reference.probeAnchor != null)
        {
            this.probeAnchor = new ETransform(reference.probeAnchor);
        }

        this.reflectionProbeUsage = (int)reference.reflectionProbeUsage;
        this.sortingLayerName = reference.sortingLayerName;
        this.sortingLayerID = reference.sortingLayerID;
        this.sortingOrder = reference.sortingOrder;
        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref TrailRenderer reference)
    {
        reference.time = this.time;
        reference.startWidth = this.startWidth;
        reference.endWidth = this.endWidth;

        if (this.widthCurve != null)
        {
            if (reference.widthCurve != null)
            {
                AnimationCurve curve = reference.widthCurve;
                this.widthCurve.SetData(ref curve);
            }
            else
            {
                reference.widthCurve = this.widthCurve.ToUnityType();
            }
        }

        reference.widthMultiplier = this.widthMultiplier;
        reference.startColor = this.startColor.ToUnityType();
        reference.endColor = this.endColor.ToUnityType();
        reference.colorGradient = this.colorGradient.ToUnityType();
        reference.autodestruct = this.autodestruct;
        reference.numCornerVertices = this.numCornerVertices;
        reference.numCapVertices = this.numCapVertices;
        reference.minVertexDistance = this.minVertexDistance;
        reference.textureMode = (LineTextureMode)this.textureMode;
        reference.alignment = (LineAlignment)this.alignment;
#if UNITY_2017_1_OR_NEWER
        reference.generateLightingData = this.generateLightingData;
#endif
        reference.enabled = this.enabled;
        reference.shadowCastingMode = (ShadowCastingMode)this.shadowCastingMode;
        reference.receiveShadows = this.receiveShadows;

        if (this.material != null)
        {
            if (reference.material != null)
            {
                Material mat = reference.material;
                this.material.SetData(ref mat);
            }
            else
            {
                reference.material = this.material.ToUnityType();
            }
        }

        if (this.sharedMaterial != null)
        {
            if (reference.sharedMaterial != null)
            {
                Material mat = reference.sharedMaterial;
                this.sharedMaterial.SetData(ref mat);
            }
            else
            {
                reference.sharedMaterial = this.sharedMaterial.ToUnityType();
            }
        }

        if (this.materials != null && this.materials.Length > 0)
        {
            reference.materials = new Material[this.materials.Length];
            for (int i = 0; i < this.materials.Length; i++)
            {
                reference.materials[i] = this.materials[i].ToUnityType();
            }
        }

        if (this.sharedMaterials != null && this.sharedMaterials.Length > 0)
        {
            reference.sharedMaterials = new Material[this.sharedMaterials.Length];
            for (int i = 0; i < this.sharedMaterials.Length; i++)
            {
                reference.sharedMaterials[i] = this.sharedMaterials[i].ToUnityType();
            }
        }

        reference.lightmapIndex = this.lightmapIndex;
        reference.realtimeLightmapIndex = this.realtimeLightmapIndex;
        reference.lightmapScaleOffset = this.lightmapScaleOffset.ToUnityType();
        reference.motionVectorGenerationMode = (MotionVectorGenerationMode)this.motionVectorGenerationMode;
        reference.realtimeLightmapScaleOffset = this.realtimeLightmapScaleOffset.ToUnityType();
        reference.lightProbeUsage = (LightProbeUsage)this.lightProbeUsage;

        if (this.lightProbeProxyVolumeOverride != null)
        {
            if (reference.lightProbeProxyVolumeOverride != null)
            {
                GameObject go = reference.lightProbeProxyVolumeOverride;
                this.lightProbeProxyVolumeOverride.SetData(ref go);
            }
            else
            {
                reference.lightProbeProxyVolumeOverride = this.lightProbeProxyVolumeOverride.ToUnityType();
            }
        }

        if (this.probeAnchor != null)
        {
            if (reference.probeAnchor != null)
            {
                Transform trans = reference.probeAnchor;
                this.probeAnchor.SetData(ref trans);
            }
        }

        reference.reflectionProbeUsage = (ReflectionProbeUsage)this.reflectionProbeUsage;
        reference.sortingLayerName = this.sortingLayerName;
        reference.sortingLayerID = this.sortingLayerID;
        reference.sortingOrder = this.sortingOrder;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override TrailRenderer ToUnityType()
    {
        TrailRenderer renderer = new TrailRenderer();
        SetData(ref renderer);
        return renderer;
    }
}
