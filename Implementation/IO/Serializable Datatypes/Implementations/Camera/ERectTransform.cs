﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a RectTransform
/// </summary>
[System.Serializable]
public class ERectTransform : SerializableUnityData<RectTransform>
{
    /// <summary>
    /// The anchorMin of this RectTransform
    /// </summary>
    public EVector2 anchorMin;
    /// <summary>
    /// The anchorMax of this RectTransform
    /// </summary>
    public EVector2 anchorMax;
    /// <summary>
    /// The anchoredPosition3D of this RectTransform
    /// </summary>
    public EVector3 anchoredPosition3D;
    /// <summary>
    /// The anchoredPosition of this RectTransform
    /// </summary>
    public EVector2 anchoredPosition;
    /// <summary>
    /// The sizeDelta of this RectTransform
    /// </summary>
    public EVector2 sizeDelta;
    /// <summary>
    /// The pivot of this RectTransform
    /// </summary>
    public EVector2 pivot;
    /// <summary>
    /// The offsetMin of this RectTransform
    /// </summary>
    public EVector2 offsetMin;
    /// <summary>
    /// The offsetMax of this RectTransform
    /// </summary>
    public EVector2 offsetMax;
    /// <summary>
    /// The position of this RectTransform
    /// </summary>
    public EVector3 position;
    /// <summary>
    /// The localPosition of this RectTransform
    /// </summary>
    public EVector3 localPosition;
    /// <summary>
    /// The eulerAngles of this RectTransform
    /// </summary>
    public EVector3 eulerAngles;
    /// <summary>
    /// The localEulerAngles of this RectTransform
    /// </summary>
    public EVector3 localEulerAngles;
    /// <summary>
    /// The right of this RectTransform
    /// </summary>
    public EVector3 right;
    /// <summary>
    /// The up of this RectTransform
    /// </summary>
    public EVector3 up;
    /// <summary>
    /// The forward of this RectTransform
    /// </summary>
    public EVector3 forward;
    /// <summary>
    /// The rotation of this RectTransform
    /// </summary>
    public EVector3 rotation;
    /// <summary>
    /// The localRotation of this RectTransform
    /// </summary>
    public EQuaternion localRotation;
    /// <summary>
    /// The localScale of this RectTransform
    /// </summary>
    public EVector3 localScale;
    /// <summary>
    /// The parent of this RectTransform
    /// </summary>
    public ETransform parent;
    /// <summary>
    /// The hasChanged state of this RectTransform
    /// </summary>
    public bool hasChanged;
    /// <summary>
    /// The hierarchyCapacity of this RectTransform
    /// </summary>
    public int hierarchyCapacity;
    /// <summary>
    /// The tag of this RectTransform
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this RectTransform
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this RectTransform
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new ERectTransform
    /// </summary>
    public ERectTransform() : base(false)
    {

    }

    /// <summary>
    /// Create a new ERectTransform
    /// </summary>
    /// <param name="col">The RectTransform you want to use</param>
    public ERectTransform(RectTransform rect) : base(false)
    {
        this.anchorMin = new EVector2(rect.anchorMin);
        this.anchorMax = new EVector2(rect.anchorMax);
        this.anchoredPosition3D = new EVector3(rect.anchoredPosition3D);
        this.anchoredPosition = new EVector2(rect.anchoredPosition);
        this.sizeDelta = new EVector2(rect.sizeDelta);
        this.pivot = new EVector2(rect.pivot);
        this.offsetMin = new EVector2(rect.offsetMin);
        this.offsetMax = new EVector2(rect.offsetMax);
        this.position = new EVector3(rect.position);
        this.localPosition = new EVector3(rect.localPosition);
        this.eulerAngles = new EVector3(rect.eulerAngles);
        this.localEulerAngles = new EVector3(rect.localEulerAngles);
        this.right = new EVector3(rect.right);
        this.up = new EVector3(rect.up);
        this.forward = new EVector3(rect.forward);
        this.localRotation = new EQuaternion(rect.localRotation);
        this.localScale = new EVector3(rect.localScale);
        this.parent = new ETransform(rect.parent);
        this.hasChanged = rect.hasChanged;
        this.hierarchyCapacity = rect.hierarchyCapacity;
        this.tag = rect.tag;
        this.name = rect.name;
        this.hideFlags = (int)rect.hideFlags;
    }

    public override void SetData(ref RectTransform reference)
    {
        reference.anchorMin = this.anchorMin;
        reference.anchorMax = this.anchorMax;
        reference.anchoredPosition3D = this.anchoredPosition3D;
        reference.anchoredPosition = this.anchoredPosition;
        reference.sizeDelta = this.sizeDelta;
        reference.pivot = this.pivot;
        reference.offsetMin = this.offsetMin;
        reference.offsetMax = this.offsetMax;
        reference.position = this.position;
        reference.localPosition = this.localPosition;
        reference.eulerAngles = this.eulerAngles;
        reference.localEulerAngles = this.localEulerAngles;
        reference.right = this.right;
        reference.up = this.up;
        reference.forward = this.forward;
        reference.localRotation = this.localRotation;
        reference.localScale = this.localScale;
        if(this.parent != null)
        {
          Transform trans = reference.parent;
          this.parent.SetData(ref trans);
        }
        reference.hasChanged = this.hasChanged;
        reference.hierarchyCapacity = this.hierarchyCapacity;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override RectTransform ToUnityType()
    {
        throw new System.NotImplementedException();
    }
}
