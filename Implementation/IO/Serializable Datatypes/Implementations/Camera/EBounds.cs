﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a Bounds
/// </summary>
[System.Serializable]
public class EBounds : SerializableUnityData<Bounds>
{
    /// <summary>
    /// The center of this bounds
    /// </summary>
    public EVector3 center;
    /// <summary>
    /// The size of this bounds
    /// </summary>
    public EVector3 size;
    /// <summary>
    /// The extents of this bounds
    /// </summary>
    public EVector3 extents;
    /// <summary>
    /// The min of this bounds
    /// </summary>
    public EVector3 min;
    /// <summary>
    /// The max of this bounds
    /// </summary>
    public EVector3 max;

    /// <summary>
    /// Create a new EBounds
    /// </summary>
    public EBounds() : base(true)
    {

    }

    /// <summary>
    /// Create a new EBounds
    /// </summary>
    /// <param name="bounds">The Bounds you want to use</param>
    public EBounds(Bounds bounds) : base(true)
    {
        this.center = new EVector3(bounds.center);
        this.size = new EVector3(bounds.size);
        this.extents = new EVector3(bounds.extents);
        this.min = new EVector3(bounds.min);
        this.max = new EVector3(bounds.max);
    }

    public override void SetData(ref Bounds reference)
    {
        reference.center = this.center;
        reference.size = this.size;
        reference.extents = this.extents;
        reference.min = this.min;
        reference.max = this.max;
    }

    public override Bounds ToUnityType()
    {
        Bounds bounds = new Bounds
        {
            center = this.center,
            size = this.size,
            extents = this.extents,
            min = this.min,
            max = this.max
        };
        return bounds;
    }
}
