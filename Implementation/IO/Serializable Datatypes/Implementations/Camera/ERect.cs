﻿using UnityEngine;
using UnityEngine.Rendering;
using EAssets.IO.SerializableDatatypes;

/// <summary>
/// This can be used to serialize a rect
/// </summary>
[System.Serializable]
public class ERect : SerializableUnityData<Rect>
{
    /// <summary>
    /// The x position of this rect
    /// </summary>
    public float x;
    /// <summary>
    /// The y position of this rect
    /// </summary>
    public float y;
    /// <summary>
    /// The position of this rect
    /// </summary>
    public EVector2 position;
    /// <summary>
    /// The center of this rect
    /// </summary>
    public EVector2 center;
    /// <summary>
    /// The min position of this rect
    /// </summary>
    public EVector2 min;
    /// <summary>
    /// The max position of this rect
    /// </summary>
    public EVector2 max;
    /// <summary>
    /// The width of this rect
    /// </summary>
    public float width;
    /// <summary>
    /// The height of this rect
    /// </summary>
    public float height;
    /// <summary>
    /// The size of this rect
    /// </summary>
    public EVector2 size;
    /// <summary>
    /// The min x size of this rect
    /// </summary>
    public float xMin;
    /// <summary>
    /// The min y size of this rect
    /// </summary>
    public float yMin;
    /// <summary>
    /// The max x size of this rect
    /// </summary>
    public float xMax;
    /// <summary>
    /// The max y size of this rect
    /// </summary>
    public float yMax;

    /// <summary>
    /// Create a new ERect
    /// </summary>
    public ERect() : base(true)
    {

    }

    /// <summary>
    /// Create a new ERect
    /// </summary>
    /// <param name="rect">The rect you want to use</param>
    public ERect(Rect rect) : base(true)
    {
        this.x = rect.x;
        this.y = rect.y;
        this.position = new EVector2(rect.position);
        this.center = new EVector2(rect.center);
        this.min = new EVector2(rect.min);
        this.max = new EVector2(rect.max);
        this.width = rect.width;
        this.height = rect.height;
        this.size = new EVector2(rect.size);
        this.xMin = rect.xMin;
        this.yMin = rect.yMin;
        this.xMax = rect.xMax;
        this.yMax = rect.yMax;
    }

    public override void SetData(ref Rect reference)
    {
        reference.x = this.x;
        reference.y = this.y;
        reference.position = this.position;
        reference.center = this.center;
        reference.min = this.min;
        reference.max = this.max;
        reference.width = this.width;
        reference.height = this.height;
        reference.size = this.size;
        reference.xMin = this.xMin;
        reference.yMin = this.yMin;
        reference.xMax = this.xMax;
        reference.yMax = this.yMax;
    }

    public override Rect ToUnityType()
    {
        Rect rect = new Rect();
        SetData(ref rect);
        return rect;
    }

    public override string ToString()
    {
        return "(" + "x:" + x + ", y:" + y + ", width:" + width + ", height:" + height + ")";
    }
}
