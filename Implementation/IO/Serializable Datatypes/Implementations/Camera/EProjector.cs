﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a Projector
/// </summary>
[System.Serializable]
public class EProjector : SerializableUnityData<Projector>
{
    /// <summary>
    /// The nearClipPlane of this Projector
    /// </summary>
    public float nearClipPlane;
    /// <summary>
    /// The farClipPlane of this Projector
    /// </summary>
    public float farClipPlane;
    /// <summary>
    /// The fieldOfView of this Projector
    /// </summary>
    public float fieldOfView;
    /// <summary>
    /// The aspectRatio of this Projector
    /// </summary>
    public float aspectRatio;
    /// <summary>
    /// The orthographic state of this Projector
    /// </summary>
    public bool orthographic;
    /// <summary>
    /// The orthographicSize of this Projector
    /// </summary>
    public float orthographicSize;
    /// <summary>
    /// The ignoreLayers of this Projector
    /// </summary>
    public int ignoreLayers;
    /// <summary>
    /// The material of this Projector
    /// </summary>
    public EMaterial material;
    /// <summary>
    /// The enabled state of this Projector
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The tag of this Projector
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this Projector
    public string name;
    /// <summary>
    /// The hideFlags of this Projector
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EProjector
    /// </summary>
    public EProjector() : base(true)
    {

    }

    /// <summary>
    /// Create a new EProjector
    /// </summary>
    /// <param name="reference">The Projector you want to use</param>
    public EProjector(Projector reference) : base(true)
    {
        this.nearClipPlane = reference.nearClipPlane;
        this.farClipPlane = reference.farClipPlane;
        this.fieldOfView = reference.fieldOfView;
        this.aspectRatio = reference.aspectRatio;
        this.orthographic = reference.orthographic;
        this.orthographicSize = reference.orthographicSize;
        this.ignoreLayers = reference.ignoreLayers;

        if (reference.material != null)
        {
            this.material = new EMaterial(reference.material);
        }

        this.enabled = reference.enabled;
        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref Projector reference)
    {
        reference.nearClipPlane = this.nearClipPlane;
        reference.farClipPlane = this.farClipPlane;
        reference.fieldOfView = this.fieldOfView;
        reference.aspectRatio = this.aspectRatio;
        reference.orthographic = this.orthographic;
        reference.orthographicSize = this.orthographicSize;
        reference.ignoreLayers = this.ignoreLayers;

        if (this.material != null)
        {
            if (reference.material != null)
            {
                Material mat = reference.material;
                this.material.SetData(ref mat);
            }
            else
            {
                reference.material = this.material.ToUnityType();
            }
        }

        reference.enabled = this.enabled;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;
    }

    public override Projector ToUnityType()
    {
        Projector projector = new Projector();
        SetData(ref projector);
        return projector;
    }
}
