﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;
using EAssets.Extensions.Objects;

/// <summary>
/// This class can be used to serialize a ParticleSystem
/// </summary>
[System.Serializable]
public class EParticleSystem : SerializableUnityData<ParticleSystem>
{
    /// <summary>
    /// The collision of this ParticleSystem
    /// </summary>
    public ECollisionModule collision;
    /// <summary>
    /// The colorBySpeed of this ParticleSystem
    /// </summary>
    public EColorBySpeedModule colorBySpeed;
    /// <summary>
    /// The colorOverLifetime of this ParticleSystem
    /// </summary>
    public EColorOverLifetimeModule colorOverLifetime;
    /// <summary>
    /// The customData of this ParticleSystem
    /// </summary>
    public ECustomDataModule customData;
    /// <summary>
    /// The emission of this ParticleSystem
    /// </summary>
    public EEmissionModule emission;
    /// <summary>
    /// The externalForces of this ParticleSystem
    /// </summary>
    public EExternalForcesModule externalForces;
    /// <summary>
    /// The forceOverLifetime of this ParticleSystem
    /// </summary>
    public EForceOverLifetimeModule forceOverLifetime;
    /// <summary>
    /// The inheritVelocity of this ParticleSystem
    /// </summary>
    public EInheritVelocityModule inheritVelocity;
    /// <summary>
    /// The lights of this ParticleSystem
    /// </summary>
    public ELightsModule lights;
    /// <summary>
    /// The limitVelocityOverLifetime of this ParticleSystem
    /// </summary>
    public ELimitVelocityOverLifetimeModule limitVelocityOverLifetime;
    /// <summary>
    /// The main of this ParticleSystem
    /// </summary>
    public EMainModule main;
    /// <summary>
    /// The noise of this ParticleSystem
    /// </summary>
    public ENoiseModule noise;
    /// <summary>
    /// The rotationBySpeed of this ParticleSystem
    /// </summary>
    public ERotationBySpeedModule rotationBySpeed;
    /// <summary>
    /// The rotationOverLifetime of this ParticleSystem
    /// </summary>
    public ERotationOverLifetimeModule rotationOverLifetime;
    /// <summary>
    /// The shape of this ParticleSystem
    /// </summary>
    public EShapeModule shape;
    /// <summary>
    /// The sizeBySpeed of this ParticleSystem
    /// </summary>
    public ESizeBySpeedModule sizeBySpeed;
    /// <summary>
    /// The sizeOverLifetime of this ParticleSystem
    /// </summary>
    public ESizeOverLifetimeModule sizeOverLifetime;
    /// <summary>
    /// The subEmitters of this ParticleSystem
    /// </summary>
    public ESubEmittersModule subEmitters;
    /// <summary>
    /// The textureSheetAnimation of this ParticleSystem
    /// </summary>
    public ETextureSheetAnimationModule textureSheetAnimation;
    /// <summary>
    /// The trails of this ParticleSystem
    /// </summary>
    public ETrailModule trails;
    /// <summary>
    /// The trigger of this ParticleSystem
    /// </summary>
    public ETriggerModule trigger;
    /// <summary>
    /// The velocityOverLifetime of this ParticleSystem
    /// </summary>
    public EVelocityOverLifetimeModule velocityOverLifetime;
    /// <summary>
    /// The time of this ParticleSystem
    /// </summary>
    public float time;
    /// <summary>
    /// The randomSeed of this ParticleSystem
    /// </summary>
    public uint randomSeed;
    /// <summary>
    /// The useAutoRandomSeed state of this ParticleSystem
    /// </summary>
    public bool useAutoRandomSeed;
    /// <summary>
    /// The tag of this ParticleSystem
    /// </summary>
    public string tag;
    /// <summary>
    /// The name of this ParticleSystem
    /// </summary>
    public string name;
    /// <summary>
    /// The hideFlags of this ParticleSystem
    /// </summary>
    public int hideFlags;

    /// <summary>
    /// Create a new EParticleSystem
    /// </summary>
    public EParticleSystem() : base(true)
    {

    }

    /// <summary>
    /// Create a new EParticleSystem
    /// </summary>
    /// <param name="reference">The ParticleSystem you want to use</param>
    public EParticleSystem(ParticleSystem reference) : base(true)
    {
        //Get all the modules
        this.collision = new ECollisionModule(reference.collision);
        this.colorBySpeed = new EColorBySpeedModule(reference.colorBySpeed);
        this.colorOverLifetime = new EColorOverLifetimeModule(reference.colorOverLifetime);
        this.customData = new ECustomDataModule(reference.customData);
        this.emission = new EEmissionModule(reference.emission);
        this.externalForces = new EExternalForcesModule(reference.externalForces);
        this.forceOverLifetime = new EForceOverLifetimeModule(reference.forceOverLifetime);
        this.inheritVelocity = new EInheritVelocityModule(reference.inheritVelocity);
        this.lights = new ELightsModule(reference.lights);
        this.limitVelocityOverLifetime = 
            new ELimitVelocityOverLifetimeModule(reference.limitVelocityOverLifetime);
        this.main = new EMainModule(reference.main);
        this.noise = new ENoiseModule(reference.noise);
        this.rotationBySpeed = new ERotationBySpeedModule(reference.rotationBySpeed);
        this.rotationOverLifetime = new ERotationOverLifetimeModule(reference.rotationOverLifetime);
        this.shape = new EShapeModule(reference.shape);
        this.sizeBySpeed = new ESizeBySpeedModule(reference.sizeBySpeed);
        this.sizeOverLifetime = new ESizeOverLifetimeModule(reference.sizeOverLifetime);
        this.subEmitters = new ESubEmittersModule(reference.subEmitters);
        this.textureSheetAnimation = new ETextureSheetAnimationModule(reference.textureSheetAnimation);
        this.trails = new ETrailModule(reference.trails);
        this.trigger = new ETriggerModule(reference.trigger);
        this.velocityOverLifetime = new EVelocityOverLifetimeModule(reference.velocityOverLifetime);

        //Get the simple data
        this.time = reference.time;
        this.randomSeed = reference.randomSeed;
        this.useAutoRandomSeed = reference.useAutoRandomSeed;
        this.tag = reference.tag;
        this.name = reference.name;
        this.hideFlags = (int)reference.hideFlags;
    }

    public override void SetData(ref ParticleSystem reference)
    {
        //Check if the ParticleSystem is currently running
        bool running = reference.isPlaying;
        //Stop it to change its values (reactivate later if it was running)
        reference.Stop();

        //Set the modules via reflection since they are read-only
        if (this.collision != null)
        {
            ParticleSystem.CollisionModule col = reference.collision;
            this.collision.SetData(ref col);
            reference.SetPropertyValue("collision", col);
        }

        if (this.colorBySpeed != null)
        {
            ParticleSystem.ColorBySpeedModule col = reference.colorBySpeed;
            this.colorBySpeed.SetData(ref col);
            reference.SetPropertyValue("colorBySpeed", col);
        }

        if (this.colorOverLifetime != null)
        {
            ParticleSystem.ColorOverLifetimeModule col = reference.colorOverLifetime;
            this.colorOverLifetime.SetData(ref col);
            reference.SetPropertyValue("colorOverLifetime", col);
        }

        if (this.customData != null)
        {
            ParticleSystem.CustomDataModule col = reference.customData;
            this.customData.SetData(ref col);
            reference.SetPropertyValue("customData", col);
        }

        if (this.emission != null)
        {
            ParticleSystem.EmissionModule col = reference.emission;
            this.emission.SetData(ref col);
            reference.SetPropertyValue("emission", col);
        }

        if (this.externalForces != null)
        {
            ParticleSystem.ExternalForcesModule col = reference.externalForces;
            this.externalForces.SetData(ref col);
            reference.SetPropertyValue("externalForces", col);
        }

        if (this.forceOverLifetime != null)
        {
            ParticleSystem.ForceOverLifetimeModule col = reference.forceOverLifetime;
            this.forceOverLifetime.SetData(ref col);
            reference.SetPropertyValue("forceOverLifetime", col);
        }

        if (this.inheritVelocity != null)
        {
            ParticleSystem.InheritVelocityModule col = reference.inheritVelocity;
            this.inheritVelocity.SetData(ref col);
            reference.SetPropertyValue("inheritVelocity", col);
        }

        if (this.lights != null)
        {
            ParticleSystem.LightsModule col = reference.lights;
            this.lights.SetData(ref col);
            reference.SetPropertyValue("lights", col);
        }

        if (this.limitVelocityOverLifetime != null)
        {
            ParticleSystem.LimitVelocityOverLifetimeModule col = reference.limitVelocityOverLifetime;
            this.limitVelocityOverLifetime.SetData(ref col);
            reference.SetPropertyValue("limitVelocityOverLifetime", col);
        }

        if (this.main != null)
        {
            ParticleSystem.MainModule col = reference.main;
            this.main.SetData(ref col);
            reference.SetPropertyValue("main", col);
        }


        if (this.noise != null)
        {
            ParticleSystem.NoiseModule col = reference.noise;
            this.noise.SetData(ref col);
            reference.SetPropertyValue("noise", col);
        }

        if (this.rotationBySpeed != null)
        {
            ParticleSystem.RotationBySpeedModule col = reference.rotationBySpeed;
            this.rotationBySpeed.SetData(ref col);
            reference.SetPropertyValue("rotationBySpeed", col);
        }

        if (this.rotationOverLifetime != null)
        {
            ParticleSystem.RotationOverLifetimeModule col = reference.rotationOverLifetime;
            this.rotationOverLifetime.SetData(ref col);
            reference.SetPropertyValue("rotationOverLifetime", col);
        }

        if (this.shape != null)
        {
            ParticleSystem.ShapeModule col = reference.shape;
            this.shape.SetData(ref col);
            reference.SetPropertyValue("shape", col);
        }

        if (this.sizeBySpeed != null)
        {
            ParticleSystem.SizeBySpeedModule col = reference.sizeBySpeed;
            this.sizeBySpeed.SetData(ref col);
            reference.SetPropertyValue("sizeBySpeed", col);
        }

        if (this.sizeOverLifetime != null)
        {
            ParticleSystem.SizeOverLifetimeModule col = reference.sizeOverLifetime;
            this.sizeOverLifetime.SetData(ref col);
            reference.SetPropertyValue("sizeOverLifetime", col);
        }

        if (this.subEmitters != null)
        {
            ParticleSystem.SubEmittersModule col = reference.subEmitters;
            this.subEmitters.SetData(ref col);
            reference.SetPropertyValue("subEmitters", col);
        }

        if (this.textureSheetAnimation != null)
        {
            ParticleSystem.TextureSheetAnimationModule col = reference.textureSheetAnimation;
            this.textureSheetAnimation.SetData(ref col);
            reference.SetPropertyValue("textureSheetAnimation", col);
        }

        if (this.trails != null)
        {
            ParticleSystem.TrailModule col = reference.trails;
            this.trails.SetData(ref col);
            reference.SetPropertyValue("trails", col);
        }

        if (this.trigger != null)
        {
            ParticleSystem.TriggerModule col = reference.trigger;
            this.trigger.SetData(ref col);
            reference.SetPropertyValue("trigger", col);
        }

        if (this.velocityOverLifetime != null)
        {
            ParticleSystem.VelocityOverLifetimeModule col = reference.velocityOverLifetime;
            this.velocityOverLifetime.SetData(ref col);
            reference.SetPropertyValue("velocityOverLifetime", col);
        }

        //Set the simple types
        reference.time = this.time;
        reference.randomSeed = this.randomSeed;
        reference.useAutoRandomSeed = this.useAutoRandomSeed;
        reference.tag = this.tag;
        reference.name = this.name;
        reference.hideFlags = (HideFlags)this.hideFlags;

        if (running) //Restart a running system
            reference.Play();
    }

    public override ParticleSystem ToUnityType()
    {
        ParticleSystem sys = new ParticleSystem();
        SetData(ref sys);
        return sys;
    }
}
