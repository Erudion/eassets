﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a RotationBySpeedModule
/// </summary>
[System.Serializable]
public class ERotationBySpeedModule : SerializableUnityData<ParticleSystem.RotationBySpeedModule>
{
    /// <summary>
    /// The enabled state of this ERotationBySpeedModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The x of this ERotationBySpeedModule
    /// </summary>
    public EMinMaxCurve x;
    /// <summary>
    /// The xMultiplier of this ERotationBySpeedModule
    /// </summary>
    public float xMultiplier;
    /// <summary>
    /// The y of this ERotationBySpeedModule
    /// </summary>
    public EMinMaxCurve y;
    /// <summary>
    /// The yMultiplier of this ERotationBySpeedModule
    /// </summary>
    public float yMultiplier;
    /// <summary>
    /// The z of this ERotationBySpeedModule
    /// </summary>
    public EMinMaxCurve z;
    /// <summary>
    /// The zMultiplier of this ERotationBySpeedModule
    /// </summary>
    public float zMultiplier;
    /// <summary>
    /// The separateAxes state of this ERotationBySpeedModule
    /// </summary>
    public bool separateAxes;
    /// <summary>
    /// The range of this ERotationBySpeedModule
    /// </summary>
    public EVector2 range;


    /// <summary>
    /// Create a new ERotationBySpeedModule
    /// </summary>
    public ERotationBySpeedModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new ERotationBySpeedModule
    /// </summary>
    /// <param name="reference">The RotationBySpeedModule you want to use</param>
    public ERotationBySpeedModule(ParticleSystem.RotationBySpeedModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.x = new EMinMaxCurve(reference.x);
        this.xMultiplier = reference.xMultiplier;
        this.y = new EMinMaxCurve(reference.y);
        this.yMultiplier = reference.yMultiplier;
        this.z = new EMinMaxCurve(reference.z);
        this.zMultiplier = reference.zMultiplier;
        this.separateAxes = reference.separateAxes;
        this.range = new EVector2(reference.range);
    }

    public override void SetData(ref ParticleSystem.RotationBySpeedModule reference)
    {
        reference.enabled = this.enabled;

        if (this.x != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.x;
            this.x.SetData(ref curve);
            reference.x = curve;
        }

        reference.xMultiplier = this.xMultiplier;

        if (this.y != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.y;
            this.y.SetData(ref curve);
            reference.y = curve;
        }

        reference.yMultiplier = this.yMultiplier;

        if (this.z != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.z;
            this.z.SetData(ref curve);
            reference.z = curve;
        }

        reference.zMultiplier = this.zMultiplier;
        reference.separateAxes = this.separateAxes;
        reference.range = this.range;
    }

    public override ParticleSystem.RotationBySpeedModule ToUnityType()
    {
        ParticleSystem.RotationBySpeedModule module = new ParticleSystem.RotationBySpeedModule();
        SetData(ref module);
        return module;
    }
}
