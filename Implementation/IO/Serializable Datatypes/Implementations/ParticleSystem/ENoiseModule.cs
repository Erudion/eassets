﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a NoiseModule
/// </summary>
[System.Serializable]
public class ENoiseModule : SerializableUnityData<ParticleSystem.NoiseModule>
{
    /// <summary>
    /// The enabled state of this ENoiseModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The separateAxes state of this ENoiseModule
    /// </summary>
    public bool separateAxes;
    /// <summary>
    /// The strength of this ENoiseModule
    /// </summary>
    public EMinMaxCurve strength;
    /// <summary>
    /// The strengthMultiplier of this ENoiseModule
    /// </summary>
    public float strengthMultiplier;
    /// <summary>
    /// The strengthX of this ENoiseModule
    /// </summary>
    public EMinMaxCurve strengthX;
    /// <summary>
    /// The strengthXMultiplier of this ENoiseModule
    /// </summary>
    public float strengthXMultiplier;
    /// <summary>
    /// The strengthY of this ENoiseModule
    /// </summary>
    public EMinMaxCurve strengthY;
    /// <summary>
    /// The strengthYMultiplier of this ENoiseModule
    /// </summary>
    public float strengthYMultiplier;
    /// <summary>
    /// The strengthZ of this ENoiseModule
    /// </summary>
    public EMinMaxCurve strengthZ;
    /// <summary>
    /// The strengthZMultiplier of this ENoiseModule
    /// </summary>
    public float strengthZMultiplier;
    /// <summary>
    /// The frequency of this ENoiseModule
    /// </summary>
    public float frequency;
    /// <summary>
    /// The damping state of this ENoiseModule
    /// </summary>
    public bool damping;
    /// <summary>
    /// The octaveCount of this ENoiseModule
    /// </summary>
    public int octaveCount;
    /// <summary>
    /// The octaveMultiplier of this ENoiseModule
    /// </summary>
    public float octaveMultiplier;
    /// <summary>
    /// The octaveScale of this ENoiseModule
    /// </summary>
    public float octaveScale;
    /// <summary>
    /// The quality of this ENoiseModule
    /// </summary>
    public int quality;
    /// <summary>
    /// The scrollSpeed of this ENoiseModule
    /// </summary>
    public EMinMaxCurve scrollSpeed;
    /// <summary>
    /// The scrollSpeedMultiplier of this ENoiseModule
    /// </summary>
    public float scrollSpeedMultiplier;
    /// <summary>
    /// The remapEnabled state of this ENoiseModule
    /// </summary>
    public bool remapEnabled;
    /// <summary>
    /// The remap of this ENoiseModule
    /// </summary>
    public EMinMaxCurve remap;
    /// <summary>
    /// The remapMultiplier of this ENoiseModule
    /// </summary>
    public float remapMultiplier;
    /// <summary>
    /// The remapX of this ENoiseModule
    /// </summary>
    public EMinMaxCurve remapX;
    /// <summary>
    /// The remapXMultiplier of this ENoiseModule
    /// </summary>
    public float remapXMultiplier;
    /// <summary>
    /// The remapY of this ENoiseModule
    /// </summary>
    public EMinMaxCurve remapY;
    /// <summary>
    /// The remapYMultiplier of this ENoiseModule
    /// </summary>
    public float remapYMultiplier;
    /// <summary>
    /// The remapZ of this ENoiseModule
    /// </summary>
    public EMinMaxCurve remapZ;
    /// <summary>
    /// The remapZMultiplier of this ENoiseModule
    /// </summary>
    public float remapZMultiplier;
#if UNITY_2017_1_OR_NEWER
    /// <summary>
    /// The positionAmount of this ENoiseModule
    /// </summary>
    public EMinMaxCurve positionAmount;
    /// <summary>
    /// The rotationAmount of this ENoiseModule
    /// </summary>
    public EMinMaxCurve rotationAmount;
    /// <summary>
    /// The sizeAmount of this ENoiseModule
    /// </summary>
    public EMinMaxCurve sizeAmount;
#endif

    /// <summary>
    /// Create a new ENoiseModule
    /// </summary>
    public ENoiseModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new ENoiseModule
    /// </summary>
    /// <param name="reference">The NoiseModule you want to use</param>
    public ENoiseModule(ParticleSystem.NoiseModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.separateAxes = reference.separateAxes;
        this.strength = new EMinMaxCurve(reference.strength);
        this.strengthMultiplier = reference.strengthMultiplier;
        this.strengthX = new EMinMaxCurve(reference.strengthX);
        this.strengthXMultiplier = reference.strengthXMultiplier;
        this.strengthY = new EMinMaxCurve(reference.strengthY);
        this.strengthYMultiplier = reference.strengthYMultiplier;
        this.strengthZ = new EMinMaxCurve(reference.strengthZ);
        this.strengthZMultiplier = reference.strengthZMultiplier;
        this.frequency = reference.frequency;
        this.damping = reference.damping;
        this.octaveCount = reference.octaveCount;
        this.octaveMultiplier = reference.octaveMultiplier;
        this.octaveScale = reference.octaveScale;
        this.quality = (int)reference.quality;
        this.scrollSpeed = new EMinMaxCurve(reference.scrollSpeed);
        this.scrollSpeedMultiplier = reference.scrollSpeedMultiplier;
        this.remapEnabled = reference.remapEnabled;
        this.remap = new EMinMaxCurve(reference.remap);
        this.remapMultiplier = reference.remapMultiplier;
        this.remapX = new EMinMaxCurve(reference.remapX);
        this.remapXMultiplier = reference.remapXMultiplier;
        this.remapY = new EMinMaxCurve(reference.remapY);
        this.remapYMultiplier = reference.remapYMultiplier;
        this.remapZ = new EMinMaxCurve(reference.remapZ);
        this.remapZMultiplier = reference.remapZMultiplier;
#if UNITY_2017_1_OR_NEWER
        this.positionAmount = new EMinMaxCurve(reference.positionAmount);
        this.rotationAmount = new EMinMaxCurve(reference.rotationAmount);
        this.sizeAmount = new EMinMaxCurve(reference.sizeAmount);
#endif
    }

    public override void SetData(ref ParticleSystem.NoiseModule reference)
    {
        reference.enabled = this.enabled;
        reference.separateAxes = this.separateAxes;

        if (this.strength != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.strength;
            this.strength.SetData(ref curve);
            reference.strength = curve;
        }

        reference.strengthMultiplier = this.strengthMultiplier;

        if (this.strengthX != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.strengthX;
            this.strengthX.SetData(ref curve);
            reference.strengthX = curve;
        }

        reference.strengthXMultiplier = this.strengthXMultiplier;

        if (this.strengthY != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.strengthY;
            this.strengthY.SetData(ref curve);
            reference.strengthY = curve;
        }

        reference.strengthYMultiplier = this.strengthYMultiplier;

        if (this.strengthZ != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.strengthZ;
            this.strengthZ.SetData(ref curve);
            reference.strengthZ = curve;
        }

        reference.strengthZMultiplier = this.strengthZMultiplier;
        reference.frequency = this.frequency;
        reference.damping = this.damping;
        reference.octaveCount = this.octaveCount;
        reference.octaveMultiplier = this.octaveMultiplier;
        reference.octaveScale = this.octaveScale;

        reference.quality = (ParticleSystemNoiseQuality)this.quality;

        if (this.scrollSpeed != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.scrollSpeed;
            this.scrollSpeed.SetData(ref curve);
            reference.scrollSpeed = curve;
        }

        reference.scrollSpeedMultiplier = this.scrollSpeedMultiplier;
        reference.remapEnabled = this.remapEnabled;

        if (this.remap != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.remap;
            this.remap.SetData(ref curve);
            reference.remap = curve;
        }

        reference.remapMultiplier = this.remapMultiplier;

        if (this.remapX != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.remapX;
            this.remapX.SetData(ref curve);
            reference.remapX = curve;
        }

        reference.remapXMultiplier = this.remapXMultiplier;

        if (this.remapY != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.remapY;
            this.remapY.SetData(ref curve);
            reference.remapY = curve;
        }

        reference.remapYMultiplier = this.remapYMultiplier;

        if (this.remapZ != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.remapZ;
            this.remapZ.SetData(ref curve);
            reference.remapZ = curve;
        }

        reference.remapZMultiplier = this.remapZMultiplier;
#if UNITY_2017_1_OR_NEWER
        if (this.positionAmount != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.positionAmount;
            this.positionAmount.SetData(ref curve);
            reference.positionAmount = curve;
        }
        if (this.rotationAmount != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.rotationAmount;
            this.rotationAmount.SetData(ref curve);
            reference.rotationAmount = curve;
        }
        if (this.sizeAmount != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.sizeAmount;
            this.sizeAmount.SetData(ref curve);
            reference.sizeAmount = curve;
        }
#endif
    }

    public override ParticleSystem.NoiseModule ToUnityType()
    {
        ParticleSystem.NoiseModule module = new ParticleSystem.NoiseModule();
        SetData(ref module);
        return module;
    }
}
