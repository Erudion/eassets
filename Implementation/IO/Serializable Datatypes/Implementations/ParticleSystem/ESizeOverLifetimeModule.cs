﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a SizeOverLifetimeModule
/// </summary>
[System.Serializable]
public class ESizeOverLifetimeModule : SerializableUnityData<ParticleSystem.SizeOverLifetimeModule>
{
    /// <summary>
    /// The enabled state of this ESizeOverLifetimeModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The size of this ESizeOverLifetimeModule
    /// </summary>
    public EMinMaxCurve size;
    /// <summary>
    /// The sizeMultiplier of this ESizeOverLifetimeModule
    /// </summary>
    public float sizeMultiplier;
    /// <summary>
    /// The x of this ESizeOverLifetimeModule
    /// </summary>
    public EMinMaxCurve x;
    /// <summary>
    /// The xMultiplier of this ESizeOverLifetimeModule
    /// </summary>
    public float xMultiplier;
    /// <summary>
    /// The y of this ESizeOverLifetimeModule
    /// </summary>
    public EMinMaxCurve y;
    /// <summary>
    /// The yMultiplier of this ESizeOverLifetimeModule
    /// </summary>
    public float yMultiplier;
    /// <summary>
    /// The z of this ESizeOverLifetimeModule
    /// </summary>
    public EMinMaxCurve z;
    /// <summary>
    /// The zMultiplier of this ESizeOverLifetimeModule
    /// </summary>
    public float zMultiplier;
    /// <summary>
    /// The separateAxes state of this ESizeOverLifetimeModule
    /// </summary>
    public bool separateAxes;

    /// <summary>
    /// Create a new ESizeOverLifetimeModule
    /// </summary>
    public ESizeOverLifetimeModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new ESizeOverLifetimeModule
    /// </summary>
    /// <param name="reference">The SizeOverLifetimeModule you want to use</param>
    public ESizeOverLifetimeModule(ParticleSystem.SizeOverLifetimeModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.size = new EMinMaxCurve(reference.size);
        this.sizeMultiplier = reference.sizeMultiplier;
        this.x = new EMinMaxCurve(reference.x);
        this.xMultiplier = reference.xMultiplier;
        this.y = new EMinMaxCurve(reference.y);
        this.yMultiplier = reference.yMultiplier;
        this.z = new EMinMaxCurve(reference.z);
        this.zMultiplier = reference.zMultiplier;
        this.separateAxes = reference.separateAxes;
    }

    public override void SetData(ref ParticleSystem.SizeOverLifetimeModule reference)
    {
        reference.enabled = this.enabled;

        if (this.size != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.size;
            this.size.SetData(ref curve);
            reference.size = curve;
        }

        reference.sizeMultiplier = this.sizeMultiplier;

        if (this.x != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.x;
            this.x.SetData(ref curve);
            reference.x = curve;
        }

        reference.xMultiplier = this.xMultiplier;

        if (this.y != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.y;
            this.y.SetData(ref curve);
            reference.y = curve;
        }

        reference.yMultiplier = this.yMultiplier;

        if (this.z != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.z;
            this.z.SetData(ref curve);
            reference.z = curve;
        }

        reference.zMultiplier = this.zMultiplier;
        reference.separateAxes = this.separateAxes;
    }

    public override ParticleSystem.SizeOverLifetimeModule ToUnityType()
    {
        ParticleSystem.SizeOverLifetimeModule module = new ParticleSystem.SizeOverLifetimeModule();
        SetData(ref module);
        return module;
    }
}
