﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a ParticleSystem.MinMaxCurve
/// </summary>
[System.Serializable]
public class EMinMaxCurve : SerializableUnityData<ParticleSystem.MinMaxCurve>
{
    /// <summary>
    /// The mode of this EMinMaxCurve
    /// </summary>
    public int mode;
    /// <summary>
    /// The curveMultiplier of this EMinMaxCurve
    /// </summary>
    public float curveMultiplier;
    /// <summary>
    /// The curveMax of this EMinMaxCurve
    /// </summary>
    public EAnimationCurve curveMax;
    /// <summary>
    /// The curveMin of this EMinMaxCurve
    /// </summary>
    public EAnimationCurve curveMin;
    /// <summary>
    /// The constantMax of this EMinMaxCurve
    /// </summary>
    public float constantMax;
    /// <summary>
    /// The constantMin of this EMinMaxCurve
    /// </summary>
    public float constantMin;
    /// <summary>
    /// The constant of this EMinMaxCurve
    /// </summary>
    public float constant;
    /// <summary>
    /// The curve of this EMinMaxCurve
    /// </summary>
    public EAnimationCurve curve;

    /// <summary>
    /// Create a new EMinMaxCurve
    /// </summary>
    public EMinMaxCurve() : base(false)
    {

    }

    /// <summary>
    /// Create a new EMinMaxCurve
    /// </summary>
    /// <param name="reference">The MinMaxCurve you want to use</param>
    public EMinMaxCurve(ParticleSystem.MinMaxCurve reference) : base(false)
    {
        this.mode = (int)reference.mode;
        this.curveMultiplier = reference.curveMultiplier;

        if(reference.curveMax != null)
            this.curveMax = new EAnimationCurve(reference.curveMax);
        if (reference.curveMin != null)
            this.curveMin = new EAnimationCurve(reference.curveMin);

        this.constantMax = reference.constantMax;
        this.constantMin = reference.constantMin;
        this.constant = reference.constant;

        if (reference.curve != null)
            this.curve = new EAnimationCurve(reference.curve);
    }

    public override void SetData(ref ParticleSystem.MinMaxCurve reference)
    {
        reference.mode = (ParticleSystemCurveMode)this.mode;
        reference.curveMultiplier = reference.curveMultiplier;

        if (this.curveMax != null)
        {
            AnimationCurve curve = reference.curveMax;
            this.curveMax.SetData(ref curve);
        }

        if (this.curveMin != null)
        {
            AnimationCurve curve = reference.curveMin;
            this.curveMin.SetData(ref curve);
        }
        reference.constantMax = this.constantMax;
        reference.constantMin = this.constantMin;
        reference.constant = this.constant;

        if (this.curve != null)
        {
            AnimationCurve curve = reference.curve;
            this.curve.SetData(ref curve);
        }
    }

    public override ParticleSystem.MinMaxCurve ToUnityType()
    {
        ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve();
        SetData(ref curve);
        return curve;
    }
}
