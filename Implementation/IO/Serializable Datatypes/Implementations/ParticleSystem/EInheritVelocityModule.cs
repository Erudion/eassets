﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a InheritVelocityModule
/// </summary>
[System.Serializable]
public class EInheritVelocityModule : SerializableUnityData<ParticleSystem.InheritVelocityModule>
{
    /// <summary>
    /// The enabled state of this EInheritVelocityModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The mode of this EInheritVelocityModule
    /// </summary>
    public int mode;
    /// <summary>
    /// The curve of this EInheritVelocityModule
    /// </summary>
    public EMinMaxCurve curve;
    /// <summary>
    /// The curveMultiplier of this EInheritVelocityModule
    /// </summary>
    public float curveMultiplier;

    /// <summary>
    /// Create a new EInheritVelocityModule
    /// </summary>
    public EInheritVelocityModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new EInheritVelocityModule
    /// </summary>
    /// <param name="reference">The InheritVelocityModule you want to use</param>
    public EInheritVelocityModule(ParticleSystem.InheritVelocityModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.mode = (int)reference.mode;
        this.curve = new EMinMaxCurve(reference.curve);
        this.curveMultiplier = reference.curveMultiplier;
    }

    public override void SetData(ref ParticleSystem.InheritVelocityModule reference)
    {
        reference.enabled = this.enabled;
        reference.mode = (ParticleSystemInheritVelocityMode)this.mode;
        if (this.curve != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.curve;
            this.curve.SetData(ref curve);
            reference.curve = curve;
        }
        reference.curveMultiplier = this.curveMultiplier;
    }

    public override ParticleSystem.InheritVelocityModule ToUnityType()
    {
        ParticleSystem.InheritVelocityModule module = new ParticleSystem.InheritVelocityModule();
        SetData(ref module);
        return module;
    }
}
