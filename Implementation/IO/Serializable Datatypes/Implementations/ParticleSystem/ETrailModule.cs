﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a TrailModule
/// </summary>
[System.Serializable]
public class ETrailModule : SerializableUnityData<ParticleSystem.TrailModule>
{
    /// <summary>
    /// The enabled state of this ETrailModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The ratio of this ETrailModule
    /// </summary>
    public float ratio;
    /// <summary>
    /// The lifetime of this ETrailModule
    /// </summary>
    public EMinMaxCurve lifetime;
    /// <summary>
    /// The lifetimeMultiplier of this ETrailModule
    /// </summary>
    public float lifetimeMultiplier;
    /// <summary>
    /// The minVertexDistance of this ETrailModule
    /// </summary>
    public float minVertexDistance;
    /// <summary>
    /// The textureMode of this ETrailModule
    /// </summary>
    public int textureMode;
    /// <summary>
    /// The worldSpace state of this ETrailModule
    /// </summary>
    public bool worldSpace;
    /// <summary>
    /// The dieWithParticles state of this ETrailModule
    /// </summary>
    public bool dieWithParticles;
    /// <summary>
    /// The sizeAffectsWidth state of this ETrailModule
    /// </summary>
    public bool sizeAffectsWidth;
    /// <summary>
    /// The sizeAffectsLifetime state of this ETrailModule
    /// </summary>
    public bool sizeAffectsLifetime;
    /// <summary>
    /// The inheritParticleColor state of this ETrailModule
    /// </summary>
    public bool inheritParticleColor;
    /// <summary>
    /// The colorOverLifetime of this ETrailModule
    /// </summary>
    public EMinMaxGradient colorOverLifetime;
    /// <summary>
    /// The widthOverTrail of this ETrailModule
    /// </summary>
    public EMinMaxCurve widthOverTrail;
    /// <summary>
    /// The widthOverTrailMultiplier of this ETrailModule
    /// </summary>
    public float widthOverTrailMultiplier;
    /// <summary>
    /// The colorOverTrail of this ETrailModule
    /// </summary>
    public EMinMaxGradient colorOverTrail;
#if UNITY_2017_1_OR_NEWER
    /// <summary>
    /// The generateLightingData state of this ETrailModule
    /// </summary>
    public bool generateLightingData;
#endif

    /// <summary>
    /// Create a new ETrailModule
    /// </summary>
    public ETrailModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new ETrailModule
    /// </summary>
    /// <param name="reference">The TrailModule you want to use</param>
    public ETrailModule(ParticleSystem.TrailModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.ratio = reference.ratio;
        this.lifetime = new EMinMaxCurve(reference.lifetime);
        this.lifetimeMultiplier = reference.lifetimeMultiplier;
        this.minVertexDistance = reference.minVertexDistance;
        this.textureMode = (int)reference.textureMode;
        this.worldSpace = reference.worldSpace;
        this.dieWithParticles = reference.dieWithParticles;
        this.sizeAffectsWidth = reference.sizeAffectsWidth;
        this.sizeAffectsLifetime = reference.sizeAffectsLifetime;
        this.inheritParticleColor = reference.inheritParticleColor;
        this.colorOverLifetime = new EMinMaxGradient(reference.colorOverLifetime);
        this.widthOverTrail = new EMinMaxCurve(reference.widthOverTrail);
        this.widthOverTrailMultiplier = reference.widthOverTrailMultiplier;
        this.colorOverTrail = new EMinMaxGradient(reference.colorOverTrail);
#if UNITY_2017_1_OR_NEWER
        this.generateLightingData = reference.generateLightingData;
#endif
}

public override void SetData(ref ParticleSystem.TrailModule reference)
    {
        reference.enabled = this.enabled;
        reference.ratio = this.ratio;

        if (this.lifetime != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.lifetime;
            this.lifetime.SetData(ref curve);
            reference.lifetime = curve;
        }

        reference.lifetimeMultiplier = this.lifetimeMultiplier;
        reference.minVertexDistance = this.minVertexDistance;
        reference.textureMode = (ParticleSystemTrailTextureMode)this.textureMode;
        reference.worldSpace = this.worldSpace;
        reference.dieWithParticles = this.dieWithParticles;
        reference.sizeAffectsWidth = this.sizeAffectsWidth;
        reference.sizeAffectsLifetime = this.sizeAffectsLifetime;
        reference.inheritParticleColor = this.inheritParticleColor;

        if (this.colorOverLifetime != null)
        {
            ParticleSystem.MinMaxGradient curve = reference.colorOverLifetime;
            this.colorOverLifetime.SetData(ref curve);
            reference.colorOverLifetime = curve;
        }

        if (this.widthOverTrail != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.widthOverTrail;
            this.widthOverTrail.SetData(ref curve);
            reference.widthOverTrail = curve;
        }

        reference.widthOverTrailMultiplier = this.widthOverTrailMultiplier;

        if (this.colorOverTrail != null)
        {
            ParticleSystem.MinMaxGradient curve = reference.colorOverTrail;
            this.colorOverTrail.SetData(ref curve);
            reference.colorOverTrail = curve;
        }

#if UNITY_2017_1_OR_NEWER
        reference.generateLightingData = this.generateLightingData;
#endif
    }

    public override ParticleSystem.TrailModule ToUnityType()
    {
        ParticleSystem.TrailModule module = new ParticleSystem.TrailModule();
        SetData(ref module);
        return module;
    }
}
