﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a TriggerModule
/// </summary>
[System.Serializable]
public class ETriggerModule : SerializableUnityData<ParticleSystem.TriggerModule>
{
    /// <summary>
    /// The enabled state of this ETriggerModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The inside of this ETriggerModule
    /// </summary>
    public int inside;
    /// <summary>
    /// The outside of this ETriggerModule
    /// </summary>
    public int outside;
    /// <summary>
    /// The enter of this ETriggerModule
    /// </summary>
    public int enter;
    /// <summary>
    /// The exit of this ETriggerModule
    /// </summary>
    public int exit;
    /// <summary>
    /// The radiusScale of this ETriggerModule
    /// </summary>
    public float radiusScale;

    /// <summary>
    /// Create a new ETriggerModule
    /// </summary>
    public ETriggerModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new ETriggerModule
    /// </summary>
    /// <param name="reference">The TriggerModule you want to use</param>
    public ETriggerModule(ParticleSystem.TriggerModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.inside = (int)reference.inside;
        this.outside = (int)reference.outside;
        this.enter = (int)reference.enter;
        this.exit = (int)reference.exit;
        this.radiusScale = reference.radiusScale;
    }

    public override void SetData(ref ParticleSystem.TriggerModule reference)
    {
        reference.enabled = this.enabled;
        reference.inside = (ParticleSystemOverlapAction)this.inside;
        reference.outside = (ParticleSystemOverlapAction)this.outside;
        reference.enter = (ParticleSystemOverlapAction)this.enter;
        reference.exit = (ParticleSystemOverlapAction)this.exit;
        reference.radiusScale = this.radiusScale;
    }

    public override ParticleSystem.TriggerModule ToUnityType()
    {
        ParticleSystem.TriggerModule module = new ParticleSystem.TriggerModule();
        SetData(ref module);
        return module;
    }
}
