﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a SizeBySpeedModule
/// </summary>
[System.Serializable]
public class ESizeBySpeedModule : SerializableUnityData<ParticleSystem.SizeBySpeedModule>
{
    /// <summary>
    /// The enabled state of this ESizeBySpeedModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The size of this ESizeBySpeedModule
    /// </summary>
    public EMinMaxCurve size;
    /// <summary>
    /// The sizeMultiplier of this ESizeBySpeedModule
    /// </summary>
    public float sizeMultiplier;
    /// <summary>
    /// The x of this ESizeBySpeedModule
    /// </summary>
    public EMinMaxCurve x;
    /// <summary>
    /// The xMultiplier of this ESizeBySpeedModule
    /// </summary>
    public float xMultiplier;
    /// <summary>
    /// The y of this ESizeBySpeedModule
    /// </summary>
    public EMinMaxCurve y;
    /// <summary>
    /// The yMultiplier of this ESizeBySpeedModule
    /// </summary>
    public float yMultiplier;
    /// <summary>
    /// The z of this ESizeBySpeedModule
    /// </summary>
    public EMinMaxCurve z;
    /// <summary>
    /// The zMultiplier of this ESizeBySpeedModule
    /// </summary>
    public float zMultiplier;
    /// <summary>
    /// The separateAxes state of this ESizeBySpeedModule
    /// </summary>
    public bool separateAxes;
    /// <summary>
    /// The range of this ESizeBySpeedModule
    /// </summary>
    public EVector2 range;

    /// <summary>
    /// Create a new ESizeBySpeedModule
    /// </summary>
    public ESizeBySpeedModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new ESizeBySpeedModule
    /// </summary>
    /// <param name="reference">The SizeBySpeedModule you want to use</param>
    public ESizeBySpeedModule(ParticleSystem.SizeBySpeedModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.size = new EMinMaxCurve(reference.size);
        this.sizeMultiplier = reference.sizeMultiplier;
        this.x = new EMinMaxCurve(reference.x);
        this.xMultiplier = reference.xMultiplier;
        this.y = new EMinMaxCurve(reference.y);
        this.yMultiplier = reference.yMultiplier;
        this.z = new EMinMaxCurve(reference.z);
        this.zMultiplier = reference.zMultiplier;
        this.separateAxes = reference.separateAxes;
        this.range = new EVector2(reference.range);
    }

    public override void SetData(ref ParticleSystem.SizeBySpeedModule reference)
    {
        reference.enabled = this.enabled;

        if (this.size != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.size;
            this.size.SetData(ref curve);
            reference.size = curve;
        }

        reference.sizeMultiplier = this.sizeMultiplier;

        if (this.x != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.x;
            this.x.SetData(ref curve);
            reference.x = curve;
        }

        reference.xMultiplier = this.xMultiplier;

        if (this.y != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.y;
            this.y.SetData(ref curve);
            reference.y = curve;
        }

        reference.yMultiplier = this.yMultiplier;

        if (this.z != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.z;
            this.z.SetData(ref curve);
            reference.z = curve;
        }

        reference.zMultiplier = this.zMultiplier;
        reference.separateAxes = this.separateAxes;

        reference.range =this.range.ToUnityType();
    }

    public override ParticleSystem.SizeBySpeedModule ToUnityType()
    {
        ParticleSystem.SizeBySpeedModule module = new ParticleSystem.SizeBySpeedModule();
        SetData(ref module);
        return module;
    }
}
