﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a ColorOverLifetimeModule
/// </summary>
[System.Serializable]
public class EColorOverLifetimeModule : SerializableUnityData<ParticleSystem.ColorOverLifetimeModule>
{
    /// <summary>
    /// The enabled state of this EColorOverLifetimeModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The color of this EColorOverLifetimeModule
    /// </summary>
    public EMinMaxGradient color;

    /// <summary>
    /// Create a new EColorOverLifetimeModule
    /// </summary>
    public EColorOverLifetimeModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new EColorOverLifetimeModule
    /// </summary>
    /// <param name="reference">The ColorOverLifetimeModule you want to use</param>
    public EColorOverLifetimeModule(ParticleSystem.ColorOverLifetimeModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.color = new EMinMaxGradient(reference.color);
    }

    public override void SetData(ref ParticleSystem.ColorOverLifetimeModule reference)
    {
        reference.enabled = this.enabled;

        if (this.color != null)
        {
            reference.color = this.color.ToUnityType();
        }
    }

    public override ParticleSystem.ColorOverLifetimeModule ToUnityType()
    {
        ParticleSystem.ColorOverLifetimeModule module = new ParticleSystem.ColorOverLifetimeModule();
        SetData(ref module);
        return module;
    }
}
