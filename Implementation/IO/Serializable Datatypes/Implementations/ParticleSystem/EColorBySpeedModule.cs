﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a ColorBySpeedModule
/// </summary>
[System.Serializable]
public class EColorBySpeedModule : SerializableUnityData<ParticleSystem.ColorBySpeedModule>
{
    /// <summary>
    /// The enabled state of this EColorBySpeedModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The color of this EColorBySpeedModule
    /// </summary>
    public EMinMaxGradient color;
    /// <summary>
    /// The range of this EColorBySpeedModule
    /// </summary>
    public EVector2 range;

    /// <summary>
    /// Create a new EColorBySpeedModule
    /// </summary>
    public EColorBySpeedModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new EColorBySpeedModule
    /// </summary>
    /// <param name="reference">The ColorBySpeedModule- you want to use</param>
    public EColorBySpeedModule(ParticleSystem.ColorBySpeedModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.color = new EMinMaxGradient(reference.color);
        this.range = new EVector2(reference.range);
    }

    public override void SetData(ref ParticleSystem.ColorBySpeedModule reference)
    {
        reference.enabled = this.enabled;

        if (this.color != null)
        {
            ParticleSystem.MinMaxGradient col = reference.color;
            this.color.SetData(ref col);
            reference.color = col;
        }

        reference.range = this.range.ToUnityType();
    }

    public override ParticleSystem.ColorBySpeedModule ToUnityType()
    {
        ParticleSystem.ColorBySpeedModule module = new ParticleSystem.ColorBySpeedModule();
        SetData(ref module);
        return module;
    }
}
