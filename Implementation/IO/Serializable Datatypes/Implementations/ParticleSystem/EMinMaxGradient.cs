﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a MinMaxGradient
/// </summary>
[System.Serializable]
public class EMinMaxGradient : SerializableUnityData<ParticleSystem.MinMaxGradient>
{
    /// <summary>
    /// The mode of this EMinMaxGradient
    /// </summary>
    public int mode;
    /// <summary>
    /// The gradientMax of this EMinMaxGradient
    /// </summary>
    public EGradient gradientMax;
    /// <summary>
    /// The gradientMin of this EMinMaxGradient
    /// </summary>
    public EGradient gradientMin;
    /// <summary>
    /// The colorMax of this EMinMaxGradient
    /// </summary>
    public EColor colorMax;
    /// <summary>
    /// The colorMin of this EMinMaxGradient
    /// </summary>
    public EColor colorMin;
    /// <summary>
    /// The color of this EMinMaxGradient
    /// </summary>
    public EColor color;
    /// <summary>
    /// The gradient of this EMinMaxGradient
    /// </summary>
    public EGradient gradient;

    /// <summary>
    /// Create a new EMinMaxGradient
    /// </summary>
    public EMinMaxGradient() : base(false)
    {

    }

    /// <summary>
    /// Create a new EMinMaxGradient
    /// </summary>
    /// <param name="reference">The MinMaxGradient you want to use</param>
    public EMinMaxGradient(ParticleSystem.MinMaxGradient reference) : base(false)
    {
        this.mode = (int)reference.mode;

        if(reference.gradientMax != null)
            this.gradientMax = new EGradient(reference.gradientMax);
        if (reference.gradientMin != null)
            this.gradientMin = new EGradient(reference.gradientMin);

        this.colorMax = new EColor(reference.colorMax);
        this.colorMin = new EColor(reference.colorMin);
        this.color = new EColor(reference.color);

        if(reference.gradient != null)
            this.gradient = new EGradient(reference.gradient);
    }

    public override void SetData(ref ParticleSystem.MinMaxGradient reference)
    {
        reference.mode = (ParticleSystemGradientMode)this.mode;

        if (this.gradientMax != null && reference.gradientMax != null)
        {
            Gradient gradient = reference.gradientMax;
            this.gradientMax.SetData(ref gradient);
        }

        if (this.gradientMin != null && reference.gradientMin != null)
        {
            Gradient gradient = reference.gradientMin;
            this.gradientMin.SetData(ref gradient);
        }

        if (this.colorMax != null)
        {
            Color col = reference.colorMax;
            this.colorMax.SetData(ref col);
            reference.colorMax = col;
        }

        if (this.colorMin != null)
        {
            Color col = reference.colorMin;
            this.colorMin.SetData(ref col);
            reference.colorMin = col;
        }

        if (this.color != null)
        {
            Color col = reference.color;
            this.color.SetData(ref col);
            reference.color = col;
        }

        if (this.gradient != null && reference.gradient != null)
        {
            Gradient gradient = reference.gradient;
            this.gradient.SetData(ref gradient);
        }
    }

    public override ParticleSystem.MinMaxGradient ToUnityType()
    {
        ParticleSystem.MinMaxGradient gradient = new ParticleSystem.MinMaxGradient();
        SetData(ref gradient);
        return gradient;
    }
}
