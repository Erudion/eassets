﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a RotationOverLifetimeModule
/// </summary>
[System.Serializable]
public class ERotationOverLifetimeModule : SerializableUnityData<ParticleSystem.RotationOverLifetimeModule>
{
    /// <summary>
    /// The enabled state of this ERotationOverLifetimeModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The x of this ERotationOverLifetimeModule
    /// </summary>
    public EMinMaxCurve x;
    /// <summary>
    /// The xMultiplier of this ERotationOverLifetimeModule
    /// </summary>
    public float xMultiplier;
    /// <summary>
    /// The y of this ERotationOverLifetimeModule
    /// </summary>
    public EMinMaxCurve y;
    /// <summary>
    /// The yMultiplier of this ERotationOverLifetimeModule
    /// </summary>
    public float yMultiplier;
    /// <summary>
    /// The z of this ERotationOverLifetimeModule
    /// </summary>
    public EMinMaxCurve z;
    /// <summary>
    /// The zMultiplier of this ERotationOverLifetimeModule
    /// </summary>
    public float zMultiplier;
    /// <summary>
    /// The separateAxes state of this ERotationOverLifetimeModule
    /// </summary>
    public bool separateAxes;

    /// <summary>
    /// Create a new ERotationOverLifetimeModule
    /// </summary>
    public ERotationOverLifetimeModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new ERotationOverLifetimeModule
    /// </summary>
    /// <param name="reference">The RotationOverLifetimeModule you want to use</param>
    public ERotationOverLifetimeModule(ParticleSystem.RotationOverLifetimeModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.x = new EMinMaxCurve(reference.x);
        this.xMultiplier = reference.xMultiplier;
        this.y = new EMinMaxCurve(reference.y);
        this.yMultiplier = reference.yMultiplier;
        this.z = new EMinMaxCurve(reference.z);
        this.zMultiplier = reference.zMultiplier;
        this.separateAxes = reference.separateAxes;

    }

    public override void SetData(ref ParticleSystem.RotationOverLifetimeModule reference)
    {
        reference.enabled = this.enabled;

        if (this.x != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.x;
            this.x.SetData(ref curve);
            reference.x = curve;
        }

        reference.xMultiplier = this.xMultiplier;

        if (this.y != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.y;
            this.y.SetData(ref curve);
            reference.y = curve;
        }

        reference.yMultiplier = this.yMultiplier;

        if (this.z != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.z;
            this.z.SetData(ref curve);
            reference.z = curve;
        }

        reference.zMultiplier = this.zMultiplier;
        reference.separateAxes = this.separateAxes;
    }

    public override ParticleSystem.RotationOverLifetimeModule ToUnityType()
    {
        ParticleSystem.RotationOverLifetimeModule module = new ParticleSystem.RotationOverLifetimeModule();
        SetData(ref module);
        return module;
    }
}
