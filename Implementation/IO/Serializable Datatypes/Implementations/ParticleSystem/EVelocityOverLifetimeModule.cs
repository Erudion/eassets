﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a VelocityOverLifetimeModule
/// </summary>
[System.Serializable]
public class EVelocityOverLifetimeModule : SerializableUnityData<ParticleSystem.VelocityOverLifetimeModule>
{
    /// <summary>
    /// The enabled state of this EVelocityOverLifetimeModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The x of this EVelocityOverLifetimeModule
    /// </summary>
    public EMinMaxCurve x;
    /// <summary>
    /// The xMultiplier of this EVelocityOverLifetimeModule
    /// </summary>
    public float xMultiplier;
    /// <summary>
    /// The y of this EVelocityOverLifetimeModule
    /// </summary>
    public EMinMaxCurve y;
    /// <summary>
    /// The yMultiplier of this EVelocityOverLifetimeModule
    /// </summary>
    public float yMultiplier;
    /// <summary>
    /// The z of this EVelocityOverLifetimeModule
    /// </summary>
    public EMinMaxCurve z;
    /// <summary>
    /// The zMultiplier of this EVelocityOverLifetimeModule
    /// </summary>
    public float zMultiplier;
    /// <summary>
    /// The space of this EVelocityOverLifetimeModule
    /// </summary>
    public int space;

    /// <summary>
    /// Create a new EVelocityOverLifetimeModule
    /// </summary>
    public EVelocityOverLifetimeModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new EVelocityOverLifetimeModule
    /// </summary>
    /// <param name="reference">The VelocityOverLifetimeModule you want to use</param>
    public EVelocityOverLifetimeModule(ParticleSystem.VelocityOverLifetimeModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.x = new EMinMaxCurve(reference.x);
        this.xMultiplier = reference.xMultiplier;
        this.y = new EMinMaxCurve(reference.y);
        this.yMultiplier = reference.yMultiplier;
        this.z = new EMinMaxCurve(reference.z);
        this.zMultiplier = reference.zMultiplier;
        this.space = (int)reference.space;
    }

    public override void SetData(ref ParticleSystem.VelocityOverLifetimeModule reference)
    {
        reference.enabled = this.enabled;

        if (this.x != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.x;
            this.x.SetData(ref curve);
            reference.x = curve;
        }

        reference.xMultiplier = this.xMultiplier;

        if (this.y != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.y;
            this.y.SetData(ref curve);
            reference.y = curve;
        }

        reference.yMultiplier = this.yMultiplier;

        if (this.z != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.z;
            this.z.SetData(ref curve);
            reference.z = curve;
        }

        reference.zMultiplier = this.zMultiplier;
        reference.space = (ParticleSystemSimulationSpace)this.space;
    }

    public override ParticleSystem.VelocityOverLifetimeModule ToUnityType()
    {
        ParticleSystem.VelocityOverLifetimeModule module = new ParticleSystem.VelocityOverLifetimeModule();
        SetData(ref module);
        return module;
    }
}
