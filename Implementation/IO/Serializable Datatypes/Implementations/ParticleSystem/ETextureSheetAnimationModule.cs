﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// This class can be used to serialize a TextureSheetAnimationModule
/// </summary>
[System.Serializable]
public class ETextureSheetAnimationModule : SerializableUnityData<ParticleSystem.TextureSheetAnimationModule>
{
    /// <summary>
    /// The enabled state of this ETextureSheetAnimationModule
    /// </summary>
    public bool enabled;
#if UNITY_2017_1_OR_NEWER
    /// <summary>
    /// The mode of this ETextureSheetAnimationModule
    /// </summary>
    public int mode;
#endif
    /// <summary>
    /// The numTilesX of this ETextureSheetAnimationModule
    /// </summary>
    public int numTilesX;
    /// <summary>
    /// The numTilesY of this ETextureSheetAnimationModule
    /// </summary>
    public int numTilesY;
    /// <summary>
    /// The animation of this ETextureSheetAnimationModule
    /// </summary>
    public int animation;
    /// <summary>
    /// The rowMode of this ETextureSheetAnimationModule
    /// </summary>
    public int rowMode;
    /// <summary>
    /// The frameOverTime of this ETextureSheetAnimationModule
    /// </summary>
    public EMinMaxCurve frameOverTime;
    /// <summary>
    /// The frameOverTimeMultiplier of this ETextureSheetAnimationModule
    /// </summary>
    public float frameOverTimeMultiplier;
    /// <summary>
    /// The startFrame of this ETextureSheetAnimationModule
    /// </summary>
    public EMinMaxCurve startFrame;
    /// <summary>
    /// The startFrameMultiplier of this ETextureSheetAnimationModule
    /// </summary>
    public float startFrameMultiplier;
    /// <summary>
    /// The cycleCount of this ETextureSheetAnimationModule
    /// </summary>
    public int cycleCount;
    /// <summary>
    /// The rowIndex of this ETextureSheetAnimationModule
    /// </summary>
    public int rowIndex;
    /// <summary>
    /// The uvChannelMask of this ETextureSheetAnimationModule
    /// </summary>
    public int uvChannelMask;

    /// <summary>
    /// Create a new ETextureSheetAnimationModule
    /// </summary>
    public ETextureSheetAnimationModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new ETextureSheetAnimationModule
    /// </summary>
    /// <param name="reference">The TextureSheetAnimationModule you want to use</param>
    public ETextureSheetAnimationModule(ParticleSystem.TextureSheetAnimationModule reference) : base(false)
    {
        this.enabled = reference.enabled;
#if UNITY_2017_1_OR_NEWER
        this.mode = (int)reference.mode;
#endif
        this.numTilesX = reference.numTilesX;
        this.numTilesY = reference.numTilesY;
        this.animation = (int)reference.animation;
        this.rowMode = (int)reference.rowMode;
        this.frameOverTime = new EMinMaxCurve(reference.frameOverTime);
        this.frameOverTimeMultiplier = reference.frameOverTimeMultiplier;
        this.startFrame = new EMinMaxCurve(reference.startFrame);
        this.startFrameMultiplier = reference.startFrameMultiplier;
        this.cycleCount = reference.cycleCount;
        this.rowIndex = reference.rowIndex;
        this.uvChannelMask = (int)reference.uvChannelMask;
    }

    public override void SetData(ref ParticleSystem.TextureSheetAnimationModule reference)
    {
        reference.enabled = this.enabled;
#if UNITY_2017_1_OR_NEWER
        reference.mode = (ParticleSystemAnimationMode)this.mode;
#endif
        reference.numTilesX = this.numTilesX;
        reference.numTilesY = this.numTilesY;
        reference.animation = (ParticleSystemAnimationType)this.animation;
        reference.rowMode = (ParticleSystemAnimationRowMode)this.rowMode;

        if (this.frameOverTime != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.frameOverTime;
            this.frameOverTime.SetData(ref curve);
            reference.frameOverTime = curve;
        }

        reference.frameOverTimeMultiplier = this.frameOverTimeMultiplier;

        if (this.startFrame != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.startFrame;
            this.startFrame.SetData(ref curve);
            reference.startFrame = curve;
        }

        reference.startFrameMultiplier = this.startFrameMultiplier;
        reference.cycleCount = this.cycleCount;
        reference.rowIndex = this.rowIndex;
        reference.uvChannelMask = (UVChannelFlags)this.uvChannelMask;
    }

    public override ParticleSystem.TextureSheetAnimationModule ToUnityType()
    {
        ParticleSystem.TextureSheetAnimationModule module = new ParticleSystem.TextureSheetAnimationModule();
        SetData(ref module);
        return module;
    }
}
