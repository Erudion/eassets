﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a MainModule
/// </summary>
[System.Serializable]
public class EMainModule : SerializableUnityData<ParticleSystem.MainModule>
{
    /// <summary>
    /// The duration of this EMainModule
    /// </summary>
    public float duration;
    /// <summary>
    /// The loop state of this EMainModule
    /// </summary>
    public bool loop;
    /// <summary>
    /// The prewarm state of this EMainModule
    /// </summary>
    public bool prewarm;
    /// <summary>
    /// The startDelay of this EMainModule
    /// </summary>
    public EMinMaxCurve startDelay;
    /// <summary>
    /// The startDelayMultiplier of this EMainModule
    /// </summary>
    public float startDelayMultiplier;
    /// <summary>
    /// The startLifetime of this EMainModule
    /// </summary>
    public EMinMaxCurve startLifetime;
    /// <summary>
    /// The startLifetimeMultiplier of this EMainModule
    /// </summary>
    public float startLifetimeMultiplier;
    /// <summary>
    /// The startSpeed of this EMainModule
    /// </summary>
    public EMinMaxCurve startSpeed;
    /// <summary>
    /// The startSpeedMultiplier of this EMainModule
    /// </summary>
    public float startSpeedMultiplier;
    /// <summary>
    /// The startSize3D state of this EMainModule
    /// </summary>
    public bool startSize3D;
    /// <summary>
    /// The startSize of this EMainModule
    /// </summary>
    public EMinMaxCurve startSize;
    /// <summary>
    /// The startSizeMultiplier of this EMainModule
    /// </summary>
    public float startSizeMultiplier;
    /// <summary>
    /// The startSizeX of this EMainModule
    /// </summary>
    public EMinMaxCurve startSizeX;
    /// <summary>
    /// The startSizeXMultiplier of this EMainModule
    /// </summary>
    public float startSizeXMultiplier;
    /// <summary>
    /// The startSizeY of this EMainModule
    /// </summary>
    public EMinMaxCurve startSizeY;
    /// <summary>
    /// The startSizeYMultiplier of this EMainModule
    /// </summary>
    public float startSizeYMultiplier;
    /// <summary>
    /// The startSizeZ of this EMainModule
    /// </summary>
    public EMinMaxCurve startSizeZ;
    /// <summary>
    /// The startSizeZMultiplier of this EMainModule
    /// </summary>
    public float startSizeZMultiplier;
    /// <summary>
    /// The startRotation3D state of this EMainModule
    /// </summary>
    public bool startRotation3D;
    /// <summary>
    /// The startRotation of this EMainModule
    /// </summary>
    public EMinMaxCurve startRotation;
    /// <summary>
    /// The startRotationMultiplier of this EMainModule
    /// </summary>
    public float startRotationMultiplier;
    /// <summary>
    /// The startRotationX of this EMainModule
    /// </summary>
    public EMinMaxCurve startRotationX;
    /// <summary>
    /// The startRotationXMultiplier of this EMainModule
    /// </summary>
    public float startRotationXMultiplier;
    /// <summary>
    /// The startRotationY of this EMainModule
    /// </summary>
    public EMinMaxCurve startRotationY;
    /// <summary>
    /// The startRotationYMultiplier of this EMainModule
    /// </summary>
    public float startRotationYMultiplier;
    /// <summary>
    /// The startRotationZ of this EMainModule
    /// </summary>
    public EMinMaxCurve startRotationZ;
    /// <summary>
    /// The startRotationZMultiplier of this EMainModule
    /// </summary>
    public float startRotationZMultiplier;
    /// <summary>
    /// The startColor of this EMainModule
    /// </summary>
    public EMinMaxGradient startColor;
    /// <summary>
    /// The gravityModifier of this EMainModule
    /// </summary>
    public EMinMaxCurve gravityModifier;
    /// <summary>
    /// The gravityModifierMultiplier of this EMainModule
    /// </summary>
    public float gravityModifierMultiplier;
    /// <summary>
    /// The simulationSpace of this EMainModule
    /// </summary>
    public int simulationSpace;
    /// <summary>
    /// The customSimulationSpace of this EMainModule
    /// </summary>
    public ETransform customSimulationSpace;
    /// <summary>
    /// The simulationSpeed of this EMainModule
    /// </summary>
    public float simulationSpeed;

#if UNITY_2017_1_OR_NEWER
    /// <summary>
    /// The useUnscaledTime state of this EMainModule
    /// </summary>
    public bool useUnscaledTime;
#endif
    /// <summary>
    /// The scalingMode of this EMainModule
    /// </summary>
    public int scalingMode;
    /// <summary>
    /// The playOnAwake state of this EMainModule
    /// </summary>
    public bool playOnAwake;
    /// <summary>
    /// The maxParticles of this EMainModule
    /// </summary>
    public int maxParticles;
#if UNITY_2017_1_OR_NEWER
    /// <summary>
    /// The emitterVelocityMode of this EMainModule
    /// </summary>
    public int emitterVelocityMode;
#endif

    /// <summary>
    /// Create a new EMainModule
    /// </summary>
    public EMainModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new EMainModule
    /// </summary>
    /// <param name="reference">The MainModule you want to use</param>
    public EMainModule(ParticleSystem.MainModule reference) : base(false)
    {
        this.duration = reference.duration;
        this.loop = reference.loop;
        this.prewarm = reference.prewarm;
        this.startDelay = new EMinMaxCurve(reference.startDelay);
        this.startDelayMultiplier = reference.startDelayMultiplier;
        this.startLifetime = new EMinMaxCurve(reference.startLifetime);
        this.startLifetimeMultiplier = reference.startLifetimeMultiplier;
        this.startSpeed = new EMinMaxCurve(reference.startSpeed);
        this.startSpeedMultiplier = reference.startSpeedMultiplier;
        this.startSize3D = reference.startSize3D;
        this.startSize = new EMinMaxCurve(reference.startSize);
        this.startSizeMultiplier = reference.startSizeMultiplier;
        this.startSizeX = new EMinMaxCurve(reference.startSizeX);
        this.startSizeXMultiplier = reference.startSizeXMultiplier;
        this.startSizeY = new EMinMaxCurve(reference.startSizeY);
        this.startSizeYMultiplier = reference.startSizeYMultiplier;
        this.startSizeZ = new EMinMaxCurve(reference.startSizeZ);
        this.startSizeZMultiplier = reference.startSizeZMultiplier;
        this.startRotation3D = reference.startRotation3D;
        this.startRotation = new EMinMaxCurve(reference.startRotation);
        this.startRotationMultiplier = reference.startRotationMultiplier;
        this.startRotationX = new EMinMaxCurve(reference.startRotationX);
        this.startRotationXMultiplier = reference.startRotationXMultiplier;
        this.startRotationY = new EMinMaxCurve(reference.startRotationY);
        this.startRotationYMultiplier = reference.startRotationYMultiplier;
        this.startRotationZ = new EMinMaxCurve(reference.startRotationZ);
        this.startRotationZMultiplier = reference.startRotationZMultiplier;
        this.startColor = new EMinMaxGradient(reference.startColor);
        this.gravityModifier = new EMinMaxCurve(reference.gravityModifier);
        this.gravityModifierMultiplier = reference.gravityModifierMultiplier;
        this.simulationSpace = (int)reference.simulationSpace;

        if(reference.customSimulationSpace != null)
            this.customSimulationSpace = new ETransform(reference.customSimulationSpace);

        this.simulationSpeed = reference.simulationSpeed;
#if UNITY_2017_1_OR_NEWER
        this.useUnscaledTime = reference.useUnscaledTime;
#endif
        this.scalingMode = (int)reference.scalingMode;
        this.playOnAwake = reference.playOnAwake;
        this.maxParticles = reference.maxParticles;
#if UNITY_2017_1_OR_NEWER
        this.emitterVelocityMode = (int)reference.emitterVelocityMode;
#endif
    }

    public override void SetData(ref ParticleSystem.MainModule reference)
    {
        reference.duration = this.duration;
        reference.loop = this.loop;
        reference.prewarm = this.prewarm;

        if (this.startDelay != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.startDelay;
            this.startDelay.SetData(ref curve);
            reference.startDelay = curve;
        }

        reference.startDelayMultiplier = this.startDelayMultiplier;

        if (this.startLifetime != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.startLifetime;
            this.startLifetime.SetData(ref curve);
            reference.startLifetime = curve;
        }

        reference.startLifetimeMultiplier = this.startLifetimeMultiplier;

        if (this.startSpeed != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.startSpeed;
            this.startSpeed.SetData(ref curve);
            reference.startSpeed = curve;
        }

        reference.startSpeedMultiplier = this.startSpeedMultiplier;
        reference.startSize3D = this.startSize3D;

        if (this.startSize != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.startSize;
            this.startSize.SetData(ref curve);
            reference.startSize = curve;
        }

        reference.startSizeMultiplier = this.startSizeMultiplier;

        if (this.startSizeX != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.startSizeX;
            this.startSizeX.SetData(ref curve);
            reference.startSizeX = curve;
        }

        reference.startSizeXMultiplier = this.startSizeXMultiplier;

        if (this.startSizeY != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.startSizeY;
            this.startSizeY.SetData(ref curve);
            reference.startSizeY = curve;
        }

        reference.startSizeYMultiplier = this.startSizeYMultiplier;

        if (this.startSizeZ != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.startSizeZ;
            this.startSizeZ.SetData(ref curve);
            reference.startSizeZ = curve;
        }

        reference.startSizeZMultiplier = this.startSizeZMultiplier;
        reference.startRotation3D = this.startRotation3D;

        if (this.startRotation != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.startRotation;
            this.startRotation.SetData(ref curve);
            reference.startRotation = curve;
        }

        reference.startRotationMultiplier = this.startRotationMultiplier;

        if (this.startRotationX != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.startRotationX;
            this.startRotationX.SetData(ref curve);
            reference.startRotationX = curve;
        }

        reference.startRotationXMultiplier = this.startRotationXMultiplier;

        if (this.startRotationY != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.startRotationY;
            this.startRotationY.SetData(ref curve);
            reference.startRotationY = curve;
        }

        reference.startRotationYMultiplier = this.startRotationYMultiplier;

        if (this.startRotationZ != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.startRotationZ;
            this.startRotationZ.SetData(ref curve);
            reference.startRotationZ = curve;
        }

        reference.startRotationZMultiplier = this.startRotationZMultiplier;

        if (this.startColor != null)
        {
            ParticleSystem.MinMaxGradient curve = reference.startColor;
            this.startColor.SetData(ref curve);
            reference.startColor = curve;
        }

        if (this.gravityModifier != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.gravityModifier;
            this.gravityModifier.SetData(ref curve);
            reference.gravityModifier = curve;
        }

        reference.gravityModifierMultiplier = this.gravityModifierMultiplier;
        reference.simulationSpace = (ParticleSystemSimulationSpace)this.simulationSpace;

        if (this.customSimulationSpace != null)
        {
            Transform trans = reference.customSimulationSpace;
            this.customSimulationSpace.SetData(ref trans);
        }

        reference.simulationSpeed = this.simulationSpeed;

#if UNITY_2017_1_OR_NEWER
        reference.useUnscaledTime = this.useUnscaledTime;
#endif
        reference.scalingMode = (ParticleSystemScalingMode)this.scalingMode;
        reference.playOnAwake = this.playOnAwake;
        reference.maxParticles = this.maxParticles;
#if UNITY_2017_1_OR_NEWER
        reference.emitterVelocityMode = (ParticleSystemEmitterVelocityMode)this.emitterVelocityMode;
#endif
    }

    public override ParticleSystem.MainModule ToUnityType()
    {
        ParticleSystem.MainModule module = new ParticleSystem.MainModule();
        SetData(ref module);
        return module;
    }
}
