﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a CustomDataModule
/// </summary>
[System.Serializable]
public class ECustomDataModule : SerializableUnityData<ParticleSystem.CustomDataModule>
{
    public bool enabled;

    /// <summary>
    /// Create a new ECustomDataModule
    /// </summary>
    public ECustomDataModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new ECustomDataModule
    /// </summary>
    /// <param name="reference">The CustomDataModule you want to use</param>
    public ECustomDataModule(ParticleSystem.CustomDataModule reference) : base(false)
    {
        this.enabled = reference.enabled;
    }

    public override void SetData(ref ParticleSystem.CustomDataModule reference)
    {
        reference.enabled = this.enabled;
    }

    public override ParticleSystem.CustomDataModule ToUnityType()
    {
        ParticleSystem.CustomDataModule module = new ParticleSystem.CustomDataModule();
        SetData(ref module);
        return module;
    }
}
