﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a SubEmittersModule
/// </summary>
[System.Serializable]
public class ESubEmittersModule : SerializableUnityData<ParticleSystem.SubEmittersModule>
{
    /// <summary>
    /// The enabled state of this ESubEmittersModule
    /// </summary>
    public bool enabled;

    /// <summary>
    /// Create a new ESubEmittersModule
    /// </summary>
    public ESubEmittersModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new ESubEmittersModule
    /// </summary>
    /// <param name="reference">The SubEmittersModule you want to use</param>
    public ESubEmittersModule(ParticleSystem.SubEmittersModule reference) : base(false)
    {
        this.enabled = reference.enabled;
    }

    public override void SetData(ref ParticleSystem.SubEmittersModule reference)
    {
        reference.enabled = this.enabled;
    }

    public override ParticleSystem.SubEmittersModule ToUnityType()
    {
        ParticleSystem.SubEmittersModule module = new ParticleSystem.SubEmittersModule();
        SetData(ref module);
        return module;
    }
}
