﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a ForceOverLifetimeModule
/// </summary>
[System.Serializable]
public class EForceOverLifetimeModule : SerializableUnityData<ParticleSystem.ForceOverLifetimeModule>
{
    /// <summary>
    /// The enabled state of this EForceOverLifetimeModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The x of this EForceOverLifetimeModule
    /// </summary>
    public EMinMaxCurve x;
    /// <summary>
    /// The y of this EForceOverLifetimeModule
    /// </summary>
    public EMinMaxCurve y;
    /// <summary>
    /// The z of this EForceOverLifetimeModule
    /// </summary>
    public EMinMaxCurve z;
    /// <summary>
    /// The xMultiplier of this EForceOverLifetimeModule
    /// </summary>
    public float xMultiplier;
    /// <summary>
    /// The yMultiplier of this EForceOverLifetimeModule
    /// </summary>
    public float yMultiplier;
    /// <summary>
    /// The zMultiplier of this EForceOverLifetimeModule
    /// </summary>
    public float zMultiplier;
    /// <summary>
    /// The space of this EForceOverLifetimeModule
    /// </summary>
    public int space;
    /// <summary>
    /// The randomized state of this EForceOverLifetimeModule
    /// </summary>
    public bool randomized;



    /// <summary>
    /// Create a new EForceOverLifetimeModule
    /// </summary>
    public EForceOverLifetimeModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new EForceOverLifetimeModule
    /// </summary>
    /// <param name="reference">The ForceOverLifetimeModule you want to use</param>
    public EForceOverLifetimeModule(ParticleSystem.ForceOverLifetimeModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.x = new EMinMaxCurve(reference.x);
        this.y = new EMinMaxCurve(reference.y);
        this.z = new EMinMaxCurve(reference.z);
        this.xMultiplier = reference.xMultiplier;
        this.yMultiplier = reference.yMultiplier;
        this.zMultiplier = reference.zMultiplier;
        this.space = (int)reference.space;
        this.randomized = reference.randomized;
    }

    public override void SetData(ref ParticleSystem.ForceOverLifetimeModule reference)
    {
        reference.enabled = this.enabled;

        if (this.x != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.x;
            this.x.SetData(ref curve);
            reference.x = curve;
        }
        if (this.y != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.y;
            this.y.SetData(ref curve);
            reference.y = curve;
        }
        if (this.z != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.z;
            this.z.SetData(ref curve);
            reference.z = curve;
        }

        reference.xMultiplier = this.xMultiplier;
        reference.yMultiplier = this.yMultiplier;
        reference.zMultiplier = this.zMultiplier;
        reference.space = (ParticleSystemSimulationSpace)this.space;
        reference.randomized = this.randomized;
    }

    public override ParticleSystem.ForceOverLifetimeModule ToUnityType()
    {
        ParticleSystem.ForceOverLifetimeModule module = new ParticleSystem.ForceOverLifetimeModule();
        SetData(ref module);
        return module;
    }
}
