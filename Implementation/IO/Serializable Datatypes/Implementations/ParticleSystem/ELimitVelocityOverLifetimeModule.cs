﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a LimitVelocityOverLifetimeModule
/// </summary>
[System.Serializable]
public class ELimitVelocityOverLifetimeModule : SerializableUnityData<ParticleSystem.LimitVelocityOverLifetimeModule>
{
    /// <summary>
    /// The enabled state of this ELimitVelocityOverLifetimeModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The limitX of this ELimitVelocityOverLifetimeModule
    /// </summary>
    public EMinMaxCurve limitX;
    /// <summary>
    /// The limitXMultiplier of this ELimitVelocityOverLifetimeModule
    /// </summary>
    public float limitXMultiplier;
    /// <summary>
    /// The limitY of this ELimitVelocityOverLifetimeModule
    /// </summary>
    public EMinMaxCurve limitY;
    /// <summary>
    /// The limitYMultiplier of this ELimitVelocityOverLifetimeModule
    /// </summary>
    public float limitYMultiplier;
    /// <summary>
    /// The limitZ of this ELimitVelocityOverLifetimeModule
    /// </summary>
    public EMinMaxCurve limitZ;
    /// <summary>
    /// The limitZMultiplier of this ELimitVelocityOverLifetimeModule
    /// </summary>
    public float limitZMultiplier;
    /// <summary>
    /// The limit of this ELimitVelocityOverLifetimeModule
    /// </summary>
    public EMinMaxCurve limit;
    /// <summary>
    /// The limitMultiplier of this ELimitVelocityOverLifetimeModule
    /// </summary>
    public float limitMultiplier;
    /// <summary>
    /// The dampen of this ELimitVelocityOverLifetimeModule
    /// </summary>
    public float dampen;
    /// <summary>
    /// The separateAxes state of this ELimitVelocityOverLifetimeModule
    /// </summary>
    public bool separateAxes;
    /// <summary>
    /// The space of this ELimitVelocityOverLifetimeModule
    /// </summary>
    public int space;

    /// <summary>
    /// Create a new ELimitVelocityOverLifetimeModule
    /// </summary>
    public ELimitVelocityOverLifetimeModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new ELimitVelocityOverLifetimeModule
    /// </summary>
    /// <param name="reference">The LimitVelocityOverLifetimeModule you want to use</param>
    public ELimitVelocityOverLifetimeModule(ParticleSystem.LimitVelocityOverLifetimeModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.limitX = new EMinMaxCurve(reference.limitX);
        this.limitXMultiplier = reference.limitXMultiplier;
        this.limitY = new EMinMaxCurve(reference.limitY);
        this.limitYMultiplier = reference.limitYMultiplier;
        this.limitZ = new EMinMaxCurve(reference.limitZ);
        this.limitZMultiplier = reference.limitZMultiplier;
        this.limit = new EMinMaxCurve(reference.limit);
        this.limitMultiplier = reference.limitMultiplier;
        this.dampen = reference.dampen;
        this.separateAxes = reference.separateAxes;
        this.space = (int)reference.space;
    }

    public override void SetData(ref ParticleSystem.LimitVelocityOverLifetimeModule reference)
    {
        reference.enabled = this.enabled;

        if (this.limitX != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.limitX;
            this.limitX.SetData(ref curve);
            reference.limitX = curve;
        }

        reference.limitXMultiplier = this.limitXMultiplier;

        if (this.limitY != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.limitY;
            this.limitY.SetData(ref curve);
            reference.limitY = curve;
        }

        reference.limitYMultiplier = this.limitYMultiplier;

        if (this.limitZ != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.limitZ;
            this.limitZ.SetData(ref curve);
            reference.limitZ = curve;
        }

        reference.limitZMultiplier = this.limitZMultiplier;

        if (this.limit != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.limit;
            this.limit.SetData(ref curve);
            reference.limit = curve;
        }

        reference.limitMultiplier = this.limitMultiplier;
        reference.dampen = this.dampen;
        reference.separateAxes = this.separateAxes;
        reference.space = (ParticleSystemSimulationSpace)this.space;
    }

    public override ParticleSystem.LimitVelocityOverLifetimeModule ToUnityType()
    {
        ParticleSystem.LimitVelocityOverLifetimeModule module = new ParticleSystem.LimitVelocityOverLifetimeModule();
        SetData(ref module);
        return module;
    }
}
