﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a ParticleSystem.CollisionModule
/// </summary>
[System.Serializable]
public class ECollisionModule : SerializableUnityData<ParticleSystem.CollisionModule>
{
    /// <summary>
    /// The enabled state of this ECollisionModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The type of this ECollisionModule
    /// </summary>
    public int type;
    /// <summary>
    /// The mode of this ECollisionModule
    /// </summary>
    public int mode;
    /// <summary>
    /// The dampen of this ECollisionModule
    /// </summary>
    public EMinMaxCurve dampen;
    /// <summary>
    /// The dampenMultiplier of this ECollisionModule
    /// </summary>
    public float dampenMultiplier;
    /// <summary>
    /// The bounce of this ECollisionModule
    /// </summary>
    public EMinMaxCurve bounce;
    /// <summary>
    /// The bounceMultiplier of this ECollisionModule
    /// </summary>
    public float bounceMultiplier;
    /// <summary>
    /// The lifetimeLoss of this ECollisionModule
    /// </summary>
    public EMinMaxCurve lifetimeLoss;
    /// <summary>
    /// The lifetimeLossMultiplier of this ECollisionModule
    /// </summary>
    public float lifetimeLossMultiplier;
    /// <summary>
    /// The minKillSpeed of this ECollisionModule
    /// </summary>
    public float minKillSpeed;
    /// <summary>
    /// The maxKillSpeed of this ECollisionModule
    /// </summary>
    public float maxKillSpeed;
    /// <summary>
    /// The collidesWith of this ECollisionModule
    /// </summary>
    public ELayerMask collidesWith;
    /// <summary>
    /// The enableDynamicColliders of this ECollisionModule
    /// </summary>
    public bool enableDynamicColliders;
    /// <summary>
    /// The maxCollisionShapes of this ECollisionModule
    /// </summary>
    public int maxCollisionShapes;
    /// <summary>
    /// The quality of this ECollisionModule
    /// </summary>
    public int quality;
    /// <summary>
    /// The voxelSize of this ECollisionModule
    /// </summary>
    public float voxelSize;
    /// <summary>
    /// The radiusScale of this ECollisionModule
    /// </summary>
    public float radiusScale;
    /// <summary>
    /// The sendCollisionMessages state of this ECollisionModule
    /// </summary>
    public bool sendCollisionMessages;
#if UNITY_2017_1_OR_NEWER
    /// <summary>
    /// The colliderForce of this ECollisionModule
    /// </summary>
    public float colliderForce;
    /// <summary>
    /// The multiplyColliderForceByCollisionAngle state of this ECollisionModule
    /// </summary>
    public bool multiplyColliderForceByCollisionAngle;
    /// <summary>
    /// The multiplyColliderForceByParticleSpeed state of this ECollisionModule
    /// </summary>
    public bool multiplyColliderForceByParticleSpeed;
    /// <summary>
    /// The multiplyColliderForceByParticleSize state of this ECollisionModule
    /// </summary>
    public bool multiplyColliderForceByParticleSize;
#endif

    /// <summary>
    /// Create a new ECollisionModule
    /// </summary>
    public ECollisionModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new ECollisionModule
    /// </summary>
    /// <param name="reference">The CollisionModule you want to use</param>
    public ECollisionModule(ParticleSystem.CollisionModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.type = (int)reference.type;
        this.mode = (int)reference.mode;
        this.dampen = new EMinMaxCurve(reference.dampen);
        this.dampenMultiplier = reference.dampenMultiplier;
        this.bounce = new EMinMaxCurve(reference.bounce);
        this.bounceMultiplier = reference.bounceMultiplier;
        this.lifetimeLoss = new EMinMaxCurve(reference.lifetimeLoss);
        this.lifetimeLossMultiplier = reference.lifetimeLossMultiplier;
        this.minKillSpeed = reference.minKillSpeed;
        this.maxKillSpeed = reference.maxKillSpeed;
        this.collidesWith = new ELayerMask(reference.collidesWith);
        this.enableDynamicColliders = reference.enableDynamicColliders;
        this.maxCollisionShapes = reference.maxCollisionShapes;
        this.quality = (int)reference.quality;
        this.voxelSize = reference.voxelSize;
        this.radiusScale = reference.radiusScale;
        this.sendCollisionMessages = reference.sendCollisionMessages;

#if UNITY_2017_1_OR_NEWER
        this.colliderForce = reference.colliderForce;
        this.multiplyColliderForceByCollisionAngle = reference.multiplyColliderForceByCollisionAngle;
        this.multiplyColliderForceByParticleSpeed = reference.multiplyColliderForceByParticleSpeed;
        this.multiplyColliderForceByParticleSize = reference.multiplyColliderForceByParticleSize;
#endif
    }

    public override void SetData(ref ParticleSystem.CollisionModule reference)
    {
        reference.enabled = this.enabled;
        reference.type = (ParticleSystemCollisionType)this.type;
        reference.mode = (ParticleSystemCollisionMode)this.mode;

        if (this.dampen != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.dampen;
            this.dampen.SetData(ref curve);
            reference.dampen = curve;
        }

        reference.dampenMultiplier = this.dampenMultiplier;

        if (this.bounce != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.bounce;
            this.bounce.SetData(ref curve);
            reference.bounce = curve;
        }

        reference.bounceMultiplier = this.bounceMultiplier;

        if (this.lifetimeLoss != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.lifetimeLoss;
            this.lifetimeLoss.SetData(ref curve);
            reference.lifetimeLoss = curve;
        }

        reference.lifetimeLossMultiplier = this.lifetimeLossMultiplier;
        reference.minKillSpeed = this.minKillSpeed;
        reference.maxKillSpeed = this.maxKillSpeed;

        if (this.collidesWith != null)
        {
            LayerMask mask = reference.collidesWith;
            this.collidesWith.SetData(ref mask);
            reference.collidesWith = mask;
        }

        reference.enableDynamicColliders = this.enableDynamicColliders;
        reference.maxCollisionShapes = this.maxCollisionShapes;
        reference.quality = (ParticleSystemCollisionQuality)this.quality;
        reference.voxelSize = this.voxelSize;
        reference.radiusScale = this.radiusScale;
        reference.sendCollisionMessages = this.sendCollisionMessages;

#if UNITY_2017_1_OR_NEWER
        reference.colliderForce = this.colliderForce;
        reference.multiplyColliderForceByCollisionAngle = this.multiplyColliderForceByCollisionAngle;
        reference.multiplyColliderForceByParticleSpeed = this.multiplyColliderForceByParticleSpeed;
        reference.multiplyColliderForceByParticleSize = this.multiplyColliderForceByParticleSize;
#endif
    }

    public override ParticleSystem.CollisionModule ToUnityType()
    {
        ParticleSystem.CollisionModule col = new ParticleSystem.CollisionModule();
        SetData(ref col);
        return col;
    }
}
