﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a ExternalForcesModule
/// </summary>
[System.Serializable]
public class EExternalForcesModule : SerializableUnityData<ParticleSystem.ExternalForcesModule>
{
    /// <summary>
    /// The enabled state of this EExternalForcesModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The multiplier of this EExternalForcesModule
    /// </summary>
    public float multiplier;

    /// <summary>
    /// Create a new EExternalForcesModule
    /// </summary>
    public EExternalForcesModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new EExternalForcesModule
    /// </summary>
    /// <param name="reference">The ExternalForcesModule you want to use</param>
    public EExternalForcesModule(ParticleSystem.ExternalForcesModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.multiplier = reference.multiplier;
    }

    public override void SetData(ref ParticleSystem.ExternalForcesModule reference)
    {
        reference.enabled = this.enabled;
        reference.multiplier = this.multiplier;
    }

    public override ParticleSystem.ExternalForcesModule ToUnityType()
    {
        ParticleSystem.ExternalForcesModule module = new ParticleSystem.ExternalForcesModule();
        SetData(ref module);
        return module;
    }
}
