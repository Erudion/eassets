﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a ShapeModule
/// </summary>
[System.Serializable]
public class EShapeModule : SerializableUnityData<ParticleSystem.ShapeModule>
{
    /// <summary>
    /// The enabled state of this EShapeModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The shapeType of this EShapeModule
    /// </summary>
    public int shapeType;
    /// <summary>
    /// The randomDirectionAmount of this EShapeModule
    /// </summary>
    public float randomDirectionAmount;
    /// <summary>
    /// The sphericalDirectionAmount of this EShapeModule
    /// </summary>
    public float sphericalDirectionAmount;
#if UNITY_2017_1_OR_NEWER
    /// <summary>
    /// The randomPositionAmount of this EShapeModule
    /// </summary>
    public float randomPositionAmount;
#endif
    /// <summary>
    /// The alignToDirection state of this EShapeModule
    /// </summary>
    public bool alignToDirection;
    /// <summary>
    /// The radius of this EShapeModule
    /// </summary>
    public float radius;
    /// <summary>
    /// The radiusMode of this EShapeModule
    /// </summary>
    public int radiusMode;
    /// <summary>
    /// The radiusSpread of this EShapeModule
    /// </summary>
    public float radiusSpread;
    /// <summary>
    /// The radiusSpeed of this EShapeModule
    /// </summary>
    public EMinMaxCurve radiusSpeed;
    /// <summary>
    /// The radiusSpeedMultiplier of this EShapeModule
    /// </summary>
    public float radiusSpeedMultiplier;
#if UNITY_2017_1_OR_NEWER
    /// <summary>
    /// The radiusThickness of this EShapeModule
    /// </summary>
    public float radiusThickness;
#endif
    /// <summary>
    /// The angle of this EShapeModule
    /// </summary>
    public float angle;
    /// <summary>
    /// The length of this EShapeModule
    /// </summary>
    public float length;
#if UNITY_2017_1_OR_NEWER
    /// <summary>
    /// The boxThickness of this EShapeModule
    /// </summary>
    public EVector3 boxThickness;
#endif
    /// <summary>
    /// The meshShapeType of this EShapeModule
    /// </summary>
    public int meshShapeType;
    /// <summary>
    /// The mesh of this EShapeModule
    /// </summary>
    public EMesh mesh;
    /// <summary>
    /// The meshRenderer of this EShapeModule
    /// </summary>
    public EMeshRenderer meshRenderer;
    /// <summary>
    /// The skinnedMeshRenderer of this EShapeModule
    /// </summary>
    public ESkinnedMeshRenderer skinnedMeshRenderer;
    /// <summary>
    /// The useMeshMaterialIndex state of this EShapeModule
    /// </summary>
    public bool useMeshMaterialIndex;
    /// <summary>
    /// The meshMaterialIndex of this EShapeModule
    /// </summary>
    public int meshMaterialIndex;
    /// <summary>
    /// The useMeshColors state of this EShapeModule
    /// </summary>
    public bool useMeshColors;
    /// <summary>
    /// The normalOffset of this EShapeModule
    /// </summary>
    public float normalOffset;
    /// <summary>
    /// The arc of this EShapeModule
    /// </summary>
    public float arc;
    /// <summary>
    /// The arcMode of this EShapeModule
    /// </summary>
    public int arcMode;
    /// <summary>
    /// The arcSpread of this EShapeModule
    /// </summary>
    public float arcSpread;
    /// <summary>
    /// The arcSpeed of this EShapeModule
    /// </summary>
    public EMinMaxCurve arcSpeed;
    /// <summary>
    /// The arcSpeedMultiplier of this EShapeModule
    /// </summary>
    public float arcSpeedMultiplier;
#if UNITY_2017_1_OR_NEWER
    /// <summary>
    /// The donutRadius of this EShapeModule
    /// </summary>
    public float donutRadius;
    /// <summary>
    /// The position of this EShapeModule
    /// </summary>
    public EVector3 position;
    /// <summary>
    /// The rotation of this EShapeModule
    /// </summary>
    public EVector3 rotation;
    /// <summary>
    /// The scale of this EShapeModule
    /// </summary>
    public EVector3 scale;
#endif

    /// <summary>
    /// Create a new EShapeModule
    /// </summary>
    public EShapeModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new EShapeModule
    /// </summary>
    /// <param name="reference">The ShapeModule you want to use</param>
    public EShapeModule(ParticleSystem.ShapeModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.shapeType = (int)reference.shapeType;
        this.randomDirectionAmount = reference.randomDirectionAmount;
        this.sphericalDirectionAmount = reference.sphericalDirectionAmount;
#if UNITY_2017_1_OR_NEWER
        this.randomPositionAmount = reference.randomPositionAmount;
#endif
        this.alignToDirection = reference.alignToDirection;
        this.radius = reference.radius;
        this.radiusMode = (int)reference.radiusMode;
        this.radiusSpread = reference.radiusSpread;
        this.radiusSpeed = new EMinMaxCurve(reference.radiusSpeed);
        this.radiusSpeedMultiplier = reference.radiusSpeedMultiplier;
#if UNITY_2017_1_OR_NEWER
        this.radiusThickness = reference.radiusThickness;
#endif
        this.angle = reference.angle;
        this.length = reference.length;
#if UNITY_2017_1_OR_NEWER
        this.boxThickness = new EVector3(reference.boxThickness);
#endif
        this.meshShapeType = (int)reference.meshShapeType;

        if(reference.mesh != null)
            this.mesh = new EMesh(reference.mesh);

        if(reference.meshRenderer != null)
            this.meshRenderer = new EMeshRenderer(reference.meshRenderer);

        if(reference.skinnedMeshRenderer != null)
            this.skinnedMeshRenderer = new ESkinnedMeshRenderer(reference.skinnedMeshRenderer);

        this.useMeshMaterialIndex = reference.useMeshMaterialIndex;
        this.meshMaterialIndex = reference.meshMaterialIndex;
        this.useMeshColors = reference.useMeshColors;
        this.normalOffset = reference.normalOffset;
        this.arc = reference.arc;
        this.arcMode = (int)reference.arcMode;
        this.arcSpread = reference.arcSpread;
        this.arcSpeed = new EMinMaxCurve(reference.arcSpeed);
        this.arcSpeedMultiplier = reference.arcSpeedMultiplier;
#if UNITY_2017_1_OR_NEWER
        this.donutRadius = reference.donutRadius;
        this.position = new EVector3(reference.position);
        this.rotation = new EVector3(reference.rotation);
        this.scale = new EVector3(reference.scale);
#endif
    }

    public override void SetData(ref ParticleSystem.ShapeModule reference)
    {
        reference.enabled = this.enabled;
        reference.shapeType = (ParticleSystemShapeType)this.shapeType;
        reference.randomDirectionAmount = this.randomDirectionAmount;
        reference.sphericalDirectionAmount = this.sphericalDirectionAmount;
#if UNITY_2017_1_OR_NEWER
        reference.randomPositionAmount = this.randomPositionAmount;
#endif
        reference.alignToDirection = this.alignToDirection;
        reference.radius = this.radius;
        reference.radiusMode = (ParticleSystemShapeMultiModeValue)this.radiusMode;
        reference.radiusSpread = this.radiusSpread;

        if (this.radiusSpeed != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.radiusSpeed;
            this.radiusSpeed.SetData(ref curve);
            reference.radiusSpeed = curve;
        }

        reference.radiusSpeedMultiplier = this.radiusSpeedMultiplier;

#if UNITY_2017_1_OR_NEWER
        reference.radiusThickness = this.radiusThickness;
#endif
        reference.angle = this.angle;
        reference.length = this.length;
#if UNITY_2017_1_OR_NEWER
        reference.boxThickness = this.boxThickness.ToUnityType();
#endif

        reference.meshShapeType = (ParticleSystemMeshShapeType)this.meshShapeType;

        if (this.mesh != null && reference.mesh != null)
        {
            Mesh mesh = reference.mesh;
            this.mesh.SetData(ref mesh);
        }
        if (this.meshRenderer != null && reference.meshRenderer != null)
        {
            MeshRenderer meshrenderer = reference.meshRenderer;
            this.meshRenderer.SetData(ref meshrenderer);
        }
        if (this.skinnedMeshRenderer != null && reference.skinnedMeshRenderer != null)
        {
            SkinnedMeshRenderer mesh = reference.skinnedMeshRenderer;
            this.skinnedMeshRenderer.SetData(ref mesh);
        }

        reference.useMeshMaterialIndex = this.useMeshMaterialIndex;
        reference.meshMaterialIndex = this.meshMaterialIndex;
        reference.useMeshColors = this.useMeshColors;
        reference.normalOffset = this.normalOffset;
        reference.arc = this.arc;
        reference.arcMode = (ParticleSystemShapeMultiModeValue)this.arcMode;
        reference.arcSpread = this.arcSpread;

        if (this.arcSpeed != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.arcSpeed;
            this.arcSpeed.SetData(ref curve);
            reference.arcSpeed = curve;
        }

        reference.arcSpeedMultiplier = this.arcSpeedMultiplier;

#if UNITY_2017_1_OR_NEWER
        reference.donutRadius = this.donutRadius;
        reference.position = this.position.ToUnityType();
        reference.rotation = this.rotation.ToUnityType();
        reference.scale = this.scale.ToUnityType();
#endif
    }

    public override ParticleSystem.ShapeModule ToUnityType()
    {
        ParticleSystem.ShapeModule module = new ParticleSystem.ShapeModule();
        SetData(ref module);
        return module;
    }
}
