﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a EmissionModule
/// </summary>
[System.Serializable]
public class EEmissionModule : SerializableUnityData<ParticleSystem.EmissionModule>
{
    /// <summary>
    /// The enabled state of this EEmissionModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The rateOverTime of this EEmissionModule
    /// </summary>
    public EMinMaxCurve rateOverTime;
    /// <summary>
    /// The rateOverTimeMultiplier of this EEmissionModule
    /// </summary>
    public float rateOverTimeMultiplier;
    /// <summary>
    /// The rateOverDistance of this EEmissionModule
    /// </summary>
    public EMinMaxCurve rateOverDistance;
    /// <summary>
    /// The rateOverDistanceMultiplier of this EEmissionModule
    /// </summary>
    public float rateOverDistanceMultiplier;

    /// <summary>
    /// Create a new EEmissionModule
    /// </summary>
    public EEmissionModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new EEmissionModule
    /// </summary>
    /// <param name="reference">The EmissionModule you want to use</param>
    public EEmissionModule(ParticleSystem.EmissionModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.rateOverTime = new EMinMaxCurve(reference.rateOverTime);
        this.rateOverTimeMultiplier = reference.rateOverTimeMultiplier;
        this.rateOverDistance = new EMinMaxCurve(reference.rateOverTimeMultiplier);
        this.rateOverDistanceMultiplier = reference.rateOverTimeMultiplier;
    }

    public override void SetData(ref ParticleSystem.EmissionModule reference)
    {
        reference.enabled = this.enabled;
        reference.rateOverTime = this.rateOverTime.ToUnityType();
        reference.rateOverTimeMultiplier = this.rateOverTimeMultiplier;
        reference.rateOverDistance = this.rateOverDistance.ToUnityType();
        reference.rateOverDistanceMultiplier = this.rateOverTimeMultiplier;
    }

    public override ParticleSystem.EmissionModule ToUnityType()
    {
        ParticleSystem.EmissionModule module = new ParticleSystem.EmissionModule();
        SetData(ref module);
        return module;
    }
}
