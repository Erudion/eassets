﻿using EAssets.IO.SerializableDatatypes;
using UnityEngine;

/// <summary>
/// This class can be used to serialize a LightModule
/// </summary>
[System.Serializable]
public class ELightsModule : SerializableUnityData<ParticleSystem.LightsModule>
{
    /// <summary>
    /// The enabled state of this ELightsModule
    /// </summary>
    public bool enabled;
    /// <summary>
    /// The ratio of this ELightsModule
    /// </summary>
    public float ratio;
    /// <summary>
    /// The useRandomDistribution state of this ELightsModule
    /// </summary>
    public bool useRandomDistribution;
    /// <summary>
    /// The light of this ELightsModule
    /// </summary>
    public ELight light;
    /// <summary>
    /// The useParticleColor state of this ELightsModule
    /// </summary>
    public bool useParticleColor;
    /// <summary>
    /// The sizeAffectsRange state of this ELightsModule
    /// </summary>
    public bool sizeAffectsRange;
    /// <summary>
    /// The alphaAffectsIntensity state of this ELightsModule
    /// </summary>
    public bool alphaAffectsIntensity;
    /// <summary>
    /// The range of this ELightsModule
    /// </summary>
    public EMinMaxCurve range;
    /// <summary>
    /// The rangeMultiplier of this ELightsModule
    /// </summary>
    public float rangeMultiplier;
    /// <summary>
    /// The intensity of this ELightsModule
    /// </summary>
    public EMinMaxCurve intensity;
    /// <summary>
    /// The intensityMultiplier of this ELightsModule
    /// </summary>
    public float intensityMultiplier;
    /// <summary>
    /// The maxLights of this ELightsModule
    /// </summary>
    public int maxLights;

    /// <summary>
    /// Create a new ELightModule
    /// </summary>
    public ELightsModule() : base(false)
    {

    }

    /// <summary>
    /// Create a new ELightModule
    /// </summary>
    /// <param name="reference">The LightModule you want to use</param>
    public ELightsModule(ParticleSystem.LightsModule reference) : base(false)
    {
        this.enabled = reference.enabled;
        this.ratio = reference.ratio;
        this.useRandomDistribution = reference.useRandomDistribution;
        if(reference.light != null)
            this.light = new ELight(reference.light);
        this.useParticleColor = reference.useParticleColor;
        this.sizeAffectsRange = reference.sizeAffectsRange;
        this.alphaAffectsIntensity = reference.alphaAffectsIntensity;
        this.range = new EMinMaxCurve(reference.range);
        this.rangeMultiplier = reference.rangeMultiplier;
        this.intensity = new EMinMaxCurve(reference.intensity);
        this.intensityMultiplier = reference.intensityMultiplier;
        this.maxLights = reference.maxLights;
    }

    public override void SetData(ref ParticleSystem.LightsModule reference)
    {
        reference.enabled = this.enabled;
        reference.ratio = this.ratio;
        reference.useRandomDistribution = this.useRandomDistribution;

        if (this.light != null)
        {
            Light light = reference.light;
            this.light.SetData(ref light);
        }

        reference.useParticleColor = this.useParticleColor;
        reference.sizeAffectsRange = this.sizeAffectsRange;
        reference.alphaAffectsIntensity = this.alphaAffectsIntensity;

        if (this.range != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.range;
            this.range.SetData(ref curve);
            reference.range = curve;
        }

        reference.rangeMultiplier = this.rangeMultiplier;

        if (this.intensity != null)
        {
            ParticleSystem.MinMaxCurve curve = reference.intensity;
            this.intensity.SetData(ref curve);
            reference.intensity = curve;
        }

        reference.intensityMultiplier = this.intensityMultiplier;
        reference.maxLights = this.maxLights;
    }

    public override ParticleSystem.LightsModule ToUnityType()
    {
        ParticleSystem.LightsModule module = new ParticleSystem.LightsModule();
        SetData(ref module);
        return module;
    }
}
