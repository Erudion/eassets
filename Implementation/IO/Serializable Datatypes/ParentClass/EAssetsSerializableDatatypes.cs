﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace EAssets
{
    namespace IO
    {
        //Since Unity data types are non-serializable, 
        //these Wrapper-Classes should allow easier 
        //saving and loading of those data-types
        //If you want to serialize some data from unity,
        //implement a wrapper class. This class should
        //inherit from SerializableUnityData and use the
        //type you want to serialize.
        //The wrapper class needs to store all important
        //data: e.g. Vector3 should store x, y and z.
        namespace SerializableDatatypes
        {
            #region Parent Classes
            /// <summary>
            /// This class is the base class for the SerializableUnityData class
            /// <para>You should not inherit from this class</para>
            /// </summary>
            [System.Serializable]
            public abstract class ParentSerializableClass
            {
                /// <summary>
                /// Allows to directly load data. A reference is not needed
                /// This means that the Load<> method and the ToUnityType method
                /// can both be used. Otherwise both will throw an error
                /// </summary>
                public bool IsDirectlyLoadable { get; protected set; } = true;
            }

            /// <summary>
            /// This class is used to serialize unity-data.
            /// <para>Implement a class with this to add it to the serializable data types</para>
            /// <para>Style: public class Classname:SerializableUnityData[Type you want to serialize/implement]</para>
            /// </summary>
            [System.Serializable]
            public abstract class SerializableUnityData<T> : ParentSerializableClass
            {
                /// <summary>
                /// Returns a unity version of the wrapper type
                /// </summary>
                /// <returns></returns>
                public abstract T ToUnityType();

                /// <summary>
                /// Sets the data of the unity data with the data of the wrapper class
                /// </summary>
                /// <param name="reference">The unity data you want to set</param>
                public abstract void SetData(ref T reference);

                /// <summary>
                /// Create a new SerializableUnityData
                /// </summary>
                /// <param name="loadable">Defines if Load and ToUnityData can be used</param>
                public SerializableUnityData(bool loadable = true)
                {
                    //Set the loadability
                    IsDirectlyLoadable = loadable;
                }
            }
            #endregion

            #region Reflection Helper
            /// <summary>
            /// Allows you to get all subclasses of a given type/class
            /// </summary>
            public static class ReflectiveEnumerator
            {
                static ReflectiveEnumerator() { }
                /// <summary>
                /// Get all subclasses of a given type/class
                /// </summary>
                /// <typeparam name="T">The class you want to check</typeparam>
                /// <param name="constructorArgs">Values used to initialize an instance</param>
                /// <returns></returns>
                public static int GetSubClassCount<T>(params object[] constructorArgs) where T : class
                {
                    List<T> objects = new List<T>();
                    foreach (Type type in
                        Assembly.GetAssembly(typeof(T)).GetTypes()
                        .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(T))))
                    {
                        objects.Add((T)Activator.CreateInstance(type, constructorArgs));
                    }
                    return objects.Count;
                }
                /// <summary>
                /// Returns a list of all subclasses of a given class/type
                /// </summary>
                /// <typeparam name="T">The type you want to check</typeparam>
                /// <param name="constructorArgs">The used constructor arguments</param>
                /// <returns></returns>
                public static List<Type> GetSubClassTypes<T>(params object[] constructorArgs) where T : class
                {
                    List<Type> objects = new List<Type>();
                    foreach (Type type in
                        Assembly.GetAssembly(typeof(T)).GetTypes()
                        .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(T))))
                    {
                        objects.Add(Activator.CreateInstance(type, constructorArgs).GetType());
                    }
                    return objects;
                }
            }
            #endregion
        }
    }
}
