﻿using System;
using System.Collections.Generic;
using UnityEngine;
using EAssets.IO.SerializableDatatypes; //This import is used to serialize unity types in the savefile class
using EAssets.Extensions.Objects;
using EAssets.Extensions.Types;

//Info to the savefile: The savefile currently works with reflection
//In this case this means that the classes you want to 
//serialize/deserialize need to have an accessible constructor
namespace EAssets
{
    namespace IO
    {
        namespace Saving
        {
            /// <summary>
            /// This class creates a savefile in which you can store your data. 
            /// </summary>
            public class Savefile
            {
                #region Attributes
                /// <summary>
                /// The name under which this savefile will be stored
                /// </summary>
                private readonly string filename;
                /// <summary>
                /// The data to be saved
                /// </summary>
                private Dictionary<string, object> savedata;
                /// <summary>
                /// If true, the data is saved everytime new data is written
                /// </summary>
                private readonly bool autosave;

                /// <summary>
                /// All supported wrapper types
                /// </summary>
                private static List<Type> myUnityTypes = new List<Type>();
                /// <summary>
                /// All supported unity types
                /// </summary>
                private static List<Type> unityTypes = new List<Type>();

                /// <summary>
                /// A map for unity built in to custom types
                /// </summary>
                private static Dictionary<Type, Type> UnityToCustomTypes;
                /// <summary>
                /// A map for custom to built in unity types
                /// </summary>
                private static Dictionary<Type, Type> CustomToUnityTypes;
                #endregion

                #region Convenience Properties
                /// <summary>
                /// Get all the keys in the savefile
                /// </summary>
                public List<string> Keys
                {
                    get
                    {
                        List<string> tmp = new List<string>();
                        foreach (KeyValuePair<string, object> item in savedata)
                        {
                            tmp.Add(item.Key);
                        }

                        return tmp;
                    }
                }
                /// <summary>
                /// Get all data in the savefile
                /// </summary>
                public List<object> Data
                {
                    get
                    {
                        List<object> tmp = new List<object>();
                        foreach (KeyValuePair<string, object> item in savedata)
                        {
                            tmp.Add(item.Value);
                        }

                        return tmp;
                    }
                }

                /// <summary>
                /// Returns the number of entries of this savefile
                /// </summary>
                public int Entries
                {
                    get
                    {
                        return savedata.Count;
                    }
                }
                #endregion

                #region Constructor
                /// <summary>
                /// Create a new savefile
                /// </summary>
                /// <param name="savefileName">The name of this savefile</param>
                /// <param name="filetype">The type of this savefile</param>
                public Savefile(string savefileName, bool autosaveOnWrite = false)
                {
                    //Regular data setup
                    filename = savefileName;
                    autosave = autosaveOnWrite;
                    savedata = new Dictionary<string, object>();

                    if (myUnityTypes == null || myUnityTypes.Count == 0)  //Setup the type information (only once!)
                    {
                        //Get all classes that inherit from ParentSerializableClass
                        myUnityTypes = ReflectiveEnumerator.GetSubClassTypes<ParentSerializableClass>();

                        if (unityTypes == null || unityTypes.Count == 0) //If not setup, setup the unityTypes
                        {
                            unityTypes = new List<Type>();
                            foreach (Type t in myUnityTypes) //Get all implemented wrapper types
                            {
                                unityTypes.Add(t.BaseType.GetGenericArguments()[0]);
                            }
                        }
                    }

                    //Set the type maps
                    if (UnityToCustomTypes == null || CustomToUnityTypes == null)
                    {
                        UnityToCustomTypes = new Dictionary<Type, Type>();
                        CustomToUnityTypes = new Dictionary<Type, Type>();
                        for (int i = 0; i < myUnityTypes.Count; i++)
                        {
                            UnityToCustomTypes.Add(unityTypes[i], myUnityTypes[i]);
                            CustomToUnityTypes.Add(myUnityTypes[i], unityTypes[i]);
                        }
                    }
                }
                #endregion

                #region Static Helper Methods
                /// <summary>
                /// Returns a loaded savefile
                /// </summary>
                /// <param name="name">The name of the savefile you want to load</param>
                /// <returns></returns>
                public static Savefile LoadSavefile(string name)
                {
                    Savefile file = new Savefile(name);
                    file.LoadFromFile();
                    return file;
                }
                #endregion

                #region Save/Load Methods
                /// <summary>
                /// Save the data stored in the savefile
                /// <para>This is only required if autosave is set to false</para>
                /// </summary>
                public void SaveToFile()
                {
                    DataIO.WriteDataBinary(savedata, this.filename, "Savefiles");
                }

                /// <summary>
                /// Load data from a file with a matching name
                /// </summary>
                public void LoadFromFile()
                {
                    DataIO.ReadDataBinary<Dictionary<string, object>>(out savedata, this.filename, "Savefiles");
                }
                #endregion

                #region Add Data
                /// <summary>
                /// Add data to the savefile
                /// <para>If autosave is on it is directly written to the file</para>
                /// </summary>
                /// <param name="key">The name of the data</param>
                /// <param name="data">The data</param>
                /// <example>
                /// <code>
                /// //Create a new savefile or use a predefined one
                /// Savefile file = new Savefile("savefileName");
                /// 
                /// //Get some arbitrary data
                /// typeToSave data = someData
                /// 
                /// //Store the data in the savefile
                /// file.Save{typeToSave}("dataKey",data);
                /// </code>
                /// </example>
                public void Save<T>(string key, T data)
                {
                    if (savedata == null) //Create a new dictionary if there is none yet
                        savedata = new Dictionary<string, object>();

                    if (savedata.ContainsKey(key)) //Print a warning if the key already exists
                        Debug.LogWarning("Key already existed: " + key + "; Data was still stored.");

                    if (unityTypes.Contains(data.GetType())) //Unity data needs to be converted before storing
                    {
                        //Convert unity data
                        savedata[key] = Activator.CreateInstance(UnityToCustomTypes[data.GetType()], data);
                    }
                    else //Regular types can be stored without conversion
                    {
                        savedata[key] = data;
                    }

                    if (autosave) //If autosave is true store data to file
                        SaveToFile();
                }
                #endregion

                #region Get Data
                /// <summary>
                /// Get data from the savefile
                /// </summary>
                /// <typeparam name="T">The type of data you want returned</typeparam>
                /// <param name="key">The key of the data you want returned</param>
                /// <param name="defaultValue">The default value returned in case of a mismatch</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Get a savefile from file
                /// Savefile file = Savefile.LoadSavefile("savefileName");
                /// 
                /// //Loads data from the savefile. If the data exists it is returned else the given default value is returned
                /// typeToLoad value = file.Load{typeToLoad}("dataKey",defaultValue);
                /// 
                /// //Do something with the loaded value here...
                /// </code>
                /// </example>
                public T Load<T>(string key, T defaultValue = default)
                {
                    if (savedata == null)   //If there is no savefile return a default
                        return defaultValue;

                    object tmp = savedata[key]; //Get the stored data

                    bool loadable = true; //Check if this data is directly loadable
                    if (myUnityTypes.Contains(tmp.GetType()))
                    {

                        if (tmp is SerializableUnityData<T> savedata)
                        {
                            //Get the IsDirectlyLoadable attribute
                            object o = Activator.CreateInstance(UnityToCustomTypes[typeof(T)], null);

                            if (o is ParentSerializableClass psc)
                                loadable = psc.IsDirectlyLoadable;
                            else
                                loadable = false;


                            //If directly loadable return the unity type object
                            if (loadable)
                            {
                                return savedata.ToUnityType();
                            }
                        }
                    }

                    if (loadable) //If loadable this means it is a loadable non unity type
                    {
                        if (tmp.GetType().Equals(typeof(T)))
                        {
                            return (T)tmp;
                        }
                        else
                        {
                            DataIO.GlobalLogWarning("Typemismatch at Load. Expected type: " + tmp.GetType() + "; Requested type: " + typeof(T));
                            return defaultValue;
                        }
                    }
                    else //Else you cant load it
                    {
                        Debug.LogWarning("Data of type " + tmp.GetType() + " are not directly loadable!");
                        return defaultValue;
                    }

                }

                /// <summary>
                /// Load data into an object
                /// </summary>
                /// <typeparam name="T">Must be a class</typeparam>
                /// <param name="key">The key you want to use</param>
                /// <param name="reference">The object you want to load data into</param>
                /// <example>
                /// <code>
                /// //Get a savefile from file
                /// Savefile file = Savefile.LoadSavefile("savefileName");
                /// 
                /// SomeReferenceType reference = someReference;
                /// 
                /// //Loads data from the savefile by storing the data in a given reference
                /// //The ref keyword allows you to load value types (like structs, floats or the like)
                /// //to be loaded this way as well
                /// file.LoadInto{typeToLoad}("dataKey",ref reference);
                /// 
                /// //Do something with the loaded data here...
                /// reference.SomeMethod();
                /// refrence.Field = someData;
                /// etc. ...
                /// </code>
                /// </example>
                public void LoadInto<T>(string key, ref T reference)
                {
                    if (savedata == null || savedata.Count == 0)
                        return;

                    object tmp = savedata[key]; //The data stored
                    if (myUnityTypes.Contains(tmp.GetType()))
                    {

                        if (tmp is SerializableUnityData<T> savedata)
                        {
                            savedata.SetData(ref reference);
                        }
                        else
                        {
                            if (unityTypes.Contains(typeof(T)))
                            {
                                Debug.LogWarning("Could not load data from type " + typeof(T)
                                    + " since it was not defined");
                            }
                            return;
                        }
                    }
                    else
                    {
                        reference = (T)tmp;
                    }
                }

                /// <summary>
                /// Returns a read-only container for the data of this savefile
                /// </summary>
                /// <returns></returns>
                public SavefileData GetSavefileData()
                {
                    return new SavefileData(this.filename, this.savedata);
                }
                #endregion
            }

            /// <summary>
            /// This is a container class to store the data from a savefile
            /// </summary>
            public class SavefileData
            {
                #region Attributes
                /// <summary>
                /// The name of the file where this data comes from
                /// </summary>
                public string Filename { get; private set; }
                /// <summary>
                /// The data from the savefile
                /// </summary>
                public Dictionary<string, object> Data { get; private set; }
                #endregion

                #region Constructor
                /// <summary>
                /// Create a new container for the savedata
                /// </summary>
                /// <param name="dic"></param>
                public SavefileData(string filename, Dictionary<string, object> dic)
                {
                    this.Filename = filename;
                    this.Data = dic;
                }
                #endregion

                public static implicit operator Dictionary<string, object>(SavefileData data)
                {
                    return data.Data;
                }
            }
        }
    }
}
