﻿using UnityEngine;

/// <summary>
/// This class contains some serializable data to show that this data can be stored
/// </summary>
public class SimpleMonoBehaviour : MonoBehaviour
{
    public int myInt;
    public float myFloat;
    public string myString;
    public decimal myDecimal;

    public Vector3 vec = new Vector3(1,2,3);
}
