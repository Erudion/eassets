﻿using UnityEngine;
using EAssets.IO.Saving;

public class SavefileExample : MonoBehaviour
{
    /// <summary>
    /// Create a new savefile, initialize it in Awake, Start or the like
    /// since unity has no access to PersistentDataPath on creation of an object
    /// which is needed for the savefile
    /// </summary>
    public Savefile savefile;

    /// <summary>
    /// A reference to a gameobject that we want to save
    /// </summary>
    private GameObject go;

    void Awake()
    {
        //This notation is recommended when loading an existent savefile
        //This helps keeping clear that no new savefile is created
        //savefile = Savefile.LoadSavefile("mySavefile");

        //This notation is recommended when creating a new savefile
        //The savefile needs a name and no more inputs
        savefile = new Savefile("mySavefile");

        //You can directly set data if you want to
        savefile.Save("myInt",6);
        //If you try to write the same key twice the savefile will throw a warning
        //but it will still save the data (the warning is made to avoid accidentaly
        //overwrite needed data with another type or the like)
        savefile.Save("myInt", 7);

        //You can load data by giving its name and type
        Debug.Log(savefile.Load<int>("myInt"));

        //On a typemismatch the value is not returned.
        //The savefile returns a default value or the
        //given default-value (in this case 2.2)
        //Debug.Log(savefile.Load<float>("myInt", 2.2f));


        //Now lets save a gameobject and all its components
        go = GameObject.Find("SavedCube");
        savefile.Save("SavedCube", go);

        //Now lets change the data a bit
        
        //Change position and rotation
        go.transform.position = new Vector3(2,3,4);
        go.transform.rotation = Quaternion.Euler(45,25,10);
        
        //Change the boxcollider properties
        go.GetComponent<BoxCollider>().center = new Vector3(2,2,2);
        go.GetComponent<BoxCollider>().isTrigger = true;

        //Change the values of the SimpleMonobehaviour
        go.GetComponent<SimpleMonoBehaviour>().myInt = 999;
        go.GetComponent<SimpleMonoBehaviour>().myString = "No way!";
        go.GetComponent<SimpleMonoBehaviour>().myDecimal = 999.999M;
        go.GetComponent<SimpleMonoBehaviour>().vec = new Vector3(9,9,9);
    }

    private void Update()
    {
        //Press R to load the data that was saved before changing the values
        if (Input.GetKeyDown(KeyCode.R))
        {
            //Instantiable data can be loaded using load
            //Some data can or should not be created on load
            //use LoadInto to load the data into a given reference instead
            Debug.Log(savefile.Load<int>("myInt"));

            //LoadInto takes a key of the savefile and an object reference
            //to load the data directly into the reference.
            //Note that the ref keyword is used to allow loading data
            //into structs and variables as well
            savefile.LoadInto<GameObject>("SavedCube", ref go);

            Debug.Log("Hello");
        }
    }
}
