﻿using UnityEngine;
using EAssets.IO;
using EAssets.IO.Cached;

/// <summary>
/// This class shows how to use the DataIO and the CachedDataIO
/// </summary>
public class IOManager : MonoBehaviour
{
    //These three values are simple types and serializable to binary, xml and json
    //This is important since the DataIO supports all 3 extensions and so should the
    //data if stored with a given extension

    public int myValue = 1; //Just some int value
    public float myOtherValue = 2.3f; //Just some float value
    public string myString = "Hello!"; //Just some string

    // Start is called before the first frame update
    void Start()
    {
        //Lets beginn by writing data to file using the DataIO class
        //You need to specify: The value to write (can be a class but 
        //MUST be serializable), the name of the file to create/overwrite
        //and the relative path. The DataIO always writes directly into the
        //PersistentDataPath of Unity. Therefor you can only set a relative path
        //to that directory
        DataIO.WriteDataBinary(myValue, "myNewFile0", "myPath");
        DataIO.WriteDataBinary(myOtherValue, "myNewFile1", "myPath");
        DataIO.WriteDataBinary(myString, "myNewFile2", "myPath");

        //You can also store data in a XML format
        //DataIO.WritaDataXML(myValue, "myNewFile0", "myPath");

        //You can also store data in a JSON format
        //DataIO.WriteDataJSON(myValue, "myNewFile0", "myPath");

        //You can also use the Cached version of the DataIO class
        CachedDataIO.SaveData(myValue, "myNewFile0", "myPath");
        CachedDataIO.SaveData(myOtherValue, "myNewFile1", "myPath");
        CachedDataIO.SaveData(myString, "myNewFile2", "myPath");

        //To store data as xml add the extension
        //CachedDataIO.SaveData(myValue, "myNewFile0", "myPath", ".xml");

        //To store data as json add the extension
        //CachedDataIO.SaveData(myValue, "myNewFile0", "myPath", ".json");
        //Currently supported types are .dat, .xml and .json


        //These values will NOT be directly stored to file
        //They will instead by cached and managed by the CachedDataIO
        //This allows for faster reading while cached it also minimizes
        //the writing count to 1. To store data to the filesystem you will
        //need to flush the CachedDataIO like this:
        CachedDataIO.FlushCache();

        //After flushing, the data will all get stored to the desired location in
        //the desired format (.dat, .xml or .json)
    }

    private void Update()
    {
        //Press A to load with the DataIO
        if (Input.GetKeyDown(KeyCode.A))
        {
            //You can also directly read xml and json
            //DataIO.ReadDataXML<int>(out int value, "myNewFile0", "myPath");
            //DataIO.ReadDataJSON<int>(out int value, "myNewFile0", "myPath");

            //Try to read the int from file
            if (DataIO.ReadDataBinary<int>(out int value, "myNewFile0", "myPath"))
            {
                Debug.Log(value);
            }
        }

        //Press D to load with the CachedDataIO
        if (Input.GetKeyDown(KeyCode.D))
        {
            //You can also directly read xml and json
            //CachedDataIO.LoadData<int>("myNewFile0", "myPath", ".xml");
            //CachedDataIO.LoadData<int>("myNewFile0", "myPath", ".json");

            //Read data into the CachedDataIO. It will then be accesible from there
            //as well since it will get cached, as will changes to it
            Debug.Log(CachedDataIO.LoadData<int>("myNewFile0", "myPath"));
            
        }
    }
}
