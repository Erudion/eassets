This example shows you how to use the DataIO and the CachedDataIO. The intention behind these classes is to simplify storing and loading persistent data.
The DataIO class allows to directly store/load data to file. The CachedDataIO caches the data for later storage.

Scene: The scene contains one object called "DataIOExample". This object contains a script called IOManager.
This script contains information on how to write data to file.
In the update section you find information on how to load data from the file system. Press A to load using the DataIO. Press D to load using the CachedDataIO