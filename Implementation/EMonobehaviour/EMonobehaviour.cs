﻿using EAssets.DataStorage;
using EAssets.Events;
using EAssets.IO.Saving;
using System.Collections.Generic;
using UnityEngine;

namespace EAssets
{
    public class EMonoBehaviour : MonoBehaviour, IEMonoBehaviour
    {
        /// <summary>
        /// Contains all runtime generated fields
        /// </summary>
        public List<string> RuntimeFields { get; set; } = new List<string>();
        /// <summary>
        /// Contains all runtime generated events
        /// </summary>
        public List<string> RegisteredEvents { get; set; } = new List<string>();

        /// <summary>
        /// A list of attached tags
        /// </summary>
        protected List<string> TagList;

        /// <summary>
        /// Contains saaved data for this object
        /// </summary>
        Savefile savefile;

        /// <summary>
        /// Create a new field for this class on the datadump.
        /// All runtime fields are static, public and scene persistent because they 
        /// are stored on the datadump
        /// </summary>
        /// <typeparam name="T">The type of the field</typeparam>
        /// <param name="fieldname">The name of the field</param>
        /// <param name="value">The value of the new field</param>
        protected void GenerateRuntimeField<T>(string fieldname, T value = default)
        {
            //Create a new entry on the datadump
            DataDump.Set<T>(fieldname, value);

            //Store the name of the field
            RuntimeFields.Add(fieldname);
        }

        /// <summary>
        /// Sets the runtime field of this object to a given value. Returns true if it exists, else false
        /// </summary>
        /// <typeparam name="T">The type of data to set</typeparam>
        /// <param name="fieldname">The name of the field</param>
        /// <param name="value">The value to set the field to</param>
        /// <returns></returns>
        protected bool SetRuntimeField<T>(string fieldname, T value)
        {
            if (RuntimeFields.Contains(fieldname))
                return DataDump.TrySet<T>(fieldname, value);
            else
                return false;
        }

        /// <summary>
        /// Try to get a runtime field of this object. Returns its value if it exists, else a default value
        /// </summary>
        /// <typeparam name="T">The type of the field to get</typeparam>
        /// <param name="fieldname">The name of the field you want to get</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns></returns>
        protected T GetRuntimeField<T>(string fieldname, T defaultValue = default)
        {
            if (!RuntimeFields.Contains(fieldname))
                return defaultValue;

            if (DataDump.TryGet<T>(fieldname, out T value))
            {
                return value;
            }

            return defaultValue;
        }

        /// <summary>
        /// Register a method to the Eventcontroller
        /// </summary>
        /// <typeparam name="T">The input type of the method</typeparam>
        /// <param name="key">The name of the event</param>
        /// <param name="method">The method you want to register</param>
        protected void RegisterEventMethod<T>(string key, TypedInputEvent<T>.TypedInputEventDelegate method)
        {
            EventController.Register<T>(key, method);
        }

        /// <summary>
        /// Signal an event in the Eventcontroller
        /// </summary>
        /// <typeparam name="T">The input value of the signal</typeparam>
        /// <param name="key">The signal name you want to send</param>
        /// <param name="value">The input value of the signal</param>
        protected void SignalMethod<T>(string key, T value)
        {
            EventController.Signal<T>(key, value);
        }

        /// <summary>
        /// Stores all EMonoBehaviour names for savefilecreation. The ulong contains a unique id
        /// (as long as it does not overflow)
        /// </summary>
        private static Dictionary<string, ulong> IDDictionary = new Dictionary<string, ulong>();

        private void Awake()
        {
            if(savefile == null)
            {
                Debug.Log("Loaded data");
                string EMonoName = gameObject.name + "_" + this.GetType().Name;

                if (!IDDictionary.ContainsKey(EMonoName))
                {
                    savefile = Savefile.LoadSavefile(EMonoName);
                    IDDictionary.Add(EMonoName, 0);
                    Debug.Log(EMonoName);
                }
                else //(IDDictionary.ContainsKey(EMonoName))
                {
                    ulong id = IDDictionary[EMonoName]; //Get the last id
                    ulong nID = id + 1; //Create a new ID 

                    string tmp = EMonoName; //Store the MonoName

                    EMonoName += "_" + nID; //Extend the EMonoName
                    savefile = Savefile.LoadSavefile(EMonoName); //Load the savefile/Create the savefile

                    IDDictionary[tmp] = nID; //Store the new ID
                }
            }
        }

        private void OnDestroy()
        {
            if(savefile != null)
            {
                Debug.Log("Saved data");
                savefile.SaveToFile();
            }
        }
    }

    /// <summary>
    /// This interface was made to help to receive data from EMonoBehaviours
    /// </summary>
    public interface IEMonoBehaviour
    {
        List<string> RuntimeFields { get; set; }

        List<string> RegisteredEvents { get; set; }
    }
}