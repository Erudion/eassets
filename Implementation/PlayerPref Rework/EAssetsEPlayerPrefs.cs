﻿using EAssets.IO.SerializableDatatypes;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace EAssets
{
    namespace EPlayerPrefs
    {
        public static class EPlayerPrefs
        {
            #region Standard Wrapper
            public static void SetInt(string key, int value)
            {
                PlayerPrefs.SetInt(key, value);
            }
            public static void SetFloat(string key, float value)
            {
                PlayerPrefs.SetFloat(key, value);
            }
            public static void SetString(string key, string value)
            {
                PlayerPrefs.SetString(key, value);
            }
            public static int GetInt(string key, int value)
            {
                return PlayerPrefs.GetInt(key, value);
            }
            public static float GetFloat(string key, float value)
            {
                return PlayerPrefs.GetFloat(key, value);
            }
            public static string GetString(string key, string value)
            {
                return PlayerPrefs.GetString(key, value);
            }

            public static bool HasKey(string key)
            {
                return PlayerPrefs.HasKey(key);
            }
            public static void DeleteAll(string key)
            {
                PlayerPrefs.DeleteKey(key);
            }
            public static void DeleteAll()
            {
                PlayerPrefs.DeleteAll();
            }
            public static void Save()
            {
                PlayerPrefs.Save();
            }
            #endregion

            #region Attributes
            /// <summary>
            /// The types of the children of PlayerPrefExtensionParent
            /// </summary>
            private static List<System.Type> prefChildren;
            /// <summary>
            /// The types of the generics of the children of PlayerPrefExtensionParent
            /// </summary>
            private static List<System.Type> prefChildrenTypes;
            /// <summary>
            /// The map of prefChildrenTypes to prefChildren
            /// </summary>
            private static Dictionary<System.Type, System.Type> TypeToWrapper;
            #endregion

            #region Generic Setters and Getters
            /// <summary>
            /// Set data
            /// </summary>
            /// <typeparam name="T">The type to set</typeparam>
            /// <param name="key">The key to use</param>
            /// <param name="value">The value to set</param>
            public static void Set<T>(string key, T value)
            {
                if (prefChildren == null)
                    GetTypeLists();

                if (TypeToWrapper.TryGetValue(typeof(T), out System.Type t))
                {
                    object o = System.Activator.CreateInstance(t);
                    PlayerPrefExtension<T> pref = (PlayerPrefExtension<T>)o;
                    SetT<T>(key, value, pref);
                    return;
                }
                else
                {
                    Debug.LogWarning("No wrapper found for type: " + typeof(T).ToString());
                    return;
                }
            }

            /// <summary>
            /// Get a value from the PlayerPrefs
            /// </summary>
            /// <typeparam name="T">The type to get</typeparam>
            /// <param name="key">The key to use</param>
            /// <returns></returns>
            public static T Get<T>(string key)
            {
                if (prefChildren == null)
                    GetTypeLists();

                if (TypeToWrapper.TryGetValue(typeof(T), out System.Type t))
                {
                    object o = System.Activator.CreateInstance(t);
                    PlayerPrefExtension<T> pref = (PlayerPrefExtension<T>)o;
                    return GetT<T>(key, pref);
                }
                else
                {
                    Debug.LogWarning("No wrapper found for type: " + typeof(T).ToString());
                    return default;
                }
            }

            /// <summary>
            /// Initialize the type-lists used for reflection
            /// </summary>
            private static void GetTypeLists()
            {
                if (prefChildren == null)
                {
                    prefChildren = new List<System.Type>();

                    //Get all classes that inherit from ParentSerializableClass
                    prefChildren = ReflectiveEnumerator.GetSubClassTypes<PlayerPrefExtensionParent>(null);

                    if (prefChildrenTypes == null) //If not setup, setup the unityTypes
                    {
                        prefChildrenTypes = new List<System.Type>();

                        prefChildrenTypes = new List<System.Type>();
                        foreach (System.Type t in prefChildren) //Get all implemented wrapper types
                        {
                            prefChildrenTypes.Add(t.BaseType.GetGenericArguments()[0]);
                        }

                        TypeToWrapper = new Dictionary<System.Type, System.Type>();
                        for (int i = 0; i < prefChildren.Count; i++)
                        {
                            TypeToWrapper.Add(prefChildrenTypes[i], prefChildren[i]);
                        }
                    }
                }
            }

            /// <summary>
            /// Sets data on the EPlayerPref
            /// </summary>
            /// <typeparam name="T">The type you want to store</typeparam>
            /// <param name="key">The key you want to use</param>
            /// <param name="value">The value you want to set</param>
            /// <param name="pref">The extension to use</param>
            private static void SetT<T>(string key, T value, PlayerPrefExtension<T> pref)
            {
                pref.PlayerPrefWrite(key, value);
            }
            /// <summary>
            /// Get data from the EPlayerPref
            /// </summary>
            /// <typeparam name="T">The type you want to load</typeparam>
            /// <param name="key">The key to load</param>
            /// <param name="pref">The extension to use</param>
            /// <returns></returns>
            private static T GetT<T>(string key, PlayerPrefExtension<T> pref)
            {
                return pref.PlayerPrefRead(key);
            }
            #endregion

            #region ParentWrapperClasses
            /// <summary>
            /// This is the parent class of all Extension methods. Dont inherit from this class.
            /// </summary>
            private abstract class PlayerPrefExtensionParent
            {

            }
            /// <summary>
            /// Use this class to write and read data to the playerprefs
            /// </summary>
            /// <typeparam name="T">The type to store/load</typeparam>
            private abstract class PlayerPrefExtension<T> : PlayerPrefExtensionParent
            {
                public abstract void PlayerPrefWrite(string key, T value);

                public abstract T PlayerPrefRead(string key);
            }
            #endregion

            #region PlayerPrefWrappers
            /// <summary>
            /// Is used to read/write Vector2
            /// </summary>
            private class Vector2Pref : PlayerPrefExtension<Vector2>
            {
                public override Vector2 PlayerPrefRead(string key)
                {
                    Vector2 res = new Vector2();
                    string readString = PlayerPrefs.GetString(key);

                    List<string> values = readString.Split(';').ToList<string>();

                    if (values.Count < 2)
                        Debug.LogError("Error when loading Vector2: " + key);

                    if (!float.TryParse(values[0], out res.x))
                        Debug.LogError("Error when loading Vector2: " + key);
                    if (!float.TryParse(values[1], out res.y))
                        Debug.LogError("Error when loading Vector2: " + key);

                    return res;
                }

                public override void PlayerPrefWrite(string key, Vector2 value)
                {
                    string writeString = value.x + ";" + value.y;
                    PlayerPrefs.SetString(key, writeString);
                }
            }

            /// <summary>
            /// Is used to read/write Vector3
            /// </summary>
            private class Vector3Pref : PlayerPrefExtension<Vector3>
            {
                public override Vector3 PlayerPrefRead(string key)
                {
                    Vector3 res = new Vector3();
                    string readString = PlayerPrefs.GetString(key);

                    List<string> values = readString.Split(';').ToList<string>();

                    if (values.Count < 3)
                        Debug.LogError("Error when loading Vector3: " + key);

                    if (!float.TryParse(values[0], out res.x))
                        Debug.LogError("Error when loading Vector3: " + key);
                    if (!float.TryParse(values[1], out res.y))
                        Debug.LogError("Error when loading Vector3: " + key);
                    if (!float.TryParse(values[2], out res.z))
                        Debug.LogError("Error when loading Vector3: " + key);

                    return res;
                }

                public override void PlayerPrefWrite(string key, Vector3 value)
                {
                    string writeString = value.x + ";" + value.y + ";" + value.z;
                    PlayerPrefs.SetString(key, writeString);
                }
            }

            /// <summary>
            /// Is used to read/write Quaternion
            /// </summary>
            private class QuaternionPref : PlayerPrefExtension<Quaternion>
            {
                public override Quaternion PlayerPrefRead(string key)
                {
                    Quaternion res = new Quaternion();
                    string readString = PlayerPrefs.GetString(key);

                    List<string> values = readString.Split(';').ToList<string>();

                    if (values.Count < 3)
                        Debug.LogError("Error when loading Quaternion: " + key);

                    if (!float.TryParse(values[0], out res.x))
                        Debug.LogError("Error when loading Quaternion: " + key);
                    if (!float.TryParse(values[1], out res.y))
                        Debug.LogError("Error when loading Quaternion: " + key);
                    if (!float.TryParse(values[2], out res.z))
                        Debug.LogError("Error when loading Quaternion: " + key);
                    if (!float.TryParse(values[3], out res.w))
                        Debug.LogError("Error when loading Quaternion: " + key);

                    return res;
                }

                public override void PlayerPrefWrite(string key, Quaternion value)
                {
                    string writeString = value.x + ";" + value.y + ";" + value.z + ";" + value.w;
                    PlayerPrefs.SetString(key, writeString);
                }
            }

            /// <summary>
            /// Is used to read/write Color
            /// </summary>
            private class ColorPref : PlayerPrefExtension<Color>
            {
                public override Color PlayerPrefRead(string key)
                {
                    Color res = new Color();
                    string readString = PlayerPrefs.GetString(key);

                    List<string> values = readString.Split(';').ToList<string>();

                    if (values.Count < 3)
                        Debug.LogError("Error when loading Color: " + key);

                    if (!float.TryParse(values[0], out res.r))
                        Debug.LogError("Error when loading Color: " + key);
                    if (!float.TryParse(values[1], out res.g))
                        Debug.LogError("Error when loading Color: " + key);
                    if (!float.TryParse(values[2], out res.b))
                        Debug.LogError("Error when loading Color: " + key);
                    if (!float.TryParse(values[3], out res.a))
                        Debug.LogError("Error when loading Color: " + key);

                    return res;
                }

                public override void PlayerPrefWrite(string key, Color value)
                {
                    string writeString = value.r + ";" + value.g + ";" + value.b + ";" + value.a;
                    PlayerPrefs.SetString(key, writeString);
                }
            }

            /// <summary>
            /// Is used to read/write int[]
            /// </summary>
            private class IntArrayPref : PlayerPrefExtension<int[]>
            {
                public override int[] PlayerPrefRead(string key)
                {
                    List<int> res = new List<int>();
                    string readString = PlayerPrefs.GetString(key);

                    List<string> values = readString.Split(',').ToList<string>();

                    if (values.Count < 1)
                        Debug.LogError("Error when loading int[]: " + key);

                    int tmp = 0;
                    foreach (string str in values)
                    {
                        if (!int.TryParse(str, out tmp))
                            Debug.LogError("Error when loading int[]: " + key);
                        else
                            res.Add(tmp);
                    }

                    return res.ToArray();
                }

                public override void PlayerPrefWrite(string key, int[] value)
                {
                    string writeString = "";

                    for (int i = 0; i < value.Length; i++)
                    {
                        if (i == 0)
                        {
                            writeString += value[i];
                        }
                        else
                        {
                            writeString += "," + value[i];
                        }
                    }

                    PlayerPrefs.SetString(key, writeString);
                }
            }

            /// <summary>
            /// Is used to read/write float[]
            /// </summary>
            private class FloatArrayPref : PlayerPrefExtension<float[]>
            {
                public override float[] PlayerPrefRead(string key)
                {
                    List<float> res = new List<float>();
                    string readString = PlayerPrefs.GetString(key);

                    List<string> values = readString.Split(',').ToList<string>();

                    if (values.Count < 1)
                        Debug.LogError("Error when loading float[]: " + key);

                    float tmp = 0f;
                    foreach (string str in values)
                    {
                        if (!float.TryParse(str, out tmp))
                            Debug.LogError("Error when loading float[]: " + key);
                        else
                            res.Add(tmp);
                    }

                    return res.ToArray();
                }

                public override void PlayerPrefWrite(string key, float[] value)
                {
                    string writeString = "";

                    for (int i = 0; i < value.Length; i++)
                    {
                        if (i == 0)
                        {
                            writeString += value[i];
                        }
                        else
                        {
                            writeString += "," + value[i];
                        }
                    }

                    PlayerPrefs.SetString(key, writeString);
                }
            }

            /// <summary>
            /// Is used to read/write string[]
            /// <para>Note: The § sign can not be used for storage! It will separate two strings</para>
            /// </summary>
            private class StringArrayPref : PlayerPrefExtension<string[]>
            {
                public override string[] PlayerPrefRead(string key)
                {
                    string readString = PlayerPrefs.GetString(key);

                    List<string> values = readString.Split('§').ToList<string>();

                    if (values.Count < 1)
                        Debug.LogError("Error when loading string[]: " + key);

                    return values.ToArray();
                }

                public override void PlayerPrefWrite(string key, string[] value)
                {
                    string writeString = "";

                    for (int i = 0; i < value.Length; i++)
                    {
                        if (i == 0)
                        {
                            writeString += value[i];
                        }
                        else
                        {
                            writeString += "§" + value[i];
                        }
                    }

                    PlayerPrefs.SetString(key, writeString);
                }
            }

            /// <summary>
            /// Is used to read/write Vector2[]
            /// </summary>
            private class Vector2ArrayPref : PlayerPrefExtension<Vector2[]>
            {
                public override Vector2[] PlayerPrefRead(string key)
                {
                    List<Vector2> res = new List<Vector2>();
                    string readString = PlayerPrefs.GetString(key);

                    List<string> values = readString.Split(':').ToList<string>();

                    float x, y;
                    string[] tmp;
                    foreach (string vector in values)
                    {
                        tmp = vector.Split(';');
                        for (int i = 0; i < tmp.Length; i++)
                        {
                            tmp[i] = tmp[i].Replace('{', ' ');
                            tmp[i] = tmp[i].Replace('}', ' ');
                        }
                        if (tmp.Length < 2)
                            Debug.LogError("Error at Vector2[]: " + key);

                        x = float.Parse(tmp[0]);
                        y = float.Parse(tmp[1]);

                        res.Add(new Vector2(x, y));
                    }

                    return res.ToArray();
                }

                public override void PlayerPrefWrite(string key, Vector2[] value)
                {
                    string writeString = "";

                    for (int i = 0; i < value.Length; i++)
                    {
                        if (i == 0)
                        {
                            writeString += "{" + value[i].x + ";" + value[i].y + "}";
                        }
                        else
                        {
                            writeString += ":" + "{" + value[i].x + ";" + value[i].y + "}";
                        }
                    }

                    PlayerPrefs.SetString(key, writeString);
                }
            }

            /// <summary>
            /// Is used to read/write Vector3[]
            /// </summary>
            private class Vector3ArrayPref : PlayerPrefExtension<Vector3[]>
            {
                public override Vector3[] PlayerPrefRead(string key)
                {
                    List<Vector3> res = new List<Vector3>();
                    string readString = PlayerPrefs.GetString(key);

                    List<string> values = readString.Split(':').ToList<string>();

                    float x, y, z;
                    string[] tmp;
                    foreach (string vector in values)
                    {
                        tmp = vector.Split(';');
                        for (int i = 0; i < tmp.Length; i++)
                        {
                            tmp[i] = tmp[i].Replace('{', ' ');
                            tmp[i] = tmp[i].Replace('}', ' ');
                        }
                        if (tmp.Length < 3)
                            Debug.LogError("Error at Vector3[]: " + key);

                        x = float.Parse(tmp[0]);
                        y = float.Parse(tmp[1]);
                        z = float.Parse(tmp[2]);

                        res.Add(new Vector3(x, y, z));
                    }

                    return res.ToArray();
                }

                public override void PlayerPrefWrite(string key, Vector3[] value)
                {
                    string writeString = "";

                    for (int i = 0; i < value.Length; i++)
                    {
                        if (i == 0)
                        {
                            writeString += "{" + value[i].x + ";" + value[i].y + ";" + value[i].z + "}";
                        }
                        else
                        {
                            writeString += ":" + "{" + value[i].x + ";" + value[i].y + ";" + value[i].z + "}";
                        }
                    }

                    PlayerPrefs.SetString(key, writeString);
                }
            }
            #endregion
        }
    }
}
