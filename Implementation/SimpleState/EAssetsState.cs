﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EAssets
{
    //Contains a state list
    namespace State
    {
        /// <summary>
        /// This class contains named booleans to store and load state values, like decisions etc.
        /// </summary>
        public static class StateList
        {
            private static Dictionary<string, bool> statelist = new Dictionary<string, bool>();

            /// <summary>
            /// Returns the value of a state, if it exists, else it always returns false
            /// </summary>
            /// <param name="name">The name of the state</param>
            /// <returns></returns>
            public static bool GetState(string name)
            {
                if (statelist.ContainsKey(name))
                    return statelist[name];
                else
                    return false;
            }

            /// <summary>
            /// Tries to set a state, if it exists. Else it is created
            /// </summary>
            /// <param name="name">The name of the state</param>
            /// <param name="value">The value of the state</param>
            public static void SetState(string name, bool value)
            {
                if (statelist.ContainsKey(name))
                    statelist[name] = value;
                else
                    statelist.Add(name, value);
            }

            /// <summary>
            /// Creates a new state if none with the same name exists
            /// </summary>
            /// <param name="name">The name of the state</param>
            /// <param name="value">The value of the state</param>
            public static void CreateState(string name, bool value)
            {
                if (!statelist.ContainsKey(name))
                    statelist.Add(name, value);
            }

            /// <summary>
            /// Returns true if a state with the given name exists, else false
            /// </summary>
            /// <param name="name">The name you want to check</param>
            /// <returns></returns>
            public static bool StateExists(string name)
            {
                return statelist.ContainsKey(name);
            }

            /// <summary>
            /// Stores the complete statelist on file
            /// </summary>
            public static void StoreStates()
            {
                IO.DataIO.SaveData(statelist, "Statelist", "CustomState");
            }
            /// <summary>
            /// Loads the old statelist
            /// </summary>
            public static void LoadStates()
            {
                IO.DataIO.LoadData<Dictionary<string, bool>>("Statelist", "CustomState");
            }
        }
    }
}
