The Extension class contains extensions for the object, Type and assembly classes. These extensions are mainly used for reflection.
You are free to import these extensions for this purpose.