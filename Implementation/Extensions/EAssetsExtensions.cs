﻿//------------------------------------------------------------------------------
// EAssetsExtension.cs
//
// Copyright 2019 E-In-A-Box 
//
// Created by Christian Koch
// Owner: Christian Koch
// 
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace EAssets
{
    namespace Extensions
    {
        namespace Objects
        {
            /// <summary>
            /// This class contains extensions for object used for relection
            /// </summary>
            public static class ObjectExtensions
            {
                /// <summary>
                /// Set a property of this object
                /// </summary>
                /// <param name="obj">This object</param>
                /// <param name="propname">The name of the property</param>
                /// <param name="value">The value you want to set it to</param>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //This will not do anything if the object is not a class or struct
                /// o.SetPropertyValue("propName",propValue);
                /// </code>
                /// </example>
                public static void SetPropertyValue(this object obj, string propname, object value)
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for SetPropertyValue");
                    }

                    if (!obj.GetType().IsClass && !obj.GetType().IsValueType) //A non class/non struct wont have a property
                        return;

                    PropertyInfo prop = obj.GetType().GetProperty(propname, BindingFlags.Instance | BindingFlags.Public);

                    if (prop != null && value != null && prop.CanWrite)
                    {
                        prop.SetValue(obj, value, null);
                    }

                }

                /// <summary>
                /// Set a field of this object
                /// </summary>
                /// <param name="obj">This object</param>
                /// <param name="fieldname">The name of the field you want to set</param>
                /// <param name="value">The value you want to set it to</param>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //This will not do anything if the object is not a class or struct
                /// o.SetFieldValue("fieldName",fieldValue);
                /// </code>
                /// </example>
                public static void SetFieldValue(this object obj, string fieldname, object value)
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for SetFieldValue");
                    }

                    if (!obj.GetType().IsClass && !obj.GetType().IsValueType) //A non class/non struct wont have a field
                        return;

                    FieldInfo fieldInfo = obj.GetType().GetField(fieldname, BindingFlags.Instance | BindingFlags.Public);
                    if (fieldInfo != null)
                    {
                        fieldInfo.SetValue(obj, value);
                    }
                }

                /// <summary>
                /// Returns a property of this object
                /// </summary>
                /// <param name="obj">This object</param>
                /// <param name="propname">The name of the property you want returned</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //This will return null if the object is not a class or struct
                /// object value = o.GetPropertyValue("propName");
                /// 
                /// //value does now contain the value of the property or null
                /// 
                /// //do stuff with value here...
                /// </code>
                /// </example>
                public static object GetPropertyValue(this object obj, string propname)
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for GetPropertyValue");
                    }

                    if (!obj.GetType().IsClass && !obj.GetType().IsValueType) //A non class/non struct wont have a property
                        return null;

                    if (obj != null && propname != string.Empty)
                    {
                        PropertyInfo info = obj.GetType().GetProperty(propname);
                        //check if there is a property and it is no indexer
                        if (info != null && (info.GetIndexParameters() == null || info.GetIndexParameters().Length == 0))
                        {
                            return obj.GetType().GetProperty(propname).GetValue(obj, null);
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                        return null;
                }

                /// <summary>
                /// Returns a field of this object
                /// </summary>
                /// <param name="obj">This object</param>
                /// <param name="fieldname">The name of the field you want to set</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //This will return null if the object is not a class or struct
                /// object value = o.GetFieldValue("fieldName");
                /// 
                /// //value does now contain the value of the field or null
                /// 
                /// //do stuff with value here...
                /// </code>
                /// </example>
                public static object GetFieldValue(this object obj, string fieldname)
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for GetFieldValue");
                    }

                    if (!obj.GetType().IsClass && !obj.GetType().IsValueType) //A non class/non struct wont have a field
                        return null;

                    if (obj != null)
                    {
                        if (obj.GetType().GetField(fieldname) != null)
                        {
                            return obj.GetType().GetField(fieldname).GetValue(obj);
                        }
                    }
                    return null;
                }

                /// <summary>
                /// Invokes a method of this object if possible. Returns null if not
                /// </summary>
                /// <param name="obj">This object</param>
                /// <param name="methodname">The name of the method to invoke</param>
                /// <param name="values">The values to pass to the method</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //This will return null if the object is not a class or struct
                /// //Only use returnValue if anything is returned
                /// object returnValue = o.Invoke("methodName",inputs); //inputs can be any array of objects
                /// //If the inputs dont match any method, invoke will throw an error
                /// </code>
                /// </example>
                public static object Invoke(this object obj, string methodname, object[] values = null)
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for Invoke");
                    }

                    if (!obj.GetType().IsClass && !obj.GetType().IsValueType) //A non class/non struct wont have a method
                        return null;

                    if (obj == null)
                        return null;

                    Type type = obj.GetType(); //Get type

                    MethodInfo info = type.GetMethod(methodname);
                    if (info != null)
                        return info.Invoke(obj, values);

                    return null;
                }

                /// <summary>
                /// Returns the field names of a class
                /// </summary>
                /// <param name="obj">This object</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //This array will now contain all names of the public, non-static fields of the object o
                /// string[] fieldnames = o.GetFieldNames(); //Returns null if object is neither class nor struct
                /// 
                /// //Do something with the fieldnames here...
                /// </code>
                /// </example>
                public static string[] GetFieldNames(this object obj)
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for GetFieldNames");
                    }

                    if (!obj.GetType().IsClass && !obj.GetType().IsValueType) //A non class/non struct wont have fields
                        return null;

                    if (obj == null)
                        return new string[] { "null" };

                    BindingFlags bindingFlags =
                          BindingFlags.Public
                        | BindingFlags.Instance;

                    FieldInfo[] fields = obj.GetType().GetFields(bindingFlags);

                    List<string> arr = new List<string>();
                    for (int i = 0; i < fields.Length; i++)
                    {
                        arr.Add(fields[i].Name);
                    }

                    return arr.ToArray();
                }

                /// <summary>
                /// Returns the property names of a class
                /// </summary>
                /// <param name="obj">This object</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //This array will now contain all names of the public, non-static properties of the object o
                /// string[] propertyNames = o.GetPropertyNames(); //Returns null if object is neither class nor struct
                /// 
                /// //Do something with the propertynames here...
                /// </code>
                /// </example>
                public static string[] GetPropertyNames(this object obj)
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for GetPropertyNames");
                    }

                    if (!obj.GetType().IsClass && !obj.GetType().IsValueType) //A non class/non struct wont have properties
                        return null;

                    if (obj == null)
                        return new string[] { "null" };

                    BindingFlags bindingFlags =
                          BindingFlags.Public
                        | BindingFlags.Instance;

                    List<string> arr = new List<string>();
                    PropertyInfo[] prop = obj.GetType().GetProperties(bindingFlags);

                    for (int i = 0; i < prop.Length; i++)
                    {
                        arr.Add(prop[i].Name);
                    }

                    return arr.ToArray();
                }

                /// <summary>
                /// Returns all properties that are writable
                /// </summary>
                /// <param name="obj">This object</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //This array will now contain all names of the public, non-static properties of the object o
                /// //This method filters out all properties that are read only (e.g. have private setters)
                /// string[] propertyNames = o.GetWritablePropertyNames(); //Returns null if object is neither class nor struct
                /// 
                /// //Do something with the propertynames here...
                /// </code>
                /// </example>
                public static string[] GetWritablePropertyNames(this object obj)
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for GetWritablePropertyNames");
                    }

                    if (!obj.GetType().IsClass && !obj.GetType().IsValueType) //A non class/non struct wont have writable properties
                        return null;

                    string[] str = obj.GetPropertyNames();

                    List<string> strlist = new List<string>();
                    foreach (string s in str)
                    {
                        if (!obj.PropertyIsReadOnly(s))
                        {
                            strlist.Add(s);
                        }
                    }

                    return strlist.ToArray();
                }

                /// <summary>
                /// Returns all writable attributes
                /// <para>Which is all fields and all writable properties that are public and non-static</para>
                /// </summary>
                /// <param name="obj">This object</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //This array will now contain all names of the public, non-static properties and fields of the object o
                /// //This method filters out all properties that are read only (e.g. have private setters)
                /// string[] attributeNames = o.GetAllWritableAttributes(); //Returns null if object is neither class nor struct
                /// 
                /// //Do something with the attributenames here...
                /// </code>
                /// </example>
                public static string[] GetAllWritableAttributes(this object obj)
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for GetAllWritableAttributes");
                    }

                    if (!obj.GetType().IsClass && !obj.GetType().IsValueType) //A non class/non struct wont have attributes
                        return null;

                    List<string> attributes = new List<string>(obj.GetWritablePropertyNames());
                    attributes.AddRange(obj.GetFieldNames());

                    return attributes.ToArray();
                }

                /// <summary>
                /// Returns the method names of a class
                /// </summary>
                /// <param name="obj">This object</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //This array will now contain all names of the public, non-static methods of object o
                /// string[] methodNames = o.GetMethodNames(); //Returns null if object is neither class nor struct
                /// 
                /// //Do something with the methodnames here...
                /// </code>
                /// </example>
                public static string[] GetMethodNames(this object obj)
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for GetMethodNames");
                    }

                    if (!obj.GetType().IsClass && !obj.GetType().IsValueType) //A non class/non struct wont have methods
                        return null;

                    if (obj == null)
                        return new string[] { "null" };

                    BindingFlags bindingFlags =
                          BindingFlags.Public
                        | BindingFlags.Instance;

                    List<string> arr = new List<string>();
                    MethodInfo[] prop = obj.GetType().GetMethods(bindingFlags);

                    for (int i = 0; i < prop.Length; i++)
                    {
                        arr.Add(prop[i].Name);
                    }

                    return arr.ToArray();
                }

                /// <summary>
                /// Returns the number of method attributes
                /// </summary>
                /// <param name="obj">This object</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //This array will now contain the number of inputs of all public, non-static methods of object o
                /// int[] attributeCount = o.GetMethodAttributeCount(); //Returns null if object is neither class nor struct
                /// 
                /// //Do something with the attributecount here...
                /// </code>
                /// </example>
                public static int[] GetMethodsAttributeCount(this object obj)
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for GetMethodAttributeCount");
                    }

                    if (!obj.GetType().IsClass && !obj.GetType().IsValueType) //A non class/non struct wont have methods
                        return null;

                    if (obj == null)
                        return new int[] { -1 };

                    BindingFlags bindingFlags =
                          BindingFlags.Public
                        | BindingFlags.Instance;

                    List<int> methodParameterCount = new List<int>();
                    MethodInfo[] prop = obj.GetType().GetMethods(bindingFlags);

                    for (int i = 0; i < prop.Length; i++)
                    {
                        methodParameterCount.Add(prop[i].GetParameters().Length);
                    }

                    return methodParameterCount.ToArray();
                }

                /// <summary>
                /// Returns true if the property is read only
                /// </summary>
                /// <param name="obj">This object</param>
                /// <param name="propertyName">The name of the property to check</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //Returns true if the property with the given name is read-only (e.g. only has private setter)
                /// if(o.GetMethodAttributeCount("propertyName")) //Returns false if object is neither class nor struct
                /// {
                ///     //Do something here...
                /// }
                /// </code>
                /// </example>
                public static bool PropertyIsReadOnly(this object obj, string propertyName)
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for PropertyIsReadOnly");
                    }

                    if (!obj.GetType().IsClass && !obj.GetType().IsValueType) //A non class/non struct wont have a property
                        return false;

                    PropertyInfo info = obj.GetType().GetRuntimeProperty(propertyName);
                    if (info != null && info.GetIndexParameters().Length == 0)
                    {
                        //If there is no set method or it is privat it is read only
                        return info.SetMethod == null || !info.SetMethod.IsPublic;
                    }
                    else if (info.GetIndexParameters().Length != 0)
                    {
                        return true; //Indexers are also read-only since they need an input value
                    }
                    else
                        return false;
                }

                /// <summary>
                /// Returns true if this object is a class
                /// </summary>
                /// <param name="obj">This object</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //Returns true if the object is a class
                /// if(o.IsClass()) 
                /// {
                ///     //Do something here...
                /// }
                /// </code>
                /// </example>
                public static bool IsClass(this object obj)
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for IsClass");
                    }

                    return obj.GetType().IsClass;
                }

                /// <summary>
                /// Checks if this object is a primitive/simple type
                /// <para>Primitive/Simple types are: regular primitives, enums, strings, decimals, classes with primitve generics</para>
                /// </summary>
                /// <param name="obj"></param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //Returns true if the object is a either an enum, a string, a decimal or any other simple/primitive type (like int, char and float)
                /// //This also returns true if the object is a generic class with a primitive type parameter
                /// if(o.IsPrimitive()) 
                /// {
                ///     //Do something here...
                /// }
                /// </code>
                /// </example>
                public static bool IsPrimitive(this object obj)
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for IsPrimitive");
                    }

                    TypeInfo info = obj.GetType().GetTypeInfo();
                    if (info.IsGenericType && info.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        //Check if the generic type is primitive
                        return IsPrimitive(info.GetGenericArguments()[0]);
                    }
                    return info.IsPrimitive
                        || info.IsEnum
                        || obj.GetType().Equals(typeof(string))
                        || obj.GetType().Equals(typeof(decimal));
                }

                /// <summary>
                /// Checks if this object is a simple type
                /// </summary>
                /// <param name="obj">This object</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //Returns true if the object is a either an enum, a string, a decimal or any other simple/primitive type (like int, char and float)
                /// //This also returns true if the object is a generic class with a primitive type parameter
                /// if(o.IsSimple()) 
                /// {
                ///     //Do something here...
                /// }
                /// </code>
                /// </example>
                public static bool IsSimple(this object obj)
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for IsSimple");
                    }

                    return IsPrimitive(obj);
                }

                /// <summary>
                /// Cast this object to a given class. Returns null if not possible
                /// </summary>
                /// <typeparam name="T">The type you want to cast to</typeparam>
                /// <param name="obj">This object</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //Casts the object o to the given type that must be a class
                /// //Returns null if the object can not be casted to the given type
                /// T newValue = o.CastToClassType{Type}();
                /// </code>
                /// </example>
                public static T CastToClassType<T>(this object obj) where T : class
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for CastToClassType");
                    }

                    return obj as T;
                }

                /// <summary>
                /// Cast this object to the given type if possible. If not possible it will return a default value
                /// </summary>
                /// <typeparam name="T">The type to cast to</typeparam>
                /// <param name="obj">This object</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Create an object/get an existing object
                /// object o = someValue;
                /// 
                /// //Casts the object o to the given type
                /// //Returns default if not possible
                /// T newValue = o.CastToType{Type}();
                /// </code>
                /// </example>
                public static T CastToType<T>(this object obj)
                {
                    if (obj == null)
                    {
                        throw new ArgumentException("Object can not be null for CastToType");
                    }

                    if (obj is T)
                        return (T)obj;
                    else
                        return default;
                }
            }
        }

        namespace Types
        {
            /// <summary>
            /// This class contains all extensions for the type class
            /// </summary>
            public static class TypeExtensions
            {
                /// <summary>
                /// Returns the base class of this object
                /// </summary>
                /// <param name="obj">This object</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Get the type of any object
                /// Type t = someValue.GetType();
                /// 
                /// //Returns the base class of a given type
                /// Type baseClass = t.GetBaseclass();
                /// 
                /// //Do something with the baseclass here...
                /// </code>
                /// </example>
                public static Type GetBaseclass(this Type obj)
                {
                    return obj.BaseType;
                }

                /// <summary>
                /// Get the base class of a given object
                /// <para>This goes up to the highest possible class, that is directly beneath object in the hierarchy</para>
                /// </summary>
                /// <param name="obj">This object</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Get the type of any object
                /// Type t = someValue.GetType();
                /// 
                /// //Returns the first (non object!) class in the inheritance hierarchy of a given type
                /// Type baseClass = t.GetFirstBaseclass();
                /// 
                /// //Do something with the baseclass here...
                /// </code>
                /// </example>
                public static Type GetFirstBaseclass(this Type obj)
                {
                    Type b = obj.BaseType;
                    Type tmp = b;
                    while (tmp != typeof(object))
                    {
                        b = tmp;
                        tmp = b.BaseType;
                    }

                    return b;
                }

                /// <summary>
                /// Returns all base classes of this object
                /// </summary>
                /// <param name="obj">This object</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Get the type of any object
                /// Type t = someValue.GetType();
                /// 
                /// //Returns all baseClasses of this type object. This array does not contain the object type
                /// Type[] baseClasses = t.GetBaseClasses();
                /// 
                /// //Do something with the baseclasses here...
                /// </code>
                /// </example>
                public static Type[] GetBaseClasses(this Type obj)
                {
                    List<Type> b = new List<Type>();
                    Type tmp = obj.BaseType;
                    while (tmp != typeof(object))
                    {
                        b.Add(tmp);
                        tmp = tmp.BaseType;
                    }

                    return b.ToArray();
                }

                /// <summary>
                /// Returns all base classes as a list
                /// </summary>
                /// <param name="obj">This object</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Get the type of any object
                /// Type t = someValue.GetType();
                /// 
                /// //Returns all baseClasses of this type object as a list. This list does not contain the object type
                /// List{Type} baseClasses = t.GetBaseClassesAsList();
                /// 
                /// //Do something with the baseclasses here...
                /// </code>
                /// </example>
                public static List<Type> GetBaseClassesAsList(this Type obj)
                {
                    List<Type> b = new List<Type>();
                    Type tmp = obj.BaseType;
                    while (tmp != typeof(object))
                    {
                        b.Add(tmp);
                        tmp = tmp.BaseType;
                    }

                    return b;
                }

                /// <summary>
                /// Returns true if some class this class inheritet from is of the given type
                /// <para>HasAncestor(typeof(object)) should always return false since it is ignored as BaseType!</para>
                /// </summary>
                /// <param name="obj">This object</param>
                /// <param name="type">The type you want to check for</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Get the type of any object
                /// Type t = someValue.GetType();
                /// 
                /// //Returns true if the given type is an ancestor of the type t (if t inherited from it somewhere) 
                /// if(t.HasAncestor(someType))
                /// {
                ///     //Do something here...
                /// }
                /// </code>
                /// </example>
                public static bool HasAncestor(this Type obj, Type type)
                {
                    //If one of the baseclasses matches return true
                    return obj.GetBaseClassesAsList().Contains(type);
                }

                /// <summary>
                /// Checks if this type implements the given interface
                /// <para>This should also work for classes</para>
                /// </summary>
                /// <typeparam name="T">The interface to check</typeparam>
                /// <param name="obj">This type</param>
                /// <returns></returns>
                /// <example>
                /// <code>
                /// //Get the type of any object
                /// Type t = someValue.GetType();
                /// 
                /// //Returns true if the given type implements the interface of the given type
                /// if(t.ImplementsInterface{interfaceType})
                /// {
                ///     //Do something here...
                /// }
                /// </code>
                /// </example>
                public static bool ImplementsInterface<T>(this Type obj) where T : class
                {
                    return typeof(T).IsAssignableFrom(obj);
                }

            }
        }

        namespace Assemblies
        {
            /// <summary>
            /// This class contains all extensions for the assembly class
            /// </summary>
            public static class TypeLoaderExtensions
            {
                /// <summary>
                /// Returns all loadable types from an assembly
                /// </summary>
                /// <param name="assembly">This assembly</param>
                /// <returns></returns>
                public static IEnumerable<Type> GetLoadableTypes(this Assembly assembly)
                {
                    if (assembly == null)
                        throw new ArgumentNullException("Assembly");
                    try
                    {
                        return assembly.GetTypes();
                    }
                    catch (ReflectionTypeLoadException e)
                    {
                        return e.Types.Where(t => t != null);
                    }
                }
            }
        }
    }
}
