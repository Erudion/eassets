﻿using System;
using System.Collections.Generic;

namespace EAssets
{
    namespace State
    {
        //Contains values that automaticly trigger events and managed lists of those values
        namespace Triggervalues
        {
            [Serializable]
            /// <summary>
            /// A triggervalue is an integer value that can be set to values between a minimum and a maximum value.
            /// <para>Every time it is set it checks its new value and triggers events depending on what new value is set.</para>
            /// </summary>
            public class Triggervalue
            {
                #region Attributes
                /// <summary>
                /// The current value of this TriggerValue
                /// </summary>
                public int Value { get; protected set; }

                /// <summary>
                /// The minimal allowed value of this TriggerValue
                /// </summary>
                private readonly int minValue;
                /// <summary>
                /// The maximum allowed value of this TriggerValue
                /// </summary>
                private readonly int maxValue;
                #endregion

                #region Constructor
                /// <summary>
                /// Create a new triggervalue
                /// </summary>
                /// <param name="min">The minimal possible value for this value</param>
                /// <param name="max">The maximum possible value for this value</param>
                /// <param name="initial">The initial value</param>
                public Triggervalue(int min = int.MinValue, int max = int.MaxValue, int initial = 0)
                {
                    this.minValue = min;
                    this.maxValue = max;

                    if (this.Value > maxValue)
                        this.Value = maxValue;
                    else if (this.Value < minValue)
                        this.Value = minValue;
                    else
                        this.Value = initial;
                }
                #endregion

                #region Event Definitions
                /// <summary>
                /// This defines an event that can be triggered depending on the current value of this triggervalue
                /// </summary>
                public delegate void OnEvent();
                #endregion

                #region Eventlists
                /// <summary>
                /// These event are triggered if the current value is equal to a given one
                /// </summary>
                private readonly List<EqualEvent> equalEvents = new List<EqualEvent>();
                /// <summary>
                /// These event are triggered if the current value is lower than a given one
                /// </summary>
                private readonly List<LowerEvent> lowerEvents = new List<LowerEvent>();
                /// <summary>
                /// These event are triggered if the current value is lower or equal than a given one
                /// </summary>
                private readonly List<LowerEqualEvent> lowerEqualEvents = new List<LowerEqualEvent>();
                /// <summary>
                /// These event are triggered if the current value is greater than a given one
                /// </summary>
                private readonly List<GreaterEvent> greaterEvents = new List<GreaterEvent>();
                /// <summary>
                /// These event are triggered if the current value is greater or equal than a given one
                /// </summary>
                private readonly List<GreaterEqualEvent> greaterEqualEvents = new List<GreaterEqualEvent>();
                #endregion

                #region SetMethods
                /// <summary>
                /// Sets this triggervalue to a given value
                /// </summary>
                /// <param name="newValue">The value you want to set it to</param>
                public void SetValue(int newValue)
                {
                    this.Value = newValue;

                    if (this.Value > maxValue)
                        this.Value = maxValue;
                    else if (this.Value < minValue)
                        this.Value = minValue;

                    //Check all events after setting the current Value
                    CheckEvents();
                }

                /// <summary>
                /// Adds to this triggervalue
                /// </summary>
                /// <param name="addition">The value you want to add</param>
                public void AddToValue(int addition)
                {
                    SetValue(this.Value + addition);
                }

                /// <summary>
                /// Subtracts from this triggervalue
                /// </summary>
                /// <param name="subtract">The value you want to subtract</param>
                public void SubtractValue(int subtract)
                {
                    SetValue(this.Value - subtract);
                }

                /// <summary>
                /// Multiply this triggervalue with a given value
                /// </summary>
                /// <param name="multiply">The value you want to multiply with this triggervalue</param>
                public void MultiplyValue(int multiply)
                {
                    SetValue(this.Value * multiply);
                }

                /// <summary>
                /// Divides this triggervalue with a given value
                /// </summary>
                /// <param name="divide">The value you want to use to divide this triggervalue</param>
                public void DivideValue(int divide)
                {
                    SetValue(this.Value / divide);
                }
                #endregion

                #region Add Event Methods
                /// <summary>
                /// Add a new event that is triggered once when the triggervalue reaches the given value
                /// </summary>
                /// <param name="compareValue">The value the trigger value has to be to trigger the event</param>
                /// <param name="ev">The event to be triggered</param>
                public void AddOnEqualEvent(int compareValue, OnEvent ev)
                {
                    this.equalEvents.Add(new EqualEvent(ev, compareValue));
                }
                /// <summary>
                /// Add a new event that is triggered once when the triggervalue is lower than given value
                /// </summary>
                /// <param name="compareValue">The value the trigger value has to be lower than to trigger the event</param>
                /// <param name="ev">The event to be triggered</param>
                public void AddOnLowerEvent(int compareValue, OnEvent ev)
                {
                    this.lowerEvents.Add(new LowerEvent(ev, compareValue));
                }
                /// <summary>
                /// Add a new event that is triggered once when the triggervalue is lower or equal to the given value
                /// </summary>
                /// <param name="compareValue">The value the trigger value has to be lower or equal to trigger the event</param>
                /// <param name="ev">The event to be triggered</param>
                public void AddOnLowerEqualEvent(int compareValue, OnEvent ev)
                {
                    this.lowerEqualEvents.Add(new LowerEqualEvent(ev, compareValue));
                }
                /// <summary>
                /// Add a new event that is triggered once when the triggervalue is greater than the given value
                /// </summary>
                /// <param name="compareValue">The value the trigger value has to be greater than to trigger the event</param>
                /// <param name="ev">The event to be triggered</param>
                public void AddOnGreaterEvent(int compareValue, OnEvent ev)
                {
                    this.greaterEvents.Add(new GreaterEvent(ev, compareValue));
                }
                /// <summary>
                /// Add a new event that is triggered once when the triggervalue is greater or equal than the given value
                /// </summary>
                /// <param name="compareValue">The value the trigger value has to be greater or equal to trigger the event</param>
                /// <param name="ev">The event to be triggered</param>
                public void AddOnGreaterEqualEvent(int compareValue, OnEvent ev)
                {
                    this.greaterEqualEvents.Add(new GreaterEqualEvent(ev, compareValue));
                }
                #endregion

                #region Check Event Methods
                /// <summary>
                /// Checks all possible events
                /// </summary>
                private void CheckEvents()
                {
                    CheckEqualEvents();
                    CheckGreaterEvents();
                    CheckGreaterEqualEvents();
                    CheckLowerEvents();
                    CheckLowerEqualEvents();
                }

                /// <summary>
                /// Check all equal events
                /// </summary>
                private void CheckEqualEvents()
                {
                    foreach (EqualEvent ev in equalEvents)
                    {
                        ev.CheckEvent(this.Value);
                    }
                }
                /// <summary>
                /// Check all lower events
                /// </summary>
                private void CheckLowerEvents()
                {
                    foreach (LowerEvent ev in lowerEvents)
                    {
                        ev.CheckEvent(this.Value);
                    }
                }
                /// <summary>
                /// Check all lower equal events
                /// </summary>
                private void CheckLowerEqualEvents()
                {
                    foreach (LowerEqualEvent ev in lowerEqualEvents)
                    {
                        ev.CheckEvent(this.Value);
                    }
                }
                /// <summary>
                /// Check all greater events
                /// </summary>
                private void CheckGreaterEvents()
                {
                    foreach (GreaterEvent ev in greaterEvents)
                    {
                        ev.CheckEvent(this.Value);
                    }
                }
                /// <summary>
                /// Check all greater equal events
                /// </summary>
                private void CheckGreaterEqualEvents()
                {
                    foreach (GreaterEqualEvent ev in greaterEqualEvents)
                    {
                        ev.CheckEvent(this.Value);
                    }
                }
                #endregion

                #region Event classes
                /// <summary>
                /// A basic event used for a triggervalue events
                /// </summary>
                private abstract class BasicEvent
                {
                    /// <summary>
                    /// The value used for this event
                    /// </summary>
                    public int CompareValue { get; protected set; }

                    public abstract bool CheckEvent(int input);
                }
                /// <summary>
                /// This event is triggerd if the triggervalue is exactly a given value
                /// </summary>
                private class EqualEvent : BasicEvent
                {
                    public event OnEvent OnValueEqual;
                    public EqualEvent(OnEvent ev, int comp)
                    {
                        this.OnValueEqual += ev;
                        this.CompareValue = comp;
                    }

                    /// <summary>
                    /// Triggers the event and returns true if the input is equal to the compareValue
                    /// </summary>
                    /// <param name="input"></param>
                    /// <returns></returns>
                    public override bool CheckEvent(int input)
                    {
                        if (input == CompareValue)
                        {
                            OnValueEqual?.Invoke();
                            return true;
                        }

                        return false;
                    }
                }
                /// <summary>
                /// This event is triggerd if the triggervalue is lower than the given value
                /// </summary>
                private class LowerEvent : BasicEvent
                {
                    public event OnEvent OnValueLower;
                    public LowerEvent(OnEvent ev, int comp)
                    {
                        this.OnValueLower += ev;
                        this.CompareValue = comp;
                    }

                    /// <summary>
                    /// Triggers the event and returns true if the input is equal to the compareValue
                    /// </summary>
                    /// <param name="input"></param>
                    /// <returns></returns>
                    public override bool CheckEvent(int input)
                    {
                        if (input < CompareValue)
                        {
                            OnValueLower?.Invoke();
                            return true;
                        }

                        return false;
                    }
                }
                /// <summary>
                /// This event is triggerd if the triggervalue is lower or equal to the given value
                /// </summary>
                private class LowerEqualEvent : BasicEvent
                {
                    public event OnEvent OnValueLower;
                    public LowerEqualEvent(OnEvent ev, int comp)
                    {
                        this.OnValueLower += ev;
                        this.CompareValue = comp;
                    }

                    /// <summary>
                    /// Triggers the event and returns true if the input is equal to the compareValue
                    /// </summary>
                    /// <param name="input"></param>
                    /// <returns></returns>
                    public override bool CheckEvent(int input)
                    {
                        if (input <= CompareValue)
                        {
                            OnValueLower?.Invoke();
                            return true;
                        }

                        return false;
                    }
                }
                /// <summary>
                /// This event is triggerd if the triggervalue is greater than the given value
                /// </summary>
                private class GreaterEvent : BasicEvent
                {
                    public event OnEvent OnValueLower;
                    public GreaterEvent(OnEvent ev, int comp)
                    {
                        this.OnValueLower += ev;
                        this.CompareValue = comp;
                    }

                    /// <summary>
                    /// Triggers the event and returns true if the input is equal to the compareValue
                    /// </summary>
                    /// <param name="input"></param>
                    /// <returns></returns>
                    public override bool CheckEvent(int input)
                    {
                        if (input > CompareValue)
                        {
                            OnValueLower?.Invoke();
                            return true;
                        }

                        return false;
                    }
                }
                /// <summary>
                /// This event is triggerd if the triggervalue is greater or equal to the given value
                /// </summary>
                private class GreaterEqualEvent : BasicEvent
                {
                    public event OnEvent OnValueLower;
                    public GreaterEqualEvent(OnEvent ev, int comp)
                    {
                        this.OnValueLower += ev;
                        this.CompareValue = comp;
                    }

                    /// <summary>
                    /// Triggers the event and returns true if the input is equal to the compareValue
                    /// </summary>
                    /// <param name="input"></param>
                    /// <returns></returns>
                    public override bool CheckEvent(int input)
                    {
                        if (input >= CompareValue)
                        {
                            OnValueLower?.Invoke();
                            return true;
                        }

                        return false;
                    }
                }
                #endregion

                #region Operator Overloads
                /// <summary>
                /// Allows to add to this triggervalue directly
                /// </summary>
                /// <param name="tv">This triggervalue</param>
                /// <param name="val">The value to add</param>
                /// <returns></returns>
                public static Triggervalue operator +(Triggervalue tv, int val)
                {
                    tv.AddToValue(val);
                    return tv;
                }
                /// <summary>
                /// Allows to subtract from this triggervalue directly
                /// </summary>
                /// <param name="tv">This triggervalue</param>
                /// <param name="val">The value to subtract</param>
                /// <returns></returns>
                public static Triggervalue operator -(Triggervalue tv, int val)
                {
                    tv.SubtractValue(val);
                    return tv;
                }
                /// <summary>
                /// Allows to multiply with this triggervalue directly
                /// </summary>
                /// <param name="tv">This triggervalue</param>
                /// <param name="val">The value to multiply with</param>
                /// <returns></returns>
                public static Triggervalue operator *(Triggervalue tv, int val)
                {
                    tv.MultiplyValue(val);
                    return tv;
                }
                /// <summary>
                /// Allows to divide with this triggervalue directly
                /// </summary>
                /// <param name="tv">This triggervalue</param>
                /// <param name="val">The value to divide with</param>
                /// <returns></returns>
                public static Triggervalue operator /(Triggervalue tv, int val)
                {
                    tv.DivideValue(val);
                    return tv;
                }
                #endregion

                #region Implicit Definitions
                /// <summary>
                /// Returns an integer from this TriggerValue
                /// </summary>
                /// <param name="tv">The triggervalue yo want to read</param>
                public static implicit operator int(Triggervalue tv)
                {
                    return tv.Value;
                }
                #endregion

                #region Overrides
                public override string ToString()
                {
                    return Value.ToString();
                }
                #endregion

            }

            [Serializable]
            /// <summary>
            /// A triggergroup contains named triggervalues and is assigned a name for easier managing of multiple triggervalues
            /// </summary>
            public class TriggerGroup
            {
                /// <summary>
                /// The name of this group
                /// </summary>
                public string Groupname { get; protected set; }
                /// <summary>
                /// The triggervalues contained in this group
                /// </summary>
                private Dictionary<string, Triggervalue> triggervalues;

                /// <summary>
                /// Create a new triggergroup
                /// </summary>
                /// <param name="name">The name of this triggergroup</param>
                public TriggerGroup(string name)
                {
                    this.Groupname = name;
                    this.triggervalues = new Dictionary<string, Triggervalue>();
                }

                /// <summary>
                /// Returns a triggervalue by its name
                /// </summary>
                /// <param name="s">The name of the triggervalue you want to get</param>
                /// <returns></returns>
                public Triggervalue this[string s]
                {
                    get { return GetTriggervalue(s); }
                }

                /// <summary>
                /// Returns a reference to the chosen triggervalue
                /// </summary>
                /// <param name="name">The name of the triggervalue you want to get</param>
                /// <returns></returns>
                public Triggervalue GetTriggervalue(string name)
                {
                    return triggervalues[name];
                }

                /// <summary>
                /// Create a new triggervalue and add it to this group, if no value with the same name exists already.
                /// <para>Returns true if there was no such value, returns false if the value already existed</para>
                /// </summary>
                /// <param name="name">The name of this triggervalue</param>
                /// <param name="min">The minimal value for the new triggervalue</param>
                /// <param name="max">The maximal value for the new triggervalue</param>
                /// <param name="initial">The initial value for the new triggervalue</param>
                /// <returns></returns>
                public bool CreateNewTriggervalue(string name, int min, int max, int initial)
                {
                    if (!ContainsTriggervalue(name))
                    {
                        triggervalues.Add(name, new Triggervalue(min, max, initial));
                        return true;
                    }
                    return false;
                }

                /// <summary>
                /// Deletes a triggervalue and returns if it exists, else false
                /// </summary>
                /// <param name="name">The name of the triggervalue you want to delete</param>
                /// <returns></returns>
                public bool DeleteTriggervalue(string name)
                {
                    if (triggervalues.ContainsKey(name))
                    {
                        triggervalues.Remove(name);
                        return true;
                    }
                    return false;
                }

                /// <summary>
                /// Returns true if this group contains a triggervalue with the given name
                /// </summary>
                /// <param name="key">The name of the trigger value</param>
                /// <returns></returns>
                public bool ContainsTriggervalue(string key)
                {
                    return triggervalues.ContainsKey(key);
                }

                /// <summary>
                /// Returns the value of a triggervalue
                /// </summary>
                /// <param name="triggervalue">The name of the triggervalue you want to have returned</param>
                /// <returns></returns>
                public int GetValue(string triggervalue)
                {
                    return triggervalues[triggervalue].Value;
                }
            }
        }
    }
}
