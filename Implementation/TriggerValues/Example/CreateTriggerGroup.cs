﻿using UnityEngine;
using EAssets.State.Triggervalues;

/// <summary>
/// This class shows how to use a triggergroup
/// </summary>
public class CreateTriggerGroup : MonoBehaviour
{
    //We create a new triggergroup. A triggergroup contains n triggervalues
    private TriggerGroup emotions = new TriggerGroup("Emotions");

    void Awake()
    {
        //Lets add two emotions, one state and maybe some health to show how to use a triggervalue differently
        emotions.CreateNewTriggervalue("Happy", 0, 100, 0);
        emotions.CreateNewTriggervalue("Sad", 0, 100, 0);
        emotions.CreateNewTriggervalue("Hungry", 0, 100, 0);
        emotions.CreateNewTriggervalue("Health", 0, 255, 255);

        //Add an event when the health reaches 0
        emotions["Health"].AddOnEqualEvent(0, DebugDead);
    }

    void Update()
    {
        //Press F to lower the health of the above TriggerGroup
        if (Input.GetKey(KeyCode.F))
        {
            //You can check if a triggervalue exists
            if (emotions.ContainsTriggervalue("Health"))
            {
                //This indexer is readonly, but we can use this notation to change the value anyways
                emotions["Health"].SubtractValue(5);
            }

            //Log the remaining health
            Debug.Log("Remaining Health:" + emotions["Health"]);
        }
    }

    /// <summary>
    /// This method is triggered once the Health Triggervalue reaches 0
    /// </summary>
    private void DebugDead()
    {
        //Debug a sad comment
        Debug.Log("You died!");
    }
}
