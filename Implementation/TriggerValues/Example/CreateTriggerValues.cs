﻿using UnityEngine;
using EAssets.State.Triggervalues;

/// <summary>
/// This class shows how to setup a triggervalue
/// </summary>
public class CreateTriggerValues : MonoBehaviour
{
    //Create a new value that defines some behaviour, like the affection of a character or the like
    //The minimum value is now 0 and the maximum is 100, initial the value is set to 0
    public Triggervalue kindness = new Triggervalue(0, 100, 0);

    // Start is called before the first frame update
    void Awake()
    {
        //We register to events, one for >=50 and one for <50
        kindness.AddOnGreaterEqualEvent(50, NiceToSeeYou); //We are very kind to the user
        kindness.AddOnLowerEvent(50, NoThanks); //We are not as kind since the value is below 50

        //The value is checked every time it is changed
    }

    private int one = 1;
    void Update()
    {
        //Press A to increase the count
        if (Input.GetKey(KeyCode.A))
        {
            kindness += 1; //You can treat a triggervalue like an int
            Debug.Log(kindness.Value);
        }

        //Press D to decrease the count
        if (Input.GetKey(KeyCode.D))
        {
            kindness -= one; //You can even use other ints for calculations
            Debug.Log(kindness.Value);
        }
    }

    /// <summary>
    /// This method is called if the kindness variable goes above or to 50
    /// </summary>
    private void NiceToSeeYou()
    {
        //A kind message
        Debug.Log("Nice to see you!");
    }

    /// <summary>
    /// This method is called if the kindness variable goes below 50
    /// </summary>
    private void NoThanks()
    {
        //A not so kind message
        Debug.Log("Not nice to see you!");
    }
}
