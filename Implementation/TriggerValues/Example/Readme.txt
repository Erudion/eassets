This example shows you how to use the Triggervalues and TriggerGroups. The intention behind these classes was to allow easier management of events triggered on certain values of integers.
As an example: If the health of a player reaches a certain level, the player should get notified. A Triggervalue will check this for you.
A TriggerGroup contains n Triggervalues which can be used as a whole setup of values, like Health, Mana, Stamina and the like or even as some sort of emotion-meter. 

Scene: The scene has two main objects: 
-TriggerCenter: This object creates a TriggerValue named kindness. Press A to increase this value and D to decrease it. When it reaches 50 it will call a method
and log a message to the console. As long as it is below 50 it will log another message by calling a different method. 
-TriggerGroupExample: This object creates a TriggerGroup with 4 TriggerValues. One of them is called Health and can be decreased by pressing F. When it reaches 0
it will trigger a method which will log a message to the console.