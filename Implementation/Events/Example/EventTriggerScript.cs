﻿using UnityEngine;
using EAssets.Events;

/// <summary>
/// This class will trigger an event
/// </summary>
public class EventTriggerScript : MonoBehaviour
{
    //The value we want to log
    private int value = 0;
    void Update()
    {
        //Press the F key to signal the event
        if (Input.GetKeyDown(KeyCode.F))
        {
            //To signal the event we need to know its type and its name
            //We also need to give the method an input value
            EventController.Signal("Log");
            EventController.Signal<int>("LogInt", value);
            Debug.Log(EventController.Signal<int>("GetInt"));
            value = EventController.Signal<int, int>("IncreaseInt", value);
            Debug.Log(value);
        }

        //Press E to provoke a warning/an error
        if (Input.GetKeyDown(KeyCode.E))
        {
            //Wrong types will throw a warning and be logged in the console
            //The event will not get called if there is a typemismatch!
            EventController.Signal<float>("Log", 2.3f);
            EventController.Signal<float>("LogInt");
            EventController.Signal<float>("GetInt",2.4f);
            EventController.Signal<string,int>("IncreaseInt",5);
        }
    }
}
