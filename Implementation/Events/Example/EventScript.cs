﻿using UnityEngine;
using EAssets.Events;

/// <summary>
/// This script registers an event in the eventcontroller
/// The event will then be called from the EventTriggerScript
/// </summary>
public class EventScript : MonoBehaviour
{
    void Start()
    {
        //You can register events in the Eventcontroller by naming them with a key
        //and giving a reference to the method to use
        //You have to write the type of the data your method takes, in this case: int
        EventController.Register("Log", Log);
        EventController.Register<int>("LogInt", LogInt);
        EventController.Register<int>("GetInt", GetInteger);
        EventController.Register<int, int>("IncreaseInt", IncreaseInt);

        EventController.DeRegister("Log", Log);
        EventController.DeRegister<int>("LogInt", LogInt);
        EventController.DeRegister<int>("GetInt", GetInteger);
        EventController.DeRegister<int,int>("IncreaseInt", IncreaseInt);
    }

    /// <summary>
    /// This method logs a message
    /// </summary>
    public void Log()
    {
        Debug.Log("I am a simple log");
    }

    /// <summary>
    /// This method logs a given integer value
    /// </summary>
    /// <param name="i">The value we want to log</param>
    public void LogInt(int i)
    {
        Debug.Log(i);
    }

    private readonly int myValue = 8; //A value we want to return
    /// <summary>
    /// This method returns an integer from this script
    /// </summary>
    /// <returns></returns>
    public int GetInteger()
    {
        return myValue;
    }

    /// <summary>
    /// This method increases an integer and returns it
    /// </summary>
    /// <param name="i">The value we want to increase</param>
    /// <returns></returns>
    public int IncreaseInt(int i)
    {
        return i+=1;
    }
}
