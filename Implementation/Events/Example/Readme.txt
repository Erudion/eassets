This example shows you how to use the EventSystem. The intention behind this eventsystem is to make it easier to register and trigger events.
The Eventsystem right now (v. 0.9) can only use methods with 1 input type and 0 return types. In a later version this will be expanded to 0-1 input and 0-1 output values.
Since the in- and output can be classes as well it basicly allows for arbitrary in- and output. 

Scene: The scene has two main objects: 
-EventHandler: Registers one of its methods in the Eventhandler
-EventTrigger: Press F to trigger the method registered in EventHandler, Press E to provoke a type-mismatch