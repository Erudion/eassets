﻿//------------------------------------------------------------------------------
// EAssetsEvents.cs
//
// Copyright 2019 E-In-A-Box 
//
// Created by Christian Koch
// Owner: Christian Koch
// 
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace EAssets
{
    //Contains an eventcontroller for event handling
    namespace Events
    {
        /// <summary>
        /// This class lets you register events with a typed input value. You can trigger events by signaling there name
        /// </summary>
        /// <example>
        /// An example for the use of the EventController would be:
        /// <code>
        /// EventController.Register{TypeOutput,TypeInput}("SignalKey", MethodToRegister);
        /// EventController.Signal{TypeOutput,TypeInput}("SignalKey");
        /// </code>
        /// </example>
        public static class EventController
        {
            #region Attributes
            /// <summary>
            /// The eventlist that can be triggered on a signal
            /// </summary>
            private static Dictionary<string, BaseEvent> eventList = new Dictionary<string, BaseEvent>();
            /// <summary>
            /// A list of the input type for each signal. Used for type robustness
            /// </summary>
            private static Dictionary<string, Type> typeList = new Dictionary<string, Type>();
            #endregion

            #region Register Events
            /// <summary>
            /// Registers a new listener for a signal
            /// </summary>
            /// <typeparam name="T">The type of the method input</typeparam>
            /// <param name="name">The signal you want to listen to</param>
            /// <param name="method">The method you want to invoke</param>
            private static void RegisterNew<T>(string name, TypedInputEvent<T>.TypedInputEventDelegate method)
            {
                eventList.Add(name, new TypedInputEvent<T>(method));
                typeList.Add(name, typeof(T));
            }

            /// <summary>
            /// Registers a new listener for a signal
            /// </summary>
            /// <typeparam name="T">The type of the method output</typeparam>
            /// <param name="name">The signal you want to listen to</param>
            /// <param name="method">The method you want to invoke</param>
            private static void RegisterNew<T>(string name, TypedReturnEvent<T>.TypedReturnEventDelegate method)
            {
                eventList.Add(name, new TypedReturnEvent<T>(method));
                typeList.Add(name, typeof(T));
            }

            /// <summary>
            /// Registers a new listener for a signal
            /// </summary>
            /// <param name="name">The signal you want to listen to</param>
            /// <param name="method">The method you want to invoke</param>
            private static void RegisterNew(string name, NonTypedEvent.NonTypedEventDelegate method)
            {
                eventList.Add(name, new NonTypedEvent(method));
                typeList.Add(name, null);
            }

            /// <summary>
            /// Registers a new listener for a signal
            /// </summary>
            /// <param name="name">The signal you want to listen to</param>
            /// <param name="method">The method you want to invoke</param>
            /// <typeparam name="TOut">The output type of this event</typeparam>
            /// <typeparam name="TIn">The input type of this event</typeparam>
            private static void RegisterNew<TOut, TIn>(string name, TypedInOutEvent<TOut, TIn>.TypedInOutEventDelegate method)
            {
                eventList.Add(name, new TypedInOutEvent<TOut, TIn>(method));
                typeList.Add(name + "_in", typeof(TIn));
                typeList.Add(name + "_out", typeof(TOut));
            }

            /// <summary>
            /// Register a method in the eventcontroller. It will get called when the key is signalled.
            /// <para>If the key already exists it is added to the signal event handler</para>
            /// </summary>
            /// <typeparam name="T">The type of the input the method takes</typeparam>
            /// <param name="key">The key you want to listen for</param>
            /// <param name="method">The method to be triggered on a signal</param>
            /// <example>
            /// <code>
            /// //Registers a method with one input in the EventController
            /// EventController.Register{inputType}("signalName", MethodName);
            /// </code>
            /// </example>
            public static void Register<T>(string key, TypedInputEvent<T>.TypedInputEventDelegate method)
            {
                if (eventList.ContainsKey(key)) //Check if the delegate exists
                {
                    if (typeList[key].Equals(typeof(T)))    //Check if the type is correct
                    {
                        ((TypedInputEvent<T>)eventList[key]).AddEvent(method);
                    }
                    else
                    {
                        IO.DataIO.GlobalLogWarning("Type mismatch at Register: " + key + ". Expected type: " + typeList[key] + "; received type: " + typeof(T));
                    }
                }
                else
                {
                    RegisterNew<T>(key, method);
                }
            }

            /// <summary>
            /// Register a method in the eventcontroller. It will get called when the key is signalled.
            /// <para>If the key already exists it is added to the signal event handler</para>
            /// </summary>
            /// <typeparam name="T">The type of the output the method takes</typeparam>
            /// <param name="key">The key you want to listen for</param>
            /// <param name="method">The method to be triggered on a signal</param>
            /// <example>
            /// <code>
            /// //Registers a method with one output in the EventController
            /// EventController.Register{outputType}("signalName", MethodName);
            /// </code>
            /// </example>
            public static void Register<T>(string key, TypedReturnEvent<T>.TypedReturnEventDelegate method)
            {
                if (eventList.ContainsKey(key)) //Check if the delegate exists
                {
                    if (typeList[key].Equals(typeof(T)))    //Check if the type is correct
                    {
                        ((TypedReturnEvent<T>)eventList[key]).AddEvent(method);
                    }
                    else
                    {
                        IO.DataIO.GlobalLogWarning("Type mismatch at Register: " + key + ". Expected type: " + typeList[key] + "; received type: " + typeof(T));
                    }
                }
                else
                {
                    RegisterNew<T>(key, method);
                }
            }

            /// <summary>
            /// Register a method in the eventcontroller. It will get called when the key is signalled.
            /// <para>If the key already exists it is added to the signal event handler</para>
            /// </summary>
            /// <param name="key">The key you want to listen for</param>
            /// <param name="method">The method to be triggered on a signal</param>
            /// <example>
            /// <code>
            /// //Registers a method with no in- or output in the EventController
            /// EventController.Register("signalName", MethodName);
            /// </code>
            /// </example>
            public static void Register(string key, NonTypedEvent.NonTypedEventDelegate method)
            {
                if (eventList.ContainsKey(key)) //Check if the delegate exists
                {
                    ((NonTypedEvent)eventList[key]).AddEvent(method);
                }
                else
                {
                    RegisterNew(key, method);
                }
            }

            /// <summary>
            /// Register a method in the eventcontroller. It will get called when the key is signalled.
            /// <para>If the key already exists it is added to the signal event handler</para>
            /// </summary>
            /// <param name="key">The key you want to listen for</param>
            /// <param name="method">The method to be triggered on a signal</param>
            /// <typeparam name="TOut">The output type of this event</typeparam>
            /// <typeparam name="TIn">The input type of this event</typeparam>
            /// <example>
            /// <code>
            /// //Registers a method with one in- and output in the EventController
            /// EventController.Register{outputType,inputType}("signalName", MethodName);
            /// </code>
            /// </example>
            public static void Register<TOut, TIn>(string key, TypedInOutEvent<TOut, TIn>.TypedInOutEventDelegate method)
            {
                if (typeList.ContainsKey(key)) //Check if the key exists
                {
                    if (typeList[key + "_in"].Equals(typeof(TIn)))    //Check if the type is correct
                    {
                        if (typeList[key + "_out"].Equals(typeof(TOut)))
                        {
                            ((TypedInOutEvent<TOut, TIn>)eventList[key]).AddEvent(method);
                            return;
                        }
                    }
                }

                RegisterNew<TOut, TIn>(key, method);
            }

            #endregion

            #region Deregister Events
            /// <summary>
            /// Deregister a method in the eventcontroller
            /// </summary>
            /// <typeparam name="T">The type of the registered methods input</typeparam>
            /// <param name="key">The key of the signal the method would be triggered on</param>
            /// <param name="method">The method to deregister</param>
            /// <example>
            /// <code>
            /// //Deregisters a method with one input from the EventController
            /// EventController.DeRegister{inputType}("signalName", MethodName);
            /// </code>
            /// </example>
            public static void DeRegister<T>(string key, TypedInputEvent<T>.TypedInputEventDelegate method)
            {
                if (eventList.ContainsKey(key)) //Check if the delegate exists
                {
                    if (typeList[key].Equals(typeof(T)))    //Check if the type is correct
                    {
                        ((TypedInputEvent<T>)eventList[key]).RemoveEvent(method);
                    }
                    else
                    {
                        IO.DataIO.GlobalLogWarning("Type mismatch at DeRegister: " + key + ". Expected type: " + typeList[key] + "; received type: " + typeof(T));
                    }

                    eventList.Remove(key);
                }
            }

            /// <summary>
            /// Deregister a method in the eventcontroller
            /// </summary>
            /// <typeparam name="T">The type of the registered methods output</typeparam>
            /// <param name="key">The key of the signal the method would be triggered on</param>
            /// <param name="method">The method to deregister</param>
            /// <example>
            /// <code>
            /// //Deregisters a method with one output from the EventController
            /// EventController.DeRegister{outputType}("signalName", MethodName);
            /// </code>
            /// </example>
            public static void DeRegister<T>(string key, TypedReturnEvent<T>.TypedReturnEventDelegate method)
            {
                if (eventList.ContainsKey(key)) //Check if the delegate exists
                {
                    if (typeList[key].Equals(typeof(T)))    //Check if the type is correct
                    {
                        ((TypedReturnEvent<T>)eventList[key]).RemoveEvent(method);
                    }
                    else
                    {
                        IO.DataIO.GlobalLogWarning("Type mismatch at DeRegister: " + key + ". Expected type: " + typeList[key] + "; received type: " + typeof(T));
                    }

                    eventList.Remove(key);
                }
            }

            /// <summary>
            /// Deregister a method in the eventcontroller
            /// </summary>
            /// <param name="key">The key of the signal the method would be triggered on</param>
            /// <param name="method">The method to deregister</param>
            /// <example>
            /// <code>
            /// //Deregisters a method with no in- pr output from the EventController
            /// EventController.DeRegister("signalName",MethodName);
            /// </code>
            /// </example>
            public static void DeRegister(string key, NonTypedEvent.NonTypedEventDelegate method)
            {
                if (eventList.ContainsKey(key)) //Check if the delegate exists
                {
                    ((NonTypedEvent)eventList[key]).RemoveEvent(method);

                    eventList.Remove(key);
                }
            }

            /// <summary>
            /// Deregister a method in the eventcontroller
            /// </summary>
            /// <param name="key">The key of the signal the method would be triggered on</param>
            /// <param name="method">The method to deregister</param>
            /// <example>
            /// <code>
            /// //Deregisters a method with one in- pr output from the EventController
            /// EventController.DeRegister{outputType,inputType}("signalName", MethodName);
            /// </code>
            /// </example>
            public static void DeRegister<TOut, TIn>(string key, TypedInOutEvent<TOut,TIn>.TypedInOutEventDelegate method)
            {
                if (eventList.ContainsKey(key)) //Check if the delegate exists
                {
                    if (typeList[key+"_out"].Equals(typeof(TOut)))    //Check if the type is correct
                    {
                        if (typeList[key + "_in"].Equals(typeof(TIn)))
                            ((TypedInOutEvent < TOut,TIn>)eventList[key]).RemoveEvent(method);
                    }

                    eventList.Remove(key);
                }
            }
            #endregion

            #region Trigger/Signal Events
            /// <summary>
            /// Signal an event to be triggered with a given value
            /// </summary>
            /// <typeparam name="T">The type of the signalvalue you want to send</typeparam>
            /// <param name="signalName">The name of the signal you want to emit</param>
            /// <param name="input">The input value you want to send</param>
            /// <example>
            /// <code>
            /// //Signals all methods with a given key (/signalname) with one input on the EventController
            /// EventController.Signal{inputType}("signalName", inputValue);
            /// </code>
            /// </example>
            public static void Signal<T>(string signalName, T input = default)
            {
                if (eventList.ContainsKey(signalName))
                {
                    if (typeList[signalName] != null && typeList[signalName].Equals(typeof(T)))
                    {
                        ((TypedInputEvent<T>)eventList[signalName]).InvokeEvent(input);
                    }
                    else
                    {
                        IO.DataIO.GlobalLogWarning("Typemismatch at EventController. SignalName: " + signalName + "; Received Type: " + typeof(T));
                    }
                }
                else
                {
                    IO.DataIO.GlobalLogWarning("No signal with the value: " + signalName + " found");
                }
            }

            /// <summary>
            /// Signal an event to be triggered, returning a result
            /// </summary>
            /// <typeparam name="T">The type of the signalvalue you want to receive</typeparam>
            /// <param name="signalName">The name of the signal you want to emit</param>
            /// <example>
            /// <code>
            /// //Signals all methods with a given key (/signalname) with one output on the EventController
            /// T returnValue = EventController.Signal{outputType}("signalName");
            /// 
            /// //Do something with the returnvalue here...
            /// </code>
            /// </example>
            public static T Signal<T>(string signalName)
            {
                if (eventList.ContainsKey(signalName))
                {
                    if (typeList[signalName] != null && typeList[signalName].Equals(typeof(T)))
                    {
                        return ((TypedReturnEvent<T>)eventList[signalName]).InvokeEvent();
                    }
                    else
                    {
                        IO.DataIO.GlobalLogWarning("Typemismatch at EventController. SignalName: " + signalName + ". Expected Type: " + typeList[signalName].GetType() + "; Received Type: " + typeof(T));
                        return default;
                    }
                }
                else
                {
                    IO.DataIO.GlobalLogWarning("No signal with the value: " + signalName + " found");
                    return default;
                }
            }

            /// <summary>
            /// Signal an event to be triggered
            /// </summary>
            /// <param name="signalName">The name of the signal you want to emit</param>
            /// <example>
            /// <code>
            /// //Signals all methods with a given key (/signalname) with no in- or output on the EventController
            /// EventController.Signal("signalName");
            /// </code>
            /// </example>
            public static void Signal(string signalName)
            {
                if (eventList.ContainsKey(signalName))
                {
                    ((NonTypedEvent)eventList[signalName]).InvokeEvent();
                }
                else
                {
                    IO.DataIO.GlobalLogWarning("No signal with the value: " + signalName + " found");
                }
            }

            /// <summary>
            /// Signal an event to be triggered
            /// </summary>
            /// <param name="signalName">The name of the signal you want to emit</param>
            /// <param name="input">The input you want to signal</param>
            /// <typeparam name="TOut">The return type of the signal</typeparam>
            /// <typeparam name="TIn">The input type of the signal</typeparam>
            /// <example>
            /// <code>
            /// //Signals all methods with a given key (/signalname) with one in- and output on the EventController
            /// T returnValue = EventController.Signal{outputType,inputType}("signalName", inputValue);
            /// 
            /// //Do something with the returnValue here...
            /// </code>
            /// </example>
            public static TOut Signal<TOut, TIn>(string signalName, TIn input)
            {
                if (eventList.ContainsKey(signalName))
                {
                    //Check the types
                    if (typeList[signalName+"_in"] != null &&  typeList[signalName+"_in"].Equals(typeof(TIn)))
                    {
                        if (typeList[signalName + "_out"] != null && typeList[signalName + "_out"].Equals(typeof(TOut)))
                        {
                            return ((TypedInOutEvent<TOut, TIn>)eventList[signalName]).InvokeEvent(input);
                        }
                        else
                        {
                            IO.DataIO.GlobalLogWarning("Output typemismatch at signal " + signalName
                                + "; Output-Type needed: " + (typeList[signalName + "_out"] ?? null) + ", Output-Type received: " + typeof(TOut));
                        }
                    }
                    else
                    {
                        IO.DataIO.GlobalLogWarning("Input typemismatch at signal " + signalName 
                            + "; Input-Type needed: " + (typeList[signalName + "_in"] ?? null) + ", Input-Type received: " + typeof(TIn));
                    }
                }
                else
                {
                    IO.DataIO.GlobalLogWarning("No signal with the key: " + signalName + " found");
                }

                return default;
            }
            #endregion
        }

        #region Event Classes
        /// <summary>
        /// Defines a basic event
        /// <para>You should not inherit from this class</para>
        /// </summary>
        public class BaseEvent
        {

        }
        /// <summary>
        /// Defines an event with one input and one output type
        /// </summary>
        public class TypedInOutEvent<TOut, TIn> : BaseEvent
        {
            /// <summary>
            /// The delegate definition for typed-events
            /// </summary>
            public delegate TOut TypedInOutEventDelegate(TIn value);
            /// <summary>
            /// The event handle for this event class
            /// </summary>
            private event TypedInOutEventDelegate EventHandle;

            /// <summary>
            /// Create a new Typedevent
            /// </summary>
            /// <param name="method">The method you want to call for this event</param>
            public TypedInOutEvent(TypedInOutEventDelegate method)
            {
                EventHandle += method;
            }

            /// <summary>
            /// Add a new event to this handler
            /// </summary>
            /// <param name="method">The method you want to add</param>
            public void AddEvent(TypedInOutEventDelegate method)
            {
                EventHandle += method;
            }

            /// <summary>
            /// Remove a method from this handler
            /// </summary>
            /// <param name="method">The method you want to remove</param>
            public void RemoveEvent(TypedInOutEventDelegate method)
            {
                EventHandle -= method;
            }

            /// <summary>
            /// Invoke all methods of this handle, if any
            /// Returns default if no such method exists
            /// </summary>
            public TOut InvokeEvent(TIn input)
            {
                if (EventHandle != null)
                    return EventHandle.Invoke(input);
                else
                {
                    return default;
                }
            }
        }
        /// <summary>
        /// Defines a typed event with an input and no output type
        /// </summary>
        /// <typeparam name="T">The input parameter of this event</typeparam>
        public class TypedInputEvent<T> : BaseEvent
        {
            /// <summary>
            /// The delegate definition for typed-events
            /// </summary>
            /// <param name="input"></param>
            public delegate void TypedInputEventDelegate(T input);
            /// <summary>
            /// The event handle for this event class
            /// </summary>
            private event TypedInputEventDelegate EventHandle;

            /// <summary>
            /// Create a new Typedevent
            /// </summary>
            /// <param name="method">The method you want to call for this event</param>
            public TypedInputEvent(TypedInputEventDelegate method)
            {
                EventHandle += method;
            }

            /// <summary>
            /// Add a new event to this handler
            /// </summary>
            /// <param name="method">The method you want to add</param>
            public void AddEvent(TypedInputEventDelegate method)
            {
                EventHandle += method;
            }

            /// <summary>
            /// Remove a method from this handler
            /// </summary>
            /// <param name="method">The method you want to remove</param>
            public void RemoveEvent(TypedInputEventDelegate method)
            {
                EventHandle -= method;
            }

            /// <summary>
            /// Invoke all methods of this handle, if any
            /// </summary>
            /// <param name="input"></param>
            public void InvokeEvent(T input)
            {
                EventHandle?.Invoke(input);
            }
        }
        /// <summary>
        /// Defines a typed event with no input and an output type
        /// </summary>
        /// <typeparam name="T">The output parameter of this event</typeparam>
        public class TypedReturnEvent<T> : BaseEvent
        {
            /// <summary>
            /// The delegate definition for typed-events
            /// </summary>
            public delegate T TypedReturnEventDelegate();
            /// <summary>
            /// The event handle for this event class
            /// </summary>
            private event TypedReturnEventDelegate EventHandle;

            /// <summary>
            /// Create a new Typedevent
            /// </summary>
            /// <param name="method">The method you want to call for this event</param>
            public TypedReturnEvent(TypedReturnEventDelegate method)
            {
                EventHandle += method;
            }

            /// <summary>
            /// Add a new event to this handler
            /// </summary>
            /// <param name="method">The method you want to add</param>
            public void AddEvent(TypedReturnEventDelegate method)
            {
                EventHandle += method;
            }

            /// <summary>
            /// Remove a method from this handler
            /// </summary>
            /// <param name="method">The method you want to remove</param>
            public void RemoveEvent(TypedReturnEventDelegate method)
            {
                EventHandle -= method;
            }

            /// <summary>
            /// Invoke all methods of this handle, if any
            /// </summary>
            public T InvokeEvent()
            {
                if (EventHandle != null)
                    return EventHandle.Invoke();
                else
                    return default;
            }
        }
        /// <summary>
        /// Defines a non-typed event with no input and no output type
        /// </summary>
        public class NonTypedEvent : BaseEvent
        {
            /// <summary>
            /// The delegate definition for typed-events
            /// </summary>
            public delegate void NonTypedEventDelegate();
            /// <summary>
            /// The event handle for this event class
            /// </summary>
            private event NonTypedEventDelegate EventHandle;

            /// <summary>
            /// Create a new Typedevent
            /// </summary>
            /// <param name="method">The method you want to call for this event</param>
            public NonTypedEvent(NonTypedEventDelegate method)
            {
                EventHandle += method;
            }

            /// <summary>
            /// Add a new event to this handler
            /// </summary>
            /// <param name="method">The method you want to add</param>
            public void AddEvent(NonTypedEventDelegate method)
            {
                EventHandle += method;
            }

            /// <summary>
            /// Remove a method from this handler
            /// </summary>
            /// <param name="method">The method you want to remove</param>
            public void RemoveEvent(NonTypedEventDelegate method)
            {
                EventHandle -= method;
            }

            /// <summary>
            /// Invoke all methods of this handle, if any
            /// </summary>
            public void InvokeEvent()
            {
                EventHandle?.Invoke();
            }
        }
        #endregion
    }
}
