This example shows you how to use the MultiTag-class. The intention behind the MultiTag is to allow the user to add several tags to a Gameobject
instead of just a single one.

Scene: The scene contains 5 Objects. One MultiTagManager_0 object which is needed to use the MultiTag functionality.
The other 4 objects are 3 TagReceiver (x) objects which have the MultiTag script attached in which there tags are 
defined. The first TagReceiver contains a script which assigns its own tags and after that tries to get GameObjects
by their tags.