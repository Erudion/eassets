﻿using UnityEngine;
using EAssets.Multitag;

/// <summary>
/// This class shows you how to assign tags to gameobjects
/// </summary>
public class TagExample : MonoBehaviour
{
    void Start()
    {
        //No Tag "Player" assigned
        Debug.Log("Has Tag Player: " + gameObject.HasTag("Player"));
        Debug.Assert(gameObject.HasTag("Player") == false);

        //Assign "Player" tag
        gameObject.AddTag("Player");

        //The tag is now assigned
        Debug.Log("Has Tag Player: " + gameObject.HasTag("Player"));
        Debug.Assert(gameObject.HasTag("Player") == true);

        //No tag "Enemy" assigned
        Debug.Log("Has Tag Player and Enemy: " + (gameObject.HasTag("Player") && gameObject.HasTag("Enemy")));
        Debug.Assert(gameObject.HasTag("Player") && gameObject.HasTag("Enemy") == false);

        //Assign tag "Enemy"
        gameObject.AddTag("Enemy");

        //Now both "Player" and "Enemy" are assigned
        Debug.Log("Has Tag Player and Enemy: " + (gameObject.HasTag("Player") && gameObject.HasTag("Enemy")));
        Debug.Assert(gameObject.HasTag("Player") && gameObject.HasTag("Enemy") == true);

        //Now lets try to get the object(s)
        Debug.Assert(gameObject.FindGameObjectWithTag("Player").name != null); //There should be some object
        Debug.Assert(gameObject.FindGameObjectWithTag("Enemy").name != null); //There should be some object
        Debug.Assert(gameObject.FindGameObjectsWithTag("Enemy").Length == 4); //There should be all 4 objects in the scene
        Debug.Assert(gameObject.FindGameObjectWithTags("Player", "Enemy").name != null); //There should be some object
        Debug.Assert(gameObject.FindGameObjectsWithTags("Player", "Enemy").Length == 4); //There should be all 4 objects in the scene
    }
}
