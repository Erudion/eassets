﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This ScriptableObject stores a list of allowed tags that 
/// can be assigned to GameObjects via the MultiTag class
/// </summary>
[CreateAssetMenu(menuName = "EAssets/MultiTag/MultiTagManager")]
public class MultiTagManager : ScriptableObject
{
    /// <summary>
    /// A public list of allowed tags
    /// </summary>
    public List<string> Tags = new List<string>();

    /// <summary>
    /// This dictionary contains all references to GameObjects with a given tag
    /// </summary>
    protected Dictionary<string, List<GameObject>> gameObjectLists
        = new Dictionary<string, List<GameObject>>();

    #region Basic Methods
    /// <summary>
    /// Create a new tag in the MultiTagManager
    /// </summary>
    /// <param name="tag">The tag you want to add</param>
    public void CreateNewTag(string tag)
    {
        if(!Tags.Contains(tag))
            Tags.Add(tag);
    }

    /// <summary>
    /// Remove a tag in the MultiTagManager
    /// </summary>
    /// <param name="tag">The tag you want to remove</param>
    public void RemoveTag(string tag)
    {
        if (Tags.Contains(tag))
            Tags.Remove(tag);
    }

    /// <summary>
    /// Returns true if the given tag is registered
    /// </summary>
    /// <param name="tag">The tag you want to check</param>
    /// <returns></returns>
    public bool HasTag(string tag)
    {
        return Tags.Contains(tag);
    }
    #endregion

    #region Finding Methods
    /// <summary>
    /// Register a GameObject with a tag to find it later with FindGameObject(s)WithTag
    /// </summary>
    /// <param name="go">The GameObject you want to register</param>
    /// <param name="tag">The tag you want to register it for</param>
    public void RegisterGameObjectWithTag(GameObject go, string tag)
    {
        if (gameObjectLists.ContainsKey(tag)) //If the key exists, add the new object
        {
            gameObjectLists[tag].Add(go);
        }
        else //If the key does not exist, create a new list and add the object
        {
            List<GameObject> list = new List<GameObject> { go };
            gameObjectLists.Add(tag, list);
        }
    }

    /// <summary>
    /// Removes a GameObject with a tag to no longer find it with FindGameObject(s)WithTag
    /// </summary>
    /// <param name="go">The GameObject to deregister</param>
    /// <param name="tag">The tag you want to deregister it for</param>
    public void DeRegisterGameObjectWithTag(GameObject go, string tag)
    {
        if (gameObjectLists.ContainsKey(tag)) //If the key exists, remove the object
        {
            gameObjectLists[tag].Remove(go);
        }
    }

    /// <summary>
    /// Returns the first object with a given tag, else null
    /// </summary>
    /// <param name="tag">The tag you want to check</param>
    /// <returns></returns>
    public GameObject FindGameObjectWithTag(string tag)
    {
        if (gameObjectLists.ContainsKey(tag))
        {
            return gameObjectLists[tag][0];
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Returns all objects with a given tag, else null
    /// </summary>
    /// <param name="tag">The tag you want to check</param>
    /// <returns></returns>
    public GameObject[] FindGameObjectsWithTag(string tag)
    {
        if (gameObjectLists.ContainsKey(tag))
        {
            return gameObjectLists[tag].ToArray();
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Returns the first gameobject that has all given tags
    /// </summary>
    /// <param name="tags">The tags you want to check</param>
    /// <returns></returns>
    public GameObject FindGameObjectWithTags(params string[] tags)
    {
        if (gameObjectLists.ContainsKey(tags[0])) //Check if the first tag is valid
        {
            List<GameObject> gameObjects = gameObjectLists[tags[0]];
            GameObject res = null;
            foreach (GameObject go in gameObjects) //Check every gameobject with tag 0
            {
                res = go;
                for (int i = 1; i < tags.Length; i++)
                {
                    if (!gameObjectLists[tags[i]].Contains(go)) //If the object does not have this tag, do not return it
                    {
                        res = null;
                        break;
                    }
                }
            }

            return res; //Return res, which is not null if one GameObject has all tags

        }
        else //If the first tag is invalid return null
        {
            return null;
        }
    }

    /// <summary>
    /// Returns the all gameobjects that have all given tags
    /// </summary>
    /// <param name="tags">The tags you want to check</param>
    /// <returns></returns>
    public GameObject[] FindGameObjectsWithTags(params string[] tags)
    {
        if (gameObjectLists.ContainsKey(tags[0])) //Check if the first tag is valid
        {
            List<GameObject> gameObjects = gameObjectLists[tags[0]];
            GameObject res = null;
            List<GameObject> tmp = new List<GameObject>();
            foreach (GameObject go in gameObjects) //Check every gameobject with tag 0
            {
                res = go;
                for (int i = 1; i < tags.Length; i++)
                {
                    if (!gameObjectLists[tags[i]].Contains(go)) //If the object does not have this tag, do not return it
                    {
                        res = null;
                        break;
                    }
                }
                if (res != null)
                    tmp.Add(res);
            }

            return tmp.ToArray(); //Return res, which is not null if one GameObject has all tags

        }
        else //If the first tag is invalid return null
        {
            return null;
        }
    }
    #endregion
}
