﻿using UnityEngine;

/// <summary>
/// This script should be attached to a prefab. 
/// It manages the allows tags in the scene using a ScriptableObject
/// </summary>
public class MultiTagComponent : MonoBehaviour
{
    [Tooltip("Use the MultiTagManager that contains all tags you want to use")]
    public MultiTagManager manager;

    private void Awake()
    {
        //Set the gameobjects name to a given value
        gameObject.name = "MultiTagManager_0";

        DontDestroyOnLoad(this);

        if (manager == null)
        {
            Debug.LogWarning("Warning: No MultiTagManager attached at: " + gameObject.name + "; This is "
                + "required to use the multitag system");
        }
    }
}
