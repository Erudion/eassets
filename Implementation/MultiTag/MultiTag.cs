﻿using System.Collections.Generic;
using UnityEngine;
using EAssets.Multitag;

/// <summary>
/// This class manages all tags that are attached to the GameObject
/// </summary>
public class MultiTag : MonoBehaviour
{
    /// <summary>
    /// All tags that are assigned to this Gameobject
    /// </summary>
    public List<string> tags = new List<string>();

    [Tooltip("The attached script contains the MultiTagManager")]
    public MultiTagManager manager;

    private void Start()
    {
        GetManager();

        if (manager != null)
        {
            for (int i = 0; i < tags.Count; i++) //Add all references that are already assigned
            {
                manager.RegisterGameObjectWithTag(gameObject, tags[i]);
            }
        }
        else
        {
            Debug.LogWarning("No MultiTagManager found. It is needed for MultiTag usage!");
        }

        //Add the current tag to the known tags
        gameObject.AddTag(gameObject.tag);
    }

    /// <summary>
    /// Get the MultiTagManager
    /// </summary>
    private void GetManager()
    {
        //Get the multitagmanager-GameObject
        GameObject go = GameObject.Find("MultiTagManager_0");


        if (go == null)
        {
            //No Manager found
            Debug.LogWarning("No MultiTagManager found. It is needed to use the MultiTag class");
        }
        else //if(go != null)
        {
            //Get the manager object
            manager = go.GetComponent<MultiTagComponent>().manager;
        }
    }

    #region Tag Management
    /// <summary>
    /// Returns true if the given tag is assigned to this MultiTag object
    /// </summary>
    /// <param name="tag">The tag you want to check</param>
    /// <returns></returns>
    public bool HasTag(string tag)
    {
        return tags.Contains(tag);
    }

    /// <summary>
    /// Checks if all the given tags are assigned
    /// </summary>
    /// <param name="tags">The tags you want to check</param>
    /// <returns></returns>
    public bool HasTags(params string[] tags)
    {
        bool res = true;
        foreach(string str in tags)
        {
            res &= HasTag(str);
        }

        return res;
    }

    /// <summary>
    /// Returns true if any of the given tags is assigned
    /// </summary>
    /// <param name="tags">The tags you want to check</param>
    /// <returns></returns>
    public bool HasAnyTag(params string[] tags)
    {
        bool res = false;
        foreach (string str in tags)
        {
            res |= HasTag(str);
        }

        return res;
    }

    /// <summary>
    /// Add a tag to this MulitTag object. This will throw a warning if the given tag does not exist
    /// </summary>
    /// <param name="tag">The tag you want to add</param>
    /// <returns></returns>
    public void AddTag(string tag)
    {
        if (manager == null)
        {
            GetManager();

            if (manager == null)
            {
                Debug.LogWarning("TagManager needed for MultiTag but it was not found!");
            }
        }

        if (manager.HasTag(tag))
        {
            tags.Add(tag);
            manager.RegisterGameObjectWithTag(gameObject,tag);
        }
        else
            Debug.LogWarning("TagManager does not contain tag: " + tag);
    }

    /// <summary>
    /// Assigns a range of tags
    /// </summary>
    /// <param name="tags">The tags you want to assign</param>
    public void AddTags(params string[] tags)
    {
        foreach(string str in tags)
        {
            AddTag(str);
        }
    }

    /// <summary>
    /// Remove a tag from this MulitTag object
    /// </summary>
    /// <param name="tag">The tag you want to remove</param>
    /// <returns></returns>
    public void RemoveTag(string tag)
    {
        if (manager == null)
        {
            GetManager();

            if (manager == null)
            {
                Debug.LogWarning("TagManager needed for MultiTag but it was not found!");
            }
        }

        if (tags.Contains(tag))
        {
            tags.Remove(tag);
            manager.DeRegisterGameObjectWithTag(gameObject, tag);
        }
    }

    /// <summary>
    /// Removes a range of tags
    /// </summary>
    /// <param name="tags">The tags you want to remove</param>
    public void RemoveTags(params string[] tags)
    {
        foreach(string str in tags)
        {
            RemoveTag(str);
        }
    }
    #endregion

    #region Find Methods
    /// <summary>
    /// Returns the first object with a given tag, else null
    /// </summary>
    /// <param name="tag">The tag you want to check</param>
    /// <returns></returns>
    public GameObject FindGameObjectWithTag(string tag)
    {
        if (manager == null)
        {
            GetManager();

            if (manager == null)
            {
                Debug.LogWarning("TagManager needed for MultiTag but it was not found!");
            }
        }

        return manager.FindGameObjectWithTag(tag);
    }

    /// <summary>
    /// Returns all objects with a given tag, else null
    /// </summary>
    /// <param name="tag">The tag you want to check</param>
    /// <returns></returns>
    public GameObject[] FindGameObjectsWithTag(string tag)
    {
        if (manager == null)
        {
            GetManager();

            if (manager == null)
            {
                Debug.LogWarning("TagManager needed for MultiTag but it was not found!");
            }
        }

        return manager.FindGameObjectsWithTag(tag);
    }

    /// <summary>
    /// Returns the first gameobject that has all given tags
    /// </summary>
    /// <param name="tags">The tags you want to check</param>
    /// <returns></returns>
    public GameObject FindGameObjectWithTags(params string[] tags)
    {
        if (manager == null)
        {
            GetManager();

            if (manager == null)
            {
                Debug.LogWarning("TagManager needed for MultiTag but it was not found!");
            }
        }

        return manager.FindGameObjectWithTags(tags);
    }

    /// <summary>
    /// Returns the all gameobjects that have all given tags
    /// </summary>
    /// <param name="tags">The tags you want to check</param>
    /// <returns></returns>
    public GameObject[] FindGameObjectsWithTags(params string[] tags)
    {
        if (manager == null)
        {
            GetManager();

            if (manager == null)
            {
                Debug.LogWarning("TagManager needed for MultiTag but it was not found!");
            }
        }

        return manager.FindGameObjectsWithTags(tags);
    }
    #endregion

}
#region GameObject Extensions For MultiTag
namespace EAssets
{
    namespace Multitag
    {
        /// <summary>
        /// This class contains extensions for gameobjects
        /// </summary>
        public static class GameObjectExtensions
        {
            /// <summary>
            /// Add a new tag to this gameobject
            /// </summary>
            /// <param name="go">This gameobject</param>
            /// <param name="tag">The tag you want to add</param>
            public static void AddTag(this GameObject go, string tag)
            {
                if (go.GetComponent<MultiTag>() == null)
                {
                    go.AddComponent<MultiTag>();
                }

                go.GetComponent<MultiTag>().AddTag(tag);
            }

            /// <summary>
            /// Remove a tag from this gameobject
            /// </summary>
            /// <param name="go">This gameobject</param>
            /// <param name="tag">The tag you want to remove</param>
            public static void RemoveTag(this GameObject go, string tag)
            {
                if (go.GetComponent<MultiTag>() == null)
                {
                    go.AddComponent<MultiTag>();
                }

                go.GetComponent<MultiTag>().RemoveTag(tag);
            }

            /// <summary>
            /// Returns true if this gameobject has the given tag
            /// </summary>
            /// <param name="go">This gameobject</param>
            /// <param name="tag">The tag you want to check</param>
            /// <returns></returns>
            public static bool HasTag(this GameObject go, string tag)
            {
                if (go.GetComponent<MultiTag>() == null)
                {
                    go.AddComponent<MultiTag>();
                    return false;
                }

                return go.GetComponent<MultiTag>().HasTag(tag);
            }

            #region FindObjects
            /// <summary>
            /// Returns the first object with a given tag, else null
            /// </summary>
            /// <param name="go">This GameObject</param>
            /// <param name="tag">The tag you want to check</param>
            /// <returns></returns>
            public static GameObject FindGameObjectWithTag(this GameObject go, string tag)
            {
                if (go.GetComponent<MultiTag>() == null)
                {
                    go.AddComponent<MultiTag>();
                }

                return go.GetComponent<MultiTag>().FindGameObjectWithTag(tag);
            }

            /// <summary>
            /// Returns all objects with a given tag, else null
            /// </summary>
            /// <param name="go">This GameObject</param>
            /// <param name="tag">The tag you want to check</param>
            /// <returns></returns>
            public static GameObject[] FindGameObjectsWithTag(this GameObject go, string tag)
            {
                if (go.GetComponent<MultiTag>() == null)
                {
                    go.AddComponent<MultiTag>();
                }

                return go.GetComponent<MultiTag>().FindGameObjectsWithTag(tag);
            }

            /// <summary>
            /// Returns the first gameobject that has all given tags
            /// </summary>
            /// <param name="tags">The tags you want to check</param>
            /// <returns></returns>
            public static GameObject FindGameObjectWithTags(this GameObject go, params string[] tags)
            {
                if (go.GetComponent<MultiTag>() == null)
                {
                    go.AddComponent<MultiTag>();
                }

                return go.GetComponent<MultiTag>().FindGameObjectWithTags(tags);
            }

            /// <summary>
            /// Returns the all gameobjects that have all given tags
            /// </summary>
            /// <param name="tags">The tags you want to check</param>
            /// <returns></returns>
            public static GameObject[] FindGameObjectsWithTags(this GameObject go, params string[] tags)
            {
                if (go.GetComponent<MultiTag>() == null)
                {
                    go.AddComponent<MultiTag>();
                }

                return go.GetComponent<MultiTag>().FindGameObjectsWithTags(tags);
            }
            #endregion
        }

    }
}
#endregion
