﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace EAssets
{
    //Contains an implementation of a gameobject pool
    namespace Pooling
    {
        /// <summary>
        /// Defines a pool interface
        /// </summary>
        /// <typeparam name="T">The type of the pool</typeparam>
        [System.Serializable]
        public abstract class BasePool<T>
        {
            /// <summary>
            /// The pool of accessible objects
            /// </summary>
            protected Queue<T> pool;
            /// <summary>
            /// The pool of active objects
            /// </summary>
            protected Queue<T> activeObjects;

            /// <summary>
            /// The number of items in this pool
            /// </summary>
            public int ItemCount { get; protected set; }

            /// <summary>
            /// The number of not yet spawned objects in the pool
            /// </summary>
            public int NotSpawnedCount { get { return pool.Count; } }
            /// <summary>
            /// The number of already spawned objects
            /// </summary>
            public int SpawnedCount { get { return activeObjects.Count; } }

            /// <summary>
            /// Defines a generic event for onspawn and ondespawn
            /// </summary>
            /// <typeparam name="T0">The type of this event</typeparam>
            [System.Serializable]
            public class PoolEvent : UnityEvent<T> { }

            /// <summary>
            /// This event gets triggered when an object is spawned
            /// </summary>
            [SerializeField]
            public PoolEvent OnSpawn;
            /// <summary>
            /// This event gets triggered when an object is despawned
            /// </summary>
            [SerializeField]
            public PoolEvent OnDespawn;

            /// <summary>
            /// Create a new empty BasePool
            /// </summary>
            public BasePool()
            {
                pool = new Queue<T>();
                activeObjects = new Queue<T>();
            }

            /// <summary>
            /// Create a new BasePool
            /// </summary>
            /// <param name="poolObject">The object to pool</param>
            /// <param name="count">The number of items to pool</param>
            public BasePool(T poolObject, int count)
            {
                pool = new Queue<T>();
                activeObjects = new Queue<T>();

                SetupPool(poolObject, count);
            }

            /// <summary>
            /// Create a new BasePool
            /// </summary>
            /// <param name="poolObjects">The objects to pool</param>
            public BasePool(params T[] poolObjects)
            {
                pool = new Queue<T>();
                activeObjects = new Queue<T>();

                SetupPool(poolObjects);
            }

            /// <summary>
            /// Sets up the pool
            /// </summary>
            /// <param name="poolObjects">The object to pool</param>
            /// <param name="count">The number of items to pool</param>
            public abstract void SetupPool(T poolObject, int count);

            /// <summary>
            /// Sets up the pool
            /// </summary>
            /// <param name="poolObjects">The objects to setup the pool with</param>
            public virtual void SetupPool(params T[] poolObjects)
            {
                int count = poolObjects.Length;
                if (count <= 0) //The minimum count is 1
                    throw new ArgumentException("SetupPool: no objects given at setup");

                this.ItemCount = count;

                foreach (T tmp in poolObjects)
                {
                    pool.Enqueue(tmp);
                }
            }

            /// <summary>
            /// Spawns one object
            /// </summary>
            /// <returns></returns>
            public abstract T Spawn();

            /// <summary>
            /// Spawns a range of objects
            /// </summary>
            /// <param name="count">The ammount of objects spawned</param>
            /// <returns></returns>
            public abstract List<T> SpawnRange(int count);

            /// <summary>
            /// Despawns the spawned object that exists the longest
            /// </summary>
            public abstract void Despawn();

            /// <summary>
            /// Despawn the object. If it is not part of the pool it will not be despawned
            /// </summary>
            /// <param name="obj">The object to despawn</param>
            public virtual void Despawn(T obj)
            {
                if (activeObjects.Contains(obj))
                {
                    List<T> tmp = new List<T>(activeObjects);
                    int index = tmp.IndexOf(obj);

                    T tmp2 = tmp[index];
                    OnDespawn?.Invoke(tmp2);
                    tmp.Remove(obj);

                    activeObjects = new Queue<T>(tmp);
                    pool.Enqueue(tmp2);
                }
            }

            /// <summary>
            /// Despawns an object. If it is not part of the pool it will not be despawned
            /// </summary>
            /// <param name="obj">The object you want to destroy/despawn</param>
            public virtual void Destroy(T obj)
            {
                Despawn(obj);
            }

            /// <summary>
            /// Despawn the complete pool
            /// </summary>
            public void DespawnPool()
            {
                if (activeObjects == null || activeObjects.Count == 0)
                    return;

                while (activeObjects.Count != 0)
                    Despawn();
            }

        }

        /// <summary>
        /// This class defines a pool of gameobjects
        /// <para>Use advise: create a variable private Pool pool; and initialize it in the Awake or Start method: pool = new Pool(go,10);</para>
        /// </summary>
        [System.Serializable]
        public class Pool : BasePool<GameObject>
        {            
            /// <summary>
            /// Defines events taking gameobjects
            /// </summary>
            [System.Serializable]
            public class GameObjectPoolEvent : UnityEvent<GameObject> { }
            /// <summary>
            /// This event is used instead of the generic OnSpawn event
            /// </summary>
            [SerializeField]
            public GameObjectPoolEvent OnGOSpawn;
            /// <summary>
            /// This event is used instead of the generic OnDespawn event
            /// </summary>
            [SerializeField]
            public GameObjectPoolEvent OnGODespawn;

            /// <summary>
            /// Create a new pool
            /// </summary>
            public Pool() : base()
            {

            }

            /// <summary>
            /// Create a new pool
            /// </summary>
            /// <param name="go">The gameobject you want to pool</param>
            /// <param name="count">The number of objects to pool</param>
            public Pool(GameObject go, int count = 10) : base(go,count)
            {

            }
            /// <summary>
            /// Create a new pool
            /// </summary>
            /// <param name="gos">The gameobjects to pool</param>
            public Pool(params GameObject[] gos) : base(gos)
            {

            }

            /// <summary>
            /// Setup this pool
            /// </summary>
            /// <param name="go">The gameobject you want to pool (this is overwritten if Prefab is set)</param>
            /// <param name="count">The ammount of gameobject you want to pool (this is overwritten if PrefabCount is set)</param>
            /// <example>
            /// <code>
            /// //Create a new, empty pool
            /// Pool pool = new Pool();
            ///
            /// //Get any GameObject
            /// GameObject go = someGameObject;
            /// 
            /// //Create 20 pooled versions of the gameobject reference
            /// pool.SetupPool(go,20);
            /// </code>
            /// </example>
            public override void SetupPool(GameObject go, int count = 10)
            {
                if (count <= 0) //The minimum count is 1
                    count = 1;

                this.ItemCount = count;

                if (go != null)
                {
                    string prefName = go.name; //Store the name for later use
                    GameObject tmp;
                    for (int i = 0; i < count; i++)
                    {
                        tmp = GameObject.Instantiate(go);
                        tmp.name = prefName + "_" + i;
                        tmp.SetActive(false);
                        pool.Enqueue(tmp);
                    }
                }
                else
                {
                    Debug.LogWarning("No Gameobject given for pooling!");
                }
            }

            /// <summary>
            /// Spawn a new gameobject in the worlds origin
            /// </summary>
            /// <returns></returns>
            public override GameObject Spawn()
            {
                return Spawn(Vector3.zero, Quaternion.Euler(0, 0, 0), null, 0);
            }
            /// <summary>
            /// Spawn a new gameobject
            /// </summary>
            /// <param name="position">The position to spawn it at</param>
            /// <param name="rotation">The rotation of the object</param>
            /// <param name="parent">The parent of the spawned object</param>
            /// <param name="layer">The layer of the spawned object</param>
            /// <returns></returns>
            /// <example>
            /// <code>
            /// //Set the position you want to spawn your new object at
            /// Vector3 position = new Vector3(...);
            /// //Set the rotation of the new object
            /// Quaternion rotation = new Quaternion(...);
            /// //Set the parent of the new object
            /// GameObject parent = someGameObject;
            /// //Set the layer of the new object
            /// int layer = someLayer;
            /// 
            /// //Spawn an instance of your pooled objects
            /// //If there is no object in the pool the oldest one will get despawned
            /// pool.Spawn(position,rotation,parent,layer);
            /// </code>
            /// </example>
            public GameObject Spawn(Vector3 position, Quaternion rotation = default, Transform parent = null, int layer = 0)
            {
                if (pool.Count <= 0)
                    Despawn();

                //Get the gameobject
                if (pool != null && pool.Count > 0)
                {
                    GameObject tmp = pool.Dequeue();//pool[currentItem];
                    activeObjects.Enqueue(tmp);
                    //Call the onGOspawn event with the spawned gameobject
                    OnGOSpawn?.Invoke(tmp);
                    //Activate the object (if not already)
                    if (!tmp.activeSelf)
                        tmp.SetActive(true);
                    //Set position and rotation
                    tmp.transform.position = position;
                    tmp.transform.rotation = rotation;
                    //Set parent
                    tmp.transform.SetParent(parent);
                    //Set the layer of the object
                    tmp.layer = layer;

                    return tmp;
                }
                return null;
            }

            /// <summary>
            /// Spawn a range of gameobjects
            /// </summary>
            /// <param name="count">The count of gameobjects</param>
            /// <returns></returns>
            public override List<GameObject> SpawnRange(int count)
            {
                return SpawnRange(count, null);
            }
            /// <summary>
            /// Spawn a lot of gameobject
            /// </summary>
            /// <param name="count">The ammount of gameobject to spawn</param>
            /// <returns></returns>
            public List<GameObject> SpawnRange(int count, params Vector3[] positions)
            {
                List<GameObject> tmp = new List<GameObject>();
                Vector3 vec = new Vector3();
                for (int i = 0; i < count; i++)
                {
                    if (positions.Length > i)
                        vec = positions[i];

                    tmp.Add(Spawn(vec));
                }
                return tmp;
            }

            /// <summary>
            /// Despawns the currently oldest pool-object
            /// </summary>
            public override void Despawn()
            {
                if (activeObjects.Count != 0)
                {
                    GameObject tmp = activeObjects.Dequeue();
                    OnGODespawn?.Invoke(tmp); //Call the onGOdespawn event with the despawned object

                    tmp.SetActive(false);
                    pool.Enqueue(tmp);
                }
            }
            /// <summary>
            /// Despawn a gameobject. If it is not part of the pool it will not be despawned
            /// </summary>
            /// <param name="go"></param>
            public override void Despawn(GameObject go)
            {
                if (activeObjects.Contains(go))
                {
                    List<GameObject> tmp = new List<GameObject>(activeObjects);
                    int index = tmp.IndexOf(go);

                    GameObject tmp2 = tmp[index];
                    OnGODespawn?.Invoke(tmp2);
                    tmp2.SetActive(false);
                    tmp.Remove(go);

                    activeObjects = new Queue<GameObject>(tmp);
                    pool.Enqueue(tmp2);
                }
            }

            /// <summary>
            /// Despawns a gameobject. If it is not part of the pool it will not be despawned
            /// </summary>
            /// <param name="go">The gameobject you want to destroy/despawn</param>
            public override void Destroy(GameObject go)
            {
                Despawn(go);
            }
        }

        namespace Generic
        {
            /// <summary>
            /// This class is a more generic version of the pool class. It allows to pool
            /// all classes that derive from UnityEngine.Object. To change the state of these
            /// objects use the OnSpawn and OnDespawn events
            /// </summary>
            /// <typeparam name="T"></typeparam>
            public class GenericPool<T> : BasePool<T> where T : class
            {
                /// <summary>
                /// Create a new pool
                /// </summary>
                public GenericPool() : base()
                {

                }

                /// <summary>
                /// Create a new pool
                /// </summary>
                /// <param name="obj">The objects you want to pool</param>
                public GenericPool(params T[] obj) : base (obj)
                {

                }

                /// <summary>
                /// Setup this pool
                /// </summary>
                /// <param name="go">The gameobject you want to pool</param>
                /// <param name="count">The ammount of gameobject you want to pool</param>
                public override void SetupPool(params T[] obj)
                {
                    int count = obj.Length;
                    if (count <= 0) //The minimum count is 1
                        throw new ArgumentException("SetupPool: no objects given at setup");

                    this.ItemCount = count;

                    foreach (T tmp in obj)
                    {
                        pool.Enqueue(tmp);
                    }
                }

                /// <summary>
                /// Spawn a new object
                /// </summary>
                /// <returns></returns>
                public override T Spawn()
                {
                    if (pool.Count <= 0)
                        Despawn();

                    //Get the gameobject
                    if (pool != null && pool.Count > 0)
                    {
                        T tmp = pool.Dequeue();
                        activeObjects.Enqueue(tmp);
                        OnSpawn?.Invoke(tmp);
                        return tmp;
                    }
                    return null;
                }

                /// <summary>
                /// Spawns a range of generics
                /// </summary>
                /// <param name="count">The number of generics to add</param>
                /// <returns></returns>
                public override List<T> SpawnRange(int count)
                {
                    List<T> tmp = new List<T>();
                    for (int i = 0; i < count; i++)
                    {
                        tmp.Add(Spawn());
                    }
                    return tmp;
                }

                /// <summary>
                /// Despawns the currently oldest pool-object
                /// </summary>
                public override void Despawn()
                {
                    if (activeObjects.Count != 0)
                    {
                        T tmp = activeObjects.Dequeue();
                        OnDespawn?.Invoke(tmp); //Call the ondespawn event with the despawned object
                        pool.Enqueue(tmp);
                    }
                }

                /// <summary>
                /// Adds the same object to the queue 
                /// </summary>
                /// <param name="poolObjects">The object to add</param>
                /// <param name="count">The added objects</param>
                public override void SetupPool(T poolObjects, int count)
                {
                    ItemCount = count;

                    for(int i = 0;i<count;i++)
                        pool.Enqueue(poolObjects);
                }
            }
        }
    }
}
