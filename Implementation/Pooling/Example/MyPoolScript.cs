﻿using UnityEngine;
using EAssets.Pooling;

public class MyPoolScript : MonoBehaviour
{
    [Tooltip("The gameobject to pool")]
    public GameObject go;
    [Tooltip("The ammount of gameobject to pool")]
    public int gameObjectCount;
    [Tooltip("The new pool. Assign events here")]
    public Pool pool = new Pool(); //Creata a new pool

    void Awake()
    {
        pool.SetupPool(go,gameObjectCount); //Set the data of the pool (it is recommended to do this once in awake)
    }

    private Vector3 vec3;
    void Update()
    {
        //You can press S to spawn a new GameObject and press D to despawn one

        if (Input.GetKeyDown(KeyCode.S))
        {
            pool.Spawn(vec3);

            //Set a new position for the next object 
            //This way it is easier to see that they are actually spawned
            if (vec3.y > gameObjectCount)
            {
                vec3 = new Vector3(0, 0, 0);
            }
            vec3 += new Vector3(0,1,0);

        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            pool.Despawn();
        }
    }

    /// <summary>
    /// This method can be assigned to be executed onspawn
    /// </summary>
    /// <param name="go">The spawned gameobject</param>
    public void DoOnSpawn(GameObject go)
    {
        Debug.Log("New object spawned: " + go.name);
    }

    /// <summary>
    /// This method can be assigned to be executed ondespawn
    /// </summary>
    /// <param name="go">The despawned gameobject</param>
    public void DoOnDespawn(GameObject go)
    {
        Debug.Log("New object despawned: " + go.name);
    }
}
