This example shows you how to use the Pool-class. The intention behind the Pool is to lower CPU cost of spawning objects by recycling them.
The Pool allows you to create pools of Gameobjects and trigger events when they are spawned/despawned. Remember that the referenced objects can still be destroyed.
If you want the pool to persist to another scene take care of the objects as well.

Scene: The scene allows you to spawn cubes (by pressing S) and to despawn Cubes (by pressing D).
An event is fired every time you spawn/despawn an object. You can set the prefab, the pool size and the events at the "Pool" object.