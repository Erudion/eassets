var class_e_texture_sheet_animation_module =
[
    [ "ETextureSheetAnimationModule", "class_e_texture_sheet_animation_module.html#acb2d0e7bcf6afe40c10056d84550cec4", null ],
    [ "ETextureSheetAnimationModule", "class_e_texture_sheet_animation_module.html#a388aea7d962348d84cd51bea20df96fa", null ],
    [ "SetData", "class_e_texture_sheet_animation_module.html#a9492819ecdc4194f5e5aa8a4cf1548d2", null ],
    [ "ToUnityType", "class_e_texture_sheet_animation_module.html#a5569d3b1d79ae61430f9dc0223518d08", null ],
    [ "animation", "class_e_texture_sheet_animation_module.html#a594d98679eb8f0874b7beae4121660f8", null ],
    [ "cycleCount", "class_e_texture_sheet_animation_module.html#a7e6af50fe83b16d771db47308eab142a", null ],
    [ "enabled", "class_e_texture_sheet_animation_module.html#a450cda767930bf0f84b8db1714c155b4", null ],
    [ "frameOverTime", "class_e_texture_sheet_animation_module.html#a3e1255bef1ae7421d2259f94a54cee48", null ],
    [ "frameOverTimeMultiplier", "class_e_texture_sheet_animation_module.html#a34f6d3cbc9655307aff00eea9b1f56bc", null ],
    [ "numTilesX", "class_e_texture_sheet_animation_module.html#a5726d0c4af9d215762cd4cbdba6b01a4", null ],
    [ "numTilesY", "class_e_texture_sheet_animation_module.html#ad9f9a6f775b8a0a00550454f3be57d01", null ],
    [ "rowIndex", "class_e_texture_sheet_animation_module.html#a42c1e5bd1b6bd7aa387d8f248b828763", null ],
    [ "rowMode", "class_e_texture_sheet_animation_module.html#a3e97793f882e9b0385944b37fb54a6a2", null ],
    [ "startFrame", "class_e_texture_sheet_animation_module.html#a0426a41058e67b0245043e69eb66f760", null ],
    [ "startFrameMultiplier", "class_e_texture_sheet_animation_module.html#a27a9cfe3b94b5544eb950a763b9690ab", null ],
    [ "uvChannelMask", "class_e_texture_sheet_animation_module.html#a121c89059030d172db7d36af15531c0d", null ]
];