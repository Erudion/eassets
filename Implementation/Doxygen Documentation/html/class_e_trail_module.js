var class_e_trail_module =
[
    [ "ETrailModule", "class_e_trail_module.html#aff054d9bb9f6e6c54baa5c6a0a5fb251", null ],
    [ "ETrailModule", "class_e_trail_module.html#ad6344d046ceadbe83b622a1eaf019730", null ],
    [ "SetData", "class_e_trail_module.html#acfdb6d0ddb1ccfb9fc9657a9cab4c5fd", null ],
    [ "ToUnityType", "class_e_trail_module.html#af2d745d3652107fe90d71a5b3ca34fc3", null ],
    [ "colorOverLifetime", "class_e_trail_module.html#abc31edfe648e5cc2a7a9b84d4beef857", null ],
    [ "colorOverTrail", "class_e_trail_module.html#a6d9539e7d56244bfc8e97d580165f0a4", null ],
    [ "dieWithParticles", "class_e_trail_module.html#a8fc2b7a3110fe53fd212a48d0d4f384a", null ],
    [ "enabled", "class_e_trail_module.html#aaa8ebd56a26f97d1e49c2da39cf563d1", null ],
    [ "inheritParticleColor", "class_e_trail_module.html#a4013e6fabb22eeb50859ee8a3371e93c", null ],
    [ "lifetime", "class_e_trail_module.html#a63e5ed78527f0813fc2d8cba4cbe62ea", null ],
    [ "lifetimeMultiplier", "class_e_trail_module.html#a89ae2b233cf09214d4de01ef8e3ecd5d", null ],
    [ "minVertexDistance", "class_e_trail_module.html#ab5171583194d0556f07c5a7e135b7079", null ],
    [ "ratio", "class_e_trail_module.html#a0a354ed3d64c1df56f33512ddb7bb3b4", null ],
    [ "sizeAffectsLifetime", "class_e_trail_module.html#a45acc0baf6bce67855164744cc43509a", null ],
    [ "sizeAffectsWidth", "class_e_trail_module.html#ad8054a875c17826b65ec6fea90b86d58", null ],
    [ "textureMode", "class_e_trail_module.html#abdbd1092b937fb6d0bf8275a4c1a613a", null ],
    [ "widthOverTrail", "class_e_trail_module.html#a4299d18cc65a655f63997b98355f01ee", null ],
    [ "widthOverTrailMultiplier", "class_e_trail_module.html#aedcf600b9ca0c58864b2762d2a1f7aa4", null ],
    [ "worldSpace", "class_e_trail_module.html#acda02a6981f655ae423163b45ec30143", null ]
];