var class_e_capsule_collider2_d =
[
    [ "ECapsuleCollider2D", "class_e_capsule_collider2_d.html#a4cb4232ce31d5c41f03675dd96067033", null ],
    [ "ECapsuleCollider2D", "class_e_capsule_collider2_d.html#a47c9906aa5121d19210fe0740bcc1457", null ],
    [ "SetData", "class_e_capsule_collider2_d.html#af528c42765f26e00075d48179c81ac0d", null ],
    [ "ToUnityType", "class_e_capsule_collider2_d.html#a9238d4dd0ae74c051fc8e9d434c44563", null ],
    [ "density", "class_e_capsule_collider2_d.html#aeb1d0b428766082b6277130eb96c7adb", null ],
    [ "direction", "class_e_capsule_collider2_d.html#a0e8953e928519d008b285cf3cf550bde", null ],
    [ "enabled", "class_e_capsule_collider2_d.html#a744f65cee34fa4d0155a5d975e23114b", null ],
    [ "hideFlags", "class_e_capsule_collider2_d.html#a83c146a23cb7fe4a137fa3bc90aab6f0", null ],
    [ "isTrigger", "class_e_capsule_collider2_d.html#a20e30efdcb39a7656be0c7c2bec51f18", null ],
    [ "name", "class_e_capsule_collider2_d.html#a44ccf6269be5f87ac6740ec3b0af17bc", null ],
    [ "offset", "class_e_capsule_collider2_d.html#afb6101f05d12eb77ed05eaef6ff0fcde", null ],
    [ "sharedMaterial", "class_e_capsule_collider2_d.html#acaea7095b9f6344ac652ab4fb0ac047b", null ],
    [ "size", "class_e_capsule_collider2_d.html#a881b7381dfdf3257fab0624fcb063051", null ],
    [ "tag", "class_e_capsule_collider2_d.html#a7fe62a15f7a2e4f13aa39f38b00f14aa", null ],
    [ "usedByComposite", "class_e_capsule_collider2_d.html#a67f7576f4a1395eb5949243c694eea3b", null ],
    [ "usedByEffector", "class_e_capsule_collider2_d.html#a740c741d9f4bcdf473051d8de918fe44", null ]
];