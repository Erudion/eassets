var class_e_terrain_data =
[
    [ "ETerrainData", "class_e_terrain_data.html#a6693fd63265934801150c99464b1a7ff", null ],
    [ "ETerrainData", "class_e_terrain_data.html#a4495d7ef9e0a4ce9e4f43b0cce5da5b8", null ],
    [ "SetData", "class_e_terrain_data.html#a26f2d572d7b7ade0d46f2233018a2bb1", null ],
    [ "ToUnityType", "class_e_terrain_data.html#a4e6ef97a523041f15f4f6c57dac37aac", null ],
    [ "alphamapResolution", "class_e_terrain_data.html#ae14c7b8c601b2f917de8e995e84ab405", null ],
    [ "alphamaps", "class_e_terrain_data.html#aa2ae50fe755f6d0dcb1c26b2de57314e", null ],
    [ "baseMapResolution", "class_e_terrain_data.html#ac96209add2d16483c57f2ac77530f5a7", null ],
    [ "detailPrototypes", "class_e_terrain_data.html#a10d050717bbec47be1897bea9f0903c8", null ],
    [ "heightmapResolution", "class_e_terrain_data.html#a10fb70592a58add266118c30553c4c47", null ],
    [ "heights", "class_e_terrain_data.html#a1ff6c681910f48710789f0b3e317d191", null ],
    [ "hideFlags", "class_e_terrain_data.html#a920103c96f23516f22e5c78a04c9ea69", null ],
    [ "name", "class_e_terrain_data.html#a220b1882956657747074c5678d9cd4ab", null ],
    [ "size", "class_e_terrain_data.html#a2fbe7f4a8e3da72bb644db1e3c2d2aa7", null ],
    [ "splatPrototypes", "class_e_terrain_data.html#ab5ac59a94d6e01cbc3a75e276e14697a", null ],
    [ "thickness", "class_e_terrain_data.html#a506af4c3f7f8c274425d9585b675e11e", null ],
    [ "treeInstances", "class_e_terrain_data.html#a8343caf7a976ae64708c10e2e995fa27", null ],
    [ "treePrototypes", "class_e_terrain_data.html#ada8daced5a3b487e708c17ede84bdfcf", null ],
    [ "wavingGrassAmount", "class_e_terrain_data.html#a8ad622bfabdab6be741748ce785992db", null ],
    [ "wavingGrassSpeed", "class_e_terrain_data.html#afc07b00f2e801e07233534ae624c3755", null ],
    [ "wavingGrassStrength", "class_e_terrain_data.html#ac0a0858c8f3e018f934bc37170d172e5", null ],
    [ "wavingGrassTint", "class_e_terrain_data.html#ab24302b232db1232b123f39e447a94c9", null ]
];