var class_e_game_object =
[
    [ "EGameObject", "class_e_game_object.html#adae7ed566e444aedd18202b8d962bb8a", null ],
    [ "EGameObject", "class_e_game_object.html#a0ef637cb8492646ccf7dc3fd5293a5f8", null ],
    [ "SetData", "class_e_game_object.html#a61e44ba832447c35afbf52f5d7b6dcee", null ],
    [ "ToUnityType", "class_e_game_object.html#a7bbc3bb56f306d596c04f479feeb8bd8", null ],
    [ "activeSelf", "class_e_game_object.html#aba175a419584a8ebeb932b431a26f051", null ],
    [ "compData", "class_e_game_object.html#a8dc283509b25dca65b060a33921c6726", null ],
    [ "compList", "class_e_game_object.html#abe991e7e90c76a0b4089c0f6274dc012", null ],
    [ "CustomToUnityTypes", "class_e_game_object.html#a2f3fe84ffa3be1a73fb5b90f93b00782", null ],
    [ "hideFlags", "class_e_game_object.html#a24632acef8edfdf4fa082f5659cf7b80", null ],
    [ "isStatic", "class_e_game_object.html#a709496ff1e6d15fc2cc5b22578b7caf9", null ],
    [ "layer", "class_e_game_object.html#a610a05d235ba4b8bbc4479d4550dbe84", null ],
    [ "monoData", "class_e_game_object.html#a3ba88610d154eeb9ff508ca4f692867b", null ],
    [ "myUnityTypes", "class_e_game_object.html#a71b63f67551f4ee2196b823c426ffe05", null ],
    [ "name", "class_e_game_object.html#aa381eb308e8694459e2783ed5e1ec1eb", null ],
    [ "nonAllowedAttributes", "class_e_game_object.html#a25451ad6734d116d34043f018fd11df1", null ],
    [ "tag", "class_e_game_object.html#ae7ca725668a8c094d93ee2c66824ece6", null ],
    [ "UnityToCustomTypes", "class_e_game_object.html#ad353eb47e859912995aa06f55e6108f8", null ],
    [ "unityTypes", "class_e_game_object.html#a9607b1c062134810da3e82a2b0a8ddb4", null ]
];