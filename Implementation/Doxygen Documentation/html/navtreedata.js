/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "EAssets", "index.html", [
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Properties", "functions_prop.html", null ],
        [ "Events", "functions_evnt.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"class_e_assets_1_1_i_o_1_1_cached_1_1_cached_data_i_o.html",
"class_e_audio_source.html#ab1e8f591fec8a9230e3e72403cca4cc9",
"class_e_custom_data_module.html#a4be72a82465e5244c63aaa35d5cecceb",
"class_e_limit_velocity_over_lifetime_module.html#a688b2f15fd9bb6ffbf1aa6583ae40751",
"class_e_noise_module.html#aaf93c7434629aec85b86f1cbc3ce0452",
"class_e_rotation_by_speed_module.html#a59a5338f8a9fb864822b984e874e0e02",
"class_e_texture.html#a1db9fed74aac5127e6b4efc04c07d906",
"class_event_script.html#a65073eba10abc657fa93ad0b557a36c5"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';