var hierarchy =
[
    [ "EAssets.Events.BaseEvent", "class_e_assets_1_1_events_1_1_base_event.html", [
      [ "EAssets.Events.NonTypedEvent", "class_e_assets_1_1_events_1_1_non_typed_event.html", null ],
      [ "EAssets.Events.TypedInOutEvent< TOut, TIn >", "class_e_assets_1_1_events_1_1_typed_in_out_event.html", null ],
      [ "EAssets.Events.TypedInputEvent< T >", "class_e_assets_1_1_events_1_1_typed_input_event.html", null ],
      [ "EAssets.Events.TypedReturnEvent< T >", "class_e_assets_1_1_events_1_1_typed_return_event.html", null ]
    ] ],
    [ "EAssets.Pooling.BasePool< T >", "class_e_assets_1_1_pooling_1_1_base_pool.html", [
      [ "EAssets.Pooling.Generic.GenericPool< T >", "class_e_assets_1_1_pooling_1_1_generic_1_1_generic_pool.html", null ]
    ] ],
    [ "EAssets.Pooling.BasePool< GameObject >", "class_e_assets_1_1_pooling_1_1_base_pool.html", [
      [ "EAssets.Pooling.Pool", "class_e_assets_1_1_pooling_1_1_pool.html", null ]
    ] ],
    [ "EAssets.State.Triggervalues.Triggervalue.BasicEvent", "class_e_assets_1_1_state_1_1_triggervalues_1_1_triggervalue_1_1_basic_event.html", [
      [ "EAssets.State.Triggervalues.Triggervalue.EqualEvent", "class_e_assets_1_1_state_1_1_triggervalues_1_1_triggervalue_1_1_equal_event.html", null ],
      [ "EAssets.State.Triggervalues.Triggervalue.GreaterEqualEvent", "class_e_assets_1_1_state_1_1_triggervalues_1_1_triggervalue_1_1_greater_equal_event.html", null ],
      [ "EAssets.State.Triggervalues.Triggervalue.GreaterEvent", "class_e_assets_1_1_state_1_1_triggervalues_1_1_triggervalue_1_1_greater_event.html", null ],
      [ "EAssets.State.Triggervalues.Triggervalue.LowerEqualEvent", "class_e_assets_1_1_state_1_1_triggervalues_1_1_triggervalue_1_1_lower_equal_event.html", null ],
      [ "EAssets.State.Triggervalues.Triggervalue.LowerEvent", "class_e_assets_1_1_state_1_1_triggervalues_1_1_triggervalue_1_1_lower_event.html", null ]
    ] ],
    [ "EAssets.IO.Cached.CachedDataIO", "class_e_assets_1_1_i_o_1_1_cached_1_1_cached_data_i_o.html", null ],
    [ "EAssets.IO.Cached.CachedDataIO.CacheEntry", "class_e_assets_1_1_i_o_1_1_cached_1_1_cached_data_i_o_1_1_cache_entry.html", null ],
    [ "EAssets.DataStorage.DataDump", "class_e_assets_1_1_data_storage_1_1_data_dump.html", null ],
    [ "EAssets.IO.DataIO", "class_e_assets_1_1_i_o_1_1_data_i_o.html", null ],
    [ "EAssets.EPlayerPrefs.EPlayerPrefs", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs.html", null ],
    [ "EAssets.Events.EventController", "class_e_assets_1_1_events_1_1_event_controller.html", null ],
    [ "EAssets.Multitag.GameObjectExtensions", "class_e_assets_1_1_multitag_1_1_game_object_extensions.html", null ],
    [ "EAssets.Global.GlobalData", "class_e_assets_1_1_global_1_1_global_data.html", null ],
    [ "EAssets.IEMonoBehaviour", "interface_e_assets_1_1_i_e_mono_behaviour.html", [
      [ "EAssets.EMonoBehaviour", "class_e_assets_1_1_e_mono_behaviour.html", null ]
    ] ],
    [ "EAssets.DataStorage.LinkedValue< T >", "class_e_assets_1_1_data_storage_1_1_linked_value.html", null ],
    [ "MonoBehaviour", null, [
      [ "CreateTriggerGroup", "class_create_trigger_group.html", null ],
      [ "CreateTriggerValues", "class_create_trigger_values.html", null ],
      [ "EAssets.EMonoBehaviour", "class_e_assets_1_1_e_mono_behaviour.html", null ],
      [ "EventScript", "class_event_script.html", null ],
      [ "EventTriggerScript", "class_event_trigger_script.html", null ],
      [ "IOManager", "class_i_o_manager.html", null ],
      [ "MultiTag", "class_multi_tag.html", null ],
      [ "MultiTagComponent", "class_multi_tag_component.html", null ],
      [ "MyPoolScript", "class_my_pool_script.html", null ],
      [ "ReadData", "class_read_data.html", null ],
      [ "SavefileExample", "class_savefile_example.html", null ],
      [ "SimpleMonoBehaviour", "class_simple_mono_behaviour.html", null ],
      [ "StoreData", "class_store_data.html", null ],
      [ "TagExample", "class_tag_example.html", null ]
    ] ],
    [ "EAssets.Extensions.Objects.ObjectExtensions", "class_e_assets_1_1_extensions_1_1_objects_1_1_object_extensions.html", null ],
    [ "EAssets.IO.SerializableDatatypes.ParentSerializableClass", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_parent_serializable_class.html", [
      [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< T >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", null ]
    ] ],
    [ "EAssets.EPlayerPrefs.EPlayerPrefs.PlayerPrefExtension< Color >", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_player_pref_extension.html", [
      [ "EAssets.EPlayerPrefs.EPlayerPrefs.ColorPref", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_color_pref.html", null ]
    ] ],
    [ "EAssets.EPlayerPrefs.EPlayerPrefs.PlayerPrefExtension< float[]>", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_player_pref_extension.html", [
      [ "EAssets.EPlayerPrefs.EPlayerPrefs.FloatArrayPref", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_float_array_pref.html", null ]
    ] ],
    [ "EAssets.EPlayerPrefs.EPlayerPrefs.PlayerPrefExtension< int[]>", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_player_pref_extension.html", [
      [ "EAssets.EPlayerPrefs.EPlayerPrefs.IntArrayPref", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_int_array_pref.html", null ]
    ] ],
    [ "EAssets.EPlayerPrefs.EPlayerPrefs.PlayerPrefExtension< Quaternion >", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_player_pref_extension.html", [
      [ "EAssets.EPlayerPrefs.EPlayerPrefs.QuaternionPref", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_quaternion_pref.html", null ]
    ] ],
    [ "EAssets.EPlayerPrefs.EPlayerPrefs.PlayerPrefExtension< string[]>", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_player_pref_extension.html", [
      [ "EAssets.EPlayerPrefs.EPlayerPrefs.StringArrayPref", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_string_array_pref.html", null ]
    ] ],
    [ "EAssets.EPlayerPrefs.EPlayerPrefs.PlayerPrefExtension< Vector2 >", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_player_pref_extension.html", [
      [ "EAssets.EPlayerPrefs.EPlayerPrefs.Vector2Pref", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_vector2_pref.html", null ]
    ] ],
    [ "EAssets.EPlayerPrefs.EPlayerPrefs.PlayerPrefExtension< Vector2[]>", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_player_pref_extension.html", [
      [ "EAssets.EPlayerPrefs.EPlayerPrefs.Vector2ArrayPref", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_vector2_array_pref.html", null ]
    ] ],
    [ "EAssets.EPlayerPrefs.EPlayerPrefs.PlayerPrefExtension< Vector3 >", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_player_pref_extension.html", [
      [ "EAssets.EPlayerPrefs.EPlayerPrefs.Vector3Pref", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_vector3_pref.html", null ]
    ] ],
    [ "EAssets.EPlayerPrefs.EPlayerPrefs.PlayerPrefExtension< Vector3[]>", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_player_pref_extension.html", [
      [ "EAssets.EPlayerPrefs.EPlayerPrefs.Vector3ArrayPref", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_vector3_array_pref.html", null ]
    ] ],
    [ "EAssets.EPlayerPrefs.EPlayerPrefs.PlayerPrefExtensionParent", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_player_pref_extension_parent.html", [
      [ "EAssets.EPlayerPrefs.EPlayerPrefs.PlayerPrefExtension< T >", "class_e_assets_1_1_e_player_prefs_1_1_e_player_prefs_1_1_player_pref_extension.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.ReflectiveEnumerator", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_reflective_enumerator.html", null ],
    [ "EAssets.IO.Saving.Savefile", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html", null ],
    [ "EAssets.IO.Saving.SavefileData", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile_data.html", null ],
    [ "ScriptableObject", null, [
      [ "MultiTagManager", "class_multi_tag_manager.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Animation >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EAnimation", "class_e_animation.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< AnimationClip >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EAnimationClip", "class_e_animation_clip.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< AnimationCurve >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EAnimationCurve", "class_e_animation_curve.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< AnimationEvent >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EAnimationEvent", "class_e_animation_event.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Animator >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EAnimator", "class_e_animator.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< AudioClip >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EAudioClip", "class_e_audio_clip.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< AudioListener >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EAudioListener", "class_e_audio_listener.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< AudioMixer >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EAudioMixer", "class_e_audio_mixer.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< AudioMixerGroup >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EAudioMixerGroup", "class_e_audio_mixer_group.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< AudioSource >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EAudioSource", "class_e_audio_source.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Avatar >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EAvatar", "class_e_avatar.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< AvatarMask >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EAvatarMask", "class_e_avatar_mask.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< BoneWeight >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EBoneWeight", "class_e_bone_weight.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Bounds >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EBounds", "class_e_bounds.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< BoxCollider >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EBoxCollider", "class_e_box_collider.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< BoxCollider2D >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EBoxCollider2D", "class_e_box_collider2_d.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Camera >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ECamera", "class_e_camera.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< CapsuleCollider >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ECapsuleCollider", "class_e_capsule_collider.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< CapsuleCollider2D >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ECapsuleCollider2D", "class_e_capsule_collider2_d.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< CircleCollider2D >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ECircleCollider2D", "class_e_circle_collider2_d.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Color >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EColor", "class_e_color.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Color32 >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EColor32", "class_e_color32.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< CompositeCollider2D >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ECompositeCollider2D", "class_e_composite_collider2_d.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< DetailPrototype >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EDetailPrototype", "class_e_detail_prototype.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< EdgeCollider2D >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EEdgeCollider2D", "class_e_edge_collider2_d.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< EventSystem >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EEventSystem", "class_e_event_system.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Flare >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EFlare", "class_e_flare.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< GameObject >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EGameObject", "class_e_game_object.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Gradient >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EGradient", "class_e_gradient.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< GradientAlphaKey >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EGradientAlphaKey", "class_e_gradient_alpha_key.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< GradientColorKey >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EGradientColorKey", "class_e_gradient_color_key.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< HumanBone >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EHumanBone", "class_e_human_bone.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< HumanDescription >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EHumanDescription", "class_e_human_description.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< HumanLimit >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EHumanLimit", "class_e_human_limit.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< JointSpring >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EJointSpring", "class_e_joint_spring.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Keyframe >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EKeyframe", "class_e_keyframe.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< LayerMask >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ELayerMask", "class_e_layer_mask.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Light >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ELight", "class_e_light.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< LineRenderer >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ELineRenderer", "class_e_line_renderer.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Mask >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EMask", "class_e_mask.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Material >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EMaterial", "class_e_material.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Matrix4x4 >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EMatrix4x4", "class_e_matrix4x4.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Mesh >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EMesh", "class_e_mesh.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< MeshCollider >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EMeshCollider", "class_e_mesh_collider.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< MeshFilter >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EMeshFilter", "class_e_mesh_filter.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< MeshRenderer >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EMeshRenderer", "class_e_mesh_renderer.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EParticleSystem", "class_e_particle_system.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.CollisionModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ECollisionModule", "class_e_collision_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.ColorBySpeedModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EColorBySpeedModule", "class_e_color_by_speed_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.ColorOverLifetimeModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EColorOverLifetimeModule", "class_e_color_over_lifetime_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.CustomDataModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ECustomDataModule", "class_e_custom_data_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.EmissionModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EEmissionModule", "class_e_emission_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.ExternalForcesModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EExternalForcesModule", "class_e_external_forces_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.ForceOverLifetimeModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EForceOverLifetimeModule", "class_e_force_over_lifetime_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.InheritVelocityModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EInheritVelocityModule", "class_e_inherit_velocity_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.LightsModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ELightsModule", "class_e_lights_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.LimitVelocityOverLifetimeModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ELimitVelocityOverLifetimeModule", "class_e_limit_velocity_over_lifetime_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.MainModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EMainModule", "class_e_main_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.MinMaxCurve >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EMinMaxCurve", "class_e_min_max_curve.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.MinMaxGradient >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EMinMaxGradient", "class_e_min_max_gradient.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.NoiseModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ENoiseModule", "class_e_noise_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.RotationBySpeedModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ERotationBySpeedModule", "class_e_rotation_by_speed_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.RotationOverLifetimeModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ERotationOverLifetimeModule", "class_e_rotation_over_lifetime_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.ShapeModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EShapeModule", "class_e_shape_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.SizeBySpeedModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ESizeBySpeedModule", "class_e_size_by_speed_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.SizeOverLifetimeModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ESizeOverLifetimeModule", "class_e_size_over_lifetime_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.SubEmittersModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ESubEmittersModule", "class_e_sub_emitters_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.TextureSheetAnimationModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ETextureSheetAnimationModule", "class_e_texture_sheet_animation_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.TrailModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ETrailModule", "class_e_trail_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.TriggerModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ETriggerModule", "class_e_trigger_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< ParticleSystem.VelocityOverLifetimeModule >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EVelocityOverLifetimeModule", "class_e_velocity_over_lifetime_module.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< PhysicMaterial >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EPhysicsMaterial", "class_e_physics_material.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< PhysicsMaterial2D >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EPhysicMaterial2D", "class_e_physic_material2_d.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< PolygonCollider2D >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EPolygonCollider2D", "class_e_polygon_collider2_d.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Projector >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EProjector", "class_e_projector.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Quaternion >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EQuaternion", "class_e_quaternion.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Rect >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ERect", "class_e_rect.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< RectTransform >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ERectTransform", "class_e_rect_transform.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< RenderTexture >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ERenderTexture", "class_e_render_texture.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< RenderTextureDescriptor >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ERenderTextureDescriptor", "class_e_render_texture_descriptor.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Rigidbody >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ERigidbody", "class_e_rigidbody.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Rigidbody2D >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ERigidbody2D", "class_e_rigidbody2_d.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< RuntimeAnimatorController >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ERuntimeAnimatorController", "class_e_runtime_animator_controller.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Shader >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EShader", "class_e_shader.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< SkeletonBone >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ESkeletonBone", "class_e_skeleton_bone.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< SkinnedMeshRenderer >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ESkinnedMeshRenderer", "class_e_skinned_mesh_renderer.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< SphereCollider >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ESphereCollider", "class_e_sphere_collider.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Sprite >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ESprite", "class_e_sprite.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Terrain >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ETerrain", "class_e_terrain.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< TerrainCollider >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ETerrainCollider", "class_e_terrain_collider.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< TerrainData >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ETerrainData", "class_e_terrain_data.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Texture >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ETexture", "class_e_texture.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Texture2D >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ETexture2D", "class_e_texture2_d.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< TrailRenderer >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ETrailRenderer", "class_e_trail_renderer.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Transform >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ETransform", "class_e_transform.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< TreeInstance >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ETreeInstance", "class_e_tree_instance.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< TreePrototype >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "ETreePrototype", "class_e_tree_prototype.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Vector2 >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EVector2", "class_e_vector2.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Vector3 >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EVector3", "class_e_vector3.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< Vector4 >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EVector4", "class_e_vector4.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< WheelCollider >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EWheelCollider", "class_e_wheel_collider.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< WheelFrictionCurve >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EWheelFrictionCurve", "class_e_wheel_friction_curve.html", null ]
    ] ],
    [ "EAssets.IO.SerializableDatatypes.SerializableUnityData< WindZone >", "class_e_assets_1_1_i_o_1_1_serializable_datatypes_1_1_serializable_unity_data.html", [
      [ "EWindZone", "class_e_wind_zone.html", null ]
    ] ],
    [ "EAssets.State.StateList", "class_e_assets_1_1_state_1_1_state_list.html", null ],
    [ "EAssets.State.Triggervalues.TriggerGroup", "class_e_assets_1_1_state_1_1_triggervalues_1_1_trigger_group.html", null ],
    [ "EAssets.State.Triggervalues.Triggervalue", "class_e_assets_1_1_state_1_1_triggervalues_1_1_triggervalue.html", null ],
    [ "EAssets.Extensions.Types.TypeExtensions", "class_e_assets_1_1_extensions_1_1_types_1_1_type_extensions.html", null ],
    [ "EAssets.Extensions.Assemblies.TypeLoaderExtensions", "class_e_assets_1_1_extensions_1_1_assemblies_1_1_type_loader_extensions.html", null ],
    [ "UnityEvent", null, [
      [ "EAssets.Pooling.BasePool< T >.PoolEvent", "class_e_assets_1_1_pooling_1_1_base_pool_1_1_pool_event.html", null ],
      [ "EAssets.Pooling.Pool.GameObjectPoolEvent", "class_e_assets_1_1_pooling_1_1_pool_1_1_game_object_pool_event.html", null ]
    ] ]
];