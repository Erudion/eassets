var class_e_animation =
[
    [ "EAnimation", "class_e_animation.html#a261d4661f8c840344fae16122c7b43d9", null ],
    [ "EAnimation", "class_e_animation.html#ac428560ef1cf8664d2860ab9fa50e939", null ],
    [ "SetData", "class_e_animation.html#ac09d6f056cf62fcb87c3af52db7b221f", null ],
    [ "ToUnityType", "class_e_animation.html#a9339ce521f71c3be14a225cd94991b53", null ],
    [ "animatePhysics", "class_e_animation.html#a71f18291c7271243488f31f433b91c0f", null ],
    [ "clip", "class_e_animation.html#aac68ecb29fc27d86274640fc5b77d0de", null ],
    [ "cullingType", "class_e_animation.html#a55f27c84f57aecd105b4aee1756764d6", null ],
    [ "enabled", "class_e_animation.html#a92ea38abf63dbcb5e49d3b0ebc88d779", null ],
    [ "hideFlags", "class_e_animation.html#a539a46ba5faacbbbb977c94018b28a2f", null ],
    [ "localBounds", "class_e_animation.html#ac2f8a2da1977d149354cb92ba4e3bf8a", null ],
    [ "name", "class_e_animation.html#a54d1870126248e386935526fe7355e8b", null ],
    [ "playAutomatically", "class_e_animation.html#afc23217c1a09c83719aec789f18ccd31", null ],
    [ "tag", "class_e_animation.html#a34f0835de4e85c52367ea0259c8ef780", null ],
    [ "wrapMode", "class_e_animation.html#a0c6ca7296c1c76c091ce53da17561657", null ]
];