var class_e_rotation_over_lifetime_module =
[
    [ "ERotationOverLifetimeModule", "class_e_rotation_over_lifetime_module.html#a33ebbd889b184d5acbf759a3940d2a89", null ],
    [ "ERotationOverLifetimeModule", "class_e_rotation_over_lifetime_module.html#ad883d3aeb7748e864d7b70c801025e97", null ],
    [ "SetData", "class_e_rotation_over_lifetime_module.html#a1725b25b78369586693a3a80d67a16bd", null ],
    [ "ToUnityType", "class_e_rotation_over_lifetime_module.html#aae272c637187f7c1f9bc28f6ac70916c", null ],
    [ "enabled", "class_e_rotation_over_lifetime_module.html#a0ecf313b5d0ba255c679ea248c68bfce", null ],
    [ "separateAxes", "class_e_rotation_over_lifetime_module.html#a10cc279771dfe76389bec1b1f221c7f6", null ],
    [ "x", "class_e_rotation_over_lifetime_module.html#a937595042491de1dedadb069e8325b14", null ],
    [ "xMultiplier", "class_e_rotation_over_lifetime_module.html#a8f7bfa0b61376112ab5c279ac7f35b49", null ],
    [ "y", "class_e_rotation_over_lifetime_module.html#a3b52110b26655f05d27dafdc6ae719a0", null ],
    [ "yMultiplier", "class_e_rotation_over_lifetime_module.html#a8afaf48dcdde36055bb9449d3a6641d0", null ],
    [ "z", "class_e_rotation_over_lifetime_module.html#adf907ed841b2382efb54fd7f2f99b96f", null ],
    [ "zMultiplier", "class_e_rotation_over_lifetime_module.html#a7a4f309e002c0a75fd1dcc23cde49003", null ]
];