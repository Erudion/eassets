var class_e_detail_prototype =
[
    [ "EDetailPrototype", "class_e_detail_prototype.html#a1d23b5082b398f80b274ed213fe2e8ca", null ],
    [ "EDetailPrototype", "class_e_detail_prototype.html#aaacb7e3320b8eec06c7a3f853a073766", null ],
    [ "SetData", "class_e_detail_prototype.html#a7be227a5b55231d929cb73283e4f19d7", null ],
    [ "ToUnityType", "class_e_detail_prototype.html#a3f995781b822245df7093e20f4a77ec4", null ],
    [ "bendFactor", "class_e_detail_prototype.html#ac791733813c0cb33454ba7eb730613ed", null ],
    [ "dryColor", "class_e_detail_prototype.html#a386ec8f0201e1a72f1ac49cf8443a2d2", null ],
    [ "healthyColor", "class_e_detail_prototype.html#a3ff2d84574ed8f6903f8094d044b5079", null ],
    [ "maxHeight", "class_e_detail_prototype.html#ab8abf9397e0bc158215aaf151d2cbf30", null ],
    [ "maxWidth", "class_e_detail_prototype.html#a4aa0e8277f7c1ae37251e1077b7ce430", null ],
    [ "minHeight", "class_e_detail_prototype.html#a758cc2de9f9583625976a6bf22858e57", null ],
    [ "minWidth", "class_e_detail_prototype.html#a533039e4b54ee9bba37585ac738fd986", null ],
    [ "noiseSpread", "class_e_detail_prototype.html#aedd126f6f8d2c95cc67d057ffc8fa3aa", null ],
    [ "prototype", "class_e_detail_prototype.html#ac49ffcd6139c94a91bed872efc05a1da", null ],
    [ "prototypeTexture", "class_e_detail_prototype.html#a90807533958cbda328bea786cff0ffbf", null ],
    [ "renderMode", "class_e_detail_prototype.html#a6073c6570250dc6607179bf617ba3328", null ],
    [ "usePrototypeMesh", "class_e_detail_prototype.html#aa53265d5cf0d9d54ab6e44ddb55a8156", null ]
];