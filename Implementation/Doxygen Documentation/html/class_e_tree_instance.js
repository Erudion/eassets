var class_e_tree_instance =
[
    [ "ETreeInstance", "class_e_tree_instance.html#ae807b20a6d7c6acdd4fed7193c1a4a38", null ],
    [ "ETreeInstance", "class_e_tree_instance.html#ac39e672d6fa7ff8780ae2b5f8a438e11", null ],
    [ "SetData", "class_e_tree_instance.html#a88cd6ff4c39f8c429f33739a60e024df", null ],
    [ "ToUnityType", "class_e_tree_instance.html#a4d604407e021a12ce37e74cf3e61313a", null ],
    [ "color", "class_e_tree_instance.html#ace0db0d1e42fe1b28ee1b9e78ebec3d6", null ],
    [ "heightScale", "class_e_tree_instance.html#a996b9a574fbd498a2fe9cf984d1b1890", null ],
    [ "lightmapColor", "class_e_tree_instance.html#ab01fc0667242ffe47fde0cdbea5c4608", null ],
    [ "position", "class_e_tree_instance.html#ae5d60601cbe26ba09d8122b412c4b884", null ],
    [ "prototypeIndex", "class_e_tree_instance.html#a2c4de099627f53aba8588598e9ed9942", null ],
    [ "rotation", "class_e_tree_instance.html#a84949ec969fd57ab39124098f24588e1", null ],
    [ "widthScale", "class_e_tree_instance.html#a4a602badc947fd0d33459bc77550b1be", null ]
];