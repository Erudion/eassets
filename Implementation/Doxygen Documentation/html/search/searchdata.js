var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuvwxyz",
  1: "bcdefgilmnopqrstv",
  2: "e",
  3: "abcdefghilmnoprstw",
  4: "abcdefghiklmnopqrstuvwxyz",
  5: "cdefgiknrstv",
  6: "e"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "properties",
  6: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Properties",
  6: "Events"
};

