var class_e_human_limit =
[
    [ "EHumanLimit", "class_e_human_limit.html#afd5cc3d64047b6c484d412b1e288bf36", null ],
    [ "EHumanLimit", "class_e_human_limit.html#a423e5c057a19cd1af1b433b285b6353a", null ],
    [ "SetData", "class_e_human_limit.html#a511963dee220b506f797588f694316fa", null ],
    [ "ToUnityType", "class_e_human_limit.html#a28956f0be375566a553497447326cece", null ],
    [ "axisLength", "class_e_human_limit.html#af4243c3c25e96e9e514020f0801e5aa8", null ],
    [ "center", "class_e_human_limit.html#acafff38ea3f59fbcf9e8a3847e97c27b", null ],
    [ "max", "class_e_human_limit.html#a867d67e948331927766b64e299f236e7", null ],
    [ "min", "class_e_human_limit.html#ad0422f2f89e7114dc1eb0b8b5e1f28b3", null ],
    [ "useDefaultValues", "class_e_human_limit.html#abcc3587591276b034ca2beeeb19793e1", null ]
];