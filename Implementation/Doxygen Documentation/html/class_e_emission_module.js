var class_e_emission_module =
[
    [ "EEmissionModule", "class_e_emission_module.html#a1cacba697bb184d728781a348af703bb", null ],
    [ "EEmissionModule", "class_e_emission_module.html#a07b9d81acdb00a54a432e080adb4ecd3", null ],
    [ "SetData", "class_e_emission_module.html#a296d4fd3a3aa0fbecd60829fe01307f2", null ],
    [ "ToUnityType", "class_e_emission_module.html#a17d0f7e8e9176597db1d617b37cb048b", null ],
    [ "enabled", "class_e_emission_module.html#a3c083f41c8c28e4785af23cfa888e0eb", null ],
    [ "rateOverDistance", "class_e_emission_module.html#a088f6c9c874c6538aaa0348992d49b07", null ],
    [ "rateOverDistanceMultiplier", "class_e_emission_module.html#a699bf8e2000d5071dec58bacbfcac6e8", null ],
    [ "rateOverTime", "class_e_emission_module.html#a5c56394ea8642b13b1171a409e1865c4", null ],
    [ "rateOverTimeMultiplier", "class_e_emission_module.html#a18890bc52bc7b09b3725213d0bd90e7e", null ]
];