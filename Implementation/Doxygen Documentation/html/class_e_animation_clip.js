var class_e_animation_clip =
[
    [ "EAnimationClip", "class_e_animation_clip.html#ac59bcc1f7285d62f21eab249725a0fdf", null ],
    [ "EAnimationClip", "class_e_animation_clip.html#a64b1d7eaff9db97d19d876197ed0105c", null ],
    [ "SetData", "class_e_animation_clip.html#a264a0521fb059faf85d1d72f6adbba6b", null ],
    [ "ToUnityType", "class_e_animation_clip.html#a7763c0f79efa735b53a34e5ec4f15925", null ],
    [ "events", "class_e_animation_clip.html#a205999c7038426b4ad3d13684d49370b", null ],
    [ "frameRate", "class_e_animation_clip.html#abbd8e87c8b1b9649a47cc705073ed746", null ],
    [ "hideFlags", "class_e_animation_clip.html#ac92154413835aed8dc93a1b1254e1d92", null ],
    [ "legacy", "class_e_animation_clip.html#aab445d96a66be52780ae9816b52acbf6", null ],
    [ "localBounds", "class_e_animation_clip.html#a26b8c1428604f7f1fd9d9a538d54a866", null ],
    [ "name", "class_e_animation_clip.html#a6bcd39a5b36635007f336a4fcb51c9e4", null ],
    [ "wrapMode", "class_e_animation_clip.html#a8b3df5653b72da167cf381788ec22a7c", null ]
];