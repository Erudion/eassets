var class_e_material =
[
    [ "EMaterial", "class_e_material.html#a9cb6f37de3c07cb6b46d5d31e238bf9e", null ],
    [ "EMaterial", "class_e_material.html#a20e707c4712f55e1dc6039837f534785", null ],
    [ "SetData", "class_e_material.html#a096eb20ccae7ab728b6a6e33e148ffcf", null ],
    [ "ToUnityType", "class_e_material.html#a31975a510af5c7aa72200889968f41b7", null ],
    [ "color", "class_e_material.html#aeff37be4696d188a84a7962af303d2d6", null ],
    [ "doubleSidedGI", "class_e_material.html#a32a466061aafb1317ba15776aee5221b", null ],
    [ "enableInstancing", "class_e_material.html#a084636951f34ba87ad50369099945d86", null ],
    [ "globalIlluminationFlags", "class_e_material.html#a9e5d6f944ae5a72398e38834b383c338", null ],
    [ "hideFlags", "class_e_material.html#a6943bfa40d9b8b54e7eae0a0f4b96903", null ],
    [ "mainTexture", "class_e_material.html#af0c91ebcb1de7490c6c6b70b9bf13e21", null ],
    [ "mainTextureOffset", "class_e_material.html#a6bcc27cab8f5da599f6d8480af1b01c2", null ],
    [ "mainTextureScale", "class_e_material.html#a269413f0fae038f8ea467ac9ab949372", null ],
    [ "name", "class_e_material.html#a144d08d4cf19f5ea4d99cf1c25f9a927", null ],
    [ "renderQueue", "class_e_material.html#a2d1e9eefeaa427eba0b662243f4e54ff", null ],
    [ "shader", "class_e_material.html#ae91b2e9b96a78629c9f010279cc48a53", null ],
    [ "shaderKeywords", "class_e_material.html#ae6ebf91bc03418120354a7d2c04a7dd0", null ]
];