var class_e_min_max_gradient =
[
    [ "EMinMaxGradient", "class_e_min_max_gradient.html#a9a28c5016e1a7bc6d1a9cb3de6a0cb09", null ],
    [ "EMinMaxGradient", "class_e_min_max_gradient.html#a6f3bbb91eac70a00f1874e9894d43c2e", null ],
    [ "SetData", "class_e_min_max_gradient.html#a3dd38a64b0ddc1de229b80c8f362a525", null ],
    [ "ToUnityType", "class_e_min_max_gradient.html#afaf3e841f3c502377d18360020176824", null ],
    [ "color", "class_e_min_max_gradient.html#af97dcceac6a844566df07af4cd0518ab", null ],
    [ "colorMax", "class_e_min_max_gradient.html#a2bb2b8d332dc4d6d9442337f21d3304e", null ],
    [ "colorMin", "class_e_min_max_gradient.html#a63b4a77f2316e1b579575b29e6b52db8", null ],
    [ "gradient", "class_e_min_max_gradient.html#ab14d9f334182d585daa44eb7db09a598", null ],
    [ "gradientMax", "class_e_min_max_gradient.html#a081ba258f9242db618d3f1bff714a71c", null ],
    [ "gradientMin", "class_e_min_max_gradient.html#acb7757228122a78995b72e336ff47ccf", null ],
    [ "mode", "class_e_min_max_gradient.html#ad27751be942f05bc5527034945f54505", null ]
];