var class_e_human_description =
[
    [ "EHumanDescription", "class_e_human_description.html#a2cad3f0bea3b305dabf6aadc6cdfbba1", null ],
    [ "EHumanDescription", "class_e_human_description.html#a37f3a3e03f8bffaf030220544d8f182e", null ],
    [ "SetData", "class_e_human_description.html#ae90fd1fe11992720dabf678e4395fec1", null ],
    [ "ToUnityType", "class_e_human_description.html#ae38c7e601010b97e4ecd988953ae21ee", null ],
    [ "armStretch", "class_e_human_description.html#ab919d14a51517cba02e6b8c2d7183224", null ],
    [ "feetSpacing", "class_e_human_description.html#a6844b38c2c989c29427bc55960395f6f", null ],
    [ "hasTranslationDoF", "class_e_human_description.html#a06c7b94f7ccdbceab82bc856325263e6", null ],
    [ "human", "class_e_human_description.html#aed4c682166cabdf4b70ac00da6ab3024", null ],
    [ "legStretch", "class_e_human_description.html#a26899ef6c0570e8392906a9e109eaf84", null ],
    [ "lowerArmTwist", "class_e_human_description.html#a451fb9dcaceefe8abd563a314f01f11e", null ],
    [ "lowerLegTwist", "class_e_human_description.html#aa183b0c531035c5c5b800b1129c1ed24", null ],
    [ "skeleton", "class_e_human_description.html#ad17e5972d51775ad167a28ca9dbbb208", null ],
    [ "upperArmTwist", "class_e_human_description.html#a8cf1eccbafd5b0917abefa115d6dd374", null ],
    [ "upperLegTwist", "class_e_human_description.html#a7e80ed9efc1c5e45318c653a1dfefc3b", null ]
];