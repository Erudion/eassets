var class_e_polygon_collider2_d =
[
    [ "EPolygonCollider2D", "class_e_polygon_collider2_d.html#acd8f5695c0dfe752780ccd96ff60e8e3", null ],
    [ "EPolygonCollider2D", "class_e_polygon_collider2_d.html#a0e4cfc3ff114f83b49dec2aaa12c3ac3", null ],
    [ "SetData", "class_e_polygon_collider2_d.html#a5132210491dd640a1b0705e4f79c9a5d", null ],
    [ "ToUnityType", "class_e_polygon_collider2_d.html#a63dd0ecc14ab268e6a4bd595370a4f7c", null ],
    [ "autoTiling", "class_e_polygon_collider2_d.html#af79bbe32a650bc5e535a77f4327bc831", null ],
    [ "density", "class_e_polygon_collider2_d.html#a30b6fe652a39e44975d8776766c354d2", null ],
    [ "enabled", "class_e_polygon_collider2_d.html#a51362aa02dc5b2ccd138bb6608ffa513", null ],
    [ "hideFlags", "class_e_polygon_collider2_d.html#a06c57111ddf77f8df0da9b125f44d1e6", null ],
    [ "isTrigger", "class_e_polygon_collider2_d.html#a4c8e355c754a8343f271fcb3aaaa0ae9", null ],
    [ "name", "class_e_polygon_collider2_d.html#a236f7803963706df98d4c16d15d4d1fc", null ],
    [ "offset", "class_e_polygon_collider2_d.html#aa84fbf6fd22405bcbc145823b803a724", null ],
    [ "pathCount", "class_e_polygon_collider2_d.html#aee267046e6849066eba27dcac9a59d07", null ],
    [ "points", "class_e_polygon_collider2_d.html#ad9af54cf4309f9c346dd501af0443506", null ],
    [ "sharedMaterial", "class_e_polygon_collider2_d.html#a81c7885d4fb3dc63facc00b6a9867d12", null ],
    [ "tag", "class_e_polygon_collider2_d.html#ab6b25188be8dafaaac4a903be53790da", null ],
    [ "usedByComposite", "class_e_polygon_collider2_d.html#a95300358eba9cfef3df8c618fb1a9af4", null ],
    [ "usedByEffector", "class_e_polygon_collider2_d.html#ac03c8623b8ea6811b52c3eff2d68ba2f", null ]
];