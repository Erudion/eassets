var class_e_sprite =
[
    [ "ESprite", "class_e_sprite.html#af9bff4d1b2844cc4cdd6de6305b8ef6b", null ],
    [ "ESprite", "class_e_sprite.html#a8714334fe48a8b453be978373c14e930", null ],
    [ "SetData", "class_e_sprite.html#a088db89c725394183a74be49f2991188", null ],
    [ "ToUnityType", "class_e_sprite.html#a7015087a488ec6fabdc0a080e420f19c", null ],
    [ "hideFlags", "class_e_sprite.html#aba49530f83ac5533efc41e9410427961", null ],
    [ "name", "class_e_sprite.html#ad13683a304f2cfcd41f9559571ca438a", null ],
    [ "pivot", "class_e_sprite.html#a8a78343c1d29e887935ed690d298a06b", null ],
    [ "pixelsPerUnit", "class_e_sprite.html#aa27c863792a3144a254b42bfe9ff26af", null ],
    [ "rect", "class_e_sprite.html#a08a113623cce572cfde5d39e228ddcc7", null ],
    [ "texture", "class_e_sprite.html#a81ac5a3d1e9f4ce02f065d82fef9e4fc", null ]
];