var class_e_quaternion =
[
    [ "EQuaternion", "class_e_quaternion.html#a0da249502978e068b34d80daccc67bbe", null ],
    [ "EQuaternion", "class_e_quaternion.html#a75f26ff70ca4b12450b51399bee200f3", null ],
    [ "EQuaternion", "class_e_quaternion.html#a9a770aa0ec4a91a31d1882ab5e29dd46", null ],
    [ "Euler", "class_e_quaternion.html#aa9f5866e609c8eb78413a046c64c3ae4", null ],
    [ "Euler", "class_e_quaternion.html#a4020a3395f76cfa189e5b05b525b1fa6", null ],
    [ "operator EQuaternion", "class_e_quaternion.html#afd05ee285a7852d3d3a4235a840cd7a1", null ],
    [ "operator Quaternion", "class_e_quaternion.html#a650ecd635d20cf5261427039677d15d9", null ],
    [ "SetData", "class_e_quaternion.html#a8cc53c086a47ea72e72e09625f96b602", null ],
    [ "ToString", "class_e_quaternion.html#a9a6d963796e10f7476150226077fccce", null ],
    [ "ToUnityType", "class_e_quaternion.html#a8a1793825bbf12c1dc035276dae1000b", null ],
    [ "w", "class_e_quaternion.html#aa672483b6c07365201d8b23445f5a0e9", null ],
    [ "x", "class_e_quaternion.html#a0712003a31f824935f1bdc4025291308", null ],
    [ "y", "class_e_quaternion.html#adbacce268b1a30f4b0e564c0afd74d75", null ],
    [ "z", "class_e_quaternion.html#a7d12019e72791294ac4481086116b8d9", null ]
];