var class_e_edge_collider2_d =
[
    [ "EEdgeCollider2D", "class_e_edge_collider2_d.html#a4295f142396b311f2f792f4280dd647f", null ],
    [ "EEdgeCollider2D", "class_e_edge_collider2_d.html#a1296afa912e268945e5588b367cb207b", null ],
    [ "SetData", "class_e_edge_collider2_d.html#a63f43010be5707f33b479e8353d92849", null ],
    [ "ToUnityType", "class_e_edge_collider2_d.html#a847c70b57ba8e39196e2eff5bab59fb8", null ],
    [ "density", "class_e_edge_collider2_d.html#ac164dc3945d18fde85a473bffef63ac0", null ],
    [ "edgeRadius", "class_e_edge_collider2_d.html#a3dadd29c70ad980085165b9638a138a8", null ],
    [ "enabled", "class_e_edge_collider2_d.html#a7e2af7f8b146377c297c34a693550e73", null ],
    [ "hideFlags", "class_e_edge_collider2_d.html#ab125db3e2af36712f25983fb9d1a32c0", null ],
    [ "isTrigger", "class_e_edge_collider2_d.html#abab7e229a3d29741ebc834b8eed5a4d6", null ],
    [ "name", "class_e_edge_collider2_d.html#a819acb1b018e9a35b57f586db8ed7f62", null ],
    [ "offset", "class_e_edge_collider2_d.html#a70eba07a227a45c1aeab53b489ad75b6", null ],
    [ "points", "class_e_edge_collider2_d.html#ae48a8056ce118761f45c6617734bf7e8", null ],
    [ "sharedMaterial", "class_e_edge_collider2_d.html#ac6838515b10d6d44c7e932b1e1aed58b", null ],
    [ "tag", "class_e_edge_collider2_d.html#a97230eaf78b1cfa3b1e3eaa4531c8d67", null ],
    [ "usedByComposite", "class_e_edge_collider2_d.html#a208fb84b26ba90b907c3c718d0e0ef05", null ],
    [ "usedByEffector", "class_e_edge_collider2_d.html#a69d05b9249dbf66f2ed3f2a13a335853", null ]
];