var class_e_physic_material2_d =
[
    [ "EPhysicMaterial2D", "class_e_physic_material2_d.html#a31bea8eb4dbe866cb90a471e336069a8", null ],
    [ "EPhysicMaterial2D", "class_e_physic_material2_d.html#af94ce49afba4f86c8603bce3e8e48eec", null ],
    [ "SetData", "class_e_physic_material2_d.html#ae368ad7efbbab8eaf21c911e9a9c04eb", null ],
    [ "ToUnityType", "class_e_physic_material2_d.html#a612b46faa6e28c2c44464b08cdbe7b8d", null ],
    [ "bounciness", "class_e_physic_material2_d.html#a0c79e0a5a3543bbad45a657d8455129b", null ],
    [ "friction", "class_e_physic_material2_d.html#abee64941c6af68851663c48957651a0d", null ],
    [ "hideFlags", "class_e_physic_material2_d.html#a32b21390d312a1f87c2a4e6503684bec", null ],
    [ "name", "class_e_physic_material2_d.html#a42713cf61f31ab4b7742aed2ff24409e", null ]
];