var class_e_render_texture_descriptor =
[
    [ "ERenderTextureDescriptor", "class_e_render_texture_descriptor.html#aa59ec8919e826e7c57b29e96c52c9af0", null ],
    [ "ERenderTextureDescriptor", "class_e_render_texture_descriptor.html#a436ae79c548a7c55d967be006c8cdf53", null ],
    [ "SetData", "class_e_render_texture_descriptor.html#a822eded5008d852de70b9d3ad05fab8c", null ],
    [ "ToUnityType", "class_e_render_texture_descriptor.html#aed6d9d716ef5aea4a863bad80272bb24", null ],
    [ "autoGenerateMips", "class_e_render_texture_descriptor.html#af2f008c664a9bd0ed2c6172e213a5908", null ],
    [ "colorFormat", "class_e_render_texture_descriptor.html#ac825f776683d3f54dc2149b67f7a0a21", null ],
    [ "depthBufferBits", "class_e_render_texture_descriptor.html#af46634555fa6ce70b49e40cc08d1c3d8", null ],
    [ "dimension", "class_e_render_texture_descriptor.html#af7bc75411b08366bf8722f73e5183269", null ],
    [ "enableRandomWrite", "class_e_render_texture_descriptor.html#a8e92769b23ce22aa20fa8c9e4f0b635e", null ],
    [ "height", "class_e_render_texture_descriptor.html#affd1c8845da48af08a20ee2c995c99bd", null ],
    [ "memoryless", "class_e_render_texture_descriptor.html#afb5ef59d4ff920fc721ce4e382042fa4", null ],
    [ "msaaSamples", "class_e_render_texture_descriptor.html#a84b89e9a6bace7a5ec15bc4a710bb636", null ],
    [ "shadowSamplingMode", "class_e_render_texture_descriptor.html#a05ce3233560bd27fdd2166f405f476db", null ],
    [ "sRGB", "class_e_render_texture_descriptor.html#a4152cdb75b27aa52e7bc9f9e1ece017e", null ],
    [ "useMipMap", "class_e_render_texture_descriptor.html#a55ce447b42c9d830948fdb6c56afc731", null ],
    [ "volumeDepth", "class_e_render_texture_descriptor.html#a8f528c0a15641a742da467232e803a00", null ],
    [ "vrUsage", "class_e_render_texture_descriptor.html#acf2efeedb8f5240f568cb847a57c9d49", null ],
    [ "width", "class_e_render_texture_descriptor.html#a089d76126f0a5de47927e4fafcd61cea", null ]
];