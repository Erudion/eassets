var class_e_box_collider2_d =
[
    [ "EBoxCollider2D", "class_e_box_collider2_d.html#a3011bc5b17015109cb336d5f640f4624", null ],
    [ "EBoxCollider2D", "class_e_box_collider2_d.html#a883873237764785fd1ae005c4128bc59", null ],
    [ "SetData", "class_e_box_collider2_d.html#adce1fbc8e2609ea6e9a3aae07b7bf8e6", null ],
    [ "ToUnityType", "class_e_box_collider2_d.html#acaef17c3e1367a1c774d20e496f9b96b", null ],
    [ "autoTiling", "class_e_box_collider2_d.html#a1a5cb212401d30ff7d820ee680c64227", null ],
    [ "density", "class_e_box_collider2_d.html#a1ed0841d728fbbca5ffa99312887e0ba", null ],
    [ "edgeRadius", "class_e_box_collider2_d.html#ab4bd52476cf4ea2700b446bcd18d1ecc", null ],
    [ "enabled", "class_e_box_collider2_d.html#aef78a0965de8906069046ab1b839d011", null ],
    [ "hideFlags", "class_e_box_collider2_d.html#a5915fdcbf39bc570b47d1e0ededa1ee5", null ],
    [ "isTrigger", "class_e_box_collider2_d.html#a9091280bdb402b367e682f731ba56dd1", null ],
    [ "name", "class_e_box_collider2_d.html#ab184cd4a79ff9bb44872dac0040f6b5c", null ],
    [ "offset", "class_e_box_collider2_d.html#a90da4bab034a3176ccada749679a15eb", null ],
    [ "sharedMaterial", "class_e_box_collider2_d.html#ac55b21658f3d36615dff6b437065498b", null ],
    [ "size", "class_e_box_collider2_d.html#a200d7f1c979e983e1163beab6b384e2c", null ],
    [ "tag", "class_e_box_collider2_d.html#a264c09d8e63ae0e94e96617b41ab9639", null ],
    [ "usedByComposite", "class_e_box_collider2_d.html#a5fc2522000270983c79a191b31ca903a", null ],
    [ "usedByEffector", "class_e_box_collider2_d.html#a858138942056a60df13bd173ad122ee7", null ]
];