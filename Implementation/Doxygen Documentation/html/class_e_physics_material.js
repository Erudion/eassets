var class_e_physics_material =
[
    [ "EPhysicsMaterial", "class_e_physics_material.html#a39d90b6439fe6e55829a399c68036c3c", null ],
    [ "EPhysicsMaterial", "class_e_physics_material.html#a78fd8b87591865969656e29d15fc01ab", null ],
    [ "SetData", "class_e_physics_material.html#aeb7d3cbbb6e639571180623e8f744e11", null ],
    [ "ToUnityType", "class_e_physics_material.html#ab15d9639a48cc651b544660c12f9a08b", null ],
    [ "bounceCombine", "class_e_physics_material.html#a1ae040d33828d120c817de85d0be955c", null ],
    [ "bounciness", "class_e_physics_material.html#a823352ecc6c49af8fa18087b1e6c400b", null ],
    [ "dynamicFriction", "class_e_physics_material.html#a00189c8d53172c379ba0ed67294805b1", null ],
    [ "frictionCombine", "class_e_physics_material.html#a26e8ece35c4b6d72b7e997d282a5c2ef", null ],
    [ "hideFlags", "class_e_physics_material.html#a3a483fd792ce89c5395701d5a093ebf5", null ],
    [ "name", "class_e_physics_material.html#a4a677228e6cfc8f34113d9b69d2ba15a", null ],
    [ "staticFriction", "class_e_physics_material.html#a4824139c013acf8b2888717c9b8ab99f", null ]
];