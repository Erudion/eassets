var class_e_rotation_by_speed_module =
[
    [ "ERotationBySpeedModule", "class_e_rotation_by_speed_module.html#acf4ad2aa7eba413b171a5310676f75a6", null ],
    [ "ERotationBySpeedModule", "class_e_rotation_by_speed_module.html#ab70f43c6b51d179317d51cea4d429c6f", null ],
    [ "SetData", "class_e_rotation_by_speed_module.html#a77c5b2db0db31f77fae2b93d686c9850", null ],
    [ "ToUnityType", "class_e_rotation_by_speed_module.html#afc2c1d2acaac126122914a1deeff08cb", null ],
    [ "enabled", "class_e_rotation_by_speed_module.html#a0bcda7471a07df0500de51bd064dc9f2", null ],
    [ "range", "class_e_rotation_by_speed_module.html#a806b71cc1460e785eef68619f6ce2690", null ],
    [ "separateAxes", "class_e_rotation_by_speed_module.html#a5aab5cf20a78962e98bd0db675449ead", null ],
    [ "x", "class_e_rotation_by_speed_module.html#ab01950f9c94ad69e5453af7dac7a7ce8", null ],
    [ "xMultiplier", "class_e_rotation_by_speed_module.html#a6458dbed34981d31b8378cfdb4f5aab5", null ],
    [ "y", "class_e_rotation_by_speed_module.html#a59a5338f8a9fb864822b984e874e0e02", null ],
    [ "yMultiplier", "class_e_rotation_by_speed_module.html#a794dac9e4ddef86e17c47919f3ae945b", null ],
    [ "z", "class_e_rotation_by_speed_module.html#a1ebbe67be1bdbd783905ce8b2f511c22", null ],
    [ "zMultiplier", "class_e_rotation_by_speed_module.html#a51c9ba751d9803218dd2d071fdeaacd4", null ]
];