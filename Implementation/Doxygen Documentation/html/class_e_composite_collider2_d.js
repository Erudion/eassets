var class_e_composite_collider2_d =
[
    [ "ECompositeCollider2D", "class_e_composite_collider2_d.html#a7e6f8786cffe2a6848b48fb7d3e11af4", null ],
    [ "ECompositeCollider2D", "class_e_composite_collider2_d.html#af0f1e5360173c49a3acc356ce676fd49", null ],
    [ "SetData", "class_e_composite_collider2_d.html#a40891e4fa3f0accd4ef90c312d23be2c", null ],
    [ "ToUnityType", "class_e_composite_collider2_d.html#abba40c5514244052ab3bd6b7cadf4ce4", null ],
    [ "density", "class_e_composite_collider2_d.html#a1f31395d9ec68b6cbc02da01088dd80d", null ],
    [ "edgeRadius", "class_e_composite_collider2_d.html#adc047ad81423dd7272d241cfed89b97b", null ],
    [ "enabled", "class_e_composite_collider2_d.html#a7a4e753efed7eafd02241880b241d602", null ],
    [ "generationType", "class_e_composite_collider2_d.html#afc6684fb1ddc661f35db62d9ee7e4820", null ],
    [ "geometryType", "class_e_composite_collider2_d.html#abfe6cb357b0016d89735c35bb3eb1b91", null ],
    [ "hideFlags", "class_e_composite_collider2_d.html#ab91cac8ded36304bcc595df1b0c812be", null ],
    [ "isTrigger", "class_e_composite_collider2_d.html#a9d422f4f9dce86016e4207daccaf683a", null ],
    [ "name", "class_e_composite_collider2_d.html#ac414b68b9a4728e5399f9bd7f32af0ff", null ],
    [ "offset", "class_e_composite_collider2_d.html#a47de264542bddc851f4375d6ce9964ad", null ],
    [ "sharedMaterial", "class_e_composite_collider2_d.html#a91e8560b13ec5908331697b46659007e", null ],
    [ "tag", "class_e_composite_collider2_d.html#a2bfc9a0996f4d3a291d2ad053293221a", null ],
    [ "usedByComposite", "class_e_composite_collider2_d.html#ab383f71367670af3cb6a7170c6e109c6", null ],
    [ "usedByEffector", "class_e_composite_collider2_d.html#a55c51a9a3cbca4cc5594c9f4767cc8cf", null ],
    [ "vertexDistance", "class_e_composite_collider2_d.html#a2a44ec14e72647055d9b1a8ec946d7ce", null ]
];