var class_e_assets_1_1_i_o_1_1_saving_1_1_savefile =
[
    [ "Savefile", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#a249d0b4f0853c88051a38eb6390d9c22", null ],
    [ "GetSavefileData", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#a2f5c9c2942cac0c623d98b7c464c6443", null ],
    [ "Load< T >", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#a8b48571215ccbc39fc89f4b2c06a7143", null ],
    [ "LoadFromFile", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#a8ce206c50df2c5af7895047c9c7e94da", null ],
    [ "LoadInto< T >", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#a24db18e52a9a5f13790bece6cc8dd058", null ],
    [ "LoadSavefile", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#a0923446602fc38048cfaa1ad2070011b", null ],
    [ "Save< T >", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#ae8d1b30e62c95b70e8b24c490d8d75be", null ],
    [ "SaveToFile", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#a0826ac9d8aa23a6dbb2dd8331d9f23ee", null ],
    [ "autosave", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#a931b5f9ccc1c45c7ec0f77f0eae88c52", null ],
    [ "CustomToUnityTypes", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#a15e90098b8ebf973e94759da9958b1ab", null ],
    [ "filename", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#aaa3b2b0554ae8ddb91597c35b3eea886", null ],
    [ "myUnityTypes", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#a7a0f82f0086aca840836ebdbaaeea3e2", null ],
    [ "savedata", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#a9b109a687eea97e51f16d4647a191e92", null ],
    [ "UnityToCustomTypes", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#a33c6c44cc1512348be2f573d0e6f4c56", null ],
    [ "unityTypes", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#ad3658c326bb210bea4a719c9e0171699", null ],
    [ "Data", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#ac1067f58b89d59337f44effa3c5d53a6", null ],
    [ "Entries", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#a6760189ecdd3c7127fa19a091c1ff4cb", null ],
    [ "Keys", "class_e_assets_1_1_i_o_1_1_saving_1_1_savefile.html#ab536b63ed988ac0a02da4a11234b38dc", null ]
];