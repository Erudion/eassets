//------------------------------------------------------------------------------
// EAssetsDataDump.cs
//
// Copyright 2019 E-In-A-Box 
//
// Created by Christian Koch on 01.07.2019
// Owner: Christian Koch
// 
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

//Contains different assets
namespace EAssets
{
    namespace DataStorage
    {
        /// <summary>
        /// This class stores data of arbitrary types. These are scene-persistent but not session-persistent (if the game is closed all data is lost).
        /// </summary>
        /// <example>
        /// A simple useage example would be:
        /// <code>
        /// DataDump.Set("DataKey",data);
        /// DataDump.Get{typeInformation}("DataKey");
        /// </code>
        /// </example>
        public static class DataDump
        {
            #region Attributes
            private static Dictionary<string, object> dictionary = new Dictionary<string, object>();
            private static Dictionary<string, Type> typelist = new Dictionary<string, Type>();
            private static List<string> keylist = new List<string>();

            /// <summary>
            /// Set this to true to allow the DataDump to be cleared
            /// </summary>
            public static bool AllowClearFlag = false;
            #endregion

            #region Get and Set Methods
            /// <summary>
            /// Sets a value in the data dump
            /// </summary>
            /// <typeparam name="T">The type of the value you want to assign</typeparam>
            /// <param name="key">The key you want to use</param>
            /// <param name="value">The value you want to store</param>
            /// <example>
            /// <code>
            /// //Force create a new value in the DataDump (deletes an old value)
            /// DataDump.Set{TypeOfInput}("valueKey",value);
            /// </code>
            /// </example>
            public static void Set<T>(string key, object value)
            {
                //Remove existing entries
                if (keylist.Contains(key))
                {
                    dictionary.Remove(key);
                    typelist.Remove(key);
                    keylist.Remove(key);
                }

                dictionary.Add(key, value);
                typelist.Add(key, value.GetType());
                keylist.Add(key);
            }

            /// <summary>
            /// Try to set a value. If it exists, it is not set in which case false is returned. Else it is set and true is returned
            /// </summary>
            /// <typeparam name="T">The type of the value</typeparam>
            /// <param name="key">The key you want to use</param>
            /// <param name="value">The value you want to store</param>
            /// <returns></returns>
            /// <example>
            /// <code>
            /// //Tries to create a new input but only does if the key is not already in use
            /// //Returns true if a new value is created else false
            /// if(DataDump.TrySet{TypeOfInput}("valueKey",value))
            /// {
            ///     //Do something here...
            /// }
            /// </code>
            /// </example>
            public static bool TrySet<T>(string key, object value)
            {
                if (!dictionary.ContainsKey(key))
                {
                    dictionary.Add(key, value);
                    typelist.Add(key, value.GetType());
                    keylist.Add(key);

                    return true;
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Get a value from the DataDump. TryGetValue is recomended. Returns default if the value does not exist
            /// </summary>
            /// <typeparam name="T">The type of the value you want to get</typeparam>
            /// <param name="key">The key of the value you want to get</param>
            /// <returns></returns>
            /// <example>
            /// <code>
            /// //Returns a value from the DataDump if the key exists, else a default value
            /// T returnValue = DataDump.Get{TypeOfInput}("valueKey");
            /// </code>
            /// </example>
            public static T Get<T>(string key)
            {
                if (!dictionary.ContainsKey(key))
                    return default;

                if (dictionary.TryGetValue(key, out object tmp))
                {
                    if (tmp.GetType().Equals(typeof(T)))
                    {
                        return (T)tmp;
                    }
                    else
                    {
                        IO.DataIO.GlobalLogWarning("Typemismatch at DataDump. Key: " + key + ". Found Type: " + tmp.GetType() + "; Requested Type: " + typeof(T));
                        return default;
                    }
                }
                else
                {
                    IO.DataIO.GlobalLogWarning("No key with the value: " + key + " found");
                    return default;
                }
            }

            /// <summary>
            /// Tries to get a value from the DataDump. Returns true if it found a value, else false
            /// </summary>
            /// <param name="key">The key you want to use</param>
            /// <param name="tmp">The value if any</param>
            /// <returns></returns>
            /// <example>
            /// <code>
            /// //Returns true and the value from the DataDump if the key exists, else false and a default value
            /// if(DataDump.TryGet{TypeOfInput}("valueKey", out T value))
            /// {
            ///     //Do something with the received value here
            /// }
            /// </code>
            /// </example>
            public static bool TryGet<T>(string key, out T tmp)
            {
                if (dictionary.TryGetValue(key, out object t))
                {
                    if (t.GetType().Equals(typeof(T)))
                    {
                        tmp = (T)t;
                        return true;
                    }
                    else
                    {
                        IO.DataIO.GlobalLogWarning("Typemismatch at DataDump. Key: " + key + ". Found Type: " + t.GetType() + "; Requested Type: " + typeof(T));
                        tmp = default;
                        return false;
                    }
                }
                else
                {
                    IO.DataIO.GlobalLogWarning("No key with the value: " + key + " found");
                    tmp = default;
                    return false;
                }
            }

            /// <summary>
            /// Tries to read a value from the DataDump. If it exists it is loaded and then deleted from the DataDump. In this case true is returned. Else false
            /// </summary>
            /// <typeparam name="T">The type you want to get</typeparam>
            /// <param name="key">The key you want to use</param>
            /// <param name="tmp">The data you receive</param>
            /// <returns></returns>
            /// <example>
            /// <code>
            /// //Returns true and the value from the DataDump if the key exists, else false and a default value
            /// //DestructiveGetting a value removes it from the DataDump
            /// if(DataDump.DestructiveGet{TypeOfInput}("valueKey", out T value))
            /// {
            ///     //Do something with the received value here
            ///     
            ///     if(DataDump.DestructiveGet{TypeOfInput}("valueKey", out T value))
            ///     {
            ///         //Will never be reached because "valueKey" was already deleted
            ///     }
            /// }
            /// </code>
            /// </example>
            public static bool DestructiveGet<T>(string key, out T tmp)
            {
                if (dictionary.ContainsKey(key))
                {
                    if (dictionary[key].GetType().Equals(typeof(T)))
                    {
                        tmp = (T)dictionary[key];

                        dictionary.Remove(key);
                        typelist.Remove(key);
                        keylist.Remove(key);

                        return true;
                    }
                    else
                    {
                        IO.DataIO.GlobalLogWarning("Typemismatch at DataDump. Key: " + key + ". Found Type: " + dictionary[key].GetType() + "; Requested Type: " + typeof(T));
                        tmp = default;
                        return false;
                    }
                }
                else
                {
                    IO.DataIO.GlobalLogWarning("No key with the value: " + key + " found");
                    tmp = default;
                    return false;
                }
            }

            /// <summary>
            /// Deletes a key,value pair if it exists. If successful it returns true, else false
            /// </summary>
            /// <param name="key">The key you want to delete</param>
            /// <returns></returns>
            /// <example>
            /// <code>
            /// //Deletes a key from the DataDump with its data
            /// //Returns true if data was deleted (the key existed) else false
            /// if(DataDump.Delete("key"))
            /// {
            ///     //Do something here...
            /// }
            /// </code>
            /// </example>
            public static bool Delete(string key)
            {
                if (!Exists(key))
                    return false;

                dictionary.Remove(key);
                typelist.Remove(key);
                keylist.Remove(key);

                return true;
            }

            /// <summary>
            /// Returns true if the given key exists
            /// </summary>
            /// <param name="key">The key you want to check</param>
            /// <returns></returns>
            /// <example>
            /// <code>
            /// //Returns true if a given key exists on the DataDump, else false
            /// if(DataDump.Exists("key"))
            /// {
            ///     //Do something here...
            /// }
            /// </code>
            /// </example>
            public static bool Exists(string key)
            {
                return keylist.Contains(key);
            }

            /// <summary>
            /// Clear the DataDump. This is only allowed if AllowClearFlag is set to true. If successful it returns true, else false
            /// <para>AllowClearFlag will be set to false afterwards to avoid double clearing</para>
            /// </summary>
            /// <example>
            /// <code>
            /// //Clears the DataDump. It will not contain any entries afterwards
            /// //You need to set the AllowClearFlag first before clearing the DataDump
            /// DataDump.AllowClearFlag = true;
            /// if(DataDump.Clear())
            /// {
            ///     //This will return false since the AllowClearFlag gets reset to false after 
            ///     //clearing the DataDump. You will need to set the flag first before clearing again
            ///     if(DataDump.Clear())
            ///     {
            ///         //Will not get reached
            ///     }
            ///     
            ///     //Will return default since the DataDump got cleared and does not contain any entries anymore
            ///     T value = DataDump.Get{Type}("Key");
            /// 
            ///     //Do something here...
            /// }
            /// </code>
            /// </example>
            public static bool Clear()
            {
                if (!AllowClearFlag)
                    return false;

                dictionary.Clear();
                typelist.Clear();
                keylist.Clear();

                AllowClearFlag = false;

                return true;
            }
            #endregion

            #region Convenience Properties
            /// <summary>
            /// The number of entries in the DataDump
            /// </summary>
            /// <returns></returns>
            public static int NumberOfVariables
            {
                get
                {
                    return keylist.Count;
                }
            }

            /// <summary>
            /// All keys used in the DataDump
            /// </summary>
            public static string[] Keys
            {
                get
                {
                    return keylist.ToArray();
                }
            }
            #endregion

            #region Store Data
            /// <summary>
            /// Stores the current data dump (overwriting old data)
            /// </summary>
            public static void StoreDataDump()
            {
                IO.DataIO.WriteDataBinary(dictionary, "DictionaryData", "DataDump");
                IO.DataIO.WriteDataBinary(typelist, "TypeListData", "DataDump");
                IO.DataIO.WriteDataBinary(keylist, "KeyListData", "DataDump");
            }

            /// <summary>
            /// Loads all old data from the dictionary (overwriting old data)
            /// </summary>
            public static void LoadDataDump()
            {
                if (IO.DataIO.ReadDataBinary(out object tmp, "DictionaryData", "DataDump"))
                {
                    dictionary = (Dictionary<string, object>)tmp;
                }

                if (IO.DataIO.ReadDataBinary(out object tmp2, "TypeListData", "DataDump"))
                {
                    typelist = (Dictionary<string, Type>)tmp2;
                }

                if (IO.DataIO.ReadDataBinary(out object tmp3, "KeyListData", "DataDump"))
                {
                    keylist = (List<string>)tmp;
                }
            }
            #endregion
        }

        /// <summary>
        /// A linked value is a value of type T that is the same in every class. It is stored on the <see cref="DataDump"/>. If you change it in one script it will get changed in the others as well.
        /// </summary>
        /// <typeparam name="T">The type of this linkedvalue</typeparam>
        /// <example>
        /// An example for linkedvalues would be:
        /// <code>
        /// //Create a new variable
        /// LinkedValue{type} lv = new LinkedValue("DataKey");
        /// 
        /// //Set the variable
        /// lv.Value = newValue;
        /// 
        /// //Get the variable
        /// T variable = lv;
        /// </code>
        /// </example>
        public class LinkedValue<T>
        {
            #region Attributes
            /// <summary>
            /// The value of this linkedvalue
            /// </summary>
            public T Value
            {
                get { return DataDump.Get<T>(this.Key); }
                set { DataDump.Set<T>(this.Key, value); }
            }

            /// <summary>
            /// The key used for this value
            /// </summary>
            public string Key { get; protected set; }
            #endregion

            #region Constructor
            /// <summary>
            /// Create a new linkedvalue
            /// </summary>
            /// <param name="key"></param>
            public LinkedValue(string key, T value = default)
            {
                this.Key = key;

                if (!DataDump.Exists(key))
                    DataDump.Set<T>(key, value);
                else
                    throw new ArgumentException("The key '" + key + "' already existed on the DataDump! The linkedvalue was not created successfully!");
            }
            #endregion

            #region Additional Methods
            /// <summary>
            /// This LinkedValue can be treated as its template type
            /// </summary>
            /// <param name="lValue">The linkedvalue</param>
            public static implicit operator T(LinkedValue<T> lValue)
            {
                return lValue.Value;
            }
            #endregion

        }
    }
}
