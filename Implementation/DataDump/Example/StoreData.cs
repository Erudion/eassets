﻿using UnityEngine;
using UnityEngine.SceneManagement;
using EAssets.DataStorage;

public class StoreData : MonoBehaviour
{
    /*
        The Datadump allows you to store simple types, like integers 
    */
    [Tooltip("The name of the int value to be stored on the datadump")]
    public string valueName = "MyValueName";
    [Tooltip("The int value to store on the datadump")]
    public int value;

    /*
        The Datadump also allows you to store non-simple types, like unity objects
    */
    [Tooltip("The name of the unity object to be stored on the datadump")]
    public string objName = "MyObjName";
    [Tooltip("The unity object to store on the datadump")]
    public UnityEngine.Object obj;

    void Start()
    {
        //Set the int value
        DataDump.Set<int>(valueName, value);

        //Set the unity object
        DataDump.Set<UnityEngine.Object>(objName, obj);

        //Set hardcoded data
        DataDump.Set<string>("MyHardcodedText", "This is a test, hello!");

        //Set arrays
        DataDump.Set<int[]>("MyArray", new int[] { 1, 2, 3, 4, 5 });

        //The data can be read as soon as it is written
        Debug.Log(DataDump.Get<int>(valueName));
        Debug.Log(DataDump.Get<UnityEngine.Object>(objName).ToString());
        Debug.Log(DataDump.Get<string>("MyHardcodedText"));
        Debug.Log(DataDump.Get<int[]>("MyArray").ToString());

        //Wrong types will return a default and notify you on the console
        Debug.Log(DataDump.Get<float>("MyHardcodedText"));
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            SceneManager.LoadScene("PersistenceExample");
        }
    }
}
