﻿using UnityEngine;
using EAssets.DataStorage;

public class ReadData : MonoBehaviour
{
    void Start()
    {
        //Data can always be read from the datadump, even if the data does not exist
        //If it does not exist the datadump throws a warning and returns a default
        //On a typemismatch it does the same
        //The DataDump is scene-persistent. The data can only be removed manually
        //or by ending the application

        //We do not have the variable names here, but we can read known keys
        Debug.Log(DataDump.Get<string>("MyHardcodedText"));
        Debug.Log(DataDump.Get<int[]>("MyArray").ToString());

        //Wrong types will return a default and notify you on the console
        Debug.Log(DataDump.Get<float>("MyHardcodedText"));
    }
}
