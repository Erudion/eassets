This example shows you how to use the DataDump. The intention behind the DataDump is to allow to store data for short ammounts of time.
The DataDump is scene-persistent, but gets deleted once the Application is closed. Dont store private values or the like on the DataDump.

Scene: The first scene shows you how to store and load data on the DataDump. Press 'N' on you keyboard to load the next scene.
The second scene reads data from the DataDump stored in the first scene to show the persistency-aspect of the DataDump.