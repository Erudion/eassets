﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EAssets.IO.Saving;

namespace EAssets
{
    namespace Global
    {
        /// <summary>
        /// This class contains typed data which is globally accessible. This makes it different from the DataDump, which is loosly typed.
        /// <para>This class was made to be populated by hand</para>
        /// </summary>
        public static class GlobalData
        {
            /// <summary>
            /// This savefile can be used for all main data in the game
            /// </summary>
            public static Savefile MainSavefile = new IO.Saving.Savefile("Mainsavefile");
        }
    }
}
